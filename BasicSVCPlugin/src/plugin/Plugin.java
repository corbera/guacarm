/**
 * Basic SVC plugin example.
 * 
 * ARM assembly code example:
 * 
.data
vals: .byte 1,2,3,4,5,6,7,8

.global _start
.text 
_start: 	push {lr}
                mov r0, #0xFF
		mov r1, #0xAA
	 	mov r7, #10				
        	swi #0
        	pop {lr}
        	bx lr

 */
package plugin;

import guacarm.GUI.ColorTheme;
import java.awt.Component;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.plugins.interfaces.PluginInterface;
import guacarm.plugins.interfaces.PluginClass;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author corbera
 */
public class Plugin implements PluginClass {

    private PluginInterface api;
    private Component comp;
    private boolean GUI;
    private static final int SERVICE = 10;

    /**
     * Plugin initialization in GUI mode
     *
     * @param api Interface to interact with the application
     * @return Plugin AWT/SWING component (if any)
     */
    @Override
    public boolean initializeGUIPlugIn(PluginInterface api) {
        // Save the app API for later use
        this.api = api;
        // If we can't register the service call 1, return false to indicate
        // that the plugin has not been initialized
        if (!api.registerSVCCall(SERVICE, new MyCallBack())) {
            return false;
        }
        // The plugin will be used in GIU mode
        GUI = true;
        return true;
    }

    /**
     * Plugin initialization in command line mode
     *
     * @param api Interface to interact with the application
     */
    @Override
    public boolean initializeConsolePlugIn(PluginInterface api) {
        // Save the app API for later use
        this.api = api;
        // If we can't register the service call 1, return false to indicate
        // that the plugin has not been initialized
        if (!api.registerSVCCall(SERVICE, new MyCallBack())) {
            return false;
        }
        // The plugin will be used in commnad line mode
        GUI = false;
        return true;
    }

    @Override
    public String getShortName() {
        // Return the short name of the plugin
        return "BasicSVCPlugin";
    }

    @Override
    public void reset(SimulatorInterface api) {
        // Initialize the plugin parameters for a new simulation
    }

    @Override
    public String getName() {
        // Return the long name of the plugin
        return "Basic Sevice Call (SVC) Plugin example";
    }

    @Override
    public String getAuthor() {
        // Return the author information of the plugin
        return "Francisco Corbera";
    }

    @Override
    public String getVersion() {
        // Return the version of the plugin 
        return "0.1";
    }

    @Override
    public Component getAWTComponent() {
        // Return the AWT component associated with the pluing (null in this
        // case)
        return null;
    }

    @Override
    public JMenu getJMenu() {
        // We create the plugin menu that will be in the menu bar
        JMenu m = new JMenu();
        // We create a menu entry called "Info";
        JMenuItem item = new JMenuItem("Info");
        // This is the action associated to de menu entry 
        item.addActionListener(e -> {
            api.getLog().println(getName() + " - Version: "
                    + getVersion() + " - by: "
                    + getAuthor(), ColorTheme.UPDATED);
        });
        // We add the entry to the menu
        m.add(item);
        // and return the mneu
        return m;
    }

    @Override
    public void finalizePlugIn() {
    }

    /**
     * Callbak associated with de SVC registered
     */
    public class MyCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            // This method will be called when the simulator find a 
            // swi with value SERVICE in register 7 (linux like)
            doAction(api);
        }
    }

    /**
     * Actions to do when service SERVICE is called
     *
     * @param simApi API with the actual simulator instance
     */
    private void doAction(SimulatorInterface simApi) {
        // print messages in the application console
        api.getLog().println("Service " + SERVICE + " called", ColorTheme.FOREGROUND);
        try {
            // read the content of two registers
            api.getLog().println("R0 = 0x" + Integer.toHexString(simApi.readRegister(0)), ColorTheme.FOREGROUND);
            api.getLog().println("R1 = 0x" + Integer.toHexString(simApi.readRegister(1)), ColorTheme.FOREGROUND);
        } catch (Exception ex) {
            api.getLog().println("Error reading registers ", ColorTheme.WARNING);
        }
        int addr = simApi.getDataOffset();
        byte[] mem;
        try {
            // read the content of 8 memory positions
            mem = simApi.readMemory(addr, 8);
            for (byte b : mem) {
                api.getLog().println("Mem 0x" + Integer.toHexString(addr++) + ": 0x" + Integer.toHexString(b), ColorTheme.FOREGROUND);
            }
        } catch (Exception ex) {
            api.getLog().println("Error reading memory ", ColorTheme.WARNING);
        }

    }
}
