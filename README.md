# GUACARM

Simulador de ensmablador ARM de codigo libre, implementado en Java y desarrollado para la docencia en
el departamento de Arquitectura de computadores de la Universidad de Malaga.

![alt text](https://bitbucket.org/corbera/guacarm/raw/8a3bcbb9ea515425d9ff013e69a03b3c3b6c63b8/Imagenes/guacARM.png "guacARM")

Este proyecto se basa en las siguientes herramientas:

- [The GNU Embedded Toolchain for Arm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads): 
Ensamblador cruzado para cądigo ensamblador ARM. Se hace uso del ensamblador cruzado **as** asi como del linker **ld** para generar el correspondiente 
binario ELF y de la utilidad **objdump** para obtener el codigo ensambaldor a partir del binario ya enlazado.
- [JELF](https://github.com/fornwall/jelf): Parser de ficheros ELF para Java. Se ha usado para extraer los segmentos de codigo y datos del fichero 
binario ELF generado por el linker.
- [Unicorn](https://www.unicorn-engine.org): Simulador multiarquitecura. Se ha usado como motor de la simulacion.
- [RSyntaxTextArea](https://github.com/bobbylight/RSyntaxTextArea): Editor de codigo con syntax highlighting y code folding para Java Swing. Se ha
creado un nuevo parser para colorear la sintaxis del ensamblador ARM (lib/AssemblerARMTokenMaker.*).
- [Autocomplete](https://github.com/bobbylight/AutoComplete): Librería de "code completion" para Swing JTextComponents. Se ha creado toda la informacion 
necesaria para que funcione el "code completion" de ensamblador ARM.

Para su ejecución se necesita Java 8 de 64 bits ([https://java.com/es/download/manual.jsp](https://java.com/es/download/manual.jsp) (Windows fuera de linea.64 bits)

Programa ejecutable en guacARM.zip (para descargar, pinchar sobre él, y luego seleccionar "view raw").
