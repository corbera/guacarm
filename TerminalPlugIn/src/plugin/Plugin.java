/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.plugins.interfaces.PluginInterface;
import guacarm.plugins.interfaces.PluginClass;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Terminal plugin
 * 
 * @author corbera
 */
public class Plugin implements PluginClass {

    private PluginInterface api;
    private TerminalAPI terminal;
    private Component comp;
    private boolean initialized = false;

    /**
     * Inicializacion del plugin en modo GUI.
     *
     * @param api interfaz de llamadas con la app
     * @return component awt/swing del plugin
     */
    @Override
    public boolean initializeGUIPlugIn(PluginInterface api) {
        this.api = api;
        if (!api.registerSVCCall(3, new ReadCallBack())) {
            return false;
        }
        if (!api.registerSVCCall(4, new WriteCallBack())) {
            api.unRegisterSVCCall(3);
            return false;
        }
        TerminalWindow terminal = new TerminalWindow(api);
        this.terminal = terminal;
        comp = terminal.getAWTComponent();
        initialized = true;
        return true;
    }

    /**
     * Inicializacion del plugin en modo texto
     *
     * @param api interfaz de llamadas con la app
     */
    @Override
    public boolean initializeConsolePlugIn(PluginInterface api) {
        this.api = api;
        if (!api.registerSVCCall(3, new ReadCallBack())) {
            return false;
        }
        if (!api.registerSVCCall(4, new WriteCallBack())) {
            api.unRegisterSVCCall(3);
            return false;
        }
        terminal = new TerminalText(api);
        initialized = true;
        return true;
    }

    @Override
    public String getShortName() {
        return "Terminal";
    }

    @Override
    public void reset(SimulatorInterface api) {
        terminal.clear();
    }

    @Override
    public String getName() {
        return "guacARM Simple Terminal";
    }

    @Override
    public String getAuthor() {
        return "Francisco Corbera";
    }

    @Override
    public String getVersion() {
        return "0.1";
    }

    @Override
    public Component getAWTComponent() {
        return comp;
    }

    @Override
    public JMenu getJMenu() {
        JMenu m = new JMenu();
        JMenuItem item = new JMenuItem("Clear");
        item.addActionListener(e -> {
            clear();
        });
        m.add(item);
        return m;
    }

    @Override
    public void finalizePlugIn() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Callbak a registrar para el svc read
     */
    public class ReadCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            //System.out.println("LLamada a lectura !!!! :)");
            termRead(api);
        }
    }

    /**
     * Callbak a registrar para el svc write
     */
    public class WriteCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            //System.out.println("LLamada a escritura !!!! :)");
            termWrite(api);
        }
    }

    /**
     * Implementa llamada write a la salida estandar
     *
     * @param api api para acceso a los elementos del simulador (regs, mem)
     */
    public void termWrite(SimulatorInterface api) {
        try {
            int r0 = api.readRegister(0);
            if (r0 != 1 && r0 != 2) // no es salidad estandar o error
            {
                return;
            }
            int r1 = api.readRegister(1); // direccion buffer
            int r2 = api.readRegister(2); // size
            byte[] data = api.readMemory(r1, r2);
            terminal.write(data);
            if (comp != null) {
                this.api.awtComponentModifed();
            }
        } catch (Exception ex) {
            Logger.getLogger(Plugin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void termRead(SimulatorInterface api) {
        try {
            int r0 = api.readRegister(0);
            if (r0 != 0) // no es entrada estandar
            {
                return;
            }
            int r1 = api.readRegister(1);   // direccion buffer
            int r2 = api.readRegister(2);   // size
            byte[] data = terminal.read(r2);

            api.writeMemory(r1, data, data.length);
            api.writeRegister(0, data.length);
        } catch (Exception ex) {
            Logger.getLogger(Plugin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clear() {
        terminal.clear();
    }
}
