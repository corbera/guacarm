/**
 * Basic memory access plugin example.
 *
 * ARM assembly code example:
 *
 * .data
 * vals: .byte 1,2,3,4,5,6,7,8
 *
 * .global _start
 * .text
 * _start: 	
 * push {lr}
 * mov r0, #0
 * ldr r1, [r0]
 * ldr r2, =#0x0a0b0c0d
 * str r2, [r0]
 * mov r2, #-1
 * strb r2, [r0]
 * pop {lr}
 * bx lr
 *
 *
 */
package plugin;

import guacarm.GUI.ColorTheme;
import java.awt.Component;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.plugins.interfaces.PluginInterface;
import guacarm.plugins.interfaces.PluginClass;
import guacarm.plugins.interfaces.PluginMemCallBack;
import java.nio.ByteBuffer;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author corbera
 */
public class Plugin implements PluginClass {

    private PluginInterface api;
    private Component comp;
    private boolean GUI;
    private static final int OFFSET = 0;
    private static final int SIZE = 1024;

    /**
     * Plugin initialization in GUI mode
     *
     * @param api Interface to interact with the application
     * @return Plugin AWT/SWING component (if any)
     */
    @Override
    public boolean initializeGUIPlugIn(PluginInterface api) {
        // Save the app API for later use
        this.api = api;
        // If we can't register the read/write block ([0:1023]) hook, return false to indicate
        // that the plugin has not been initialized
        if (!api.registerMemoryReadWriteHook(OFFSET, SIZE, new MemCallBack())) {
            return false;
        }
        // The plugin will be used in GIU mode
        GUI = true;
        return true;
    }

    /**
     * Plugin initialization in command line mode
     *
     * @param api Interface to interact with the application
     */
    @Override
    public boolean initializeConsolePlugIn(PluginInterface api) {
        // Save the app API for later use
        this.api = api;
        // If we can't register the read/write block ([0:1023]) hook, return false to indicate
        // that the plugin has not been initialized
        if (!api.registerMemoryReadWriteHook(OFFSET, SIZE, new MemCallBack())) {
            return false;
        }
        // The plugin will be used in commnad line mode
        GUI = false;
        return true;
    }

    @Override
    public String getShortName() {
        // Return the short name of the plugin
        return "BasicMemPlugin";
    }

    @Override
    public void reset(SimulatorInterface sapi) {
        // Initialize the plugin parameters for a new simulation
        byte[] zeros = new byte[SIZE];
        try {
            sapi.writeMemory(OFFSET, zeros, SIZE);
        } catch (Exception ex) {
            api.getLog().println("Error writing in memory", ColorTheme.WARNING);
        }
    }

    @Override
    public String getName() {
        // Return the long name of the plugin
        return "Basic Memory Hook Plugin example";
    }

    @Override
    public String getAuthor() {
        // Return the author information of the plugin
        return "Francisco Corbera";
    }

    @Override
    public String getVersion() {
        // Return the version of the plugin 
        return "0.1";
    }

    @Override
    public Component getAWTComponent() {
        // Return the AWT component associated with the pluing (null in this
        // case)
        return null;
    }

    @Override
    public JMenu getJMenu() {
        // We create the plugin menu that will be in the menu bar
        JMenu m = new JMenu();
        // We create a menu entry called "Info";
        JMenuItem item = new JMenuItem("Info");
        // This is the action associated to de menu entry 
        item.addActionListener(e -> {
            api.getLog().println(getName() + " - Version: "
                    + getVersion() + " - by: "
                    + getAuthor(), ColorTheme.UPDATED);
        });
        // We add the entry to the menu
        m.add(item);
        // and return the mneu
        return m;
    }

    @Override
    public void finalizePlugIn() {
        
    }

    /**
     * Callbak associated with the memory block hook
     */
    public class MemCallBack implements PluginMemCallBack {

        @Override
        public void callBack(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
            doAction(si, type, offset, size, dataWrite);
        }
    }

    /**
     * Actions to do when memory block is accessed
     * 
     * @param si Inteface to talk with the simulator
     * @param type type of memory access (PluginMemCallBack.AccessType.READ or PluginMemCallBack.AccessType.WRITE)
     * @param offset address of memory access
     * @param size  size in bytes of memory access
     * @param dataWrite data to write in case of write access
     */
    private void doAction(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
        byte[] myData = {1, 2, 3, 4};
        // print messages in the application console
        api.getLog().println("Memory block [" + OFFSET + ":" + (OFFSET + SIZE - 1) + "] accessed", ColorTheme.FOREGROUND);
        api.getLog().println("Access type: " + ((type == PluginMemCallBack.AccessType.READ) ? "Read" : "Write"), ColorTheme.FOREGROUND);
        api.getLog().println("Memory address: 0x" + Integer.toHexString(offset), ColorTheme.FOREGROUND);
        api.getLog().println("Data size: " + size, ColorTheme.FOREGROUND);
        if (type == PluginMemCallBack.AccessType.WRITE) {
            String format = (size == 1) ? "%02X" : (size == 2) ? "%04X" : "%08X";
            String hexData = String.format(format, dataWrite);
            api.getLog().println("Data to write: 0x" + hexData, ColorTheme.FOREGROUND);
            ByteBuffer buf = ByteBuffer.allocate(4);
            buf.putInt(dataWrite);
            myData = buf.array();
        }
        try {
            // write at the memory offset
            si.writeMemory(offset, myData, size);
        } catch (Exception ex) {
            api.getLog().println("Error writing in memory", ColorTheme.WARNING);
        }
    }
}
