/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import java.awt.Color;
import java.awt.FontMetrics;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 *
 * @author Corbera
 */
public class Utils {

    public static boolean cancelCall = false;

    /**
     * Pasa la cadena hexadecimal hex a su valor en byte
     *
     * @param hex String hexadecimal a convertir
     * @return valor que representa a la cadena
     * @throws NumberFormatException
     */
    public static Byte hexStringToByte(String hex) throws NumberFormatException {
        return Byte.parseByte(hex, 16);
    }

    /**
     * Transforma el array de bytes en una lista de strings en el formato
     * especificado, donde cada valor coge tantos bytes como los indicados por
     * size
     *
     * @param values Array de valores a transformar
     * @param format formato de representacion (0 hex, 1 int, 2 uint)
     * @param size numero de bytes por cada string (1,2 o 4)
     * @return Lista de los strings que representan los valores
     */
    public static List<String> bytesToFormatStrings(byte[] values, int format,
            int size) {
        return bytesToFormatStrings(values, format, size, null, null);
    }

    /**
     * Transforma el array de bytes en una lista de strings en el formato
     * especificado, donde cada valor coge tantos bytes como los indicados por
     * size. Si oldvalues es != null, compara los valores en values con
     * oldvalues, y si han cambaido, por cada cadena de salida introduce un
     * booleano en updates indicando si el valor representado por la cadena ha
     * cambiado
     *
     * @param values Array de valores a transformar
     * @param format formato de representacion (0 hex, 1 int, 2 uint)
     * @param size numero de bytes por cada string (1,2 o 4)
     * @param oldValues valores anteriores
     * @param updated lista que indica si cada elemento de salida ha cambiado o
     * no
     * @return Lista de los strings que representan los valores
     */
    public static List<String> bytesToFormatStrings(byte[] values, int format,
            int size, byte[] oldValues, List<Boolean> updated) {
        List<String> list = new ArrayList<String>();
        int i = 0;
        boolean upd = false;
        String s = "";
        while (i < values.length) {
            switch (format) {
                case Configuration.HEX:
                    upd = (oldValues != null) ? (values[i] != oldValues[i]) : false;
                    s = String.format("%02X", values[i++]);
                    if (size > 1) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        s = ((i < values.length) ? String.format("%02X", values[i++]) : "00") + s;
                    }
                    if (size > 2) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        s = ((i < values.length) ? String.format("%02X", values[i++]) : "00") + s;
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        s = ((i < values.length) ? String.format("%02X", values[i++]) : "00") + s;
                    }
                    break;
                case Configuration.INT:
                    upd = (oldValues != null) ? (values[i] != oldValues[i]) : false;
                    int num = values[i++];
                    if (size > 1) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        num = (num & 0xFF) | (values[i++] << 8) & 0xFFFFFF00;
                    }
                    if (size > 2) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        num = (num & 0xFFFF) | (values[i++] << 16) & 0xFF0000;
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        num = (num & 0xFFFFFF) | (values[i++] << 24) & 0xFF000000;
                    }
                    s = Integer.toString(num);
                    break;
                case Configuration.UINT:
                    upd = (oldValues != null) ? (values[i] != oldValues[i]) : false;
                    long n = values[i++] & 0x0FF;
                    if (size > 1) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        n = (n & 0xFF) | (values[i++] << 8) & 0x0FF00;
                    }
                    if (size > 2) {
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        n = (n & 0xFFFF) | (values[i++] << 16) & 0x0FF0000;
                        upd = (oldValues != null) ? ((upd) ? upd : (values[i] != oldValues[i])) : false;
                        n = (n & 0xFFFFFF) | (values[i++] << 24) & 0x0FF000000L;
                    }
                    s = Long.toString(n);
                    break;
            }
            list.add(s);
            if (oldValues != null && updated != null) {
                updated.add(upd);
            }
        }
        return list;
    }

    /**
     * Transforma el array de bytes en una lista de strings en el formato ASCII,
     * donde cada string coge tantos bytes (caracteres ASCII) como los indicados
     * por size. Si oldvalues es != null, compara los valores en values con
     * oldvalues, y si han cambaido, por cada cadena de salida introduce un
     * booleano en updates indicando si el valor representado por la cadena ha
     * cambiado
     *
     * @param values Array de valores a transformar
     * @param oldValues valores anteriores
     * @param updated lista que indica si cada elemento de salida ha cambiado o
     * no
     * @return Lista de los strings que representan los valores
     */
    public static List<String> bytesToASCIIStrings(byte[] values,
            byte[] oldValues, List<Boolean> updated) {
        List<String> list = new ArrayList<String>();
        int i = 0;
        boolean upd;
        while (i < values.length) {
            upd = (oldValues != null) ? (values[i] != oldValues[i]) : false;
            String s = Character.toString(
                    (isAsciiPrintable((char) values[i]) ? (char) values[i] : '.'));
            i++;
            list.add(s);
            if (oldValues != null && updated != null) {
                updated.add(upd);
            }
        }
        return list;
    }

    /**
     * Devuelve una cadena formateada como un volcado de memoria de los
     * contenidos almacenados en values en el formato especificado con los bytes
     * por palabra especificados en size. La primera posicion de memoria es
     * offset. lineSize especifica cuantos valores mostrar antes del retorno de
     * carro.
     *
     * @param offset primera direccion de memoria
     * @param values valores almacenados en la memoria
     * @param format formato de representacion (0 hex, 1 int, 2 uint)
     * @param size numero de bytes de la representacion (1, 2 o 4)
     * @param lineSize numero de valores por linea
     * @param ascii si se quiere imprimir tambien el contenido en ascii
     * @return String con el volcado de memoria
     */
    public static String memToDump(int offset, byte[] values, int format, int size,
            int lineSize, boolean ascii) {
        return styledMemToDump(offset, values, format, size, lineSize, ascii, null, null, null);
    }

    /**
     * Escribe en el panel jtp una cadena formateada como un volcado de memoria
     * de los contenidos almacenados en values en el formato especificado con
     * los bytes por palabra especificados en size. La primera posicion de
     * memoria es offset. lineSize especifica cuantos valores mostrar antes del
     * retorno de carro. oldValues son los valores antiguos de memoria para
     * resaltar en color los cambios producidos.
     *
     * @param offset primera direccion de memoria
     * @param values valores almacenados en la memoria
     * @param format formato de representacion (0 hex, 1 int, 2 uint)
     * @param size numero de bytes de la representacion (1, 2 o 4)
     * @param lineWidth numero de valores por linea si no hay jtextpane o el
     * numero de pixeles del jtextpane
     * @param ascii si se quiere imprimir tambien el contenido en ascii
     * @param oldValues valores anteriores
     * @param jtp jtextpane donde escribir
     * @return String con el volcado de memoria
     */
    public static String styledMemToDump(long offset, byte[] values, int format, int size,
            int lineWidth, boolean ascii, byte[] oldValues, JTextPane jtp, Log log) {
        //log.println("linewidth "+lineWidth, Color.red);
        jtp.setEditable(true);
        int lineSize = lineWidth;
        int charPerValue = charPerInfo(format, size);
        if (jtp != null) {
            jtp.setText("");

            FontMetrics metrics = jtp.getFontMetrics(jtp.getFont());
            int width = metrics.stringWidth("H");
            //System.out.println(lineWidth + " " + width + " " + lineWidth / width);
            int charPerLine = lineWidth / width;//jtp.getFont().getSize();
            int charPerAddr = 2 + Long.toHexString(offset).length() + 2; // "0xaddr: "
            lineSize = (charPerLine - charPerAddr - 1) / (charPerValue + size + 1);
            lineSize = (lineSize < 1) ? 1 : lineSize;
            //System.out.println(charPerAddr+" "+charPerValue);
        }
        String str = new String();
        List<Boolean> updated = new ArrayList<>();
        List<String> list = bytesToFormatStrings(values, format, size, oldValues, updated);
        List<String> lista = null;
        List<Boolean> updatedASCII = null;
        if (ascii) {
            updatedASCII = new ArrayList<>();
            lista = bytesToASCIIStrings(values, oldValues, updatedASCII);
        }
        for (int i = 0; i < list.size(); i += lineSize) {
            // Cabecera con la direccion de memoria
            String addrs = String.format("0x%X", (offset + i * size)) + ": ";
            str += addrs;
            appendToJTextPane(jtp, addrs, ColorTheme.TITLE);

            // Datos de una linea
            for (int j = 0; j < lineSize; j++) {
                if (i + j < list.size()) {
                    String v = expandString(list.get(i + j), ' ', charPerValue) + " ";
                    Color c = (oldValues != null && updated.get(i + j)) ? ColorTheme.UPDATED : ColorTheme.FOREGROUND;
                    str += v;
                    appendToJTextPane(jtp, v, c);
                }
            }
            if (ascii) {
                if (i + lineSize >= list.size()) {
                    int left = lineSize - (list.size() - i);
                    String spaces = fillString(' ', left * size * 2 + left);
                    str += spaces;
                    appendToJTextPane(jtp, spaces, ColorTheme.FOREGROUND);

                }
                for (int j = 0; j < lineSize; j++) {
                    if (i + j < list.size()) {
                        for (int k = 0; k < size; k++) {
                            if ((i + j) * size + k < lista.size()) {
                                String v = lista.get((i + j) * size + k);
                                Color c = (oldValues != null && updatedASCII.get((i + j) * size + k)) ? ColorTheme.UPDATED : ColorTheme.FOREGROUND;
                                str += v;
                                appendToJTextPane(jtp, v, c);
                            }
                        }
                    }
                }
            }
            str += "\n";
            appendToJTextPane(jtp, "\n", ColorTheme.FOREGROUND);
        }
        jtp.setEditable(false);
        return str;
    }

    /**
     * <p>
     * Checks whether the character is ASCII 7 bit printable.</p>
     *
     * <pre>
     *   CharUtils.isAsciiPrintable('a')  = true
     *   CharUtils.isAsciiPrintable('A')  = true
     *   CharUtils.isAsciiPrintable('3')  = true
     *   CharUtils.isAsciiPrintable('-')  = true
     *   CharUtils.isAsciiPrintable('\n') = false
     *   CharUtils.isAsciiPrintable('&copy;') = false
     * </pre>
     *
     * @param ch the character to check
     * @return true if between 32 and 126 inclusive
     */
    public static boolean isAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127;
    }

    /**
     * Creates a string of 'char' that is 'size' chars long.
     *
     * @param ch The char to add to the string.
     * @param size The number of chars to add to the string.
     * @return The string created
     */
    public static String fillString(char ch, int size) {
        return CharBuffer.allocate(size).toString().replace('\0', ch);
    }

    /**
     * Devuevle un string que es igual al original pero con caracteres ch a su
     * izquierda hasta llegar a size de longitud
     *
     * @param str cadena original
     * @param ch caracter a añadir
     * @param size tamaño final
     * @return string del tamaño indicado
     */
    public static String expandString(String str, char ch, int size) {
        if (size <= str.length()) {
            return str;
        }
        return fillString(ch, size - str.length()) + str;
    }

    /**
     * Crea un string que representa en hexadecimal el valor val con tantos
     * digitos como los indicados por digits
     *
     * @param val valor a representar
     * @param digits numero de digitos
     * @return el valor representado
     */
    public static String intToHexString(int val, int digits) {
        String str;
        str = Integer.toHexString(val).toUpperCase();
        int left = digits - str.length();
        if (left > 0) {
            str = fillString('0', left) + str;
        }
        return str;
    }

    /**
     * Crea un string que representa el valor como un entero sin signo
     *
     * @param val valor a representar
     * @return el valor representado
     */
    public static String intToUintString(int val) {
        long l = (val < 0) ? (0x100000000L - (long) val) : val;
        return Long.toString(l);
    }

    /**
     * Añade al JTextPane el texto msg en color c
     *
     * @param tp JTextPane donde escribir
     * @param msg Texto a escribir
     * @param c Color del texto
     */
    public static void appendToJTextPane(JTextPane tp, String msg, Color c) {
        if (tp == null) {
            return;
        }
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = SimpleAttributeSet.EMPTY;
        if (c != null) {
            aset = sc.addAttribute(aset, StyleConstants.Foreground, c);
        }
        //aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);
        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }

    /**
     * Reemplaza el texto seleccionado (begin-end) del JTextPane por el texto
     * msg en color c
     *
     * @param tp JTextPane donde escribir
     * @param begin posicion de comienzo de la seleccion
     * @param end posicion final de la seleccion
     * @param msg Texto a escribir
     * @param c Color del texto
     */
    public static void replaceSelectionJTextPane(JTextPane tp, int begin, int end, String msg, Color c) {
        if (tp == null) {
            return;
        }
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = SimpleAttributeSet.EMPTY;
        if (c != null) {
            aset = sc.addAttribute(aset, StyleConstants.Foreground, c);
        }
        //aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);
        //int len = tp.getDocument().getLength();
        tp.setSelectionStart(begin);
        tp.setSelectionEnd(end);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }

    private static int charPerInfo(int format, int size) {
        int n = 0;
        switch (format) {
            case Configuration.HEX:
                switch (size) {
                    case 1:
                        n = 2;
                        break;
                    case 2:
                        n = 4;
                        break;
                    default:
                        n = 8;
                        break;
                }
                break;
            case Configuration.INT:
                switch (size) {
                    case 1:
                        n = 4;
                        break;
                    case 2:
                        n = 6;
                        break;
                    default:
                        n = 11;
                        break;
                }
                break;
            case Configuration.UINT:
                switch (size) {
                    case 1:
                        n = 3;
                        break;
                    case 2:
                        n = 5;
                        break;
                    default:
                        n = 10;
                        break;
                }
                break;
        }
        return n;
    }

    /**
     * Devuevel el entero formado por un conjunto de bytes consecutivos
     *
     * @param bytes array con los bytes
     * @param size numero de bytes a usar para contruir el entero (1, 2 o 4)
     * @param offset direccion del primer byte
     * @return valor entero
     * @throws java.lang.Exception si no puede hacer la lectura
     */
    public static int readIntFromBytes(byte[] bytes, int size, int offset) throws Exception {
        if (size < 0 || size > 4 || size == 3 || bytes.length < offset + size) {
            throw new Exception();
        }
        int val = bytes[offset] & 0x0FF;
        for (int i = 1; i < size; i++) {
            val |= (bytes[offset + i] & 0x0FF) << (i << 3);
        }
        return val;
    }

    /**
     * Escribe el valor representado en el entero en bytes separados
     *
     * @param value valor a escribir
     * @param bytes array de bytes donde escribir
     * @param size numero de bytes a escribir (1, 2 o 4)
     * @param offset direccion del primer byte a escribir
     * @throws java.lang.Exception si no se puede escribir
     */
    public static void writeIntToBytes(int value, byte[] bytes, int size, int offset) throws Exception {
        if (size < 0 || size > 4 || size == 3 || bytes.length < offset + size) {
            throw new Exception();
        }
        for (int i = 0; i < size; i++) {
            bytes[offset + i] = (byte) ((value >>> (i << 3)) & 0x0FF);
        }

    }

    /**
     * Une varios arrays de bytes en uno solo
     *
     * @param arrays los arrays
     * @return el array que los contiene a todos
     */
    public static byte[] joinByteArray(byte[]... arrays) {
        int length = 0;
        for (byte[] array : arrays) {
            length += array.length;
        }

        final byte[] result = new byte[length];

        int offset = 0;
        for (byte[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }
}
