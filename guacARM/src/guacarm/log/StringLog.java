/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.log;

import java.awt.Color;

/**
 *
 * @author corbera
 */
public class StringLog extends Log {

    private String log;

    public StringLog() {
        log = new String();
    }

    @Override
    public void print(String s) {
        log += s;
    }

    @Override
    public void print(String s, Color color) {
        print(s);
    }

    public String getLog() {
        return log;
    }

    @Override
    public void println(String s) {
        log += s + "\n";
    }

    @Override
    public void println(String s, Color color) {
        println(s);
    }
}
