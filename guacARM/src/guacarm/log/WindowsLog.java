/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.log;

import guacarm.GUI.ColorTheme;
import static guacarm.GUI.HighlightTab.highlightTab;
import guacarm.Utils;
import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author corbera
 */
public class WindowsLog extends Log {

    private JTextPane jtp;
    private ArrayList<Tag> tags;

    private class Tag {

        public int begin;
        public int end;

        public Tag(int b, int e) {
            begin = b;
            end = e;
        }
    }

    public WindowsLog() {
        this(null);
    }

    public WindowsLog(JTextPane jtp) {
        this.jtp = jtp;
        tags = new ArrayList<>();
    }

    public void setjTextArea(JTextPane jtp) {
        this.jtp = jtp;
        jtp.setBackground(ColorTheme.BACKGROUND);
        jtp.setOpaque(true);
        jtp.setForeground(ColorTheme.FOREGROUND);
    }

    @Override
    public void println(String s) {
        println(s, ColorTheme.FOREGROUND);
    }

    @Override
    public void println(String s, Color color) {
        if (jtp != null) {
            Utils.appendToJTextPane(jtp, s + "\n", color);
            highlightTab(jtp);
        }
    }

    @Override
    public void print(String s) {
        print(s, ColorTheme.FOREGROUND);
    }

    @Override
    public void print(String s, Color color) {
        if (jtp != null) {
            Utils.appendToJTextPane(jtp, s, color);
            highlightTab(jtp);
        }
    }

    /**
     * Imprime un texto y guarda una referencia del mismo para luego poder
     * sustituirlo.
     *
     * @param s texto a mostrar
     * @param color color del texto
     * @return identificador del texto para luego poder modificarlo
     */
    public int printTag(String s, Color color) {
        int begin = jtp.getDocument().getLength();
        print(s, color);
        int end = jtp.getDocument().getLength();
        int ntag = tags.size();
        tags.add(ntag, new Tag(begin, end));
        return ntag;
    }

    /**
     * Reemplaza el texto del tag correspondiente por el nuevo texto
     *
     * @param ntag identificador del tag
     * @param s nuevo texto
     */
    public void replaceTag(int ntag, String s) {
        replaceTag(ntag, s, ColorTheme.FOREGROUND);
    }

    /**
     * Reemplaza el texto del tag correspondiente por el nuevo texto
     *
     * @param ntag identificador del tag
     * @param s nuevo texto
     * @param color color del texto
     */
    public void replaceTag(int ntag, String s, Color color) {
        Tag t = tags.get(ntag);
        if (t != null) {
            Utils.replaceSelectionJTextPane(jtp, t.begin, t.end, s, color);
            t.end = t.begin + s.length();
            highlightTab(jtp);
        }
    }
}
