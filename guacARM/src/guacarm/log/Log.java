/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.log;

import java.awt.Color;

/**
 * Clase abstracata para crear Logs
 * @author Corbera
 */
public abstract class Log{
    /**
     * Imprime una cadena en el log
     * @param s cadena a escribir
     */
    public abstract void print(String s);
    
    /**
     * Imprime una cadena en el log en un determinado color
     * @param s cadena
     * @param color color
     */
    public abstract void print(String s, Color color);
    
    /**
     * Imprimer una cadena en el log seguida de \n
     * @param s cadena a escribir
     */
    public abstract void println(String s);
    
    /**
     * Imprime una cadena en el log en un determinado color seguida de \n
     * @param s cadena
     * @param color color
     */
    public abstract void println(String s, Color color);
}
