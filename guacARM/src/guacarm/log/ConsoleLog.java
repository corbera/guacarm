/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.log;

import java.awt.Color;

/**
 *
 * @author corbera
 */
public class ConsoleLog extends Log {

    @Override
    public void print(String s) {
       System.out.print(s);
    }

    @Override
    public void print(String s, Color color) {
        print(s);
    }

    @Override
    public void println(String s) {
       System.out.println(s);
    }

    @Override
    public void println(String s, Color color) {
        println(s);
    }
    
}
