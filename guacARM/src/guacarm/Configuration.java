/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;

/**
 * Clase para manejar el guardado y recuperacion de la configuracion de la
 * aplicacion al cerrar y abrir.
 *
 * @author corbera
 */
public class Configuration {

    public static final String VERSION = "0.1.4";
    public static final String APPNAME = "guacARM";
    public static final String AUTHOR = "Francisco Corbera";
    public static final String AUTHOREMAIL = "corbera@ac.uma.es";
    public static final String[] AFF = {"Department of Computer Architecture",
        "University of Malaga", "Spain"};
    public static final String ICONPATH = "/resources/guacARMv2_32.png";

    public static final int FRAMEWIDTH = 1024;
    public static final int FRAMEHEIGHT = 720;

    public static final int HEX = 0;
    public static final int INT = 1;
    public static final int UINT = 2;
    public static final int NFORMATS = 3;
    public static final String HEXS = "Hex";
    public static final String INTS = "Int";
    public static final String UINTS = "Uint";
    public static final int B8 = 0;
    public static final int B16 = 1;
    public static final int B32 = 2;
    public static final int NSIZES = 3;
    public static final String B8S = "8b";
    public static final String B16S = "16b";
    public static final String B32S = "32b";

    private Properties prop = null;
    private String fileName = "config";

    public Configuration() {
        prop = new Properties();
        InputStream is;

        try {
            is = new FileInputStream(fileName + ".txt");
            prop.load(is);
        } catch (IOException e) {
        }
    }

    /**
     * Lee una propiedad de tipo String de configuracion
     *
     * @param key propiedad
     * @param def valor por defecto
     * @return valor leido de la propiedad
     */
    public String getConfig(String key, String def) {
        return prop.getProperty(key, def);
    }

    /**
     * Guarda el valor de una propiedad de tipo String para poder ser recuperado
     * en la proxima ejecucion de la app
     *
     * @param key propiedad
     * @param val valor de la propiedad
     */
    public void setConfig(String key, String val) {
        prop.setProperty(key, val);
    }

    /**
     * Lee una propiedad de tipo int de configuracion
     *
     * @param key propiedad
     * @param def valor por defecto
     * @return valor leido de la propiedad
     */
    public int getConfig(String key, int def) {
        try {
            def = Integer.parseInt(prop.getProperty(key));
        } catch (NumberFormatException e) {
        }
        return def;
    }

    /**
     * Guarda el valor de una propiedad de tipo int para poder ser recuperado en
     * la proxima ejecucion de la app
     *
     * @param key propiedad
     * @param val valor de la propiedad
     */
    public void setConfig(String key, int val) {
        prop.setProperty(key, Integer.toString(val));
    }

    public Properties getConfig(String suffix) {
        Properties prop = new Properties();
        InputStream is;
        try {
            is = new FileInputStream(fileName + ".plugin." + suffix + ".txt");
            prop.load(is);
        } catch (IOException e) {
        }
        return prop;
    }

    public void setConfig(Properties prop, String suffix) {
        saveConfiguration(prop, fileName + ".plugin." + suffix + ".txt");
    }

    /**
     * Indica una lista de fichero abiertos (para poder ser abiertos
     * automaticamente la proxima vez)
     *
     * @param files lista de nombres de ficheros
     * @param suffix sufijo del fichero de configuracion que se va a usar (para
     * tener varios)
     */
    public void setFileList(List<String> files, String suffix) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName + suffix + ".bin");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(files);
            oos.close();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Recupera la lista de ficheros almacenada
     *
     * @param suffix sufijo del fichero de configuracion que se va a usar (para
     * tener varios)
     * @return lista de ficheros almacenados
     */
    public List<String> getFileList(String suffix) {
        ArrayList<String> files;
        try {
            FileInputStream fis = new FileInputStream(fileName + suffix + ".bin");
            ObjectInputStream ois = new ObjectInputStream(fis);
            files = (ArrayList) ois.readObject();
            ois.close();
            fis.close();
            return files;
        } catch (IOException ioe) {
            return null;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return null;
        }
    }

    public void saveConfiguration() {
        saveConfiguration(this.prop, fileName + ".txt");
    }

    /**
     * Vuelca la configuracion almacenada a fichero
     */
    public void saveConfiguration(Properties prop, String filename) {
        OutputStream os;
        try {
            os = new FileOutputStream(filename);
            prop.store(os, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ImageIcon getProgramIcon() {
        java.net.URL imgURL = getClass().getResource(ICONPATH);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + ICONPATH);
            return null;
        }
    }
}
