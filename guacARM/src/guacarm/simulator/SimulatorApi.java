/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.plugins.interfaces.SimulatorInterface;

/**
 *
 * @author corbera
 */
public class SimulatorApi implements SimulatorInterface {
    private final Simulator simulator;
    
    public SimulatorApi(Simulator s) {
        simulator = s;
    }
    
    /**
     * Escribe un valor en un registro
     * 
     * @param reg registro a escribir
     * @param val valor a escribir
     * @throws Exception si no se puede escribir en el registro
     */
    @Override
    public void writeRegister(int reg, int val) throws Exception {
        simulator.writeRegister(reg, val);
    }
    
    /**
     * Lee el contenido del registro indicado
     * 
     * @param reg registro a leer
     * @return valor almacenado en el registro
     * @throws Exception si no se puede leer el registro
     */
    @Override
    public int readRegister(int reg) throws Exception {
        return simulator.readRegister(reg);
    }
    
    /**
     * Lee el contenido de memoria especidicado por offset y size
     *
     * @param offset
     * @param size
     * @return
     */
    @Override
    public byte[] readMemory(int offset, int size) throws Exception {
        return simulator.readMem(offset, size);
    }

    /**
     * Escribe en la posicion de memoria offset los bytes indicados por size
     * leidos de buff
     *
     * @param offset posicion de memoria donde escribir
     * @param buff datos a escribir
     * @param size numero de bytes a escribir
     */
    @Override
    public void writeMemory(int offset, byte[] buff, int size) throws Exception {
        simulator.writeMem(offset, buff, size);
    }

    /**
     * Return the data segment offset
     * @return 
     */    @Override
    public int getDataOffset() {
        return simulator.getDataBaseAddr();
    }

    /**
     * Return the code segment offset
     * @return 
     */    @Override
    public int getCodeOffset() {
        return simulator.getCodeBaseAddr();
    }

    /**
     * Return the stack segment offset
     * @return 
     */    @Override
    public int getStackOffset() {
        return simulator.getStackBaseAddr();
    }
}
