/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.Utils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import unicorn.EventMemHook;
import unicorn.Unicorn;

/**
 * Manejador de memoria. Mapea trozos de memoria bajo demanda
 *
 * @author corbera
 */
public class MemoryManager {

    private static final int PAGEBITS = 18;
    private static final int GUARDBITS = 4;
    private final int PAGESIZE = 1 << PAGEBITS;
    private final Set<Long> pageTable = new HashSet<Long>();
    private final Unicorn uc;
    private final int mode = unicorn.UnicornConst.UC_HOOK_MEM_READ_UNMAPPED
            | unicorn.UnicornConst.UC_HOOK_MEM_WRITE_UNMAPPED
            | unicorn.UnicornConst.UC_HOOK_MEM_FETCH_UNMAPPED
            | unicorn.UnicornConst.UC_HOOK_MEM_READ_PROT
            | unicorn.UnicornConst.UC_HOOK_MEM_WRITE_PROT
            | unicorn.UnicornConst.UC_HOOK_MEM_FETCH_PROT;

    public MemoryManager(Unicorn uc) {
        this.uc = uc;
        uc.hook_add(new MyEventMemHook(), mode, null);
    }

    private class MyEventMemHook implements EventMemHook {

        /**
         * Es llamada cuando se produce un error de acceso a memoria. Reservara
         * memoria para la pagina a la que pertenece la direccion
         *
         * @param u simulador
         * @param address direccion que ha provocado el fallo
         * @param size
         * @param value
         * @param user
         * @return true si puede continuar con la ejecucion
         */
        @Override
        public boolean hook(Unicorn u, long address, int size, long value, Object user) {
            long page = getPage(address);
            if (!pageTable.add(page)) {
                throw new UnsupportedOperationException("Error in table page");
            }
            u.mem_map(getAddress(page), PAGESIZE, Unicorn.UC_PROT_ALL);
//            if (upGuard((int) address)) {
//                mapPage(page + 1);
//            } else if (downGuard((int) address)) {
//                mapPage(page - 1);
//            }
            return true;
        }

    }

    /**
     * Mapea una pagina en memoria del simulador
     *
     * @param page pagina a mapear
     * @return true si no estaba y la ha mapeado nueva
     */
    private boolean mapPage(long page) {
        if (pageTable.add(page)) {
            long addr = getAddress(page); 
            uc.mem_map(addr, PAGESIZE, Unicorn.UC_PROT_ALL);
            return true;
        }
        return false;
    }

    /**
     * Mapea las paginas correspondientes a la peticion de memoria
     *
     * @param address direccion base del trozo de memoria a mapear
     * @param size tamaño del trozo de memoria
     */
    public void mem_map(long address, long size) {
        long begin = getPage(address);
        long end = getPage(address + size) + 1;
        for (long page = begin; page < end; page++) {
            mapPage(page);
        }
    }

    /**
     * Lee un bloque de memoria
     *
     * @param offset direccion incial del bloque
     * @param size tamaño del bloque
     * @return bytes leidos
     * @throws Exception si no se puede leer
     */
    //                  offset                  offet+size
    //                    |                         |
    //                    V                         V
    // -P0--------|-PI---------|-P2---------|-PE---------|----------
    //                  |------|            |---------|
    //                  A=n*1024               m*1024=B
    final int BITBLQ = 10;

    public byte[] mem_read(long offset, long size) throws Exception {
        byte[] dataRead;
        long offRead;
        long pi = getPage(offset);
        long pe = getPage(offset + size - 1);
        mapPage(pi);

        if (pi == pe) { // el bloque entra completo dentro de la misma pagina
            long minsize = minSizeMemoryBlock(size); //(size - 1) >>> BITBLQ) + 1;
            long a = (offset + size <= getAddress(pi + 1)) ? offset : getAddress(pi + 1) - minsize;
            dataRead = uc.mem_read(a, minsize);
            offRead = offset - a;
        } else { // el bloque al menos pertenece a dos paginas
            // trozo de la pagina inicial
            long minsize = minSizeMemoryBlock(getAddress(pi + 1) - offset); //((getAddress(pi + 1) - offset) - 1) >>> BITBLQ) + 1;
            long a = getAddress(pi + 1) - minsize;
            dataRead = uc.mem_read(a, minsize);
            offRead = offset - a;
            // paginas intermedias completas
            for (long page = pi + 1; page < pe - 1; page++) {
                mapPage(page);
                dataRead = Utils.joinByteArray(dataRead, uc.mem_read(getAddress(page), PAGESIZE));
            }
            // trozo de la ultima pagina
            mapPage(pe);
            minsize = minSizeMemoryBlock(offset + size - getAddress(pe)); //((offset + size - getAddress(pe) - 1) >>> BITBLQ) + 1;
            dataRead = Utils.joinByteArray(dataRead, uc.mem_read(getAddress(pe), minsize));
        }
        return Arrays.copyOfRange(dataRead, (int)offRead, (int)(offRead + size));
    }

    public void mem_write(final long offset, final byte[] buff) {
        try {
            mem_write((int) offset, buff, buff.length);
        } catch (Exception ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Escribe un bloque de datos en memoria
     *
     * @param offset direccion de memoria donde escribir
     * @param buff datos a escribir
     * @param size tamaño de los datos
     * @throws Exception
     */
    public void mem_write(final int offset, final byte[] buff, final long size) throws Exception {
        byte[] dataWrite;
        long offRead;
        long pi = getPage(offset);
        long pe = getPage(offset + size - 1);
        mapPage(pi);

        if (pi == pe) { // el bloque entra completo dentro de la misma pagina
            long minsize = minSizeMemoryBlock(size); //((size - 1) >>> BITBLQ) + 1;
            long a = (offset + size <= getAddress(pi + 1)) ? offset : getAddress(pi + 1) - minsize;
            offRead = offset - a;
            if (size != minsize) {
                dataWrite = uc.mem_read(a, minsize);
                System.arraycopy(buff, 0, dataWrite, (int)offRead, (int)size);
            } else {
                dataWrite = buff;
            }
            uc.mem_write(a, dataWrite);
        } else { // el bloque al menos pertenece a dos paginas
            // trozo de la pagina inicial
            long minsize = minSizeMemoryBlock(getAddress(pi + 1) - offset); //((getAddress(pi + 1) - offset) - 1) >>> BITBLQ) + 1;
            long a = getAddress(pi + 1) - minsize;
            offRead = offset - a;
            dataWrite = uc.mem_read(a, minsize);
            System.arraycopy(buff, 0, dataWrite, (int)offRead, (int)minsize);
            uc.mem_write(a, dataWrite);
            // paginas intermedias completas
            dataWrite = new byte[PAGESIZE];
            long myoff = minsize;
            for (long page = pi + 1; page < pe - 1; page++) {
                mapPage(page);
                System.arraycopy(buff, (int)myoff, dataWrite, (int)offRead, PAGESIZE);
                myoff += PAGESIZE;
                uc.mem_write(getAddress(page), dataWrite);
            }
            // trozo de la ultima pagina
            mapPage(pe);
            minsize = minSizeMemoryBlock(offset + size - getAddress(pe)); //(offset + size - getAddress(pe) - 1) >>> BITBLQ) + 1;
            dataWrite = uc.mem_read(getAddress(pe), minsize);
            System.arraycopy(buff, (int)myoff, dataWrite, 0, (int)(offset + size - getAddress(pe)));
            uc.mem_write(getAddress(pe), dataWrite);
        }
    }

    /**
     * Devuelve la pagina a la que pertenece una direccion
     *
     * @param addr direccion
     * @return pagina a la que pertenece
     */
    private long getPage(long addr) {
        return addr >>> PAGEBITS;
    }

    /**
     * Devuelve la direccion base de una pagina
     *
     * @param page la pagina
     * @return direccion base de la pagina
     */
    private long getAddress(long page) {
        return page << PAGEBITS;
    }

    /**
     * Indica si hay que reservar memoria por debajo de la dir dada (cerca de la
     * parte inferior de la pagina a la que pertenece)
     *
     * @param addr direccion
     * @return true si hay que reservar pagina por debajo
     */
    private boolean downGuard(int addr) {
        return (getPage(addr) > 0)
                && (((addr & ((1 << PAGEBITS) - 1)) >>> (PAGEBITS - GUARDBITS)) == 0);
    }

    /**
     * Indica si hay que reservar memoria por encima de la dir dada (cerca de la
     * parte superior de la pagina a la que pertenece)
     *
     * @param addr direccion
     * @return true si hay que reservar pagina por encima
     */
    private boolean upGuard(int addr) {
        return (getPage(addr) < (1 << (32 - PAGEBITS)) - 1)
                && (((addr & ((1 << PAGEBITS) - 1)) >>> (PAGEBITS - GUARDBITS)) == ((1 << GUARDBITS) - 1));
    }

    /**
     * Tamaño minimo de bloque que es multiplo de 2^BITBLOQ y que es mas grande
     * que el tamaño dado
     *
     * @param size tamaño deseado
     * @return tamaño minimo de bloque multiplo de 2^BITBLOQ que es mayor que
     * size
     */
    private long minSizeMemoryBlock(long size) {
        return (((size - 1) >>> BITBLQ) + 1) << BITBLQ;
    }

    /**
     * pone a cero todas las paginas mapeadas
     */
    public void reset() {
        byte[] zero = new byte[PAGESIZE];
        pageTable.forEach(page -> {
            uc.mem_write(getAddress(page), zero);
        });
    }
}
