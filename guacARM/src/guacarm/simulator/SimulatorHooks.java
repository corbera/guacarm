/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.GUI.ColorTheme;
import guacarm.GenericCallBack;
import guacarm.plugins.interfaces.PluginMemCallBack;
import java.util.logging.Level;
import java.util.logging.Logger;
import unicorn.InterruptHook;
import unicorn.Unicorn;
import unicorn.ReadHook;
import unicorn.WriteHook;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.plugins.interfaces.SimulatorInterface;
import java.util.HashMap;
import java.util.Map;
import unicorn.CodeHook;
import guacarm.interrupt.EndOfInterrupt;

/**
 *
 * @author corbera
 */
public class SimulatorHooks {

    public static final int NSVC = 256;
    private final PluginSVCCallBack[] services = new PluginSVCCallBack[NSVC];  // servicios registrados
    private final Map<Integer, MyHooks> memoryHooks = new HashMap<>();
    private CodeHook endOfInterruptionHook;
    private EndOfInterrupt endOfInterruption;
    private final Simulator simulator;

    public class CodeChangeCallback implements GenericCallBack {

        @Override
        public Object callBack(Object o) {
            return activateEndOfInterruption();
        }

    }

    public SimulatorHooks(Simulator sim) {
        simulator = sim;
        simulator.registerCallbackOnCodeChange(new CodeChangeCallback());
        activate();
    }

    /**
     * Registra los distintos callbacks permitidos a los plugins swi/svc, mem
     * read, mem write
     */
    private void activate() {
        // hook para las instrucciones swi/svc
        simulator.hook_add(new MyInterruptHook(), null);
        // hook para las instrucciones subs pc, lr (retorno RTIs)
    }

    /**
     * Service Calls (SVC/SWI)
     */
    /**
     * Clase que implementa el callback que se produce cuando el simulador
     * encuentra una instrucción swi/svc
     */
    private class MyInterruptHook implements InterruptHook {
        // callback for tracing Linux interrupt

        @Override
        public void hook(Unicorn uc, int intno, Object user) {
            try {
                int service = simulator.readRegister(7);
                //System.err.println(String.format("Interrupt 0x%x, service r7=%d", intno, service));
                if (service > 0 && service < NSVC && services[service] != null) {
                    services[service].callBack(simulator.api);
                } else {
                    simulator.log.println("Invalid service call: " + service + " at 0x"
                            + Integer.toHexString(((Long) uc.reg_read(Unicorn.UC_ARM_REG_PC)).intValue() - 4),
                            ColorTheme.WARNING);
                    simulator.abortSimulation();
                }
            } catch (Exception ex) {
                //if (ex.getClass() != InterruptedException.class) {
                Logger.getLogger(SimulatorHooks.class.getName()).log(Level.SEVERE, null, ex);
                //}
            }
        }
    }

    /**
     * Registra el callback asociado a un servicio si no esta previamente
     * registrado
     *
     * @param svc numero del servicio (r7) a registrar [0, NSVC)
     * @param callback callback al cual se invocara cuando se ejecute un swi con
     * r7=svc
     * @return true si se ha podido registrar, false si no.
     */
    public boolean registerSVC(int svc, PluginSVCCallBack callback) {
        if (svc < 0 || svc >= NSVC || services[svc] != null) {
            return false;
        }
        services[svc] = callback;
        return true;
    }

    /**
     * Elimina el registro de un servicio
     *
     * @param svc numero del servicio
     */
    public void unRegisterSVC(int svc) {
        services[svc] = null;
    }

    /**
     * Indica si un servicio esta o no registrado
     *
     * @param svc numero del servicio
     * @return true si esta registrado
     */
    public boolean isRegisteredSVC(int svc) {
        return services[svc] != null;
    }

    /**
     * FINAL DE RTIs
     */
    /**
     * Clase que implementa el callback que se produce cuando el simulador
     * encuentra una instrucción final de RTI (subs pc, lr)
     */
    private class MyCodeHook implements CodeHook {
        // callback for tracing Linux interrupt

        @Override
        public void hook(Unicorn uc, long addr, int size, Object o) {
            //System.err.println(String.format("End of Interruption 0x%x, size =%d", addr, size));
            if (endOfInterruption != null) {
                endOfInterruption.callBack(uc, (int) addr);
            }
        }
    }

    /**
     * Registra el callback asociado al final de una interrupcion
     *
     * @param callback
     */
    public void registerEndOfInterruption(EndOfInterrupt callback) {
        if (endOfInterruption != callback) {
            endOfInterruption = callback;
            if (endOfInterruptionHook != null) {
                simulator.hook_del(endOfInterruptionHook);
                endOfInterruptionHook = null;
            }
        }
    }

    /**
     * Activa el callback de final de interrupcion para la lista de direcciones
     * de instrucciones subs pc, lr
     *
     * @return true si a activado alguno
     */
    public boolean activateEndOfInterruption() {
        if (endOfInterruption != null) {
            if (endOfInterruptionHook != null) {
                simulator.hook_del(endOfInterruptionHook);
                endOfInterruptionHook = null;
            }
            endOfInterruptionHook = new MyCodeHook();
            simulator.listSubsPCLRInstructions.forEach((addr) -> {
                simulator.hook_add(endOfInterruptionHook, (long) addr, (long) addr, null);
            });
            return true;
        }
        return false;
    }

    /**
     * Elimina el callback asociado al final de una interrupcion
     */
    public void unRegisterEndInterruption() {
        endOfInterruption = null;
        simulator.hook_del(endOfInterruptionHook);
        endOfInterruptionHook = null;
    }

    /**
     * ACCESOS A MEMORIA
     */
    private static final int READ = 1;
    private static final int WRITE = 2;

    /**
     * Registra un callback para los accesos de lectura a una region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura en la
     * region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadHook(int offset, int size, PluginMemCallBack callback) {
        return registerMemoryReadWriteHook(offset, size, callback, READ);
    }

    /**
     * Registra un callback para los accesos de escritura a una region de
     * memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una escritura en
     * la region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryWriteHook(int offset, int size, PluginMemCallBack callback) {
        return registerMemoryReadWriteHook(offset, size, callback, WRITE);
    }

    /**
     * Registra un callback para los accesos de lectura/escritura a una region
     * de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una
     * lectura/escritura en la region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback) {
        return registerMemoryReadWriteHook(offset, size, callback, READ | WRITE);
    }

    /**
     * Registra un callback para los accesos de lectura y/o escritura a una
     * region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura y/o
     * escritura en la region
     * @param accessType tipo de acceso a memoria (1 READ, 2 WRITE, 3
     * READ|WRITE)
     * @return true si se ha podido registrar
     */
    private boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback, int accessType) {
        if (memoryHooks.containsKey(offset)) {
            return false;
        }
        // si no da error al hacer el mapeo de memoria es que nadie lo ha mapeado
        // antes, por lo que no puede haber ningun plugin que solape memoria
        MyHooks memHook = new MyHooks();
        if ((accessType & READ) != 0) {
            memHook.readHook = new MyReadHook();
            simulator.hook_add(memHook.readHook, offset, offset + size,
                    new MyPluginMemCallBack(offset, size, callback));
        }
        if ((accessType & WRITE) != 0) {
            memHook.writeHook = new MyWriteHook();
            simulator.hook_add(memHook.writeHook, offset, offset + size,
                    new MyPluginMemCallBack(offset, size, callback));
        }
        memoryHooks.put(offset, memHook);

        // mapeamos la memoria en el simulador
        simulator.memoryManager.mem_map(offset & 0x0FFFFFFFFL, size);
        return true;
    }

    /**
     * Elimina el registro de un hook de memoria
     *
     * @param offset direccion base de la memoria asociada al hook
     * @param size tamaño de la memoria asociada al hook
     * @return true si lo ha eliminado con exito
     */
    public boolean unRegisterMemoryHook(int offset, int size) {
        MyHooks memHook = memoryHooks.get(offset);
        if (memHook != null) {
            if (memHook.readHook != null && !simulator.hook_del(memHook.readHook)) {
                return false;
            }
            if (memHook.writeHook != null && !simulator.hook_del(memHook.writeHook)) {
                return false;
            }
            memoryHooks.remove(offset);
            return true;
        }
        return false;

    }

    private class MyHooks {

        public MyReadHook readHook;
        public MyWriteHook writeHook;
    }

    private class MyReadHook implements ReadHook {

        @Override
        public void hook(Unicorn u, long address, int size, Object callback) {
            //System.out.printf(">>> Memory is being READ at 0x%x, data size = %d\n", address, size);
            ((PluginMemCallBack) callback).callBack(simulator.api, PluginMemCallBack.AccessType.READ, (int) address, size, 0);
        }
    }

    private class MyWriteHook implements WriteHook {

        @Override
        public void hook(Unicorn u, long address, int size, long value, Object callback) {
            //System.out.printf(">>> Memory is being WRITE at 0x%x, data size = %d, data value = 0x%x\n", address, size, value);
            ((PluginMemCallBack) callback).callBack(simulator.api, PluginMemCallBack.AccessType.WRITE, (int) address, size, (int) value);
        }
    }

    private class MyPluginMemCallBack implements PluginMemCallBack {

        private final int offset;
        private final int size;
        private final PluginMemCallBack callback;

        public MyPluginMemCallBack(int offset, int size, PluginMemCallBack callback) {
            this.offset = offset;
            this.size = size;
            this.callback = callback;
        }

        @Override
        public void callBack(SimulatorInterface api, AccessType type, int address, int size, int data) {
            if (address >= offset && address < offset + this.size) {
                //if (type == AccessType.WRITE)
//                System.out.println("Memory access: type " + type + 
//                        " addres 0x" + Integer.toHexString(address) +
//                        " size " + size +
//                        " data " + data +
//                        " time " + System.nanoTime());
//                try {
//                    System.out.println(Integer.toHexString(simulator.readRegister(13)));
//                } catch (Exception ex) {
//                    Logger.getLogger(SimulatorHooks.class.getName()).log(Level.SEVERE, null, ex);
//                }
                callback.callBack(api, type, address, size, data);
            }
        }

    }
}
