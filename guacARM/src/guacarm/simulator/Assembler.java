/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.GUI.ColorTheme;
import guacarm.log.ConsoleLog;
import guacarm.log.Log;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.*;

/**
 *
 * @author Corbera
 */
public class Assembler {

    public String[] stdout;
    public String[] stderr;
    public int[] errorno;
    public byte[] dataSegment;
    public byte[] textSegment;
    public long textOff;
    public long textSize;
    public long dataOff;
    public long dataSize;
    public long entryPoint;
    private final Log log;
    private String tmpDirectory;
    private String binDirectory;
    private String asmBinary;
    private final String[] asmArgs = {"-alns", "-march=armv7-a", "-mfpu=vfp"};
    private String linkerBinary;
    private String objDump;
    private final String SEP = File.separator;
    public String elfDumpOut;
    public String elfDumpErr;
    public int elfDumpNErr;

    public Assembler() {
        this(new ConsoleLog());
    }

    public Assembler(Log l) {
        log = l;
        tmpDirectory = System.getProperty("java.io.tmpdir");
        //System.out.println(tmpDirectory);
        String os = System.getProperty("os.name");
        //System.err.println(os);
        if (os.toLowerCase().indexOf("windows") >= 0) {
            binDirectory = "lib" + SEP + "windows";
            asmBinary = "arm-none-eabi-as.exe";
            linkerBinary = "ld.exe";
            objDump = "objdump.exe";
        } else if (os.toLowerCase().indexOf("mac") >= 0) {
            binDirectory = "lib" + SEP + "macos";
            asmBinary = "arm-none-eabi-as";
            linkerBinary = "ld";
            objDump = "objdump";            
        } else if (os.toLowerCase().indexOf("linux") >= 0) {
            binDirectory = "lib" + SEP + "linux";
            asmBinary = "arm-none-eabi-as";
            linkerBinary = "ld";
            objDump = "objdump";            
        }
    }

    /**
     * Ensabla los ficheros con el codigo fuente, los ensambla y obtiene la
     * salida estandar (stdout), la salida de error (stderr) y si se ha
     * producido o no error en el ensamblado (errorno).Tambien lee el fichero
     * binario generado y extrae de ahi los segmentos de datos (dataSegment) y
     * de codigo (textSegment).
     *
     * @param files lista con los nombres de los ficheros a ensamblar
     * @param builtinLibs lista con los nombres de los recursos internos que
     * contienen fuentes de librerias que se tienen que ensamblar también
     * @return lista de enteros, el primero indica el fichero erroneo (-1 si es
     * en el enlazado) y luego las lineas donde se han detectado errores
     */
    public List<Integer> assembleFiles(List<String> files, List<String> builtinLibs) {
        if (builtinLibs != null) {
            int libcount = 0;
            for (String resource : builtinLibs) {
                InputStream stream = this.getClass().getResourceAsStream(resource);
                if (stream == null) {
                    log.println("Cannot get the resource " + resource, ColorTheme.WARNING);
                    List<Integer> l = new ArrayList<>();
                    l.add(-1);
                    return l;
                }
                String filename = tmpDirectory + SEP + "_lib_" + (libcount++) + ".s";
                Path path = Paths.get(filename);
                try {
                    Files.copy(stream, path, REPLACE_EXISTING);
                } catch (IOException ex) {
                    log.println("Cannot create file " + filename, ColorTheme.WARNING);
                    List<Integer> l = new ArrayList<>();
                    l.add(-1);
                    return l;
                }
                files.add(filename);
            }
        }
        return assembleFiles(files.toArray(new String[0]));
    }

    /**
     * Ensabla los ficheros con el codigo fuente, los ensambla y obtiene la
     * salida estandar (stdout), la salida de error (stderr) y si se ha
     * producido o no error en el ensamblado (errorno). Tambien lee el fichero
     * binario generado y extrae de ahi los segmentos de datos (dataSegment) y
     * de codigo (textSegment).
     *
     * @param files array con los ficheros a ensamblar
     * @return lista de enteros, el primero indica el fichero erroneo (-1 si es
     * en el enlazado) y luego las lineas donde se han detectado errores
     */
    public List<Integer> assembleFiles(String[] files) {
        try {

            // -- Linux --
            // Run a shell command
            //Process process = Runtime.getRuntime().exec("./arm-none-eabi-as -a "+args[0]);
            // Run a shell script
            // Process process = Runtime.getRuntime().exec("path/to/hello.sh");
            // -- Windows --
            // Run a command
            //Process process = Runtime.getRuntime().exec("cmd /c dir C:\\Users\\mkyong");
            //Run a bat file
            //Process process = Runtime.getRuntime().exec(
            //        "cmd /c hello.bat", null, new File("C:\\Users\\mkyong\\"));
            stdout = new String[files.length + 1];
            stderr = new String[files.length + 1];
            errorno = new int[files.length + 1];

            List<String> ld = new ArrayList<>();

            // Ensamblado todos los ficheros fuente
            for (int i = 0; i < files.length; i++) {
                String file = files[i];
                //System.err.println(file);
                String path = FilenameUtils.getFullPath(file);
                //System.err.println(path);
                //path = (file.charAt(0) == '/') ? SEP + path : path;
                String command[] = {binDirectory + SEP + asmBinary,
                        file,
                        asmArgs[0], asmArgs[1], asmArgs[2],
                        "-I", path,
                        "-o", tmpDirectory + SEP + FilenameUtils.getBaseName(file) + ".o"};
                //System.err.println(command);
                Process process = Runtime.getRuntime().exec(command);
                ld.add(tmpDirectory + SEP + FilenameUtils.getBaseName(file) + ".o");

                String out = new String();
                String err = new String();

                BufferedReader outReader = new BufferedReader(
                        new InputStreamReader(process.getInputStream()));

                BufferedReader errReader = new BufferedReader(
                        new InputStreamReader(process.getErrorStream()));

                String line;
                while ((line = outReader.readLine()) != null) {
                    out += (line + "\n");
                }
                while ((line = errReader.readLine()) != null) {
                    err += (line + "\n");
                }
                int errn = process.waitFor();

                stdout[i] = out;
                stderr[i] = err;
                errorno[i] = errn;

                if (errn != 0) {
                    printErrors(err);
                    return parseErrors(err, i);
                }
            }

            String[] command = new String[3+ld.size()];
            command[0] = binDirectory + SEP + linkerBinary;
            command[1] = "-o";
            command[2] = tmpDirectory + SEP + "elf";
            int i = 3;
            for (String st : ld) {
                command[i++] = st;
            }
            Process process = Runtime.getRuntime().exec(command);
//            Process process = Runtime.getRuntime().exec(
//                    binDirectory + SEP
//                    + linkerBinary + " -o "
//                    + tmpDirectory + SEP + "elf " + ld);
            String out = new String();
            String err = new String();

            BufferedReader outReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            BufferedReader errReader = new BufferedReader(
                    new InputStreamReader(process.getErrorStream()));

            String line;
            while ((line = outReader.readLine()) != null) {
                out += (line + "\n");
            }
            while ((line = errReader.readLine()) != null) {
                err += (line + "\n");
            }
            int errn = process.waitFor();

            stdout[files.length] = out;
            stderr[files.length] = err;
            errorno[files.length] = errn;

            if (errn != 0) {
                log.println(err);
                List<Integer> l = new ArrayList<>();
                l.add(-1);
                return l;
            }

            process = Runtime.getRuntime().exec(
                    binDirectory + SEP
                    + objDump + " -D "
                    + tmpDirectory + SEP + "elf ");
            elfDumpOut = new String();
            elfDumpErr = new String();

            outReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            errReader = new BufferedReader(
                    new InputStreamReader(process.getErrorStream()));

            while ((line = outReader.readLine()) != null) {
                elfDumpOut += (line + "\n");
            }
            while ((line = errReader.readLine()) != null) {
                elfDumpErr += (line + "\n");
            }
            if ((elfDumpNErr = process.waitFor()) != 0) {
                log.println(elfDumpErr);
                List<Integer> l = new ArrayList<>();
                l.add(-1);
                return l;
            }
            elfDumpOut = elfToTextAndData(elfDumpOut);

            ELFFormat elf;
            try {
                elf = new ELFFormat(tmpDirectory + SEP + "elf");
            } catch (Exception e) {
                log.println("Error in elf binary. " + e.getMessage());
                List<Integer> l = new ArrayList<>();
                l.add(-1);
                return l;
            }

            dataSegment = elf.data;
            textSegment = elf.text;
            dataOff = elf.dataOff;
            dataSize = elf.dataSize;
            textOff = elf.textOff;
            textSize = elf.textSize;
            entryPoint = elf.entryPoint;

        } catch (IOException | InterruptedException e) {
            log.println(e.getMessage());
            List<Integer> l = new ArrayList<>();
            l.add(-1);
            return l;
        }
        return null;
    }

    private List<Integer> parseErrors(String err, int fileIndex) {
        List<Integer> l = new ArrayList<>();
        l.add(fileIndex);
        Matcher m = Pattern.compile("(\\d+)[:]\\sError:").matcher(err);
        while (m.find()) {
            try {
                l.add(Integer.parseInt(err.substring(m.start(), err.indexOf(":", m.start()))));
            } catch (NumberFormatException e) {
                log.println("Error parsing stderr of assembler, please report error.", Color.RED);
            }
        }
        //log.println(l.toString());
        return l;
    }

    private void printErrors(String err) {
        int pos = 0;
        int next = err.indexOf("\n", pos);
        while (next > 0) {
            String line = err.substring(pos, next);
            Color color = (line.contains("Error:")) ? Color.RED : null;
            log.println(line, color);
            pos = next + 1;
            next = err.indexOf("\n", pos);
        }
        if (pos < err.length()) {
            String line = err.substring(pos);
            Color color = (line.contains("Error:")) ? Color.RED : null;
            log.println(line, color);
        }
    }

    private String trim(String str) {
        return trim(str, false);
    }

    private String trim(String str, boolean disassm) {
        // Eliminamos lineas en blanco
        String out = str.replaceAll("(?m)^[ \t]*\r?\n", "");
        //String[] lines = out.split(System.getProperty("line.separator"));
        String[] lines = out.split("\r?\n");
        out = "";
        Pattern pt = Pattern.compile("^\\s*[\\da-fA-F]+:\\s*[\\da-fA-F]+\\s");
        for (String line : lines) {
            if (line.contains("elf:") && line.contains("file format")) {
                continue;
            }
            if (line.contains("Disassembly of section")) {
                continue;
            }
            if (disassm) {
                Matcher m = pt.matcher(line);
                if (m.find()) {
                    line = line.substring(0, m.end());
                }
            }
            out += line + System.getProperty("line.separator");
        }
        return out;
    }

    private String formatAddr(String line) {
        Pattern pt = Pattern.compile("^\\s*[0-9a-fA-F]+ ");
        Matcher m = pt.matcher(line);
        if (m.find()) {
            return line.substring(0, m.end() - 1) + ":" + line.substring(m.end() - 1);
        }
        return line;
    }

    private String elfToTextAndData(String elf) {
        int dataSegmentBegin = elf.indexOf("Disassembly of section .data:");
        int textSegmentBegin = elf.indexOf("Disassembly of section .text:");
        int dataSegmentEnd = elf.length();
        int textSegmentEnd = elf.length();
        int actualSegment = 0; // 1 data, 2 text
        int pos = -1;
        while ((pos = elf.indexOf("Disassembly of section", pos + 1)) >= 0) {
            if (pos == dataSegmentBegin) {
                if (actualSegment == 2) {
                    textSegmentEnd = pos;
                }
                actualSegment = 1;
            } else if (pos == textSegmentBegin) {
                if (actualSegment == 1) {
                    dataSegmentEnd = pos;
                }
                actualSegment = 2;
            } else if (actualSegment == 1) {
                dataSegmentEnd = pos;
                actualSegment = 0;
            } else if (actualSegment == 2) {
                textSegmentEnd = pos;
                actualSegment = 0;
            }
        }
        String textSegment = (textSegmentBegin >= 0)
                ? trim(elf.substring(textSegmentBegin, textSegmentEnd)) : "";
        String dataSegment = (dataSegmentBegin >= 0)
                ? trim(elf.substring(dataSegmentBegin, dataSegmentEnd), true) : "";
        return textSegment + "\n" + dataSegment;
    }
}
