/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import java.io.File;
import net.fornwall.jelf.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author corbera
 */
public class ELFFormat {

    public long dataOff = 0;
    public long dataSize = 0;
    public long textOff = 0;
    public long textSize = 0;
    public long entryPoint = 0;
    public byte[] data = null;
    public byte[] text = null;

    public ELFFormat(String filename) throws Exception {
        File file = new File(filename);
        byte[] bytes = Files.readAllBytes(Paths.get(filename));

        ElfFile elfFile = ElfFile.fromFile(file);
        entryPoint = elfFile.entry_point;
        for (int i = 0; i < elfFile.num_sh; i++) {
            ElfSection sh = elfFile.getSection(i);
            if (sh.getName() != null) {
                switch (sh.getName().trim().toUpperCase()) {
                    case ".TEXT": {
                        textOff = sh.address;
                        textSize = sh.size;
                        text = new byte[(int) sh.size];
                        System.arraycopy(bytes, (int) sh.section_offset, text, 0, (int) textSize);
                        break;
                    }
                    case ".DATA": {
                        dataOff = sh.address;
                        dataSize = sh.size;
                        data = new byte[(int) sh.size];
                        System.arraycopy(bytes, (int) sh.section_offset, data, 0, (int) dataSize);
                        break;
                    }
                }
            }
        }
        if (text == null) {
            throw new Exception("Text segment not found");
        }
    }
}
//    //ELF header (32 bits)		
//    //Field	Offset	Size (bytes)	Purpose
//    //e_ident_EI_MAG0	0x00	4	0x7F followed by ELF(45 4c 46) in ASCII; these four bytes constitute the magic number.
//    //e_ident_EI_CLASS	0x04	1	This byte is set to either 1 or 2 to signify 32- or 64-bit format, respectively.
//    //e_ident_EI_DATA	0x05	1	This byte is set to either 1 or 2 to signify little or big endianness, respectively. This affects interpretation of multi-byte fields starting with offset 0x10.
//    //e_ident_EI_VERSION	0x06	1	Set to 1 for the original and current version of ELF.
//    //e_ident_EI_OSABI	0x07	1	Identifies the target operating system ABI.
//    //e_ident_EI_ABIVERSION	0x08	1	Further specifies the ABI version. Its interpretation depends on the target ABI. Linux kernel (after at least 2.6) has no definition of it._5 In that case, offset and size of EI_PAD are 8.
//    //e_ident_EI_PAD	0x09	7	currently unused
//    //e_type	0x10	2	
//    //e_machine	0x12	2	
//    //e_version	0x14	4	Set to 1 for the original version of ELF.
//    //e_entry	0x18	4	This is the memory address of the entry point from where the process starts executing. This field is either 32 or 64 bits long depending on the format defined earlier.
//    //e_phoff	0x1C	4	Points to the start of the program header table. It usually follows the file header immediately, making the offset 0x34 or 0x40 for 32- and 64-bit ELF executables, respectively.
//    //e_shoff	0x20	4	Points to the start of the section header table.
//    //e_flags	0x24	4	Interpretation of this field depends on the target architecture.
//    //e_ehsize	0x28	2	Contains the size of this header, normally 64 Bytes for 64-bit and 52 Bytes for 32-bit format.
//    //e_phentsize	0x2A	2	Contains the size of a program header table entry.
//    //e_phnum	0x2C	2	Contains the number of entries in the program header table.
//    //e_shentsize	0x2E	2	Contains the size of a section header table entry.
//    //e_shnum	0x30	2	Contains the number of entries in the section header table.
//    //e_shstrndx	0x32	2	Contains index of the section header table entry that contains the section names.
//    //
//    //Program header		
//    //Field	Offset	Size (bytes)	Purpose
//    //p_type	0x00	4	Identifies the type of the segment.
//    //p_flags	0x04	4	Segment-dependent flags (position for 64-bit structure).
//    //p_offset	0x04	4	Offset of the segment in the file image.
//    //p_vaddr	0x08	4	Virtual address of the segment in memory.
//    //p_paddr	0x0C	4	On systems where physical address is relevant, reserved for segment's physical address.
//    //p_filesz	0x10	4	Size in bytes of the segment in the file image. May be 0.
//    //p_memsz	0x14	4	Size in bytes of the segment in memory. May be 0.
//    //p_flags	0x18	4	Segment-dependent flags (position for 32-bit structure).
//    //p_align	0x1C	4	0 and 1 specify no alignment. Otherwise should be a positive, integral power of 2, with p_vaddr equating p_offset modulus p_align.
//    //End of Program Header (size)	0x20		
//    //	
//    //Section header		
//    //Field	Offset	Size (bytes)	Purpose
//    //sh_name	0x00	4	An offset to a string in the .shstrtab section that represents the name of this section
//    //sh_type	0x04	4	Identifies the type of this header.
//    //sh_flags	0x08	4	Identifies the attributes of the section.
//    //sh_addr	0x0C	4	Virtual address of the section in memory, for sections that are loaded.
//    //sh_offset	0x10	4	Offset of the section in the file image.
//    //sh_size	0x14	4	Size in bytes of the section in the file image. May be 0.
//    //sh_link	0x18	4	Contains the section index of an associated section. This field is used for several purposes, depending on the type of section.
//    //sh_info	0x1C	4	Contains extra information about the section. This field is used for several purposes, depending on the type of section.
//    //sh_addralign	0x20	4	Contains the required alignment of the section. This field must be a power of two.
//    //sh_entsize	0x24	4	Contains the size, in bytes, of each entry, for sections that contain fixed-size entries. Otherwise, this field contains zero.
//    //End of Section Header (size)	0x28		
//
//    public static final int E_IDENT_EI_MAG_OFFSET = 4;
//    public static final int E_IDENT_EI_MAG_SIZE = 5;
//    public static final int E_IDENT_EI_CLASS_OFFSET = 6;
//    public static final int E_IDENT_EI_CLASS_SIZE = 7;
//    public static final int E_IDENT_EI_DATA_OFFSET = 8;
//    public static final int E_IDENT_EI_DATA_SIZE = 9;
//    public static final int E_IDENT_EI_VERSION_OFFSET = 10;
//    public static final int E_IDENT_EI_VERSION_SIZE = 11;
//    public static final int E_IDENT_EI_OSABI_OFFSET = 12;
//    public static final int E_IDENT_EI_OSABI_SIZE = 13;
//    public static final int E_IDENT_EI_ABIVERSION_OFFSET = 14;
//    public static final int E_IDENT_EI_ABIVERSION_SIZE = 15;
//    public static final int E_IDENT_EI_PAD_OFFSET = 16;
//    public static final int E_IDENT_EI_PAD_SIZE = 17;
//    public static final int E_TYPE_OFFSET = 18;
//    public static final int E_TYPE_SIZE = 19;
//    public static final int E_MACHINE_OFFSET = 20;
//    public static final int E_MACHINE_SIZE = 21;
//    public static final int E_VERSION_OFFSET = 22;
//    public static final int E_VERSION_SIZE = 23;
//    public static final int E_ENTRY_OFFSET = 24;
//    public static final int E_ENTRY_SIZE = 25;
//    public static final int E_PHOFF_OFFSET = 26;
//    public static final int E_PHOFF_SIZE = 27;
//    public static final int E_SHOFF_OFFSET = 28;
//    public static final int E_SHOFF_SIZE = 29;
//    public static final int E_FLAGS_OFFSET = 30;
//    public static final int E_FLAGS_SIZE = 31;
//    public static final int E_EHSIZE_OFFSET = 32;
//    public static final int E_EHSIZE_SIZE = 33;
//    public static final int E_PHENTSIZE_OFFSET = 34;
//    public static final int E_PHENTSIZE_SIZE = 35;
//    public static final int E_PHNUM_OFFSET = 36;
//    public static final int E_PHNUM_SIZE = 37;
//    public static final int E_SHENTSIZE_OFFSET = 38;
//    public static final int E_SHENTSIZE_SIZE = 39;
//    public static final int E_SHNUM_OFFSET = 40;
//    public static final int E_SHNUM_SIZE = 41;
//    public static final int E_SHSTRNDX_OFFSET = 42;
//    public static final int E_SHSTRNDX_SIZE = 43;
//
//    public static final int P_TYPE_OFFSET = 50;
//    public static final int P_TYPE_SIZE = 51;
//    public static final int P_OFFSET_OFFSET = 54;
//    public static final int P_OFFSET_SIZE = 55;
//    public static final int P_VADDR_OFFSET = 56;
//    public static final int P_VADDR_SIZE = 57;
//    public static final int P_PADDR_OFFSET = 58;
//    public static final int P_PADDR_SIZE = 59;
//    public static final int P_FILESZ_OFFSET = 60;
//    public static final int P_FILESZ_SIZE = 61;
//    public static final int P_MEMSZ_OFFSET = 62;
//    public static final int P_MEMSZ_SIZE = 63;
//    public static final int P_FLAGS_OFFSET = 64;
//    public static final int P_FLAGS_SIZE = 65;
//    public static final int P_ALIGN_OFFSET = 66;
//    public static final int P_ALIGN_SIZE = 67;
//
//    public static final int SH_NAME_OFFSET = 76;
//    public static final int SH_NAME_SIZE = 77;
//    public static final int SH_TYPE_OFFSET = 78;
//    public static final int SH_TYPE_SIZE = 79;
//    public static final int SH_FLAGS_OFFSET = 80;
//    public static final int SH_FLAGS_SIZE = 81;
//    public static final int SH_ADDR_OFFSET = 82;
//    public static final int SH_ADDR_SIZE = 83;
//    public static final int SH_OFFSET_OFFSET = 84;
//    public static final int SH_OFFSET_SIZE = 85;
//    public static final int SH_SIZE_OFFSET = 86;
//    public static final int SH_SIZE_SIZE = 87;
//    public static final int SH_LINK_OFFSET = 88;
//    public static final int SH_LINK_SIZE = 89;
//    public static final int SH_INFO_OFFSET = 90;
//    public static final int SH_INFO_SIZE = 91;
//    public static final int SH_ADDRALIGN_OFFSET = 92;
//    public static final int SH_ADDRALIGN_SIZE = 93;
//    public static final int SH_ENTSIZE_OFFSET = 94;
//    public static final int SH_ENTSIZE_SIZE = 95;
//
//    private Map<Integer, Integer> fields;
//    private boolean elfFormat = false;
//    private int textOff;
//    private int textSize;
//    private int dataOff;
//    private int dataSize;
//
//    public byte[] bytes;
//
//    public ELFFormat(String filename) throws Exception {
//
//        fields = new HashMap<>();
//
//        fields.put(E_IDENT_EI_MAG_OFFSET, 0x00);
//        fields.put(E_IDENT_EI_MAG_SIZE, 4);
//        fields.put(E_IDENT_EI_CLASS_OFFSET, 0x04);
//        fields.put(E_IDENT_EI_CLASS_SIZE, 1);
//        fields.put(E_IDENT_EI_DATA_OFFSET, 0x05);
//        fields.put(E_IDENT_EI_DATA_SIZE, 1);
//        fields.put(E_IDENT_EI_VERSION_OFFSET, 0x06);
//        fields.put(E_IDENT_EI_VERSION_SIZE, 1);
//        fields.put(E_IDENT_EI_OSABI_OFFSET, 0x07);
//        fields.put(E_IDENT_EI_OSABI_SIZE, 1);
//        fields.put(E_IDENT_EI_ABIVERSION_OFFSET, 0x08);
//        fields.put(E_IDENT_EI_ABIVERSION_SIZE, 1);
//        fields.put(E_IDENT_EI_PAD_OFFSET, 0x09);
//        fields.put(E_IDENT_EI_PAD_SIZE, 7);
//        fields.put(E_TYPE_OFFSET, 0x10);
//        fields.put(E_TYPE_SIZE, 2);
//        fields.put(E_MACHINE_OFFSET, 0x12);
//        fields.put(E_MACHINE_SIZE, 2);
//        fields.put(E_VERSION_OFFSET, 0x14);
//        fields.put(E_VERSION_SIZE, 4);
//        fields.put(E_ENTRY_OFFSET, 0x18);
//        fields.put(E_ENTRY_SIZE, 4);
//        fields.put(E_PHOFF_OFFSET, 0x1C);
//        fields.put(E_PHOFF_SIZE, 4);
//        fields.put(E_SHOFF_OFFSET, 0x20);
//        fields.put(E_SHOFF_SIZE, 4);
//        fields.put(E_FLAGS_OFFSET, 0x24);
//        fields.put(E_FLAGS_SIZE, 4);
//        fields.put(E_EHSIZE_OFFSET, 0x28);
//        fields.put(E_EHSIZE_SIZE, 2);
//        fields.put(E_PHENTSIZE_OFFSET, 0x2A);
//        fields.put(E_PHENTSIZE_SIZE, 2);
//        fields.put(E_PHNUM_OFFSET, 0x2C);
//        fields.put(E_PHNUM_SIZE, 2);
//        fields.put(E_SHENTSIZE_OFFSET, 0x2E);
//        fields.put(E_SHENTSIZE_SIZE, 2);
//        fields.put(E_SHNUM_OFFSET, 0x30);
//        fields.put(E_SHNUM_SIZE, 2);
//        fields.put(E_SHSTRNDX_OFFSET, 0x32);
//        fields.put(E_SHSTRNDX_SIZE, 2);
//
//        fields.put(P_TYPE_OFFSET, 0x00);
//        fields.put(P_TYPE_SIZE, 4);
//        fields.put(P_OFFSET_OFFSET, 0x04);
//        fields.put(P_OFFSET_SIZE, 4);
//        fields.put(P_VADDR_OFFSET, 0x08);
//        fields.put(P_VADDR_SIZE, 4);
//        fields.put(P_PADDR_OFFSET, 0x0C);
//        fields.put(P_PADDR_SIZE, 4);
//        fields.put(P_FILESZ_OFFSET, 0x10);
//        fields.put(P_FILESZ_SIZE, 4);
//        fields.put(P_MEMSZ_OFFSET, 0x14);
//        fields.put(P_MEMSZ_SIZE, 4);
//        fields.put(P_FLAGS_OFFSET, 0x18);
//        fields.put(P_FLAGS_SIZE, 4);
//        fields.put(P_ALIGN_OFFSET, 0x1C);
//        fields.put(P_ALIGN_SIZE, 4);
//
//        fields.put(SH_NAME_OFFSET, 0x00);
//        fields.put(SH_NAME_SIZE, 4);
//        fields.put(SH_TYPE_OFFSET, 0x04);
//        fields.put(SH_TYPE_SIZE, 4);
//        fields.put(SH_FLAGS_OFFSET, 0x08);
//        fields.put(SH_FLAGS_SIZE, 4);
//        fields.put(SH_ADDR_OFFSET, 0x0C);
//        fields.put(SH_ADDR_SIZE, 4);
//        fields.put(SH_OFFSET_OFFSET, 0x10);
//        fields.put(SH_OFFSET_SIZE, 4);
//        fields.put(SH_SIZE_OFFSET, 0x14);
//        fields.put(SH_SIZE_SIZE, 4);
//        fields.put(SH_LINK_OFFSET, 0x18);
//        fields.put(SH_LINK_SIZE, 4);
//        fields.put(SH_INFO_OFFSET, 0x1C);
//        fields.put(SH_INFO_SIZE, 4);
//        fields.put(SH_ADDRALIGN_OFFSET, 0x20);
//        fields.put(SH_ADDRALIGN_SIZE, 4);
//
//        bytes = Files.readAllBytes(Paths.get(filename));
//
//        if (bytes[0] != 0x7f || bytes[1] != 0x45 || bytes[2] != 0x4c || bytes[3] != 0x46) {
//            throw new notELFException();
//        }
//        elfFormat = true;
//        int offSectionHeader = getField(E_SHOFF_OFFSET);
//        int sizeEntrySectionHeader = getField(E_SHENTSIZE_OFFSET);
//        int numEntriesSectionHeader = getField(E_SHNUM_OFFSET);
//        boolean foundText = false, foundData = false;
//        
//        for (int i = 0; i < numEntriesSectionHeader; i++) {
//            int entryOff = offSectionHeader + sizeEntrySectionHeader * i;
//            int type = getData(entryOff + fields.get(SH_TYPE_OFFSET), fields.get(SH_TYPE_SIZE));
//            int flags = getData(entryOff + fields.get(SH_FLAGS_OFFSET), fields.get(SH_FLAGS_SIZE));
//            if (type == 1) {
//                if ((flags & 4) == 4) { // .text segment
//                    textOff = getData(entryOff + fields.get(SH_OFFSET_OFFSET), fields.get(SH_OFFSET_SIZE));
//                    textSize = getData(entryOff + fields.get(SH_SIZE_OFFSET), fields.get(SH_SIZE_SIZE));
//                    foundText = true;
//                } else { // .data segment
//                    dataOff = getData(entryOff + fields.get(SH_OFFSET_OFFSET), fields.get(SH_OFFSET_SIZE));
//                    dataSize = getData(entryOff + fields.get(SH_SIZE_OFFSET), fields.get(SH_SIZE_SIZE));                    
//                    foundData = true;
//                }
//            } 
//        }
//        
//        if (!foundData || !foundText)
//            throw new ELFSegmentNotFoundException();
//
////        System.err.println(elfFormat);
////        try {
////            System.out.println("32 bits " + Integer.toHexString(getField(E_IDENT_EI_CLASS_OFFSET)));
////            System.out.println("ABI " + Integer.toHexString(getField(E_IDENT_EI_OSABI_OFFSET)));
////            System.out.println("ISA " + Integer.toHexString(getField(E_MACHINE_OFFSET)));
////            System.out.println("Entry point " + Integer.toHexString(getField(E_ENTRY_OFFSET)));
////            System.out.println("Section Headers:\n" + getSectionsInformation());
////
////        } catch (Exception e) {
////            System.out.println("No se puede acceder a los campos");
////        }
//    }
//
//    public int getData(int offset, int size) throws Exception {
//        if (offset + size > bytes.length) {
//            throw new notELFException();
//        }
//        int val = 0x000000FF & bytes[offset];
//        if (size == 1) {
//            return val;
//        }
//        val |= 0x0000FF00 & (bytes[offset + 1] << 8);
//        if (size == 2) {
//            return val;
//        }
//        val |= 0xFFFF0000 & (bytes[offset + 2] << 16) | (bytes[offset + 3] << 24);
//        if (size == 4) {
//            return val;
//        }
//        throw new ELFWrongSizeException();
//    }
//
//    public int getField(int name) throws Exception {
//        if (!elfFormat) throw new notELFException();
//        
//        if (!fields.containsKey(name) || !fields.containsKey(name + 1)) {
//            throw new Exception();
//        }
//        int offset = fields.get(name);
//        int size = fields.get(name + 1);
//        return getData(offset, size);
//    }
//
//    public String getSectionsInformation() throws Exception {
//        if (!elfFormat) throw new notELFException();
//        
//        String str = new String();
//        int offSectionHeader = getField(E_SHOFF_OFFSET);
//        int sizeEntrySectionHeader = getField(E_SHENTSIZE_OFFSET);
//        int numEntriesSectionHeader = getField(E_SHNUM_OFFSET);
//
//        for (int i = 0; i < numEntriesSectionHeader; i++) {
//            int entryOff = offSectionHeader + sizeEntrySectionHeader * i;
//            str += "sh_type: "
//                    + Integer.toHexString(getData(entryOff + fields.get(SH_TYPE_OFFSET), fields.get(SH_TYPE_SIZE)))
//                    + " addr: "
//                    + Integer.toHexString(getData(entryOff + fields.get(SH_ADDR_OFFSET), fields.get(SH_ADDR_SIZE)))
//                    + " file off: "
//                    + Integer.toHexString(getData(entryOff + fields.get(SH_OFFSET_OFFSET), fields.get(SH_OFFSET_SIZE)))
//                    + " size: "
//                    + Integer.toHexString(getData(entryOff + fields.get(SH_SIZE_OFFSET), fields.get(SH_SIZE_SIZE)))
//                    + " flags: "
//                    + Integer.toHexString(getData(entryOff + fields.get(SH_FLAGS_OFFSET), fields.get(SH_FLAGS_SIZE)))
//                    + "\n";
//        }
//        return str;
//    }
//
//    public byte[] getDataSegment() throws Exception {
//        if (!elfFormat) throw new notELFException();
//        byte[] out = new byte[dataSize];
//        System.arraycopy(bytes, dataOff, out, 0, dataSize);
//        return out;
//    }
//    
//    public byte[] getTextSegment() throws Exception {
//        if (!elfFormat) throw new notELFException();
//        byte[] out = new byte[textSize];
//        System.arraycopy(bytes, textOff, out, 0, textSize);
//        return out;
//    }
//    
//   public class notELFException extends Exception {
//       public notELFException() {
//           super();
//       }
//       public notELFException(String str) {
//           super(str);
//       }
//   } 
//   
//   public class ELFWrongSizeException extends Exception {
//       public ELFWrongSizeException() {
//           super();
//       }
//       public ELFWrongSizeException(String str) {
//           super(str);
//       }
//   } 
//   
//   public class ELFSegmentNotFoundException extends Exception {
//       public ELFSegmentNotFoundException() {
//           super();
//       }
//       public ELFSegmentNotFoundException(String str) {
//           super(str);
//       }
//   } 
//   
//}
