/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.plugins.interfaces.SimulatorInterface;
import unicorn.Unicorn;

/**
 *
 * @author corbera
 */
public class RegisterFile {

    private static final int NREGS = 17;
    private Register[] registers = new Register[NREGS];
    private Unicorn simulator;

    public RegisterFile(Unicorn sim) {
        simulator = sim;
        registers[0] = new Register("R0", simulator.UC_ARM_REG_R0, simulator);
        registers[1] = new Register("R1", simulator.UC_ARM_REG_R1, simulator);
        registers[2] = new Register("R2", simulator.UC_ARM_REG_R2, simulator);
        registers[3] = new Register("R3", simulator.UC_ARM_REG_R3, simulator);
        registers[4] = new Register("R4", simulator.UC_ARM_REG_R4, simulator);
        registers[5] = new Register("R5", simulator.UC_ARM_REG_R5, simulator);
        registers[6] = new Register("R6", simulator.UC_ARM_REG_R6, simulator);
        registers[7] = new Register("R7", simulator.UC_ARM_REG_R7, simulator);
        registers[8] = new Register("R8", simulator.UC_ARM_REG_R8, simulator);
        registers[9] = new Register("R9", simulator.UC_ARM_REG_R9, simulator);
        registers[10] = new Register("R10", simulator.UC_ARM_REG_R10, simulator);
        registers[11] = new Register("R11", simulator.UC_ARM_REG_R11, simulator);
        registers[12] = new Register("R12", simulator.UC_ARM_REG_R12, simulator);
        registers[13] = new Register("SP", simulator.UC_ARM_REG_SP, simulator);
        registers[14] = new Register("LR", simulator.UC_ARM_REG_LR, simulator);
        registers[15] = new Register("PC", simulator.UC_ARM_REG_PC, simulator);
        registers[16] = new Register("CPSR", simulator.UC_ARM_REG_CPSR, simulator);
//        registers[17] = new Register("SPSR", simulator.UC_ARM_REG_SPSR, simulator);
    }

    public int getRegNumber(String r) {
        int num = -1;
        r = r.trim().toUpperCase();
        switch (r) {
            case "CPSR":
                return 16;
            case "SPSR":
                return 17;
            case "PC":
                return 15;
            case "LR":
                return 14;
            case "SP":
                return 13;
            case "IP":
                return 12;
            case "FP":
                return 11;
            default:
                if (r.charAt(0) == 'R') {
                    r = r.substring(1);
                }
                try {
                    num = Integer.parseInt(r);
                } catch (Exception e) {
                    num = -1;
                }
                return num;
        }
    }

    public void reset() {
        int[] modos = {0x10, 0x11, 0x12, 0x13, 0x17, 0x1f};
        for (int modo : modos) {
            simulator.reg_write(Unicorn.UC_ARM_REG_CPSR, modo);
            for (Register reg : registers) {
                reg.reset();
            }
        }
    }

    public void clearUpdate() {
        for (Register reg : registers) {
            reg.clearUpdate();
        }

    }

    public int readRegister(int reg) throws Exception {
        if (reg >= 0 && reg < NREGS) {
            return registers[reg].getValue();
        }
        throw new Exception("readRegister: Bad register number");
    }

    public void writeRegister(int reg, int val) throws Exception {
        if (reg >= 0 && reg < NREGS) {
            registers[reg].setValue(val);
            return;
        }
        throw new Exception("writeRegister: Bad register number");
    }

    public Register[] getRegisters() {
        return registers;
    }

    public Register getPC() {
        return registers[SimulatorInterface.REGISTER_PC];
    }

    public Register getCPSR() {
        return registers[SimulatorInterface.REGISTER_CPSR];
    }
}
