/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import guacarm.GUI.ColorTheme;
import guacarm.GenericCallBack;
import guacarm.log.ConsoleLog;
import guacarm.log.Log;
import guacarm.Utils;
import guacarm.interrupt.Interrupts;
import guacarm.interrupt.SubsPCLR;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import unicorn.CodeHook;
import unicorn.Hook;
import unicorn.InterruptHook;
import unicorn.ReadHook;
import unicorn.Unicorn;
import unicorn.UnicornException;
import unicorn.WriteHook;

/**
 *
 * @author Corbera
 */
public class Simulator {
    // simulador

    protected Unicorn simulator;

    private Breakpoints breakpoints;
    private long breakpoint;

    // configuracion de memoria
    private long codeBaseAddr = 0x10000;
    private long codeSize = 0x30000;
    private long dataBaseAddr = 0x00000;
    private long dataSize = 0x08000;
    private long stackBaseAddr = 0x08000;
    private long stackSize = 0x08000;
    private long entryPoint;
    private long endPC;
    private int cpsrInitial;

    private long memBaseAddr;
    private long memSize;

    private byte[] code;
    private byte[] data;
    private RegisterFile registers;

    public Log log;
    private boolean verbose = false;
    private boolean initialized = false;

    public SimulatorHooks services;
    public SimulatorApi api;

    public List<Integer> listSubsPCLRInstructions;
    private final List<GenericCallBack> onCodeChange = new ArrayList<>();
    public final Interrupts interruptions;
    public final MemoryManager memoryManager;

    public boolean abort = false;
    public boolean endOfSimulation = false;

    public Simulator() {
        this(new ConsoleLog());
    }

    public Simulator(Log l) {
        log = l;
        // Initialize emulator in ARM mode
        simulator = new Unicorn(Unicorn.UC_ARCH_ARM, Unicorn.UC_MODE_ARM);
        services = new SimulatorHooks(this);
        // Inicalizamos el banco de registros
        registers = new RegisterFile(simulator);
        breakpoints = new Breakpoints(this);
        breakpoint = -1;
        api = new SimulatorApi(this);
        cpsrInitial = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_CPSR)).intValue();
        interruptions = new Interrupts(this, log);
        listSubsPCLRInstructions = new ArrayList<>();
        memoryManager = new MemoryManager(simulator);
        abort = false;
        endOfSimulation = false;
    }

//    /**
//     * Inicializa un simulador ARM
//     *
//     * @param code segmento de codigo binario ARM a simular
//     * @param data segmento de datos
//     * @param entryPoint direccion de la primera instruccion del codigo
//     * @param codeOff direccion donde situar el segmento de codigo
//     * @param dataOff direccion donde situar el segmento de datos
//     * @param stSize tamaño del segmento de pila
//     */
//    public Simulator(byte[] code, byte[] data, long entryPoint,
//            long codeOff, long dataOff, long stSize) {
//        this(code, data, entryPoint, codeOff, dataOff, stSize, new ConsoleLog());
//    }
//
//    /**
//     * Inicializa un simulador ARM
//     *
//     * @param code segmento de codigo binario ARM a simular
//     * @param data segmento de datos
//     * @param entryPoint direccion de la primera instruccion del codigo
//     * @param codeOff direccion donde situar el segmento de codigo
//     * @param dataOff direccion donde situar el segmento de datos
//     * @param stSize tamaño del segmento de pila
//     * @param l log para escribir los mensajes de aviso
//     */
//    public Simulator(byte[] code, byte[] data, long entryPoint,
//            long codeOff, long dataOff, long stSize, Log l) {
//        log = l;
//        simulator = new Unicorn(Unicorn.UC_ARCH_ARM, Unicorn.UC_MODE_ARM);
//        services = new SimulatorHooks(this);
//        registers = new RegisterFile(simulator);
//        api = new SimulatorApi(this);
//        initialize(code, data, entryPoint, codeOff, dataOff, stSize);
//    }
    /**
     * Inicializa un simulador ARM
     *
     * @param code segmento de codigo binario ARM a simular
     * @param data segmento de datos
     * @param entryPoint direccion de la primera instruccion del codigo
     * @param codeOff direccion donde situar el segmento de codigo
     * @param dataOff direccion donde situar el segmento de datos
     * @param stSize tamaño del segmento de pila
     */
    public void initialize(byte[] code, byte[] data, long entryPoint,
            long codeOff, long dataOff, long stSize) {

        this.entryPoint = entryPoint;

        registers.reset();
        memoryManager.reset();

        codeBaseAddr = codeOff;
        codeSize = (long) code.length;
        dataBaseAddr = dataOff;
        dataSize = (data != null) ? data.length : 0;
        stackBaseAddr = (codeBaseAddr > dataBaseAddr)
                ? codeBaseAddr + addrAlignTop((int) codeSize)
                : dataBaseAddr + addrAlignTop((int) dataSize);
        stackSize = addrAlignTop((int) stSize);

        memBaseAddr = (codeBaseAddr > dataBaseAddr && dataBaseAddr != 0) ? dataBaseAddr : codeBaseAddr;
        memSize = (long) Math.floor((stackBaseAddr + stackSize) / 1024.0) * 1024;

        this.code = code;
        this.data = data;
        memoryManager.mem_write(codeBaseAddr, code);
        if (data != null) {
            memoryManager.mem_write(dataBaseAddr, data);
        }

        simulator.reg_write(Unicorn.UC_ARM_REG_PC, this.entryPoint);
        simulator.reg_write(Unicorn.UC_ARM_REG_SP, stackBaseAddr + stackSize);
        endPC = stackBaseAddr + stackSize + 4;
        simulator.reg_write(Unicorn.UC_ARM_REG_LR, endPC);

        listSubsPCLRInstructions = SubsPCLR.findSubsPCLR(code, (int) codeBaseAddr);
        onCodeChange.forEach((callback) -> {
            callback.callBack(null);
        });
        abort = false;
        endOfSimulation = false;
        initialized = true;
        
        interruptions.reset();
        
        //printSimulatorConfig();
    }

    /**
     * Registra un callback que será invocado cuando se poduzca un cambio en el
     * codigo asociado al simulador
     *
     * @param callback
     */
    public void registerCallbackOnCodeChange(GenericCallBack callback) {
        onCodeChange.add(callback);
    }

    /**
     * Elimina un callback registrado previamente
     *
     * @param callback
     */
    public void unRegisterCallbackOnCodeChange(GenericCallBack callback) {
        onCodeChange.remove(callback);
    }

    /**
     * Establece el contenido del segmento de codigo.
     *
     * @param code
     * @return
     */
    public boolean setCode(byte[] code) {
        if (code.length > codeSize) {
            return false;
        }
        this.code = code;
        memoryManager.mem_write(codeBaseAddr, code);
        listSubsPCLRInstructions = SubsPCLR.findSubsPCLR(code, (int) codeBaseAddr);
        onCodeChange.forEach((callback) -> {
            callback.callBack(null);
        });
        return true;
    }

    /**
     * Establece el contenido del segmento de datos.
     *
     * @param data
     * @return
     */
    public boolean setData(byte[] data) {
        if (data.length > dataSize) {
            return false;
        }
        this.data = data;
        memoryManager.mem_write(dataBaseAddr, data);
        return true;
    }

    /**
     * Simula la ejecucion de nInstructions
     *
     * @param nInstructions Numero de instrucciones a ejecutar
     * @return valor del nuevo PC
     */
    public int step(int nInstructions) {
        return step(nInstructions, log);
    }

    public int step(int nInstructions, Log auxlog) {
        if (auxlog == null) {
            auxlog = log;
        }
        if (!initialized || code == null) {
            return -1;
        }

        //checkInterruptions();
        int PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
        breakpoints.cleanReached(PC);
        if (verbose) {
            auxlog.println("PC: 0x" + Integer.toHexString(PC));
        }
        if (PC == endPC) {
            return -1;
        }
        try {
            //System.out.println(Integer.toHexString(PC));
            simulator.emu_start(PC, 0, 0, nInstructions);
        } catch (UnicornException e) {
            PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
            auxlog.println("Error simulating instruction 0x"
                    + Integer.toHexString(PC - 4)
                    + ": " + e.getMessage(), ColorTheme.WARNING);
            return -1;
        }
        if (abort) {
            return -1;
        }
        checkInterruptions();

        if (endOfSimulation) {
            simulator.reg_write(Unicorn.UC_ARM_REG_PC, endPC);
            return (int) endPC;
        }

        PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
        return PC;
    }

    public int step() {
        return step(1);
    }

    /**
     * Simula la ejecucion del programa hasta que termina o ejecuta durante
     * timeout ms
     *
     * @param timeout tiempo limite de simulacion en microsegundos
     * @return
     */
    public int run(int timeout) {
        return run(timeout, endPC);
    }

    /**
     * Simula la ejecucion del programa hasta que termina o ejecuta durante
     * timeout ms
     *
     * @param timeout tiempo limite de simulacion en microsegundos
     * @param until direccion de la ultima instruccion a ejecutar
     * @return
     */
    public int run(int timeout, long until) {
        if (!initialized || code == null) {
            return -1;
        }

        //checkInterruptions();
        int PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
        breakpoints.cleanReached(PC);
        try {
//            log.println("Simulating PC: " + Integer.toHexString(PC)
//                    + " until: " + Integer.toHexString((int) until)
//                    + " timeout: " + timeout);
            simulator.emu_start(PC, until, timeout, 0);
        } catch (UnicornException e) {
            PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
            log.println("Error simulating instruction 0x"
                    + Integer.toHexString(PC - 4)
                    + ": " + e.getMessage(), ColorTheme.WARNING);
            return -1;
        }
        if (abort) {
            return -1;
        }
        PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
        if ((breakpoint = breakpoints.isReached()) != -1) {
            PC = ((Long) breakpoint).intValue();
            simulator.reg_write(Unicorn.UC_ARM_REG_PC, breakpoint);
        } else {
            checkInterruptions();
        }

        if (endOfSimulation) {
            simulator.reg_write(Unicorn.UC_ARM_REG_PC, endPC);
            return (int) endPC;
        }

        PC = ((Long) simulator.reg_read(Unicorn.UC_ARM_REG_PC)).intValue();
//        log.println("Simulating PC: " + Integer.toHexString(PC)
//                + " until: " + Integer.toHexString((int) until)
//                + " timeout: " + timeout);
        return PC;
    }

    public void reset() {
        if (!initialized) {
            return;
        }
        abort = false;
        endOfSimulation = false;
        registers.reset();
        memoryManager.reset();

        memoryManager.mem_write(codeBaseAddr, code);
        if (data != null) {
            memoryManager.mem_write(dataBaseAddr, data);
        }

        simulator.reg_write(Unicorn.UC_ARM_REG_CPSR, cpsrInitial);
        simulator.reg_write(Unicorn.UC_ARM_REG_PC, entryPoint);
        simulator.reg_write(Unicorn.UC_ARM_REG_SP, stackBaseAddr + stackSize);
        simulator.reg_write(Unicorn.UC_ARM_REG_LR, endPC);
        breakpoints.reset();
        
        interruptions.reset();
    }

    /**
     * Establede la direccion base del segmento de codigo
     *
     * @param codeBasseAddr
     */
    public void setCodeBasseAddr(long codeBasseAddr) {
        this.codeBaseAddr = codeBasseAddr & 0xFFFFFFFC;
        listSubsPCLRInstructions = SubsPCLR.findSubsPCLR(code, (int) codeBaseAddr);
        onCodeChange.forEach((callback) -> {
            callback.callBack(null);
        });
    }

    /**
     * Establece el tamaño del segmento de codigo
     *
     * @param codeSize
     */
    public void setCodeSize(long codeSize) {
        this.codeSize = codeSize & 0xFFFFFFFC;
    }

    /**
     * Establece la direccion base del segmento de datos
     *
     * @param dataBaseAddr
     */
    public void setDataBaseAddr(long dataBaseAddr) {
        this.dataBaseAddr = dataBaseAddr & 0xFFFFFFFC;
    }

    /**
     * Establece el tamaño del segemento de datos
     *
     * @param dataSize
     */
    public void setDataSize(long dataSize) {
        this.dataSize = dataSize & 0xFFFFFFFC;
    }

    /**
     * Establece la direccion base del segmento de pila
     *
     * @param stackBaseAddr
     */
    public void setStackBaseAddr(long stackBaseAddr) {
        this.stackBaseAddr = stackBaseAddr & 0xFFFFFFFC;
    }

    /**
     * Establece el tamaño del segmento de pila
     *
     * @param stackSize
     */
    public void setStackSize(long stackSize) {
        this.stackSize = stackSize & 0xFFFFFFFC;
    }

    /**
     * Establece la direccion de inicio de ejecucion
     *
     * @param entry
     */
    public void setEntryPoint(long entry) {
        this.entryPoint = entry;
    }

    /**
     * Get the register file
     *
     * @return
     */
    public RegisterFile getRegisterFile() {
        return registers;
    }

    /**
     * Lee el contenido de memoria especidicado por offset y size
     *
     * @param offset
     * @param size
     * @return
     * @throws java.lang.Exception
     */
    public byte[] readMem(long offset, int size) throws Exception {
        return memoryManager.mem_read(offset, size);
//        int minsize = Math.max(1024, (size / 1024) * 1024);
//        if (minsize != size) {
//            //byte[] m = simulator.mem_read(offset, minsize);
//            byte[] m = memoryManager.mem_read(offset, minsize);
//            byte[] r = new byte[size];
//            System.arraycopy(m, 0, r, 0, size);
//            return r;
//        }
//        return simulator.mem_read(offset, minsize);
    }

    /**
     * Escribe en la posicion de memoria offset los bytes indicados por size
     * leidos de buff
     *
     * @param offset posicion de memoria donde escribir
     * @param buff datos a escribir
     * @param size numero de bytes a escribir
     * @throws java.lang.Exception
     */
    public void writeMem(int offset, byte[] buff, int size) throws Exception {
        memoryManager.mem_write(offset, buff, size);
//        int minsize = Math.max(1024, (size / 1024) * 1024);
//        if (minsize != size) {
//            byte m[] = simulator.mem_read(offset, minsize);
//            System.arraycopy(buff, 0, m, 0, size);
//            simulator.mem_write(offset, m);
//        } else {
//            simulator.mem_write(offset, buff);
//        }
    }

    /**
     * Lee el contenido del registro indicado
     *
     * @param reg registro a leer
     * @return valor almacenado en el registro
     * @throws Exception si no se puede leer el registro
     */
    public int readRegister(int reg) throws Exception {
        return registers.readRegister(reg);
    }

    /**
     * Escribe un valor en un registro
     *
     * @param reg registro a escribir
     * @param val valor a escribir
     * @throws Exception si no se puede escribir en el registro
     */
    public void writeRegister(int reg, int val) throws Exception {
        registers.writeRegister(reg, val);
    }

    /**
     * Activa(true)/Desactiva(false) el modo verbose
     *
     * @param v
     */
    public void setVerboseMode(boolean v) {
        verbose = v;
    }

    /**
     * Devuelve el contenido del registro PC
     *
     * @return
     */
    public int getPC() {
        int val = 0;
        try {
            val = registers.readRegister(15);
        } catch (Exception ex) {
            Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return val;
    }

    /**
     * Devuelve el valor del PC al final de la ejecucion
     *
     * @return
     */
    public int getEndPC() {
        return (int) endPC;
    }

    /**
     * Dice si la instrucción de la posicion que indica PC es una instruccion BL
     *
     * @return
     * @throws java.lang.Exception
     */
    public boolean isNextInstructionBL() throws Exception {
        byte[] inst = readMem(getPC(), 4);
        List<String> insts = Utils.bytesToFormatStrings(inst, 0, 4);
        return insts.get(0).charAt(1) == 'B';
    }

    /**
     * Devuelve la direccion base del segmento .data
     *
     * @return direccion de .data
     */
    public int getDataBaseAddr() {
        return (int) dataBaseAddr;
    }

    /**
     * Devuelve la direccion base del segmento .text
     *
     * @return direccion de .text
     */
    public int getCodeBaseAddr() {
        return (int) codeBaseAddr;
    }

    /**
     * Devuelve la direccion base del segmento de pila
     *
     * @return direccion del segmento de pila
     */
    public int getStackBaseAddr() {
        return (int) stackBaseAddr;
    }

    /**
     * Alinea a multiplo de 4 la direccion addr por encima
     *
     * @param addr
     * @return
     */
    private int addrAlignTop(int addr) {
        int off = ((addr & 3) != 0) ? 4 : 0;
        return (addr & ~(3)) + off;
    }

    /**
     * Imprime los datos de configuracion de memoria y puntos de entrada y
     * salida
     */
    private void printSimulatorConfig() {
        log.println("Data: " + dataBaseAddr + " - " + dataSize);
        log.println("Code: " + codeBaseAddr + " - " + codeSize);
        log.println("Stack: " + stackBaseAddr + " - " + stackSize);
        log.println("Memory: " + memBaseAddr + " - " + memSize);
        log.println("Entry point: " + entryPoint);
        log.println("EndPC: " + endPC);
    }

    /**
     * Devuelve la instancia del simulador unicorn usada para la simulacion
     *
     * @return
     */
    public Unicorn getUnicornInstance() {
        return simulator;
    }

    /**
     * Establece un BREAKPOINT en la direccion especificada
     *
     * @param addr direccion del BREAKPOINT
     */
    public void setBreakpoint(long addr) {
        breakpoints.setBreakpoint(addr);
    }

    /**
     * Elimina un BREAKPOINT de la direccion especificada
     *
     * @param addr direccion del BREAKPOINT
     */
    public void unsetBreakpoint(long addr) {
        breakpoints.unsetBreakpoint(addr);
    }

    /**
     * Devuelve true si se a parado la simulacion por un BREAKPOINT
     *
     * @return
     */
    public boolean isBreakpoint() {
        return breakpoint != -1;
    }

    /**
     * Resetea el flag de "se ha alcanzado un BREAKPOINT".
     */
    public void cleanBreakpoint() {
        breakpoint = -1;
    }

    /**
     * Devuelve el flag de BREAKPOINT.
     *
     * @return
     */
    public long getBreakpointAddr() {
        return breakpoint;
    }

    /**
     * Registra un Hook que sera llamado cuando se ejecute una instruccion
     * svc/swi
     *
     * @param hook hook que se llamara
     * @param o objeto que se le pasara al hook
     */
    public void hook_add(InterruptHook hook, Object o) {
        simulator.hook_add(hook, o);
    }

    /**
     * Registra un Hook que sera llamado cuando se lea una posicion de memoria
     * de un rango
     *
     * @param hook hook que se llamara
     * @param offset direccion base del rango de direcciones
     * @param size tamaño del rango de direcciones
     * @param o objeto que se le pasara al hook
     */
    public void hook_add(ReadHook hook, long offset, long size, Object o) {
        simulator.hook_add(hook, offset, size, o);
    }

    /**
     * Registra un Hook que sera llamado cuando se escriba una posicion de
     * memoria de un rango
     *
     * @param hook hook que se llamara
     * @param offset direccion base del rango de direcciones
     * @param size tamaño del rango de direcciones
     * @param o objeto que se le pasara al hook
     */
    public void hook_add(WriteHook hook, long offset, long size, Object o) {
        simulator.hook_add(hook, offset, size, o);

    }

    /**
     * Registra un Hook que sera llamado cuando se ejecute la instruccion de la
     * posicion de memoria indicada
     *
     * @param hook hook que se llamara
     * @param offset direccion de la instruccion
     * @param size tamaño de la instrucción
     * @param o objeto que se le pasara al hook
     */
    public void hook_add(CodeHook hook, long offset, long size, Object o) {
        simulator.hook_add(hook, offset, size, o);
    }

    /**
     * Elimina un hook del simulador
     *
     * @param hook hook a eliminar
     * @return true si lo puede eliminar
     */
    public boolean hook_del(Hook hook) {
        try {
            simulator.hook_del(hook);
        } catch (UnicornException e) {
            return false;
        }
        return true;
    }

    /**
     * Escribe un bloque de datos en memoria
     *
     * @param offset direccion de comienzo de la memoria a escribir
     * @param data datos a escribir
     */
    public void memWrite(long offset, byte[] data) {
        try {
            memoryManager.mem_write((int) offset, data, data.length);
        } catch (Exception ex) {
            Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Lee un bloque de datos de la memoria
     *
     * @param offset direccion de memoria del bloque a leer
     * @param size tamaño del bloque
     * @return datos leidos de la memoria
     */
    public byte[] memRead(long offset, long size) {
        try {
            return memoryManager.mem_read((int) offset, (int) size);
        } catch (Exception ex) {
            Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Chequea si hay interrupciones pendientes y las lanza
     */
    private void checkInterruptions() {
        List<Interrupts.InterType> pending = interruptions.getPending();
        pending.forEach((type) -> {
            interruptions.throwInterruption(type);
        });
    }

    public void printAllRegisters() {
        for (int reg = 0; reg < Unicorn.UC_ARM_REG_ENDING; reg++) {
            if (((Long) simulator.reg_read(reg)).intValue() == 0x11) {
                System.out.println("Registro " + reg + ": " + "0x11");
            }
        }
    }

    public void endOfSimulation() {
        endOfSimulation = true;
        simulator.emu_stop();
    }

    public void abortSimulation() {
        abort = true;
        simulator.emu_stop();
        simulator.reg_write(Unicorn.UC_ARM_REG_PC, -1);
    }
}
