/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import java.util.HashMap;
import java.util.Map;
import unicorn.CodeHook;
import unicorn.Unicorn;

/**
 *
 * @author corbera
 */
public class Breakpoints {

    private Simulator simulator;
    private static long reached;
    private Map<Long, BreakpointHook> hooks;
    private static int initialAddr;

    public Breakpoints(Simulator sim) {
        simulator = sim;
        reached = -1;
        hooks = new HashMap<>();
        initialAddr = -1;
    }

    /**
     * callback for tracing instruction
     */
    private class BreakpointHook implements CodeHook {

        @Override
        public void hook(Unicorn u, long address, int size, Object user_data) {
            //System.err.println(Integer.toHexString((int)address)+" "+Long.toHexString((Long)user_data));
            //System.err.println("---" + Long.toHexString((Long)u.reg_read(u.UC_ARM_REG_PC)));
            if (hooks.containsKey(address) && address == (long)user_data) {
                if (address != initialAddr) {
                    u.emu_stop();
                    //u.reg_write(u.UC_ARM_REG_PC, address);
                    reached = address;
                    //System.out.println("Breakpoint reached at address 0x" + Integer.toHexString((int) address));
                } else {
                    initialAddr = -1;
                }
            }
        }
    }

    /**
     * Establece un breakpoint en la direccion indicada
     *
     * @param addr direccion para poner el breakpoint
     */
    //BreakpointHook bh = new BreakpointHook();
    public void setBreakpoint(long addr) {
        BreakpointHook bh = new BreakpointHook();
        simulator.getUnicornInstance().hook_add(bh, addr, addr, (Long) addr);
        hooks.put(addr, bh);
    }

    /**
     * Elimina un breakpoint de la direccion indicada
     *
     * @param addr direccion del breakpoint
     */
    public void unsetBreakpoint(long addr) {
        BreakpointHook bh = hooks.get(addr);
        if (bh != null) {
            simulator.getUnicornInstance().hook_del(bh);
            hooks.remove(addr);
        }
    }

    /**
     * Resetea los breakpoints
     */
    public void reset() {
        for (long bh : hooks.keySet()) {
            unsetBreakpoint(bh);
        }
    }
    
    /**
     * Indica si se ha llegado a un breakpoint
     *
     * @return
     */
    public long isReached() {
        return reached;
    }

    /**
     * Reestablece a false el flag de alcanzado.
     */
    public void cleanReached(int initialAddr) {
        reached = -1;
        this.initialAddr = initialAddr;
    }
}
