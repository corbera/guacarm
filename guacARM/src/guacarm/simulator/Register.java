/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.simulator;

import unicorn.Unicorn;

/**
 *
 * @author corbera
 */
public class Register {

    private String name;
    private int identifier;
    private boolean updated;
    private Unicorn simulator;
    private int val;

    /**
     * Crea una instancia de registro
     *
     * @param name nombre del registro
     * @param identifier numero que lo identifica
     * @param sim referencia al simulador
     */
    public Register(String name, int identifier, Unicorn sim) {
        this.name = name;
        this.identifier = identifier;
        updated = false;
        simulator = sim;
        val = 0;
    }

    /**
     * Resetea el contenido del registro
     */
    public void reset() {
        simulator.reg_write(identifier, 0);
        updated = false;
        val = 0;
    }

    /**
     * Indica que se ha visualizado el valor del registro y que se puede
     * resetear el flag de "updated"
     */
    public void clearUpdate() {
        updated = false;
    }

    /**
     * Escribe un valor en el registro
     *
     * @param val valor a escribir
     */
    public void setValue(int val) {
        int oldValue = ((Long) simulator.reg_read(identifier)).intValue();
        if (val != oldValue) {
            updated = true;
            simulator.reg_write(identifier, Long.valueOf(val));
            this.val = val;
        }
    }

    /**
     * lee el valor almacenado en el registro
     *
     * @return valor almacenado
     */
    public int getValue() {
        int newVal = ((Long) simulator.reg_read(identifier)).intValue();
        if (newVal != val) {
            updated = true;
            val = newVal;
        }
        return newVal;
    }

    /**
     * Indica si el registro ha sido modificado desde la ultima vez que se
     * visualizo (clearUpdate)
     *
     * @return true si ha sido modificado
     */
    public boolean isUpdated() {
        getValue();
        return updated;
    }

    /**
     * Devuelve el nombre del registro
     *
     * @return nombre del registro
     */
    public String getName() {
        return name;
    }
}
