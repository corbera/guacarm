/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.coreservices;

import guacarm.log.Log;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.simulator.Simulator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servicios de librería útiles implementados como llamadas al sistema (numero
 * de servicio en r7)
 *
 * @author corbera
 */
public class CoreServices {

    enum TYPE {
        DECIMAL, HEXADECIMAL, BINARY
    };

    private final Simulator simulator;
    private final Log log;

    public CoreServices(Simulator s, Log l) {
        simulator = s;
        log = l;
        init();
    }

    public void init() {
        if (!simulator.services.isRegisteredSVC(1)) {
            simulator.services.registerSVC(1, new ExitCallBack());  // exit
        }
        if (!simulator.services.isRegisteredSVC(0x80)) {
            simulator.services.registerSVC(0x80, new AtoiCallBack());  // atoi
        }
        if (!simulator.services.isRegisteredSVC(0x81)) {
            simulator.services.registerSVC(0x81, new AtohCallBack());  // atoi
        }
        if (!simulator.services.isRegisteredSVC(0x82)) {
            simulator.services.registerSVC(0x82, new AtobCallBack());  // atoi
        }
        if (!simulator.services.isRegisteredSVC(0x90)) {
            simulator.services.registerSVC(0x90, new ItoaCallBack());  // itoa
        }
        if (!simulator.services.isRegisteredSVC(0x91)) {
            simulator.services.registerSVC(0x91, new ItohCallBack());  // itoh
        }
        if (!simulator.services.isRegisteredSVC(0x92)) {
            simulator.services.registerSVC(0x92, new ItobCallBack());  // itob
        }
    }

    /**
     * Callback a registrar para el svc exit
     */
    public class ExitCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            simulator.endOfSimulation();
        }
    }

    /*
     * Callback para atoi
     * 
     * 0x80 - atoi 
        Convierte cadena en decimal apuntada por r0 a entero y lo devuelve por r0.
        En r1 devuelve -1 si se ha producido algún error

     */
    public class AtoiCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            atoi(api, TYPE.DECIMAL);
        }

    }

    /*
     * Callback para atoi
     * 
     * 0x81 - atoh 
        Convierte cadena en hexadecimal apuntada por r0 a entero y lo devuelve por r0.
        En r1 devuelve -1 si se ha producido algún error

     */
    public class AtohCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            atoi(api, TYPE.HEXADECIMAL);
        }

    }

    /*
     * Callback para atoi
     * 
     * 0x82 - atob 
        Convierte cadena en binario apuntada por r0 a entero y lo devuelve por r0.
        En r1 devuelve -1 si se ha producido algún error

     */
    public class AtobCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            atoi(api, TYPE.BINARY);
        }

    }

    /*
     * Callback para itoa
     *
     *  0x90 - itoa
        Convierte entero dado por r0 a cadena de caracteres apuntada por r1 
        (debe haber espacio suficiente de memoria reservado) en decimal.
        En r1 devuelve m1 si se ha producdo algún error

     */
    public class ItoaCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            itoa(api, TYPE.DECIMAL);
        }
    }

    /*
     * Callback para itoh
     *
     *  0x91 - itoh
        Convierte entero dado por r0 a cadena de caracteres apuntada por r1 
        (debe haber espacio suficiente de memoria reservado) en hexadecimal.
        En r1 devuelve m1 si se ha producdo algún error

     */
    public class ItohCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            itoa(api, TYPE.HEXADECIMAL);
        }
    }

    /*
     * Callback para itob
     *
     *  0x92 - itob
        Convierte entero dado por r0 a cadena de caracteres apuntada por r1 
        (debe haber espacio suficiente de memoria reservado) en binario.
        En r1 devuelve m1 si se ha producdo algún error

     */
    public class ItobCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            itoa(api, TYPE.BINARY);
        }
    }

    /**
     * Realiza la función de conversión de entero a cadena segun el tipo
     * indicado
     *
     * @param api interfaz con el simulador
     * @param type tipo para la conversion (decimal, hexadecimal, binario)
     */
    private void itoa(SimulatorInterface api, TYPE type) {
        try {
            int val = api.readRegister(api.REGISTER_R0);
            String s = "";
            switch (type) {
                case DECIMAL:
                    s = Integer.toString(val) + "*";
                    break;
                case HEXADECIMAL:
                    s = Integer.toHexString(val) + "*";
                    break;
                case BINARY:
                    s = Integer.toBinaryString(val) + "*";
                    break;
            }
            byte b[] = s.getBytes();
            b[b.length - 1] = 0;
            int addr = api.readRegister(api.REGISTER_R1);
            api.writeMemory(addr, b, b.length);
            api.writeRegister(api.REGISTER_R1, 0);
        } catch (Exception ex) {
            try {
                api.writeRegister(api.REGISTER_R1, -1);
            } catch (Exception ex1) {
                Logger.getLogger(CoreServices.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    /**
     * Realiza la función de conversión de cadena en el formato indicado a entero
     * @param api interfaz con el simulador
     * @param type tipo para la conversion (decimal, hexadecimal, binario)
     */
    private void atoi(SimulatorInterface api, TYPE type) {
        try {
            int addr = api.readRegister(api.REGISTER_R0);
            byte[] cad = api.readMemory(addr, 1024);
            int i = 0;
            while (cad[i++] != 0 && i < cad.length);
            String s = new String(cad, 0, i - 1, "UTF-8");
            int val = 0;
            switch (type) {
                case DECIMAL:
                    val = Integer.parseInt(s);
                    break;
                case HEXADECIMAL:
                    val = Integer.parseInt(s, 16);
                    break;
                case BINARY:
                    val = (int)Long.parseLong(s, 2);
                    break;
            }
            api.writeRegister(api.REGISTER_R0, val);
            api.writeRegister(api.REGISTER_R1, 0);
        } catch (NumberFormatException e) {
            try {
                api.writeRegister(api.REGISTER_R1, -1);
            } catch (Exception ex1) {
                Logger.getLogger(CoreServices.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (Exception ex) {
            Logger.getLogger(CoreServices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
