/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import guacarm.log.Log;
import guacarm.simulator.Simulator;
import java.awt.Color;
import java.awt.Rectangle;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
//import javax.swing.JPopupMenu;
import javax.swing.text.BadLocationException;
import org.fife.ui.rtextarea.Gutter;

/**
 *
 * @author corbera
 */
public class ARMBinaryCodePane extends RSyntaxtCodePane {

    private Color colorPC;
    private Color colorBreakpoint;
    private Integer nLinePC = -1;
    //private JPopupMenu breakPointMenu;
    private Map<Integer, Integer> addr2line;   // mapea direcciones en numeros de linea del codigo
    private Map<Integer, Integer> line2addr;   // mapea numeros de linea con direcciones del codigo
    private Map<Integer, Integer> line2offset; // mapea numeros de linea con offset dentro del texto
    private Map<Integer, Integer> offset2line; // mapea offset dentro del texto con numeros de linea 
    private Set<Integer> breakpoints;           // lineas donde se ha establecido un BREAKPOINT
    private Simulator simulator;
    private Gutter gutter;

    /**
     * Crea un nuevo panel de codigo binario para simular (no es editable)
     *
     * @param notify callback que se ejecuta cuando se modifica el texto
     * @param log log que se debe utilizar para mostrar los mensajes de aviso
     */
    public ARMBinaryCodePane(Callable notify, Log log, Simulator sim) {
        super(notify, log, false);
        binary = true;
        textArea.setEditable(false);
        addr2line = new HashMap<>();
        line2addr = new HashMap<>();
        line2offset = new HashMap<>();
        offset2line = new HashMap<>();
        colorPC = ColorTheme.UPDATED;
        colorBreakpoint = ColorTheme.BREAKPOINT;
        highlightColor = colorPC;
        textArea.setHighlightCurrentLine(true);
        textArea.setFadeCurrentLineHighlight(true);
        simulator = sim;

        // configuracion de los bookmarks
        gutter = scrollPane.getGutter();
        gutter.setBookmarkingEnabled(true);
        URL url = getClass().getResource("/resources/Breakpoint.png");
        gutter.setBookmarkIcon(new ImageIcon(url));
        // breakpoints
        breakpoints = new TreeSet();
        // creamos un popup menu para los BREAKPOINT
        //breakPointMenu = new JPopupMenu("Breakpoints");
        //JMenuItem menuItem = new JMenuItem("Toggle break-point");
        //breakPointMenu.add(menuItem);

//        menuItem.addActionListener(new java.awt.event.ActionListener() {
//            public void actionPerformed(java.awt.event.ActionEvent evt) {
//                toggleBreakPoint();
//            }
//        });
        //textArea.setPopupMenu(breakPointMenu);
        gutter.setToggleBookmarkNotifier((line) -> toggleBreakPoint(line));
    }

    /**
     * Establece el contenido del panel de codigo
     *
     * @param str contenido
     */
    public void setText(String str) {
        super.setText(str);
        // patron para buscar las lineas que continenen una direccion de memoria
        Pattern pt = Pattern.compile("([0-9a-fA-F]+)[:]\\s+[0-9a-fA-F]{8}");
        int pos = 0;
        int next = str.indexOf("\n", pos);
        int lineCount = 0;
        while (next > -1) {
            line2offset.put(lineCount, pos);
            offset2line.put(pos, lineCount);
            String line = str.substring(pos, next);
            Matcher m = pt.matcher(line);
            if (m.find()) {
                int addr = 0;
                try {
                    addr = Integer.parseInt(line.substring(m.start(), line.indexOf(":", m.start())), 16);
                } catch (Exception e) {
                    System.err.println("Error parsing stdout of objdump, please report error.");
                    System.exit(-1);
                }
                addr2line.put(addr, lineCount);
                line2addr.put(lineCount, addr);
                //System.out.println(addr + " : "+lineCount);
            }
            pos = next + 1;
            next = str.indexOf("\n", pos);
            lineCount++;
        }
        if (pos < str.length()) {
            String line = str.substring(pos);
            Matcher m = pt.matcher(line);
            if (m.find()) {
                int addr = 0;
                try {
                    addr = Integer.parseInt(line.substring(m.start(), line.indexOf(":", m.start())), 16);
                } catch (Exception e) {
                    System.err.println("Error parsing stdout of objdump, please report error.");
                    System.exit(-1);
                }
                addr2line.put(addr, lineCount);
                line2addr.put(lineCount, addr);
                //System.out.println(addr + " : "+lineCount);
            }
        }
    }

    /**
     * Establece el valor de PC que se esta simulando
     *
     * @param PC
     */
    public void setPC(int PC) {
        unSetPC();
        nLinePC = addr2line.get(PC);
        if (nLinePC == null) {
            mylog.println("PC (0x" + Integer.toHexString(PC) + ") not belongs to binary address set.", ColorTheme.WARNING);
            nLinePC = -1;
        } else {
            setHighlightLine(nLinePC, highlightColor);
            Rectangle viewRect = null;
            try {
                viewRect = textArea.modelToView(line2offset.get(nLinePC));
            } catch (BadLocationException ex) {
                Logger.getLogger(ARMBinaryCodePane.class.getName()).log(Level.SEVERE, null, ex);
            }
            // Scroll to make the rectangle visible
            if (viewRect != null) {
                textArea.scrollRectToVisible(viewRect);
            }
//            try {
//                linePC = textArea.addLineHighlight(nLinePC, highlightColor);
//            } catch (BadLocationException ex) {
//                mylog.println("Error highlighting line " + nLinePC + ". Please report error.", ColorTheme.WARNING);
//            }
        }
    }

    /**
     * Quita el resalte de la linea correspondiente a PC
     */
    public void unSetPC() {
        if (nLinePC > -1) {
            unsetHighlightLine(nLinePC);
//            if (breakpoints.contains(nLinePC)) {
//                setHighlightLine(nLinePC, colorBreakpoint);
//            }
        }
        nLinePC = -1;
    }

    /**
     * Activa/desactiva un break point
     */
    private void toggleBreakPoint() {
        try {
            int offset = textArea.getLineStartOffsetOfCurrentLine();
            int line = offset2line.get(offset);
            if (line2addr.get(line) != null) { // linea de codigo
                if (breakpoints.contains(line)) {  // quita BREAKPOINT
                    breakpoints.remove(line);
                    unsetHighlightLine(line);
                    simulator.unsetBreakpoint(line2addr.get(line));
                } else {
                    //scrollPane.getGutter().toggleBookmark(line);
                    breakpoints.add(line);
                    setHighlightLine(line, colorBreakpoint);
                    simulator.setBreakpoint(line2addr.get(line));
                }
            }
        } catch (Exception ex) {
            mylog.println("Bookmark bad location", ColorTheme.WARNING);
        }
    }

    /**
     * Activa/desactiva un break point en la linea especificada
     *
     * @param line asociada al break point
     */
    private void toggleBreakPoint(Integer line) {
        try {
            if (line2addr.get(line) != null) { // linea de codigo
                if (breakpoints.contains(line)) {  // quita BREAKPOINT
                    breakpoints.remove(line);
                    //unsetHighlightLine(line);
                    simulator.unsetBreakpoint(line2addr.get(line));
                } else {
                    //scrollPane.getGutter().toggleBookmark(line);
                    breakpoints.add(line);
                    //setHighlightLine(line, colorBreakpoint);
                    simulator.setBreakpoint(line2addr.get(line));
                }
            }
        } catch (Exception ex) {
            mylog.println("Bookmark bad location", ColorTheme.WARNING);
        }
    }

    /**
     * Dice si la direccion addr se corresponde con una linea de BREAKPOINT
     *
     * @param addr direccion
     * @return true si es un BREAKPOINT
     */
    public boolean isBreakPoint(int addr) {
        int line = addr2line.get(addr);
        return (breakpoints.contains(line));
    }

    /**
     *
     * @return true si hay algun BREAKPOINT
     */
    public boolean isThereBreakPoints() {
        return !breakpoints.isEmpty();
    }

    public void unSetAllBreakPoints() {
        for (int line : breakpoints) {
            simulator.unsetBreakpoint(line2addr.get(line));
        }
        breakpoints.clear();
        gutter.removeAllTrackingIcons();
    }
}
