package guacarm.GUI;

import guacarm.log.Log;
import guacarm.log.WindowsLog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.util.List;
import java.util.Map;
import javax.swing.text.BadLocationException;

import org.fife.ui.rsyntaxtextarea.ErrorStrip;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextAreaEditorKit;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.Gutter;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * The root pane used by the demos. This allows both the applet and the
 * stand-alone application to share the same UI.
 *
 * @author Robert Futrell
 * @version 1.0
 */
public class RSyntaxtCodePane extends JRootPane implements HyperlinkListener,
        SyntaxConstants, DocumentListener {

    protected RTextScrollPane scrollPane;
    public RSyntaxTextArea textArea;

    public File associateFile;
    public boolean binary = false;
    public boolean modified;
    private Callable notifyModified;
    private Map<Integer, Object> highlightLines;
    protected Color highlightColor;
    protected Log mylog;
    private ARMAsmAutoCompletion ac;

    public RSyntaxtCodePane(Callable notify, Log log, boolean menu) {
        textArea = createTextArea();
        //textArea.setSyntaxEditingStyle(SYNTAX_STYLE_ASSEMBLER_X86);
        textArea.setSyntaxEditingStyle(SYNTAX_STYLE_ASSEMBLER_ARM);
        textArea.setHighlightCurrentLine(true);
        textArea.setAutoIndentEnabled(true);
        setTheme("dark.xml");

        scrollPane = new RTextScrollPane(textArea, true);
        Gutter gutter = scrollPane.getGutter();
        gutter.setBookmarkingEnabled(true);

        getContentPane().add(scrollPane);
        ErrorStrip errorStrip = new ErrorStrip(textArea);
        errorStrip.setBackground(java.awt.Color.blue);
        getContentPane().add(errorStrip, BorderLayout.LINE_END);
        if (menu) {
            setJMenuBar(createMenuBar());
        }

        textArea.getDocument().addDocumentListener(this);
        modified = false;
        notifyModified = notify;
        highlightColor = ColorTheme.WARNING;
        highlightLines = new HashMap<>();
        mylog = log;

        ac = new ARMAsmAutoCompletion(textArea);
    }

    public RSyntaxtCodePane(Callable notify, Log log) {
        this(notify, log, true);
    }

    private void addSyntaxItem(String name, String res, String style,
            ButtonGroup bg, JMenu menu) {
        JRadioButtonMenuItem item = new JRadioButtonMenuItem(
                new ChangeSyntaxStyleAction(name, res, style));
        bg.add(item);
        menu.add(item);
    }

    private void addThemeItem(String name, String themeXml, ButtonGroup bg,
            JMenu menu) {
        JRadioButtonMenuItem item = new JRadioButtonMenuItem(
                new ThemeAction(name, themeXml));
        bg.add(item);
        menu.add(item);
    }

    private static Action createCopyAsStyledTextAction(String themeName) throws IOException {
        String resource = "/org/fife/ui/rsyntaxtextarea/themes/" + themeName + ".xml";
        Theme theme = Theme.load(RSyntaxtCodePane.class.getResourceAsStream(resource));
        return new RSyntaxTextAreaEditorKit.CopyAsStyledTextAction(themeName, theme);
    }

    private JMenuBar createMenuBar() {

        JMenuBar mb = new JMenuBar();
        JMenu menu;
        ButtonGroup bg;

//        menu = new JMenu("Language");
//        bg = new ButtonGroup();
//        addSyntaxItem("ActionScript", "ActionScriptExample.txt", SYNTAX_STYLE_ACTIONSCRIPT, bg, menu);
//        addSyntaxItem("C", "CExample.txt", SYNTAX_STYLE_CPLUSPLUS, bg, menu);
//        addSyntaxItem("CSS", "CssExample.txt", SYNTAX_STYLE_CSS, bg, menu);
//        addSyntaxItem("Dockerfile", "DockerfileExample.txt", SYNTAX_STYLE_DOCKERFILE, bg, menu);
//        addSyntaxItem("Go", "GoExample.txt", SYNTAX_STYLE_GO, bg, menu);
//        addSyntaxItem("Hosts", "HostsExample.txt", SYNTAX_STYLE_HOSTS, bg, menu);
//        addSyntaxItem("HTML", "HtmlExample.txt", SYNTAX_STYLE_HTML, bg, menu);
//        addSyntaxItem("INI", "IniExample.txt", SYNTAX_STYLE_INI, bg, menu);
//        addSyntaxItem("Java", "JavaExample.txt", SYNTAX_STYLE_JAVA, bg, menu);
//        addSyntaxItem("JavaScript", "JavaScriptExample.txt", SYNTAX_STYLE_JAVASCRIPT, bg, menu);
//        addSyntaxItem("JSP", "JspExample.txt", SYNTAX_STYLE_JSP, bg, menu);
//        addSyntaxItem("JSON", "JsonExample.txt", SYNTAX_STYLE_JSON_WITH_COMMENTS, bg, menu);
//        addSyntaxItem("Less", "LessExample.txt", SYNTAX_STYLE_LESS, bg, menu);
//        addSyntaxItem("Perl", "PerlExample.txt", SYNTAX_STYLE_PERL, bg, menu);
//        addSyntaxItem("PHP", "PhpExample.txt", SYNTAX_STYLE_PHP, bg, menu);
//        addSyntaxItem("Ruby", "RubyExample.txt", SYNTAX_STYLE_RUBY, bg, menu);
//        addSyntaxItem("SQL", "SQLExample.txt", SYNTAX_STYLE_SQL, bg, menu);
//        addSyntaxItem("TypeScript", "TypeScriptExample.txt", SYNTAX_STYLE_TYPESCRIPT, bg, menu);
//        addSyntaxItem("XML", "XMLExample.txt", SYNTAX_STYLE_XML, bg, menu);
//        addSyntaxItem("YAML", "YamlExample.txt", SYNTAX_STYLE_YAML, bg, menu);
//        menu.getItem(2).setSelected(true);
//        mb.add(menu);
//        menu = new JMenu("File");
//        for (Pair<String, fileMenuAction> pair : menuFileActions) {
//            JMenuItem item = new JMenuItem(pair.getKey());
//            menu.add(item);
//            item.addActionListener(this);
//        }
//        mb.add(menu);
        menu = new JMenu("View");
        JCheckBoxMenuItem cbItem = new JCheckBoxMenuItem(new CodeFoldingAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new ViewLineHighlightAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new ViewLineNumbersAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new AnimateBracketMatchingAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new BookmarksAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new WordWrapAction());
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new ToggleAntiAliasingAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new MarkOccurrencesAction());
        cbItem.setSelected(true);
        menu.add(cbItem);
        cbItem = new JCheckBoxMenuItem(new TabLinesAction());
        menu.add(cbItem);
        mb.add(menu);

        bg = new ButtonGroup();
        menu = new JMenu("Themes");
        addThemeItem("Default", "default.xml", bg, menu);
        addThemeItem("Default (System Selection)", "default-alt.xml", bg, menu);
        addThemeItem("Dark", "dark.xml", bg, menu);
        addThemeItem("Monokai", "monokai.xml", bg, menu);
        addThemeItem("Eclipse", "eclipse.xml", bg, menu);
        addThemeItem("IDEA", "idea.xml", bg, menu);
        addThemeItem("Visual Studio", "vs.xml", bg, menu);
        mb.add(menu);

//        menu = new JMenu("Help");
//        JMenuItem item = new JMenuItem(new AboutAction());
//        menu.add(item);
//        mb.add(menu);
        return mb;

    }

    /**
     * Creates the text area for this application.
     *
     * @return The text area.
     */
    private RSyntaxTextArea createTextArea() {

        RSyntaxTextArea textArea = new RSyntaxTextArea(25, 70);
        textArea.setTabSize(3);
        textArea.setCaretPosition(0);
        textArea.addHyperlinkListener(this);
        textArea.requestFocusInWindow();
        textArea.setMarkOccurrences(true);
        textArea.setCodeFoldingEnabled(true);
        textArea.setClearWhitespaceLinesEnabled(false);

        InputMap im = textArea.getInputMap();
        ActionMap am = textArea.getActionMap();
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "decreaseFontSize");
        am.put("decreaseFontSize", new RSyntaxTextAreaEditorKit.DecreaseFontSizeAction());
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "increaseFontSize");
        am.put("increaseFontSize", new RSyntaxTextAreaEditorKit.IncreaseFontSizeAction());

        int ctrlShift = InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK;
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, ctrlShift), "copyAsStyledText");
        am.put("copyAsStyledText", new RSyntaxTextAreaEditorKit.CopyAsStyledTextAction());

        try {

            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_M, ctrlShift), "copyAsStyledTextMonokai");
            am.put("copyAsStyledTextMonokai", createCopyAsStyledTextAction("monokai"));

            im.put(KeyStroke.getKeyStroke(KeyEvent.VK_E, ctrlShift), "copyAsStyledTextEclipse");
            am.put("copyAsStyledTextEclipse", createCopyAsStyledTextAction("eclipse"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return textArea;
    }

    /**
     * Focuses the text area.
     */
    void focusTextArea() {
        textArea.requestFocusInWindow();
    }

    /**
     * Called when a hyperlink is clicked in the text area.
     *
     * @param e The event.
     */
    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            URL url = e.getURL();
            if (url == null) {
                UIManager.getLookAndFeel().provideErrorFeedback(null);
            } else {
                JOptionPane.showMessageDialog(this,
                        "URL clicked:\n" + url.toString());
            }
        }
    }

    /**
     * Sets the content in the text area to that in the file associatedFile.
     *
     */
    public void setText() {
        if (associateFile == null) {
            return;
        }
        BufferedReader r;
        try {
            r = new BufferedReader(new FileReader(associateFile));
            textArea.read(r, null);
            r.close();
            textArea.setCaretPosition(0);
            textArea.discardAllEdits();
        } catch (RuntimeException re) {
            throw re; // FindBugs
        } catch (Exception e) {
        }
    }

    /**
     * Sets the content in the text area to that in the file associatedFile.
     *
     */
    public void setText(String str) {
        textArea.setText(str);
        textArea.setCaretPosition(0);
        textArea.discardAllEdits();
    }

    /**
     * Save the content in the text area to the specified file.
     */
    public void saveText() {
        if (associateFile == null) {
            return;
        }
        BufferedWriter r;
        try {
            r = new BufferedWriter(new FileWriter(associateFile));
            textArea.write(r);
            r.close();
        } catch (RuntimeException re) {
            throw re; // FindBugs
        } catch (Exception e) {
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        modified = true;
        try {
            notifyModified.call();
        } catch (Exception ex) {
            Logger.getLogger(RSyntaxtCodePane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        modified = true;
        try {
            notifyModified.call();
        } catch (Exception ex) {
            Logger.getLogger(RSyntaxtCodePane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        modified = true;
        try {
            notifyModified.call();
        } catch (Exception ex) {
            Logger.getLogger(RSyntaxtCodePane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Shows the About dialog.
     */
    private class AboutAction extends AbstractAction {

        AboutAction() {
            putValue(NAME, "About RSyntaxTextArea...");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(RSyntaxtCodePane.this,
                    "<html><b>RSyntaxTextArea</b> - A Swing syntax highlighting text component"
                    + "<br>Version 3.0.0"
                    + "<br>Licensed under a modified BSD license",
                    "About RSyntaxTextArea",
                    JOptionPane.INFORMATION_MESSAGE);
        }

    }

    /**
     * Toggles whether matched brackets are animated.
     */
    private class AnimateBracketMatchingAction extends AbstractAction {

        AnimateBracketMatchingAction() {
            putValue(NAME, "Animate Bracket Matching");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setAnimateBracketMatching(
                    !textArea.getAnimateBracketMatching());
        }

    }

    /**
     * Toggles whether bookmarks are enabled.
     */
    private class BookmarksAction extends AbstractAction {

        BookmarksAction() {
            putValue(NAME, "Bookmarks");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            scrollPane.setIconRowHeaderEnabled(
                    !scrollPane.isIconRowHeaderEnabled());
        }

    }

    /**
     * Changes the syntax style to a new value.
     */
    private class ChangeSyntaxStyleAction extends AbstractAction {

        private String res;
        private String style;

        ChangeSyntaxStyleAction(String name, String res, String style) {
            putValue(NAME, name);
            this.res = res;
            this.style = style;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setText();
            textArea.setCaretPosition(0);
            textArea.setSyntaxEditingStyle(style);
        }

    }

    /**
     * Toggles whether code folding is enabled.
     */
    private class CodeFoldingAction extends AbstractAction {

        CodeFoldingAction() {
            putValue(NAME, "Code Folding");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setCodeFoldingEnabled(!textArea.isCodeFoldingEnabled());
        }

    }

    /**
     * Toggles whether "mark occurrences" is enabled.
     */
    private class MarkOccurrencesAction extends AbstractAction {

        MarkOccurrencesAction() {
            putValue(NAME, "Mark Occurrences");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setMarkOccurrences(!textArea.getMarkOccurrences());
        }

    }

    /**
     * Toggles whether "tab lines" are enabled.
     */
    private class TabLinesAction extends AbstractAction {

        private boolean selected;

        TabLinesAction() {
            putValue(NAME, "Tab Lines");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            selected = !selected;
            textArea.setPaintTabLines(selected);
        }

    }

    /**
     * Changes the theme.
     */
    private class ThemeAction extends AbstractAction {

        private String xml;

        ThemeAction(String name, String xml) {
            putValue(NAME, name);
            this.xml = xml;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setTheme(xml);
//            InputStream in = getClass().
//                    getResourceAsStream("/org/fife/ui/rsyntaxtextarea/themes/" + xml);
//            try {
//                Theme theme = Theme.load(in);
//                theme.apply(textArea);
//            } catch (IOException ioe) {
//                ioe.printStackTrace();
//            }
        }
    }

    private void setTheme(String xml) {
        InputStream in = getClass().
                getResourceAsStream("/org/fife/ui/rsyntaxtextarea/themes/" + xml);
        try {
            Theme theme = Theme.load(in);
            theme.apply(textArea);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Toggles anti-aliasing.
     */
    private class ToggleAntiAliasingAction extends AbstractAction {

        ToggleAntiAliasingAction() {
            putValue(NAME, "Anti-Aliasing");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setAntiAliasingEnabled(!textArea.getAntiAliasingEnabled());
        }

    }

    /**
     * Toggles whether the current line is highlighted.
     */
    private class ViewLineHighlightAction extends AbstractAction {

        ViewLineHighlightAction() {
            putValue(NAME, "Current Line Highlight");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setHighlightCurrentLine(
                    !textArea.getHighlightCurrentLine());
        }

    }

    /**
     * Toggles line number visibility.
     */
    private class ViewLineNumbersAction extends AbstractAction {

        ViewLineNumbersAction() {
            putValue(NAME, "Line Numbers");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            scrollPane.setLineNumbersEnabled(
                    !scrollPane.getLineNumbersEnabled());
        }
    }

    /**
     * Toggles word wrap.
     */
    private class WordWrapAction extends AbstractAction {

        WordWrapAction() {
            putValue(NAME, "Word Wrap");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            textArea.setLineWrap(!textArea.getLineWrap());
        }
    }

    /**
     * Resalta las lineas especificadas en el color especificado
     *
     * @param lines lista de enteros con los numeros de lineas a resaltar
     * @param color color para resaltar las lineas
     */
    public void setHighlightLines(List<Integer> lines, Color color) {
        for (Integer line : lines) {
            try {
                highlightLines.put(line, textArea.addLineHighlight(line - 1, color));
            } catch (Exception ex) {
                mylog.println("Error highlighting line " + " line. Please report error.", Color.RED);
            }
        }
    }

    public void setHighlightLines(List<Integer> lines) {
            setHighlightLines(lines, highlightColor);
    }

    public void setHighlightLine(Integer line) {
        setHighlightLine(line, Color.RED);
    }

    public void setHighlightLine(Integer line, Color color) {
        if (highlightLines.containsKey(line)) {
            unsetHighlightLine(line);
        }
        try {
            highlightLines.put(line, textArea.addLineHighlight(line, color));
        } catch (BadLocationException ex) {
            mylog.println("Error highlighting line " + line + ". Please report error.", color);
        }
    }

    public void unsetHighlightLine(Integer line) {
        textArea.removeLineHighlight(highlightLines.get(line));
        highlightLines.remove(line);
    }

    public void unsetHighlight() {
        Iterator<Integer> it = highlightLines.keySet().iterator();
        while (it.hasNext()) {
            Integer line = it.next();
            textArea.removeLineHighlight(highlightLines.get(line));
            it.remove();
        }
    }

    public void setHighlightColor(Color c) {
        highlightColor = c;
    }
}
