/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import guacarm.Configuration;
import guacarm.log.Log;
import guacarm.Utils;
import guacarm.simulator.Simulator;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

/**
 *
 * @author corbera
 */
public class MemoryView {

    private Simulator simulator;
    private JPanel rootPanel;
    private JPanel formatPanel;
    private Log log;
    private JTextPane text;
    private JRadioButton[] formatsRadioButtons;
    private JRadioButton[] sizeRadioButtons;
    private JComboBox<String> memoryOffsets;
    private int format;
    private int size;
    private int viewSize;
    private byte[] oldValues;

    /**
     * Al crear el objeto toma la referencia al objeto simulador para poder
     * acceder al contenido de la memoria y el panel donde escribir
     *
     * @param sim simulador
     * @param format panel para la seleccion del formato y offset
     * @param data panel para imprimir los valores
     * @param log logger donde escribir los mensajes
     * @param f formato de representacion de los datos (HEX, INT, UINT)
     * @param size numero de bytes de la representacion (1,2,4)
     */
    public MemoryView(Simulator sim, JPanel format, JPanel data,
            Log log, int f, int size) {
        simulator = sim;
        rootPanel = data;
        formatPanel = format;
        this.format = f;
        this.log = log;
        this.size = size;
        formatsRadioButtons = new JRadioButton[Configuration.NFORMATS];
        sizeRadioButtons = new JRadioButton[Configuration.NSIZES];
        viewSize = 0x400;
        oldValues = new byte[viewSize];
        createView();
    }

    /**
     * Crea la vista para mostrar la memoria en el rootPane
     */
    private void createView() {
        // Panel de seleccion del formato y offset
        formatPanel.setBackground(ColorTheme.BACKGROUND);
        formatPanel.setOpaque(true);
        formatPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        //c.ipadx = 5;
        JRadioButton b = new JRadioButton(Configuration.HEXS);
        b.setName(Configuration.HEXS);
        decorate(b);
        b.setForeground(ColorTheme.HEXC);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedFormat(Configuration.HEX);
            }
        });
        formatsRadioButtons[Configuration.HEX] = b;

        c.gridx = 1;
        b = new JRadioButton(Configuration.INTS);
        b.setName(Configuration.INTS);
        decorate(b);
        b.setForeground(ColorTheme.INTC);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedFormat(Configuration.INT);
            }
        });
        formatsRadioButtons[Configuration.INT] = b;

        c.gridx = 2;
        b = new JRadioButton(Configuration.UINTS);
        b.setName(Configuration.UINTS);
        decorate(b);
        b.setForeground(ColorTheme.UINTC);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedFormat(Configuration.UINT);
            }
        });
        formatsRadioButtons[Configuration.UINT] = b;

        c.gridx = 3;
        b = new JRadioButton(Configuration.B8S);
        b.setName(Configuration.B8S);
        decorate(b);
        b.setForeground(ColorTheme.TITLE);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedSize(Configuration.B8);
            }
        });
        sizeRadioButtons[Configuration.B8] = b;

        c.gridx = 4;
        b = new JRadioButton(Configuration.B16S);
        b.setName(Configuration.B16S);
        decorate(b);
        b.setForeground(ColorTheme.TITLE);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedSize(Configuration.B16);
            }
        });
        sizeRadioButtons[Configuration.B16] = b;

        c.gridx = 5;
        b = new JRadioButton(Configuration.B32S);
        b.setName(Configuration.B32S);
        decorate(b);
        b.setForeground(ColorTheme.TITLE);
        b.setBackground(ColorTheme.BACKGROUND);
        formatPanel.add(b, c);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSelectedSize(Configuration.B32);
            }
        });
        sizeRadioButtons[Configuration.B32] = b;

        c.gridx = 6;
        c.weightx = 1.0;
        c.anchor = GridBagConstraints.WEST;
        // direccion base a mostrar
        memoryOffsets = new JComboBox<>();
        memoryOffsets.setEditable(true);
        formatPanel.add(memoryOffsets, c);
        memoryOffsets.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setOffset((String) memoryOffsets.getEditor().getItem());
            }
        });

        setSelectedSize(Configuration.B32);
        setSelectedFormat(Configuration.HEX);

        // Panel de los datos
        rootPanel.setBackground(ColorTheme.BACKGROUND);
        rootPanel.setOpaque(true);
        rootPanel.setLayout(new GridBagLayout());
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.ipadx = 10;
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(0, 10, 0, 10);

        text = new JTextPane();
        text.setBackground(ColorTheme.BACKGROUND);
        text.setOpaque(true);
        text.setForeground(ColorTheme.FOREGROUND);
        text.setFont(new Font("Courier New", Font.BOLD,
                text.getFont().getSize()));
        text.setEditable(false);
        rootPanel.add(text, c);
    }

    public void update() {
        if (memoryOffsets.getSelectedItem() != null) {
            setOffset((String) memoryOffsets.getSelectedItem());
        }
    }

    private boolean update(long off) {
        if (off < 0) {
            return false;
        }
        try {
            byte[] mem = simulator.readMem(off, viewSize);
            //System.out.println(rootPanel.getSize().getWidth());
            Utils.styledMemToDump(off, mem, format, 1 << size,
                    //(int) rootPanel.getSize().getWidth(), true, oldValues, text, log);
                    (int) formatPanel.getSize().getWidth(), true, oldValues, text, log);
            //text.setText(Utils.memToDump(off, mem, format, 1 << size, 4, true));
            text.setCaretPosition(0);
            System.arraycopy(mem, 0, oldValues, 0, viewSize);
        } catch (Exception e) {
            log.println("Memory range can't be readed: " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Aplica formato de visualizacion al componente
     *
     */
    private void decorate(JToggleButton c) {
        Font f = c.getFont();
        c.setFont(new Font(f.getFamily(), Font.BOLD, f.getSize()));
        c.setForeground(ColorTheme.TITLE);
        c.setVerticalTextPosition(SwingConstants.TOP);
        c.setVerticalAlignment(SwingConstants.TOP);
        c.setHorizontalAlignment(SwingConstants.CENTER);
        c.setHorizontalTextPosition(SwingConstants.CENTER);
    }

    /**
     * Selecciona el radio button indicado por format y borra los demas
     *
     * @param format radio button a seleccionar
     */
    public void setSelectedFormat(int format) {
        for (int i = 0; i < Configuration.NFORMATS; i++) {
            formatsRadioButtons[i].setSelected(i == format);
        }
        this.format = format;
        update();
    }

    /**
     * Selecciona el radio button indicado por size y borra los demas
     *
     * @param size radio button a seleccionar
     */
    public void setSelectedSize(int size) {
        for (int i = 0; i < Configuration.NSIZES; i++) {
            sizeRadioButtons[i].setSelected(i == size);
        }
        this.size = size;
        update();
    }

    public int getMemoryFormat() {
        for (JRadioButton rb : formatsRadioButtons) {
            if (rb.isSelected()) {
                switch (rb.getName()) {
                    case Configuration.HEXS:
                        return Configuration.HEX;
                    case Configuration.INTS:
                        return Configuration.INT;
                    case Configuration.UINTS:
                        return Configuration.UINT;
                }
            }
        }
        return Configuration.HEX;
    }

    public int getMemorySize() {
        for (JRadioButton rb : sizeRadioButtons) {
            if (rb.isSelected()) {
                switch (rb.getName()) {
                    case Configuration.B8S:
                        return Configuration.B8;
                    case Configuration.B16S:
                        return Configuration.B16;
                    case Configuration.B32S:
                        return Configuration.B32;
                }
            }
        }
        return Configuration.HEX;
    }

    /**
     * Elimina todas las entradas del combobox con las direcciones base de
     * memoria a mostrar
     */
    public void cleanMemoryAddress() {
        memoryOffsets.removeAllItems();
    }

    /**
     * Añade una nueva direccion de memoria al desplegable de direcciones base a
     * mostrar
     *
     * @param addr direccion a añadir
     */
    public void setMemoryAddress(String addr) {
        memoryOffsets.addItem(addr);
    }

    /**
     * Establece el offset de la posicion de memoria a mostrar en el panel
     *
     * @param addr
     */
    public void setOffset(String addr) {
        update(stripAddress(addr));
    }

    /**
     * Elimina de la cadena addr todo lo que no es el numero de la direccion y
     * devuelve solo dicho numero: "0x8000 (.data)" --> 32768
     *
     * @param addr direccion con texto
     * @return direccion como numero
     */
    private long stripAddress(String addr) {
        addr = addr.toLowerCase();
        Pattern pt = Pattern.compile("(0x([a-f]|\\d)+|(\\d+))");
        Matcher m = pt.matcher(addr);
        if (m.find()) {
            addr = addr.substring(m.start(), m.end());
            int base = (addr.contains("x")) ? 16 : 10;
            return Long.parseLong(addr.substring(addr.indexOf("x") + 1), base);
        }
        return -1;
    }
}
