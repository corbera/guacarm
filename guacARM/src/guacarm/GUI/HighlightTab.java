/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author corbera
 */
public class HighlightTab {

    /**
     * Resalta el tab (para llamar la atencion) del JTabbedPane al cual pertence
     * el componente dado (mira hasta MAXDEPTH niveles superiores de la
     * jerarquia buscando el JTabbedPane).
     *
     * @param component componente sobre el cual se quiere llamar la atencion
     * @return true si lo ha podido resaltar
     */
    public static boolean highlightTab(Component component) {
        return highlightTab(component, 0);
    }

    static final int MAXDEPTH = 5;      // maxima profundidad en la jerarquia para buscar un jtabbedpane
    static final Color HIGHLIGHTCOLOR = ColorTheme.HIHGLIGHT;     // color de fondo para llamar la atencion sobre el tab

    static Map<JTabbedPane, MyChangeListener> panes = new HashMap<>();  // asocia a cada jtabbedpane que tenga un tab resaltado, el changelistener encargado de gestionar cuando deja de estar resaltado un tab

    static MyTimerTask timerTask = new MyTimerTask();
    static Timer timer = new Timer();
    
    static class MyChangeListener extends HashMap<Component, Boolean> implements ChangeListener {

        private final JTabbedPane pane; // pane al que se asocia el listener
        public final Color backgroundColor;    // color de fondo normal para cuando deje de estar resaltado

        public MyChangeListener(JTabbedPane p) {
            this.pane = p;
            this.backgroundColor = p.getBackgroundAt(0);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (this.isEmpty()) {
                pane.removeChangeListener(this);
                panes.remove(pane);
            }
            Component component = pane.getSelectedComponent();
            if (this.containsKey(component)) {
                this.put(component, true);
                endHighLightTab(component, pane, backgroundColor);
            }
        }
    }

    static class MyTimerTask extends TimerTask {

        public boolean running = false;
        
        @Override
        public void run() {
            if (panes.isEmpty()) {
                this.running = false;
                this.cancel();
            } else {
                for (JTabbedPane pane : panes.keySet()) {
                    MyChangeListener listener = panes.get(pane);
                    for (Component component : listener.keySet()) {
                        if (listener.get(component)) {
                            endHighLightTab(component, pane, listener.backgroundColor);
                            listener.remove(component);
                        } else {
                            blinkTab(component, pane, listener.backgroundColor);
                        }
                    }
                }
            }
        }
    }

    /**
     * Resalta el componente dado correspondiente a un nivel "level" de la
     * jerarquia
     *
     * @param component componente a resaltar
     * @param level nivel de la jerarquia
     * @return true si lo ha podido resaltar
     */
    private static boolean highlightTab(Component component, int level) {
        if (component == null || level == MAXDEPTH) {
            return false;
        }
        if (component.getParent().getClass() != JTabbedPane.class) {
            return highlightTab(component.getParent(), level + 1);
        } else {
            return highlightTab(component, component.getParent());
        }
    }

    /**
     * Resalta el componente dado perteneciente a jtabbedpane parent
     *
     * @param component componente a resaltar
     * @param parent jtabbedpane al que pertenece
     * @return true si lo ha podido resaltar
     */
    private static boolean highlightTab(Component component, Component parent) {
        if (parent == null || parent.getClass() != JTabbedPane.class
                || component == null) {
            return false;
        }
        JTabbedPane pane = (JTabbedPane) parent;
        if (pane.getSelectedComponent() == component) {
            return false;
        }
        if (!panes.containsKey(pane)) {
            MyChangeListener myChangeListener = new MyChangeListener(pane);
            myChangeListener.put(component, false);
            panes.put(pane, myChangeListener);
            pane.addChangeListener(myChangeListener);
        } else {
            panes.get(pane).put(component, false);
        }
        beginHighLightTab(component, pane);
        return true;
    }

    private static void beginHighLightTab(Component component, JTabbedPane pane) {
        pane.setBackgroundAt(pane.indexOfComponent(component), HIGHLIGHTCOLOR);
//        if (!timerTask.running) {
//            timerTask = new MyTimerTask();
//            timerTask.running = true;
//            timer.scheduleAtFixedRate(timerTask, 0, 500);
//        }
    }

    private static void endHighLightTab(Component component, JTabbedPane pane, Color color) {
        pane.setBackgroundAt(pane.indexOfComponent(component), color);
    }

    private static void blinkTab(Component component, JTabbedPane pane, Color color) {
        int index = pane.indexOfComponent(component);
        Color actualColor = pane.getBackgroundAt(index);
        pane.setBackgroundAt(index, actualColor == color ? HIGHLIGHTCOLOR : color);
    }
}
