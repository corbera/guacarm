/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import guacarm.Configuration;
import guacarm.Utils;
import guacarm.simulator.Simulator;
import guacarm.simulator.RegisterFile;
import guacarm.simulator.Register;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

/**
 * Gestor de los datos visualizados en la tabla que representa el contenido de
 * los registros del procesador
 *
 * @author Corbera
 */
public class RegisterView {

    private static final int FORMATPANELHEIGHT = 40;
    private final Simulator simulator;
    private final RegisterFile registerFile;
    private final JLabel[] registerName;
    private final JLabel[] registerValue;
    private final int[] registerFormat;
    private final JRadioButton[] formatsRadioButtons;
    private JLabel nLabel, zLabel, cLabel, vLabel;
    private final int numberRegisters;
    private final JPanel rootPanel;

    /**
     * Al crear el objeto toma la referencia al objeto simulador para poder
     * acceder al contenido y nombre de los registros, y el panel donde escribir
     *
     * @param sim simulador
     * @param panel panel donde escribir
     */
    public RegisterView(Simulator sim, JPanel panel) {
        simulator = sim;
        rootPanel = panel;
        registerFile = simulator.getRegisterFile();
        numberRegisters = registerFile.getRegisters().length;
        registerName = new JLabel[numberRegisters];
        registerValue = new JLabel[numberRegisters];
        registerFormat = new int[numberRegisters];
        formatsRadioButtons = new JRadioButton[Configuration.NFORMATS];
        nLabel = new JLabel();
        zLabel = new JLabel();
        cLabel = new JLabel();
        vLabel = new JLabel();
        createView();
    }

    /**
     * Inicializa el contenido de la tabla al valor real de los registros.
     */
    public void init() {
        int regn = 0;
        for (Register reg : registerFile.getRegisters()) {
            registerName[regn].setText(reg.getName());
            registerValue[regn++].setText("0x" + Utils.intToHexString(reg.getValue(), 8));
        }
    }

    /**
     * Actualiza el contenido de la tabla al valor real de los registros.
     */
    public void update() {
        int row = 0;
        for (Register reg : registerFile.getRegisters()) {
            registerValue[row].setText(applyformat(reg.getValue(), registerFormat[row]));
            registerValue[row].setForeground(reg.isUpdated()
                    ? ColorTheme.UPDATED
                    : ColorTheme.FORMATC[registerFormat[row]]);
            row++;
            reg.clearUpdate();
        }
        updateFlags();
        //simulator.printAllRegisters();
    }

    /**
     * Establece los labels para mostrar el nombre y valor de un registro
     *
     * @param reg numero del registro
     * @param name jlabel donde escribir el nombre
     * @param value jlabel donde escribir el valor
     */
    public void setRegisterLabels(int reg, JLabel name, JLabel value) {
        registerName[reg] = name;
        registerValue[reg] = value;
    }

    /**
     * Crea la vista para mostrar los registros en el rootPane
     */
    private void createView() {
        rootPanel.setBackground(ColorTheme.BACKGROUND);
        rootPanel.setOpaque(true);
        rootPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        rootPanel.add(makeFormatPanel(), c);
        c.gridy = 1;
        rootPanel.add(makeRegsPanel(), c);
        c.gridy = 2;
        c.weighty = 1.0;
        c.anchor = GridBagConstraints.PAGE_START;
        rootPanel.add(makeFlagsPanel(), c);

        setSelectedFormat(Configuration.HEX);
    }

    /**
     * Crea el panel de seleccion del formato de visualizacion
     *
     * @return pane con los botones
     */
    private JPanel makeFormatPanel() {
        JPanel p = new JPanel();
        p.setBackground(ColorTheme.BACKGROUND);
        p.setOpaque(true);
        p.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.PAGE_START;

        c.ipady = 5;
        c.gridx = 0;
        c.gridy = 0;
        JRadioButton b = new JRadioButton("Hex");
        decorate(b);
        b.setForeground(ColorTheme.HEXC);
        b.setBackground(ColorTheme.BACKGROUND);
        b.setName("Hex");
        p.add(b, c);
        b.addActionListener((ActionEvent e) -> {
            setSelectedFormat(Configuration.HEX);
        });
        formatsRadioButtons[Configuration.HEX] = b;

        c.gridx = 1;
        c.gridy = 0;
        b = new JRadioButton("Int");
        decorate(b);
        b.setForeground(ColorTheme.INTC);
        b.setBackground(ColorTheme.BACKGROUND);
        b.setName("Int");
        p.add(b, c);
        b.addActionListener((ActionEvent e) -> {
            setSelectedFormat(Configuration.INT);
        });
        formatsRadioButtons[Configuration.INT] = b;

        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1.0;
        c.anchor = GridBagConstraints.WEST;
        b = new JRadioButton("Uint");
        decorate(b);
        b.setForeground(ColorTheme.UINTC);
        b.setBackground(ColorTheme.BACKGROUND);
        b.setName("Uint");
        p.add(b, c);
        b.addActionListener((ActionEvent e) -> {
            setSelectedFormat(Configuration.UINT);
        });
        formatsRadioButtons[Configuration.UINT] = b;
        return p;
    }

    /**
     * crea el panel con el contenido de los registros
     *
     * @return panel de los registros
     */
    private JPanel makeRegsPanel() {
        JPanel p = new JPanel();
        p.setBackground(ColorTheme.BACKGROUND);
        p.setOpaque(true);
        p.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        //c.anchor = GridBagConstraints.PAGE_START;
        //c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 5;
        c.ipadx = 5;
        int cont = 0;
        for (Register r : registerFile.getRegisters()) {
            // Label para el nombre del registro
            c.gridx = 0;
            c.gridy = ++cont;
            if (cont == registerFile.getRegisters().length) {
                c.weighty = 1.0;
                c.gridx = 0;
            }
            JLabel l = new JLabel(r.getName());
            l.setForeground(ColorTheme.TITLE);
            l.setFont(new Font(l.getFont().getFamily(),
                    Font.BOLD, l.getFont().getSize()));
            p.add(l, c);

            // Label para el valor del registro
            c.gridx = 1;
            //c.gridwidth = 3;
            JLabel l2 = new JLabel("valor");
            l2.setForeground(ColorTheme.HEXC);
            l2.setFont(new Font("Courier New", Font.BOLD,
                    l2.getFont().getSize()));
            l2.setName(Integer.toString(cont - 1));
            l2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    iterateFormats(e);
                }
            });
            p.add(l2, c);
            setRegisterLabels(cont - 1, l, l2);
        }
        return p;
    }

    /**
     * crea el panel de los flags
     *
     * @return panel de los flags
     */
    private JPanel makeFlagsPanel() {
        // flags
        JPanel p = new JPanel();
        p.setBackground(ColorTheme.BACKGROUND);
        p.setOpaque(true);
        p.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        Font f = nLabel.getFont();
        f = new Font(f.getFamily(), Font.BOLD, f.getSize());

        c.ipady = 5;
        c.ipadx = 5;
        c.gridx = 0;
        c.gridy = 0;
        nLabel = new JLabel("N(x)");
        nLabel.setFont(f);
        nLabel.setForeground(ColorTheme.TITLE);
        p.add(nLabel, c);
        c.gridx = 1;
        zLabel = new JLabel("Z(x)");
        zLabel.setFont(f);
        zLabel.setForeground(ColorTheme.TITLE);
        p.add(zLabel, c);
        c.gridx = 2;
        cLabel = new JLabel("C(x)");
        cLabel.setFont(f);
        cLabel.setForeground(ColorTheme.TITLE);
        p.add(cLabel, c);
        c.gridx = 3;
        vLabel = new JLabel("V(x)");
        vLabel.setFont(f);
        vLabel.setForeground(ColorTheme.TITLE);
        p.add(vLabel, c);
        return p;
    }

    /**
     * Aplica formato de visualizacion al componente
     *
     */
    private void decorate(JToggleButton c) {
        Font f = c.getFont();
        c.setFont(new Font(f.getFamily(), Font.BOLD, f.getSize()));
        c.setForeground(ColorTheme.TITLE);
        c.setVerticalTextPosition(SwingConstants.TOP);
        c.setVerticalAlignment(SwingConstants.TOP);
        c.setHorizontalAlignment(SwingConstants.CENTER);
        c.setHorizontalTextPosition(SwingConstants.CENTER);
    }

    /**
     * Selecciona el radio button indicado por format y borra los demas
     *
     * @param format radio button a seleccionar
     */
    public void setSelectedFormat(int format) {
        for (int i = 0; i < Configuration.NFORMATS; i++) {
            formatsRadioButtons[i].setSelected(i == format);
        }
        for (int i = 0; i < registerFormat.length; i++) {
            registerFormat[i] = format;
        }
        update();
    }

    /**
     * Devuelve una cadena que reresenta el numero val en el formato indicado
     *
     * @param val valor a representar
     * @param format formato de la representacion
     * @return cadena que representa el valor
     */
    private String applyformat(int val, int format) {
        String str = "";
        switch (format) {
            case Configuration.HEX:
                str = "0x" + Utils.intToHexString(val, 8);
                break;
            case Configuration.INT:
                str = Integer.toString(val);
                break;
            case Configuration.UINT:
                str = Utils.intToUintString(val);
                break;
        }
        return str;
    }

    private int previousCPSR = 0;

    /**
     * Actualiza la informacion visual de los flags
     */
    private void updateFlags() {
        int val = registerFile.getCPSR().getValue();
        if ((val & 0x80000000) != (previousCPSR & 0x80000000)) {
            nLabel.setForeground(ColorTheme.UPDATED);
        } else {
            nLabel.setForeground(ColorTheme.TITLE);
        }
        nLabel.setText("N(" + ((val < 0) ? 1 : 0) + ")");
        if ((val & 0x40000000) != (previousCPSR & 0x40000000)) {
            zLabel.setForeground(ColorTheme.UPDATED);
        } else {
            zLabel.setForeground(ColorTheme.TITLE);
        }
        zLabel.setText("Z(" + ((val & 0x40000000) >> 30) + ")");
        if ((val & 0x20000000) != (previousCPSR & 0x20000000)) {
            cLabel.setForeground(ColorTheme.UPDATED);
        } else {
            cLabel.setForeground(ColorTheme.TITLE);
        }
        cLabel.setText("C(" + ((val & 0x20000000) >> 29) + ")");
        if ((val & 0x10000000) != (previousCPSR & 0x10000000)) {
            vLabel.setForeground(ColorTheme.UPDATED);
        } else {
            vLabel.setForeground(ColorTheme.TITLE);
        }
        vLabel.setText("V(" + ((val & 0x10000000) >> 28) + ")");
        previousCPSR = val;
    }

    /**
     * Cambia el formato de representacion del registro especificado rotando
     * entre los tres modos de representacion
     *
     * @param reg registro al que se le va a cambiar el formato de rep.
     */
    private void iterateFormats(MouseEvent e) {
        int reg = Integer.parseInt(((JLabel) e.getSource()).getName());
        registerFormat[reg] = (registerFormat[reg] + 1) % Configuration.NFORMATS;
        update();
    }

    public int getRegisterFileFormat() {
        for (JRadioButton rb : formatsRadioButtons) {
            if (rb.isSelected()) {
                switch (rb.getName()) {
                    case Configuration.HEXS:
                        return Configuration.HEX;
                    case Configuration.INTS:
                        return Configuration.INT;
                    case Configuration.UINTS:
                        return Configuration.UINT;
                    default:
                        return Configuration.HEX;
                }
            }
        }
        return Configuration.HEX;
    }

    public int[] getRegisterFormat() {
        return registerFormat;
    }

    public void setRegisterFileFormat(int[] f) {
        System.arraycopy(f, 0, registerFormat, 0, 
                Math.min(registerFormat.length, f.length));
        update();
    }
}
