/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.ToolTipManager;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionCellRenderer;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.LanguageAwareCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.ToolTipSupplier;

/**
 *
 * @author corbera
 */
public class ARMAsmAutoCompletion {

    private static final String armInstDescription = "resources/arm7tdmi.xml";
    private AutoCompletion ac;
    private CompletionProvider provider;
    private RSyntaxTextArea textArea;
    private static final Color ALT_BG_COLOR = new Color(0xf0f0f0);

    public ARMAsmAutoCompletion(RSyntaxTextArea ta) {
        textArea = ta;
        provider = createCompletionProvider();
        // Install auto-completion onto our text area.
        ac = new AutoCompletion(provider);
        ac.setListCellRenderer(new ARMAsmCellRenderer());
        ac.setShowDescWindow(true);
        ac.setParameterAssistanceEnabled(true);
        ac.install(textArea);
        textArea.setToolTipSupplier((ToolTipSupplier) provider);
        ToolTipManager.sharedInstance().registerComponent(textArea);
        CompletionCellRenderer.setAlternateBackground(
                ALT_BG_COLOR);
    }

    /**
     * Returns the provider to use when editing code.
     *
     * @return The provider.
     * @see #createCommentCompletionProvider()
     * @see #createStringCompletionProvider()
     */  
    private CompletionProvider createCodeCompletionProvider() {

        // Add completions for the C standard library.
        DefaultCompletionProvider cp = new DefaultCompletionProvider();

        // First try loading resource (running from demo jar), then try
        // accessing file (debugging in Eclipse).
        ClassLoader cl = getClass().getClassLoader();
        InputStream in = cl.getResourceAsStream(armInstDescription);
        //InputStream in = cl.getResourceAsStream("resources/c.xml");
        try {
            if (in != null) {
                cp.loadFromXML(in);
                in.close();
            } else {
                cp.loadFromXML(new File(armInstDescription));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        // Add some handy shorthand completions.
        //cp.addCompletion(new ShorthandCompletion(cp, "main",
        //					"int main(int argc, char **argv)"));
        return cp;

    }

    /**
     * Returns the provider to use when in a comment.
     *
     * @return The provider.
     * @see #createCodeCompletionProvider()
     * @see #createStringCompletionProvider()
     */
    private CompletionProvider createCommentCompletionProvider() {
        DefaultCompletionProvider cp = new DefaultCompletionProvider();
        cp.addCompletion(new BasicCompletion(cp, "TODO:", "A to-do reminder"));
        cp.addCompletion(new BasicCompletion(cp, "FIXME:", "A bug that needs to be fixed"));
        return cp;
    }

    /**
     * Creates the completion provider for a C editor. This provider can be
     * shared among multiple editors.
     *
     * @return The provider.
     */
    private CompletionProvider createCompletionProvider() {

        // Create the provider used when typing code.
        CompletionProvider codeCP = createCodeCompletionProvider();

        // The provider used when typing a string.
        CompletionProvider stringCP = createStringCompletionProvider();

        // The provider used when typing a comment.
        CompletionProvider commentCP = createCommentCompletionProvider();

        // Create the "parent" completion provider.
        LanguageAwareCompletionProvider provider = new LanguageAwareCompletionProvider(codeCP);
        provider.setStringCompletionProvider(stringCP);
        provider.setCommentCompletionProvider(commentCP);

        return provider;

    }

    /**
     * Returns the completion provider to use when the caret is in a string.
     *
     * @return The provider.
     * @see #createCodeCompletionProvider()
     * @see #createCommentCompletionProvider()
     */
    private CompletionProvider createStringCompletionProvider() {
        DefaultCompletionProvider cp = new DefaultCompletionProvider();
        cp.addCompletion(new BasicCompletion(cp, "%c", "char", "Prints a character"));
        cp.addCompletion(new BasicCompletion(cp, "%i", "signed int", "Prints a signed integer"));
        cp.addCompletion(new BasicCompletion(cp, "%f", "float", "Prints a float"));
        cp.addCompletion(new BasicCompletion(cp, "%s", "string", "Prints a string"));
        cp.addCompletion(new BasicCompletion(cp, "%u", "unsigned int", "Prints an unsigned integer"));
        cp.addCompletion(new BasicCompletion(cp, "\\n", "Newline", "Prints a newline"));
        return cp;
    }

}
