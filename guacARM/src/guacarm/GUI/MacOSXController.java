/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

/**
 *
 * @author corbera
 */
import javax.swing.JOptionPane;
//import com.apple.mrj.MRJAboutHandler;
//import com.apple.mrj.MRJPrefsHandler;
//import com.apple.mrj.MRJQuitHandler;
import guacarm.Configuration;
import java.awt.Desktop;
import java.awt.Font;
import java.net.URI;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class MacOSXController
{ //implements MRJAboutHandler, MRJQuitHandler, MRJPrefsHandler {

    public void handleAbout() {
        String title = Configuration.APPNAME + " " + Configuration.VERSION;
        String about = title 
                + "<br>" + "Francisco Corbera <br>"
                + "Computer Architecture Departament <br>"
                + "University of Malaga <br>";
        // for copying style
        JLabel label = new JLabel();
        Font font = label.getFont();

        // create some css from the label's font
        StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
        style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
        style.append("font-size:" + font.getSize() + "pt;");

        // html content
        JEditorPane ep = new JEditorPane("text/html", "<html><body style=\"" + style + "\">" //
                + about + "<a href=\"https://bitbucket.org/UMADACTC/guacarm/\">Source code</a>" //
                + "</body></html>");

        // handle link events
        ep.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                    try {
                        Desktop desktop = java.awt.Desktop.getDesktop();
                        URI oURL = new URI(e.getURL().toString());
                        desktop.browse(oURL);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    //ProcessHandler.launchUrl(e.getURL().toString()); // roll your own link launcher or use Desktop if J6+
                }
            }
        });
        ep.setEditable(false);
        ep.setBackground(label.getBackground());

        // show
        JOptionPane.showMessageDialog(null, ep, title, JOptionPane.INFORMATION_MESSAGE);

//        JOptionPane.showMessageDialog(null,
//                about,
//                Configuration.APPNAME+" "+Configuration.VERSION,
//                JOptionPane.INFORMATION_MESSAGE);
    }

    public void handlePrefs() throws IllegalStateException {
        JOptionPane.showMessageDialog(null,
                "prefs",
                "prefs",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void handleQuit() throws IllegalStateException {
        System.exit(0);
    }
}
