/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import java.net.URL;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;
import javax.swing.WindowConstants;

/**
 *
 * @author corbera
 */
public class HelpWindow extends javax.swing.JFrame {

    private static final String INDEX = "/resources/docs/help.html";
    private URL rootUrl;

    /**
     * Creates new form HelpWindow2
     */
    public HelpWindow() {
        myInitComponents();
        rootUrl = getClass().getResource(INDEX);
        //System.out.println(rootUrl);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        Platform.runLater(() -> { // FX components need to be managed by JavaFX
            webView = new WebView();
            webView.getEngine().load(rootUrl.toExternalForm());
            webViewHistory = webView.getEngine().getHistory();
            jfxPanel1.setScene(new Scene(webView));
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextPane1 = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        rootjButton = new javax.swing.JButton();
        prevjButton = new javax.swing.JButton();
        nextjButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();

        jTextPane1.setPreferredSize(new java.awt.Dimension(800, 600));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(800, 600));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        rootjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/homeBN32.png"))); // NOI18N
        rootjButton.setToolTipText("Root");
        rootjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rootjButtonActionPerformed(evt);
            }
        });

        prevjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/backwardBN32.png"))); // NOI18N
        prevjButton.setToolTipText("Previous");
        prevjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevjButtonActionPerformed(evt);
            }
        });

        nextjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/forwardBN32.png"))); // NOI18N
        nextjButton.setToolTipText("Next");
        nextjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(rootjButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prevjButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nextjButton)
                        .addGap(0, 461, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rootjButton)
                    .addComponent(prevjButton)
                    .addComponent(nextjButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void myInitComponents() {
        jfxPanel1 = new JFXPanel();
        jPanel1 = new javax.swing.JPanel();
        rootjButton = new javax.swing.JButton();
        prevjButton = new javax.swing.JButton();
        nextjButton = new javax.swing.JButton();

        jfxPanel1.setPreferredSize(new java.awt.Dimension(800, 600));

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(800, 600));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        rootjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/homeBN32.png"))); // NOI18N
        rootjButton.setToolTipText("Root");
        rootjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rootjButtonActionPerformed(evt);
            }
        });

        prevjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/backwardBN32.png"))); // NOI18N
        prevjButton.setToolTipText("Previous");
        prevjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevjButtonActionPerformed(evt);
            }
        });

        nextjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/forwardBN32.png"))); // NOI18N
        nextjButton.setToolTipText("Next");
        nextjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(rootjButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(prevjButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(nextjButton)
                                                .addGap(0, 461, Short.MAX_VALUE))
                                        .addComponent(jfxPanel1, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(rootjButton)
                                        .addComponent(prevjButton)
                                        .addComponent(nextjButton))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jfxPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }

    private void rootjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rootjButtonActionPerformed
        Platform.runLater(() -> { // FX components need to be managed by JavaFX
            webView.getEngine().load(rootUrl.toExternalForm());
            updateButtons();
        });
    }//GEN-LAST:event_rootjButtonActionPerformed

    private void prevjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevjButtonActionPerformed
        Platform.runLater(() -> { // FX components need to be managed by JavaFX
            if (webViewHistory != null && webViewHistory.getCurrentIndex() > 0) {
                webViewHistory.go(-1);
            }
            updateButtons();
        });
    }//GEN-LAST:event_prevjButtonActionPerformed

    private void nextjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextjButtonActionPerformed
        Platform.runLater(() -> { // FX components need to be managed by JavaFX
            if (webViewHistory != null && webViewHistory.getCurrentIndex() < webViewHistory.getEntries().size() - 1) {
                webViewHistory.go(1);
            }
            updateButtons();
        });
    }//GEN-LAST:event_nextjButtonActionPerformed

    private void updateButtons() {
        nextjButton.setEnabled(
                webViewHistory != null && webViewHistory.getCurrentIndex() < webViewHistory.getEntries().size() - 1);
        prevjButton.setEnabled(
                webViewHistory != null && webViewHistory.getCurrentIndex() > 0);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JButton nextjButton;
    private javax.swing.JButton prevjButton;
    private javax.swing.JButton rootjButton;
    // End of variables declaration//GEN-END:variables
    private JFXPanel jfxPanel1;
    private WebView webView;
    private WebHistory webViewHistory;
}
