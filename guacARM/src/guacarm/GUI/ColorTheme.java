/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import java.awt.Color;

/**
 *
 * @author casa
 */
public class ColorTheme {
    public static final Color BACKGROUND = new Color(41, 49, 52);
    public static final Color UPDATED = Color.CYAN;
    public static final Color FOREGROUND = Color.LIGHT_GRAY;
    public static final Color TITLE = Color.WHITE;
    public static final Color WARNING = new Color(212, 49, 17);
    public static final Color BREAKPOINT = new Color(255, 60, 0);
    public static final Color HEXC = Color.LIGHT_GRAY;
    public static final Color INTC = new Color(230, 255, 204);
    public static final Color UINTC = new Color(255, 217, 179);
    public static final Color[] FORMATC = {HEXC, INTC, UINTC};
    public static final Color HIHGLIGHT = Color.RED;
    public static final Color DEBUG = INTC;
}
