/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.GUI;

import guacarm.log.Log;
import java.io.File;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Corbera
 */
public class FileActions {

    private final JFileChooser fileChooser;
    private final Log log;

    public FileActions(Log log) {
        this(null, log);
    }

    public FileActions(String currentDirectory, Log log) {
        fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new asmFileFilter());
        fileChooser.setAcceptAllFileFilterUsed(false);
        File workingDirectory = new File(
                (currentDirectory == null)
                        ? System.getProperty("user.dir") : currentDirectory);
        fileChooser.setCurrentDirectory(workingDirectory);
        fileChooser.setMultiSelectionEnabled(true);
        this.log = log;
    }

    public void setCurrentDirectory(String currentDirectory) {
        File workingDirectory = new File(currentDirectory);
        fileChooser.setCurrentDirectory(workingDirectory);
    }

    public boolean fileMenuLoadAction(JTabbedPane jtp, Callable notify) {
        return fileMenuLoadAction(jtp, null, notify);
    }

    public boolean fileMenuLoadAction(JTabbedPane jtp, List<String> _files, Callable notify) {
        try {
            if (_files == null) {
                int returnVal = fileChooser.showOpenDialog(jtp.getSelectedComponent());
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File[] files = fileChooser.getSelectedFiles();
                    files = notOpenFiles(files, jtp);
                    if (files != null) {
                        for (File file : files) {
                            RSyntaxtCodePane rsc = new RSyntaxtCodePane(notify, log);
                            jtp.add("", rsc);
                            jtp.setSelectedComponent(rsc);
                            rsc.associateFile = file;
                            rsc.setText();
                            jtp.setTitleAt(jtp.indexOfComponent(rsc), file.getName());
                            rsc.modified = false;
                        }
                        return true;
                    }
                } else {
                    System.out.println("Open command cancelled by user.");
                }
            } else {
                for (String fileName : _files) {
                    File file = new File(fileName);
                    if (!file.exists()) {
                        log.println("Error loading file " + fileName);
                        continue;
                    }
                    RSyntaxtCodePane rsc = new RSyntaxtCodePane(notify, log);
                    jtp.add("", rsc);
                    jtp.setSelectedComponent(rsc);
                    rsc.associateFile = file;
                    rsc.setText();
                    jtp.setTitleAt(jtp.indexOfComponent(rsc), file.getName());
                    rsc.modified = false;
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void fileMenuSaveAction(JTabbedPane jtp, RSyntaxtCodePane rsc) {
        if (rsc.binary) {
            return;
        }
        try {
            if (rsc.associateFile != null) {
                rsc.saveText();
                jtp.setTitleAt(jtp.indexOfComponent(rsc),
                        rsc.associateFile.getName());
                rsc.modified = false;
            } else {
                fileMenuSaveAsAction(jtp, rsc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Recorre todas las tabs y llama al metodo para salvar el conetenido de
     * cada una de ellas a fichero.
     *
     * @param jtp
     */
    public void fileMenuSaveAllAction(JTabbedPane jtp) {
        for (int i = 0; i < jtp.getComponentCount(); i++) {
            fileMenuSaveAction(jtp, (RSyntaxtCodePane) jtp.getComponentAt(i));
        }
    }

    public void fileMenuSaveAsAction(JTabbedPane jtp, RSyntaxtCodePane rsc) {
        if (rsc.binary) {
            return;
        }
        try {
            int retrival;
            String filename = jtp.getTitleAt(jtp.indexOfComponent(rsc));
            if (filename.endsWith("*")) {
                filename = filename.substring(0, filename.length() - 1);
            }
            fileChooser.setSelectedFile(new File(filename));
            retrival = fileChooser.showSaveDialog(jtp.getSelectedComponent());
            if (retrival == JFileChooser.APPROVE_OPTION) {
                rsc.associateFile = fileChooser.getSelectedFile();
                rsc.saveText();
                rsc.modified = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean fileMenuCloseAction(JTabbedPane jtp, RSyntaxtCodePane rsc) {
        return fileMenuCloseAction(true, false, jtp, rsc);
    }

    /**
     * Cierra una pestaña de codigo.
     *
     * @param ask
     * @param force
     * @param jtp
     * @param rsc
     * @return true si la pestaña cerrada es la de simulacion
     */
    public boolean fileMenuCloseAction(boolean ask, boolean force, JTabbedPane jtp, RSyntaxtCodePane rsc) {
        boolean bin = rsc.binary;
        if (rsc.binary || !rsc.modified
                || rsc.textArea.getText() == null || rsc.textArea.getText().equals("")) {

            jtp.remove(rsc);
            return bin;
        }
        if (ask) {
            int op = JOptionPane.showConfirmDialog(jtp.getSelectedComponent(),
                    "Save file "
                    + jtp.getTitleAt(jtp.indexOfComponent(rsc))
                    + "?",
                    "Close confirmation",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (op == 0) { // Save
                fileMenuSaveAction(jtp, rsc);
                jtp.remove(rsc);
            } else if (op == 1) { // Close sin salvar
                jtp.remove(rsc);
            }
        } else if (!force) {
            fileMenuSaveAction(jtp, rsc);
            jtp.remove(rsc);
        } else {
            jtp.remove(rsc);
        }
        return bin;
    }

    public void fileMenuCloseAllAction(JTabbedPane jtp) {
        boolean force = false;
        RSyntaxtCodePane[] tabs = new RSyntaxtCodePane[jtp.getComponentCount()];
        if (tabs.length == 0) {
            return;
        }
        if (areThereModified(jtp)) {
            int op = JOptionPane.showConfirmDialog(jtp,
                    "Save modified files before close?",
                    "Close confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            switch (op) {
                case 0: // save
                    force = false;
                    break;
                case 1:// Close sin salvar
                    force = true;
                    break;
            }
        } else {
            force = true;
        }
        for (int i = 0; i < tabs.length; i++) {
            tabs[i] = (RSyntaxtCodePane) jtp.getComponentAt(i);
        }
        for (RSyntaxtCodePane tab : tabs) {
            fileMenuCloseAction(false, force, jtp, tab);
        }
    }

    private boolean areThereModified(JTabbedPane jtp) {
        for (Component rsc : jtp.getComponents()) {
            if (!((RSyntaxtCodePane) rsc).binary
                    && ((RSyntaxtCodePane) rsc).modified) {
                return true;
            }
        }
        return false;
    }

    private File[] notOpenFiles(File[] files, JTabbedPane jtp) {
        ArrayList<File> out = new ArrayList<>();
        for (File f : files) {
            boolean open = false;
            for (Component rsc : jtp.getComponents()) {
                if (((RSyntaxtCodePane) rsc).binary) {
                    continue;
                }
                if (f.getAbsoluteFile().compareTo(((RSyntaxtCodePane) rsc).associateFile.getAbsoluteFile()) == 0) {
                    open = true;
                    break;
                }
            }
            if (!open) {
                out.add(f);
            }
        }
        if (out.size() > 0) {
            return (File[]) out.toArray(new File[1]);
        }
        return null;
    }

    public String getCurrentDirectory() {
        return fileChooser.getCurrentDirectory().getAbsolutePath();
    }

    public String getPluginFileName() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new JarFileFilter());
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setCurrentDirectory(this.fileChooser.getCurrentDirectory());

        int returnVal = fileChooser.showOpenDialog(fileChooser);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file != null) {
                return file.getAbsolutePath();
            }
        }
        return null;
    }

    private class asmFileFilter extends FileFilter {

        private final String[] extensions = {".asm", ".txt", ".s"};

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            }
            if (file.isFile()) {
                String path = file.getAbsolutePath().toLowerCase();
                for (int i = 0, n = extensions.length; i < n; i++) {
                    String extension = extensions[i];
                    if (path.endsWith(extension)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            return "Filter for ARM assembly files";
        }
    }

    private class JarFileFilter extends FileFilter {

        private final String[] extensions = {".jar"};

        @Override
        public boolean accept(File file) {
            if (file.isFile()) {
                String path = file.getAbsolutePath().toLowerCase();
                for (int i = 0, n = extensions.length; i < n; i++) {
                    String extension = extensions[i];
                    if (path.endsWith(extension)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public String getDescription() {
            return "Filter for JAR files";
        }
    }
}
