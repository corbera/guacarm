/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm;

import guacarm.log.ConsoleLog;
import guacarm.log.Log;
import guacarm.plugins.PluginManager;
import guacarm.simulator.Simulator;
import guacarm.simulator.Assembler;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.apache.commons.cli.*;

/**
 *
 * @author Corbera
 */
public class guacARM {

    /**
     * Open the main window
     */
    private static void startGUI() {
        JFrame frame = new AppWindow();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    /**
     * Run the application in CLI mode
     */
    private static void startCLI(String[] args) {
        Log consoleLog = new ConsoleLog();
        String[] address = null;
        String[] segments = null;
        String[] registers = null;
        String[] files = null;
        String[] plugins = null;
        int delay = 10000000;

        Options options = new Options();
        // establecemos las opciones
        Option opt = Option.builder("m").argName("off1hex:off2hex ...").hasArgs().desc("Memory ranges to print: [off1hex-off2hex)").build();
        options.addOption(opt);
        opt = Option.builder("s").argName("(data|code|stack):size").hasArgs().desc("Memory segment and size to print: data:16").build();
        options.addOption(opt);
        opt = Option.builder("r").argName("i j ...").hasArgs().desc("Registers to print").build();
        options.addOption(opt);
        opt = Option.builder("f").argName("file1 file2 ...").hasArgs().desc("Source code files").build();
        options.addOption(opt);
        opt = Option.builder("t").argName("time us").hasArg().desc("Max time of simulation in microseconds").build();
        options.addOption(opt);
        opt = Option.builder("p").argName("plugin.jar").hasArg().desc("Plugin to load").build();
        options.addOption(opt);

        // Parseamos las opciones
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmdLine = parser.parse(options, args);

            if (cmdLine.hasOption("m")) {
                address = cmdLine.getOptionValues("m");
            }
            if (cmdLine.hasOption("s")) {
                segments = cmdLine.getOptionValues("s");
            }
            if (cmdLine.hasOption("r")) {
                registers = cmdLine.getOptionValues("r");
            }
            if (cmdLine.hasOption("f")) {
                files = cmdLine.getOptionValues("f");
            } else {
                new HelpFormatter().printHelp(guacARM.class.getCanonicalName() + " filename", options, true);    // Error, imprimimos la ayuda                 
                System.exit(0);
            }
            if (cmdLine.hasOption("t")) {
                int t = 0;
                try {
                    t = Integer.parseInt(cmdLine.getOptionValue("t"));
                } catch (Exception e) {
                }
                if (true) {//t != 0) {
                    delay = t;
                }
                //System.out.println(delay);
            }
            if (cmdLine.hasOption("p")) {
                plugins = cmdLine.getOptionValues("p");
            }

        } catch (org.apache.commons.cli.ParseException ex) {
            consoleLog.println(ex.getMessage());
            new HelpFormatter().printHelp(guacARM.class.getCanonicalName() + " filename", options, true);    // Error, imprimimos la ayuda 
            System.exit(0);
        } catch (java.lang.NumberFormatException ex) {
            new HelpFormatter().printHelp(guacARM.class.getCanonicalName() + " filename", options, true);    // Error, imprimimos la ayuda  
            System.exit(0);
        }

        // Ensamblamos el codigo fuente pasado como argumento
        consoleLog.println("Assembling ...");
        Assembler asm = new Assembler();
        if (asm.assembleFiles(files) != null) {
            consoleLog.println("Error assembling.");
            System.exit(-1);
        }
        consoleLog.println("Assembling done.");

        //consoleLog.println(asm.elfDumpOut);
        // informamos si ha habido error
        for (int i = 0; i < asm.errorno.length; i++) {
            if (asm.errorno[i] != 0) {
                consoleLog.println(asm.stdout[i]);
                consoleLog.println(asm.stderr[i]);
                System.exit(-1);
            }
        }

        // si no ha habido error, pasamos a la simulacion
        final Simulator simulator = new Simulator(consoleLog);
        simulator.initialize(
                asm.textSegment,
                asm.dataSegment,
                asm.entryPoint,
                asm.textOff,
                asm.dataOff,
                0x400
        );

        /**
         * Plugins *
         */
        consoleLog.println("Loading plugins ..");
        PluginManager plmanager;
        if (plugins != null) {
            plmanager = new PluginManager(simulator, consoleLog, new Configuration());
            for (String plname : plugins) {
                if (plmanager.loadConsolePlugin(plname) == null) {
                    System.exit(-1);
                }
            }
        }

        simulator.setVerboseMode(true);

        consoleLog.println("Simulating ..");

        final int fdelay = delay * 2;
        boolean interr = false;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        try {
            List<Future<Integer>> future = executor.invokeAll(Arrays.asList(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    simulator.run(fdelay);
                    return 255;
                }
            }), (long) (delay), TimeUnit.MICROSECONDS);
            try {
                if (future.size() > 0 && future.get(0).get() == 255) {
                    interr = true;
                }
            } catch (Exception ex) {
                interr = false;
            }
        } catch (Exception ex) {
            interr = false;
        }
        executor.shutdown();

        if (interr) {
            consoleLog.println("Done.");
        } else {
            consoleLog.println("Simulation timeout.");
        }

        if (registers != null) {
            consoleLog.println("Registers:");
            for (String r : registers) {
                r = r.trim().toUpperCase();
                int reg = simulator.getRegisterFile().getRegNumber(r);
                if (reg < 0 || reg > 16) {
                    consoleLog.println(r + " - Register not valid (number of register)");
                } else {
                    int val = 0;
                    try {
                        val = simulator.getRegisterFile().readRegister(reg);
                    } catch (Exception ex) {
                        Logger.getLogger(guacARM.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    consoleLog.println(r + ": " + String.format("0x%08X", val));
                }
            }
        }
        if (address != null) {
            consoleLog.println("Memory:");
            for (String a : address) {
                a = a.trim().toUpperCase();
                int sep = a.indexOf(":");
                if (sep < 0) {
                    consoleLog.println(a + " - Memory range not valid (off1hex:off2hex)");
                } else {
                    int off1 = Integer.parseInt(a.substring(0, sep), 16);
                    int off2 = Integer.parseInt(a.substring(sep + 1), 16);
                    try {
                        byte[] mem = simulator.readMem(off1, off2 - off1);
                        consoleLog.println(Utils.memToDump(off1, mem, 0, 4, 4, true));
                    } catch (Exception e) {
                        consoleLog.println(a + " - Memory range can't be readed: " + e.getMessage());
                    }
                }
            }
        }
        if (segments != null) {
            consoleLog.println("Segments:");
            for (String s : segments) {
                s = s.trim().toUpperCase();
                int sep = s.indexOf(":");
                if (sep < 0) {
                    consoleLog.println(s + " - Segment specification not valid (segment:size)");
                } else {
                    String segment = s.substring(0, sep);
                    int size = Integer.parseInt(s.substring(sep + 1));
                    int addr = 0;
                    if (segment.equals("DATA")) {
                        addr = simulator.getDataBaseAddr();
                    } else if (segment.equals("CODE")) {
                        addr = simulator.getCodeBaseAddr();
                    } else if (segment.equals("STACK")) {
                        addr = simulator.getStackBaseAddr();
                    } else {
                        consoleLog.println(s + " - Segment name not valid (data|code|stack)");
                        return;
                    }
                    try {
                        byte[] mem = simulator.readMem(addr, size);
                        consoleLog.println(s);
                        consoleLog.println(Utils.memToDump(addr, mem, 0, 4, 4, true));
                    } catch (Exception e) {
                        consoleLog.println(s + " - Memory range can't be readed: " + e.getMessage());
                    }
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.setProperty("java.library.path", ".:" + System.getProperty("java.library.path"));
        //System.out.println(System.getProperty("java.library.path"));
        //System.out.println("Working Directory = " +System.getProperty("user.dir"));
        if (args.length > 0) {
            startCLI(args);
        } else {
            startGUI();
        }
    }
}
