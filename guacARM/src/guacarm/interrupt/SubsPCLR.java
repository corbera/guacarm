/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.interrupt;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase para identificar las instrucciones subs pc, lr ya que son la manera de
 * retornar de las RTIs y unicon no las gestiona bien
 *
 * @author corbera
 */
public class SubsPCLR {

    private static final int instID = 0x025ef000;
    private static final int instMask = 0x0ffff000;

    /**
     * Informa si una instrucción dada es una subs pc, lr
     *
     * @param inst instruccion a estudiar
     * @return true si es una instrucción subs pc, lr
     */
    public static boolean isSubsPCLR(int inst) {
        return (inst & instMask) == instID;
    }

    /**
     * Busca en un codigo dado (array de bytes) las instrucciones subs pc, lr y
     * devuelve las posiciones en el array donde comienzan
     *
     * @param code codigo a estudiar
     * @return lista con las posiciones del codigo donde empiezan las
     * instruccione subs pc, lr
     */
    public static List<Integer> findSubsPCLR(byte[] code, int offset) {
        List<Integer> l = new ArrayList<>();
        for (int i = 0; i < code.length; i += 4) {
            if (isSubsPCLR(bytesToInt(code, i))) {
                l.add(i+offset);
            }
        }
        return l;
    }

    /**
     * Para una instruccion subs pc, lr, #inm, devuelve el valor de inm
     * @param inst instruccion a examinar
     * @return valor de operando inmediato inm
     */
    public static int getInmediateValue(int inst) {
        int inm = inst & 0x0ff;
        int rot = (inst & 0x0f00) >>> 7;
        return (inm >>> rot) | (inm << (Integer.SIZE - rot));
    }
    
    /**
     * transforma 4 bytes en un entero (el primero el menos significativo)
     *
     * @param bytes array con los bytes
     * @param pos posicion del primer byte
     * @return entero que forman los 4 primeros bytes
     */
    public static int bytesToInt(byte[] bytes, int pos) {
        return (bytes[pos + 3] & 0x0ff) << 24
                | (bytes[pos + 2] & 0x0ff) << 16
                | (bytes[pos + 1] & 0x0ff) << 8
                | (bytes[pos] & 0x0ff);
    }
}
