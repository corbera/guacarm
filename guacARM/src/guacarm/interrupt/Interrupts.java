/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.interrupt;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.simulator.Simulator;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import unicorn.Unicorn;

/**
 * Clase para dar soporte a las interrupciones IRQ y FIQ
 *
 * @author corbera
 */
public class Interrupts {

    public enum InterType {
        IRQ, FIQ
    };

    private static final int IRQMASK = 0x080; // bit 7
    private static final int FIQMASK = 0x040; // bit 6
    private static final int INTTABLEBASEADDR = 0x0; // dirección base de la tabla de interrupciones
    private static final int IRQVECT = 0x18; // desplazamiento del vector de interrupciones para IRQ
    private static final int FIQVECT = 0x1C; // desplazamiento del vector de interrupciones para FIQ

    private int[] spsr;
    private int[] cpsrMode;
    private int[] pending;
    private boolean enable;
    private boolean onflyInterrupt = false;
    
    private final Simulator simulator;
    private final Log log;

    public Interrupts(Simulator simulator, Log log) {
        this.simulator = simulator;
        this.log = log;
        this.reset();
    }

    public void reset() {
        this.spsr = new int[]{0, 0}; // valor del registro SPSR de los modos FIQ e IRQ respectivamente
        this.cpsrMode = new int[]{0x011, 0x012}; // bits de modo M[4:0] del registro CPSR para FIQ e IRQ respectivamente
        this.pending = new int[]{0, 0}; // no hay interrupciones pendientes
        this.enable = false;
        onflyInterrupt = false;        
    }
    
    /**
     * Activa las interrupciones en el simulador
     */
    public void enableInterruptions() {
        enable = true;
        simulator.services.registerEndOfInterruption(new MyEndOfInterruption());
        simulator.services.activateEndOfInterruption();
    }

    /**
     * Desactiva las interrupciones
     */
    public void disableInterruptions() {
        enable = false;
        simulator.services.unRegisterEndInterruption();
    }

    /**
     * Indica que hay pendiente una interrupcion del tipo dado
     *
     * @param type tipo de la interrupcion
     */
    public void setPending(InterType type) {
        int indx = typeIndx(type);
        pending[indx] = pending[indx ^ 1] + 1;
    }

    /**
     * Devuelve el tipo de interrupcion que haya pendiente (prioridad a las FIQ)
     *
     * @return tipo de interrupcion pendiente
     */
    public List<InterType> getPending() {
        List<InterType> list = new ArrayList<>();
        if (enable) {
            if (pending[0] > 0 && (pending[0] < pending[1] || pending[1] == 0)) {
                list.add(InterType.FIQ);
                if (pending[1] > 0) {
                    list.add(InterType.IRQ);
                }
            } else if (pending[1] > 0 && (pending[1] < pending[0] || pending[0] == 0)) {
                list.add(InterType.IRQ);
                if (pending[0] > 0) {
                    list.add(InterType.FIQ);
                }
            }
        }
        return list;
    }

    /**
     * Callback que se llamara cuando termine una interrupcion
     */
    private class MyEndOfInterruption implements EndOfInterrupt {

        @Override
        public void callBack(Unicorn u, int addr) {
            endOfInterruption(addr);
        }

    }

    /**
     * Realiza las acciones asociadas con el final de una interrupcion en la
     * direccion dada
     *
     * @param addr direccion de la instruccion subs pc, lr
     */
    private void endOfInterruption(int addr) {
        // restauramos el valor de CPSR que previamente se salvo en el SPSR
        // del modo de interrupcion actual
        // para ve el modo actual, miramos el valor de CPSR
        int cpsr;
        try {
            int PC = simulator.readRegister(SimulatorInterface.REGISTER_PC);
            if (PC != addr) {
                return;
            }
            byte[] instb = simulator.readMem(addr, 4);
            int inst = SubsPCLR.bytesToInt(instb, 0);
            if (!SubsPCLR.isSubsPCLR(inst)) {
                //throw new Exception("Bad endOfInterruption at address 0x"+Integer.toHexString(addr));
                return;
            }
            int inm = SubsPCLR.getInmediateValue(inst);
            simulator.writeRegister(
                    SimulatorInterface.REGISTER_PC,
                    simulator.readRegister(SimulatorInterface.REGISTER_LR) - inm
            );
        } catch (Exception ex) {
            Logger.getLogger(Interrupts.class
                    .getName()).log(Level.SEVERE, null, ex);
            return;
        }
        try {
            cpsr = simulator.readRegister(SimulatorInterface.REGISTER_CPSR);
        } catch (Exception ex) {
            log.println("End of interruption: cannot read register CPSR.", ColorTheme.WARNING);
            return;
        }
        int typeInx = ((cpsr & 0x01F) == cpsrMode[0]) ? 0 : (((cpsr & 0x01F) == cpsrMode[1]) ? 1 : -1);
        try {
            simulator.writeRegister(SimulatorInterface.REGISTER_CPSR, spsr[typeInx]);
        } catch (Exception ex) {
            log.println("End of interruption: cannot write register CPSR." + ex.toString(), ColorTheme.WARNING);
        }
        //System.err.println("Salidendo " + typeInx);
        onflyInterrupt = false;
        List<InterType> p = getPending();
        if (p.size() > 0) {
            throwInterruption(p.get(0));
        }
    }

    /**
     * Provoca una interrupcion de un tipo dado
     *
     * @param type tipo de interrupcion (IRQ o FIQ)
     * @return true si se ha podido lanzar la interrupción (si esta habilitada)
     */
    public synchronized boolean throwInterruption(InterType type) {
        if (onflyInterrupt || !enable) {
            return false;
        }
        int cpsr;
        try {
            cpsr = simulator.readRegister(SimulatorInterface.REGISTER_CPSR);
        } catch (Exception ex) {
            log.println("Throwing interruption: cannot read register CPSR.", ColorTheme.WARNING);
            return false;
        }
        if (!isEnableInt(type, cpsr)) {
            return false;
        }
        onflyInterrupt = true;
        int typeInx = typeIndx(type);
        pending[typeInx] = 0;
        pending[typeInx ^ 1] = (pending[typeInx ^ 1] == 0) ? 0 : 1;
        // la interrupcion esta habilitada, ejecutar
        // salvar cpsr en el registro spsr del modo de interrupcion actual
        spsr[typeInx] = cpsr;
        // cambiar el contenido de cpsr para pasar al modo indicado por type
        try {
            int newCPSR = (cpsr & 0xffffffe0) | cpsrMode[typeInx]; // new mode
            newCPSR = newCPSR | 0xC0; // Disable IRQ & FIQ
            simulator.writeRegister(SimulatorInterface.REGISTER_CPSR, newCPSR);
        } catch (Exception ex) {
            log.println("Throwing interruption: cannot write register CPSR.", ColorTheme.WARNING);
            return false;
        }
        try {
            // almacenamos en lr la direccion de retorno que para estas interrupciones
            // es PC+8
            simulator.writeRegister(
                    SimulatorInterface.REGISTER_LR,
                    simulator.readRegister(SimulatorInterface.REGISTER_PC) + 4
            );
        } catch (Exception ex) {
            log.println("Throwing interruption: error accessing registers LR and PC", ColorTheme.WARNING);
            return false;
        }
        try {
            // se salta a la direccion de la RTI (vector) PC = addr
            simulator.writeRegister(SimulatorInterface.REGISTER_PC, getAddrIntVector(type));
        } catch (Exception ex) {
            log.println("Throwing interruption: error writing register PC", ColorTheme.WARNING);
            return false;
        }
        //System.err.println("Entrando en " + type);
        return true;
    }

    /**
     * Analiza el valor de CPSR para ver si el tipo de interrupcion dado esta
     * habilitado o no
     *
     * @param type tipo de interrupcion (FIQ o IRQ)
     * @param value valor del cspr
     * @return true si la interrupcion esta habilitada
     */
    private boolean isEnableInt(InterType type, int value) {
        return enable & (((type == InterType.FIQ) && ((value & FIQMASK) == 0))
                || ((type == InterType.IRQ) && ((value & IRQMASK) == 0)));
    }

    private int typeIndx(InterType type) {
        return (type == InterType.FIQ) ? 0 : 1;
    }

    private int getAddrIntVector(InterType type) {
        return INTTABLEBASEADDR + ((type == InterType.FIQ) ? FIQVECT : IRQVECT);
    }

}
