/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins;

import guacarm.Configuration;
import guacarm.GUI.ColorTheme;
import guacarm.GUI.FileActions;
import static guacarm.GUI.HighlightTab.highlightTab;
import guacarm.coreservices.CoreServices;
import guacarm.interrupt.Interrupts;
import guacarm.log.Log;
import guacarm.plugins.builtin.BuiltinPluginClass;
import guacarm.simulator.Simulator;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import guacarm.plugins.interfaces.PluginClass;
import guacarm.plugins.interfaces.PluginMemCallBack;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.Policy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.simulator.SimulatorHooks;
import java.util.Properties;
import javax.swing.JSeparator;

/**
 *
 * @author corbera
 */
public class PluginManager {

    private boolean GUI;
    private Simulator simulator;
    private Configuration config;
    private PluginSVC pluginsSVC;
    private CoreServices coreServices;
    private PluginMemHook pluginsMemHook;
    private final Map<PluginClass, PluginStuff> plugins;
    private Log log;
    private FileActions fileActions;
    private JTabbedPane pane;
    private JMenu menu;

    public PluginManager(Simulator s, Log l, Configuration conf) {
        this(s, l, conf, null, null, null, false);
    }

    /**
     * Manejador de plugins
     *
     * @param s referencia al simulador para pasarle la api a los plugins
     * @param l referencia al log de la applicacion para pasarlo a los plugins
     * @param fa referencia al objeto para leer ficheros (que se usará para leer
     * de fichero los plugins)
     * @param pane panel donde irán los UI de los plugins en la app
     * @param menu menu donde irán las entradas de menu para cada uno de los
     * plugins
     * @param GUI true si la app está corriendo en modo grafico
     */
    public PluginManager(Simulator s, Log l, Configuration conf, FileActions fa, JTabbedPane pane, JMenu menu, boolean GUI) {
        simulator = s;
        log = l;
        config = conf;
        plugins = new HashMap<>();
        pluginsSVC = new PluginSVC(simulator);
        pluginsMemHook = new PluginMemHook(simulator, log);
        coreServices = new CoreServices(simulator, log);
        this.GUI = GUI;
        this.pane = pane;
        this.menu = menu;
        fileActions = fa;
        initBuiltInPlugins();
    }

    /**
     * Registra el callback para un numero de servicio dado (si no esta
     * previamente registrado)
     *
     * @param svc numero del servicio a registrar
     * @param callBack callback asociado al servicio
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerSVC(int svc, PluginSVCCallBack callBack, PluginClass plID) {
        if (svc < (SimulatorHooks.NSVC >>> 1) && plugins.containsKey(plID)) {
            return pluginsSVC.registerSVC(svc, callBack, plID);
        }
        return false;
    }

    /**
     * Elimina el callback asociado al numero de servicio dado si pertence a
     * plugin que lo llama
     *
     * @param svc servicio a borrar
     * @param plID identificador del plugin
     * @return true si lo ha podido borrar
     */
    public boolean unRegisterSVC(int svc, PluginClass plID) {
        if (plugins.containsKey(plID)) {
            return pluginsSVC.unRegisterSVC(svc, plID);
        }
        return false;
    }

    /**
     * Registra un callback para los accesos de lectura a una region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura en la
     * region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        if (plugins.containsKey(plID)) {
            return pluginsMemHook.registerMemoryReadHook(offset, size, callback, plID);
        }
        return false;
    }

    /**
     * Registra un callback para los accesos de escritura a una region de
     * memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una escritura en
     * la region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryWriteHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        if (plugins.containsKey(plID)) {
            return pluginsMemHook.registerMemoryWriteHook(offset, size, callback, plID);
        }
        return false;
    }

    /**
     * Registra un callback para los accesos de lectura/escritura a una region
     * de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una
     * lectura/escritura en la region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        if (plugins.containsKey(plID)) {
            return pluginsMemHook.registerMemoryReadWriteHook(offset, size, callback, plID);
        }
        return false;
    }

    /**
     * Elimina el registro de un hook de memoria
     *
     * @param offset direccion de comienzo del bloque de memoria
     * @param size tamaño del bloque de memoria
     * @param plID identificador del plugin que se quiere quitar
     * @return true si lo ha eliminado con exito
     */
    public boolean unRegisterMemoryHook(int offset, int size, PluginClass plID) {
        if (plugins.containsKey(plID)) {
            return pluginsMemHook.unRegisterMemoryHook(offset, size, plID);
        }
        return false;
    }

    /**
     * Registra un nuevo plugin
     *
     * @param plID plugin
     * @return true si lo ha podido registrar
     */
    public boolean registerPlugIn(PluginClass plID) {
        plugins.put(plID, null);
        return true;
    }
    
    /**
     * Registra un nuevo plugin con su awt component asociado
     *
     * @param plID plugin
     * @param comp awt component asociado al plugin
     * @param jmi elemento de menu asociado al plugin
     * @param filename fichero asociado al plugin
     * @return true si lo ha podido registrar
     */
    public boolean registerPlugIn(PluginClass plID, Component comp, JMenu jmi, String filename) {
        PluginStuff ps = new PluginStuff();
        ps.awtComponent = comp;
        ps.jmenu = jmi;
        ps.filename = filename;
        plugins.put(plID, ps);
        return true;
    }

    /**
     * Elimina todos los servicios asociados al plugin
     *
     * @param plID identificador del plugin
     */
    public void unRegisterPlugIn(PluginClass plID) {
        if (plugins.containsKey(plID)) {
            pluginsSVC.unRegisterPlugIn(plID);
            pluginsMemHook.unRegisterPlugIn(plID);
            plugins.remove(plID);
            plID.finalizePlugIn();
        }
    }

    public void finalizeAllPlugins() {
        plugins.keySet().forEach(plID -> {
            plID.finalizePlugIn();
        });
    }
    
    /**
     * Indica si el servicio con identificador svc esta ya registrado
     *
     * @param svc numero de servicio
     * @return true si esta registrado
     */
    public boolean isRegisteredSVC(int svc) {
        return simulator.services.isRegisteredSVC(svc);
    }

    /**
     * Devuelve el awt component asociado a un plugin (null si no tiene)
     *
     * @param plID plugin
     * @return awt component asociado
     */
    public Component getAWTComponent(PluginClass plID) {
        return plugins.get(plID).awtComponent;
    }

    /**
     * Devuelve el menu asociado a un plugin (null si no tiene) para descargalo
     *
     * @param plID plugin
     * @return menu
     */
    public JMenu getMenu(PluginClass plID) {
        return plugins.get(plID).jmenu;
    }

    /**
     * Devuelve el nombre del fichero asociado a un plugin (null si no tiene)
     *
     * @param plID plugin
     * @return nombre del fichero .jar asociado
     */
    public String getFileName(PluginClass plID) {
        return plugins.get(plID).filename;
    }

    /**
     * Carga un plugin de un fichero .jar (plugin.Plugin)
     *
     * @param name nombre del fichero .jar que contiene el plugin
     * @param verbose true si queremos mensajes de error
     * @return true si lo ha podido cargar
     */
    private PluginClass loadPlugInFromFile(String name) {
        return loadPlugInFromFile(name, true);
    }

    /**
     * Carga un plugin de un fichero .jar (plugin.Plugin)
     *
     * @param name nombre del fichero .jar que contiene el plugin
     * @param verbose true si queremos mensajes de error
     * @return true si lo ha podido cargar
     */
    private PluginClass loadPlugInFromFile(String name, boolean verbose) {
        Policy.setPolicy(new PluginPolicy());
        System.setSecurityManager(new SecurityManager());

//        File authorizedJarFile = new File(name);
//        ClassLoader authorizedLoader = URLClassLoader.newInstance(new URL[]{authorizedJarFile.toURL()});
//        Plugin authorizedPlugin = (Plugin) authorizedLoader.loadClass("plugins.authorized.Authorized").newInstance();
//        authorizedPlugin.run();
        File unauthorizedJarFile = new File(name);
        ClassLoader unauthorizedLoader;
        try {
            unauthorizedLoader = URLClassLoader.newInstance(new URL[]{unauthorizedJarFile.toURL()});
        } catch (MalformedURLException ex) {
            if (verbose) {
                log.println("Malformed URL exception loading plugin", ColorTheme.WARNING);
                Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        PluginClass unauthorizedPlugin;
        try {
            unauthorizedPlugin = (PluginClass) unauthorizedLoader.loadClass("plugin.Plugin").newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            if (verbose) {
                log.println("Can't load plugin class (plugin.Plugin) from " + name, ColorTheme.WARNING);
                Logger.getLogger(PluginManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        if (unauthorizedPlugin != null) {
            for (PluginClass p : plugins.keySet()) {
                if (samePluginID(p, unauthorizedPlugin)) {
                    return null;
                }
            }
        }
        return unauthorizedPlugin;
    }

    /**
     * Retrun true if two plugins are the same
     *
     * @param a first plugin
     * @param b second plugin
     * @return true if a and b are the same plugin
     */
    private boolean samePluginID(PluginClass a, PluginClass b) {
        return ((a.getAuthor() == null ? b.getAuthor() == null : a.getAuthor().equals(b.getAuthor()))
                && (a.getName() == null ? b.getName() == null : a.getName().equals(b.getName())));
    }

    /**
     * Carga un plugin en modo consola dado el nombre del fichero jar que lo
     * contiene
     *
     * @param name nombre del fichero jar con el plugin
     * @return plugin (PluginClass) si lo ha podido cargar, null en otro caso
     */
    public PluginClass loadConsolePlugin(String name) {
        if (!GUI) {
            PluginClass pl = loadPlugInFromFile(name);
            if (pl != null
                    && registerPlugIn(pl)
                    && pl.initializeConsolePlugIn(new PluginApi(this, log, pl))) {
                return pl;
            }
        }
        return null;
    }

    /**
     * Carga un plugin en modo grafico, seleccionando el fichero, y creando un
     * menu para descargarlo
     *
     * @return plugin (PluginClass) si lo ha podido cargar, null en otro caso
     */
    public PluginClass loadGUIPlugin() {
        return loadGUIPlugin(null);
    }

    /**
     * Carga un plugin con el nombre del fichero dado en modo grafico. Si el
     * nombre es null, abre ventana de selección de ficheros
     *
     * @param name nombre del fichero, null para seleccionarlo
     * @return plugin (PluginClass) si lo ha podido cargar, null en otro caso
     */
    public PluginClass loadGUIPlugin(String name) {
        return loadGUIPlugin(name, true);
    }

    /**
     * Carga un plugin con el nombre del fichero dado en modo grafico. Si el
     * nombre es null, abre ventana de selección de ficheros
     *
     * @param name nombre del fichero, null para seleccionarlo
     * @param verbose true si queremos mensajes de error
     * @return plugin (PluginClass) si lo ha podido cargar, null en otro caso
     */
    public PluginClass loadGUIPlugin(String name, boolean verbose) {
        if (GUI) {
            if (name == null) {
                name = fileActions.getPluginFileName();
            }
            if (name != null) {
                PluginClass pl = loadPlugInFromFile(name, verbose);
                return registerAndInitializePlugin(pl, name);
            }
        }
        return null;
    }

    /**
     * Registra e inicializa un plugin.Si no se realiza todo el proceso completo
     * devuelve null, en caso contrario devuelve el propio plugin
     *
     * @param pl plugin a inicializar
     * @param filename nombre del fichero asociado al plugin
     * @return null si no se puede inicializar, pl en caso contrario
     */
    public PluginClass registerAndInitializePlugin(PluginClass pl, String filename) {
        if (pl != null && registerPlugIn(pl)) {
            if (!pl.initializeGUIPlugIn(new PluginApi(this, log, pl))) {
                log.println("Plugin " + pl.getShortName() + " can't be initialized", ColorTheme.WARNING);
                unRegisterPlugIn(pl);
            } else {
                Component comp = pl.getAWTComponent();
                JMenu jm = new JMenu(pl.getShortName());
                JMenuItem jmi = new JMenuItem("Unload");
                JMenu plmenu = pl.getJMenu();
                if (plmenu != null) {
                    for (Component c : plmenu.getMenuComponents()) {
                        jm.add(c);
                    }
                }
                jm.add(jmi);
                jmi.addActionListener(e -> {
                    unLoad(pl);
//                    unRegisterPlugIn(pl);
//                    if (comp != null) {
//                        pane.remove(comp);
//                    }
//                    menu.remove(jm);
                });
                if (registerPlugIn(pl, comp, jm, filename)) {
                    if (comp != null) {
                        pane.add(pl.getShortName(), comp);
                    }
                    menu.add(jm);
                    return pl;
                } else {
                    log.println("Plugin " + pl.getShortName() + " can't be registered", ColorTheme.WARNING);
                    unRegisterPlugIn(pl);
                }

            }
        }
        return null;
    }

    /**
     * Carga todos los plugins asociados a la lista de ficheros
     *
     * @param list lista de strings con los nombres de los ficheros
     * @return lista con los plugins cargados
     */
    public List<PluginClass> loadGUIPlugins(List<String> list) {
        List<PluginClass> plist = null;
        if (list != null) {
            for (String filename : list) {
                PluginClass plugin = loadGUIPlugin(filename, false);
                if (plugin != null) {
                    if (plist == null) {
                        plist = new ArrayList<>();
                    }
                    plist.add(plugin);
                }
            }
        }
        return plist;
    }

    /**
     * Descarga el plugin dado
     *
     * @param plugin plugin a descargar
     */
    public void unLoad(PluginClass plugin) {
        PluginStuff stuff = plugins.get(plugin);
        if (stuff != null) {
            if (stuff.awtComponent != null) {
                pane.remove(stuff.awtComponent);
            }
            if (stuff.jmenu != null) {
                menu.remove(stuff.jmenu);
            }
        }
        enableBuiltInPlugin(plugin);
        unRegisterPlugIn(plugin);
    }

    /**
     * Devuelve la lista de todos los nombres de los ficheros asociados a los
     * plugins cargados (con path absoluto)
     *
     * @return lista de strings con los nombres de los ficheros
     */
    public List<String> getPluginsFileNames() {
        int nPlugins = plugins.size();
        List<String> names = null;
        if (nPlugins > 0) {
            names = new ArrayList<>();
            for (Map.Entry<PluginClass, PluginStuff> entry : plugins.entrySet()) {
                if (entry.getValue().filename != null) {
                    names.add(entry.getValue().filename);
                }
            }
        }
        return names;
    }

    /**
     * Devuelve array de los nombres de los builtin plugins que están activos
     *
     * @return
     */
    public String[] getBuiltInPluginsState() {
        String[] out = new String[builtinPlugins.size()];
        int count = 0;
        for (Pair<BuiltinPluginClass, JMenuItem> pair : builtinPlugins.values()) {
            out[count++] = pair.b.isEnabled() ? BUILTINPLUGIS.NONE.toString() : pair.a.getType().toString();
        }
        return out;
    }

    /**
     * Carga todos los builtin plugins indicados
     *
     * @param pls nombres de los plugins
     */
    public void loadBuiltInPlugins(String[] pls) {
        for (String name : pls) {
            try {
                BUILTINPLUGIS type = BUILTINPLUGIS.valueOf(name);
                if (!type.equals(BUILTINPLUGIS.NONE)) {
                    loadBuiltinPlugin(type);
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * Realiza las acciones necesarias cuando un plugin llama la atencion a la
     * applicacion (modificacion de us AWT component)
     *
     * @param plugin plugin que llama la atención
     */
    public void awtComponentModified(PluginClass plugin) {
        PluginStuff stuff = plugins.get(plugin);
        if (stuff != null) {
            highlightTab(stuff.awtComponent);
        }
    }

    /**
     * Resetea todos los plugins
     */
    public void resetPlugins() {
        plugins.keySet().forEach((pl) -> {
            pl.reset(simulator.api);
        });
    }

    public void testPlugins() {
        // Para probar y depurar plugins dentro de la applicacion
        registerAndInitializePlugin(new guacarm.plugins.builtin.berryclip.Plugin(), null);
    }

    /**
     * Gestion de los plubgins incluidos en la aplicacion
     */
    private enum BUILTINPLUGIS {
        CLIPBERRY, TERMINAL, NONE
    };

    private final Map<BUILTINPLUGIS, Pair<BuiltinPluginClass, JMenuItem>> builtinPlugins = new HashMap<>();

    /**
     * Activa un plugin interno dado (deshabilita entrada de menu)
     *
     * @param idx
     */
    private void loadBuiltinPlugin(BUILTINPLUGIS idx) {
        Pair<BuiltinPluginClass, JMenuItem> pair = builtinPlugins.get(idx);
        BuiltinPluginClass pl = (BuiltinPluginClass) registerAndInitializePlugin(pair.a, null);
        if (pl == pair.a) {
            pair.b.setEnabled(false);
        }
    }

    /**
     * inicializa los plugins internos (carga el plugin, crea el menu y lo deja
     * inactivo)
     */
    private void initBuiltInPlugins() {
        builtinPlugins.put(BUILTINPLUGIS.CLIPBERRY, new Pair<>("Berryclip",
                new guacarm.plugins.builtin.berryclip.Plugin<BUILTINPLUGIS>().setType(BUILTINPLUGIS.CLIPBERRY)));
        builtinPlugins.put(BUILTINPLUGIS.TERMINAL, new Pair<>("Terminal",
                new guacarm.plugins.builtin.terminal.Plugin<BUILTINPLUGIS>().setType(BUILTINPLUGIS.TERMINAL)));

        JMenu activate = new JMenu("Activate");

        builtinPlugins.keySet().forEach((plug) -> {
            Pair pair = builtinPlugins.get(plug);
            JMenuItem menupl = new JMenuItem(pair.name);
            menupl.addActionListener(e -> {
                loadBuiltinPlugin(plug);
            });
            activate.add(menupl);
            pair.b = menupl;
        });

        menu.add(activate);
        menu.add(new JSeparator());
    }

    /**
     * Habilita un plugin interno
     *
     * @param plugin
     */
    private void enableBuiltInPlugin(PluginClass plugin) {
        if (plugin instanceof BuiltinPluginClass) {
            BUILTINPLUGIS type = ((BuiltinPluginClass<BUILTINPLUGIS>) plugin).getType();
            builtinPlugins.get(type).b.setEnabled(true);
        }
    }

    /**
     * Devuelve las librerías .s necesarias para los plugins qu estan cargados
     *
     * @return
     */
    public List<String> getBuiltinLibraries() {
        List<String> list = new ArrayList<>();
        builtinPlugins.values().forEach((Pair<BuiltinPluginClass, JMenuItem> pair) -> {
            if (!pair.b.isEnabled()) {
                list.addAll(pair.a.getBuiltinLibraries());
            }
        });
        return list;
    }

    private class Pair<A, B> {

        public final String name;
        public A a;
        public B b;

        Pair(String s, A a) {
            name = s;
            this.a = a;
        }
    }

    /**
     * Habilita las interrupciones en el simulador
     */
    public void enableInterruptions() {
        simulator.interruptions.enableInterruptions();
    }

    /**
     * Deshabilita las interrupciones en el simulador
     */
    public void disableInterruptions() {
        simulator.interruptions.disableInterruptions();
    }

    /**
     * Simula la activacion de una interrupcion del tipo IRQ
     */
    public void throwIRQ() {
        simulator.interruptions.setPending(Interrupts.InterType.IRQ);
    }

    /**
     * Simula la activacion de una interrupcion del tipo FIQ
     */
    public void throwFIQ() {
        simulator.interruptions.setPending(Interrupts.InterType.FIQ);
    }

    /**
     * Salva la configuracion del plugin
     *
     * @param prop
     * @param plugin
     */
    public void saveConfig(Properties prop, String plugin) {
        config.setConfig(prop, plugin);
    }

    /**
     * Lee la configuracion almacenada de un plugin
     *
     * @param plugin
     * @return
     */
    public Properties getConfig(String plugin) {
        return config.getConfig(plugin);
    }
}
