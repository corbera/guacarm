/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins;

import guacarm.log.Log;
import guacarm.plugins.interfaces.PluginInterface;
import guacarm.plugins.interfaces.PluginClass;
import guacarm.plugins.interfaces.PluginMemCallBack;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import java.util.Properties;

/**
 *
 * @author corbera
 */
public class PluginApi implements PluginInterface {

    private final PluginManager plManager;
    private final Log log;
    private final PluginClass pluginID;

    public PluginApi(PluginManager pm, Log log_, PluginClass p) {
        plManager = pm;
        log = log_;
        pluginID = p;
    }

    /**
     * Registra el callback para un numero de servicio dado (si no esta
     * previamente registrado)
     *
     * @param svc numero del servicio a registrar
     * @param callBack callback asociado al servicio
     * @return true si se ha podido registrar
     */
    @Override
    public boolean registerSVCCall(int svc, PluginSVCCallBack callBack) {
        return plManager.registerSVC(svc, callBack, pluginID);
    }

    /**
     * Elimina el callback asociado al numero de servicio dado si pertence a
     * plugin que lo llama
     *
     * @param svc numero del servico
     * @return true si lo ha podido eliminar
     */
    @Override
    public boolean unRegisterSVCCall(int svc) {
        return plManager.unRegisterSVC(svc, pluginID);
    }

    /**
     * Indica si el servicio con identificador svc esta ya registrado
     *
     * @param svc numero de servicio
     * @return true si esta registrado
     */
    @Override
    public boolean isRegisteredSVC(int svc) {
        return plManager.isRegisteredSVC(svc);
    }

    /**
     * Devuelve puntero al objeto log de la aplicacion
     *
     * @return objeto log
     */
    @Override
    public Log getLog() {
        return log;
    }

    @Override
    public void unLoad() {
        plManager.unLoad(pluginID);
    }

    @Override
    public void awtComponentModifed() {
        plManager.awtComponentModified(pluginID);
    }

    /**
     * Registra un callback para los accesos de lectura a una region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura en la
     * region
     * @return true si se ha podido registrar
     */
    @Override
    public boolean registerMemoryReadHook(int offset, int size, PluginMemCallBack callback) {
        return plManager.registerMemoryReadHook(offset, size, callback, pluginID);
    }

    /**
     * Registra un callback para los accesos de escritura a una region de
     * memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una escritura en
     * la region
     * @return true si se ha podido registrar
     */
    @Override
    public boolean registerMemoryWriteHook(int offset, int size, PluginMemCallBack callback) {
        return plManager.registerMemoryWriteHook(offset, size, callback, pluginID);
    }

    /**
     * Registra un callback para los accesos de lectura/escritura a una region
     * de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una
     * lectura/escritura en la region
     * @return true si se ha podido registrar
     */
    @Override
    public boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback) {
        return plManager.registerMemoryReadWriteHook(offset, size, callback, pluginID);
    }

    /**
     * Elimina el registro de un hook de memoria
     *
     * @param offset direccion base de la memoria asociada al hook
     * @param size tamaño de la memoria asociada al hook
     * @return true si lo ha eliminado con exito
     */
    @Override
    public boolean unRegisterMemoryHook(int offset, int size) {
        return plManager.unRegisterMemoryHook(offset, size, pluginID);
    }

    @Override
    public void enableInterruptions() {
        plManager.enableInterruptions();
    }

    @Override
    public void disableInterruptions() {
        plManager.disableInterruptions();
    }

    @Override
    public void throwIRQ() {
        plManager.throwIRQ();
    }

    @Override
    public void throwFIQ() {
        plManager.throwFIQ();
    }

    @Override
    public void saveConfiguration(Properties config) {
        plManager.saveConfig(config, pluginID.getShortName());
    }

    @Override
    public Properties getConfiguration() {
        return plManager.getConfig(pluginID.getShortName());
    }
}
