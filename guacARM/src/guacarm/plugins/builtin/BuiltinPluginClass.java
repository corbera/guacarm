/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin;

import guacarm.plugins.interfaces.PluginClass;
import java.util.List;

/**
 *
 * @author corbera
 */
public interface BuiltinPluginClass<T> extends PluginClass {

    /**
     * Return the list of resources representing de source code (.s) of
     * libraries that the plugin needs to work correctly
     *
     * @return list of resources representing de files .s
     */
    public List<String> getBuiltinLibraries();
    
    public BuiltinPluginClass<T> setType(T type);
    
    public T getType();
}
