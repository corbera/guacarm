package guacarm.plugins.builtin.terminal;

import guacarm.plugins.interfaces.PluginInterface;
import guacarm.GUI.ColorTheme;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import javax.swing.InputMap;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

/**
 *
 * @author corbera Based on code from Ryan Beckett,
 * https://gist.github.com/ryan-beckett/1599018
 */
public class TerminalWindow extends JScrollPane implements TerminalAPI {

    private JTextArea txtArea = new JTextArea();
    private final String LINE_SEPARATOR = System.lineSeparator();
    private List<Character> buffer;
    private Font font = new Font("Courier New", Font.PLAIN, 14);
    private String sbuffer = "";
    private final BlockingQueue<String> queue;
    private final PluginInterface api;

    public TerminalWindow(PluginInterface api) {
        buffer = Collections.synchronizedList(new LinkedList<>());  // sincronizada para poder meter y sacar por distintos threads 
        queue = new LinkedBlockingQueue<>();
        setViewportView(txtArea);
        txtArea.addKeyListener(new KeyListener());
        disableArrowKeys(txtArea.getInputMap());
        txtArea.setFont(font);
        txtArea.setForeground(ColorTheme.FOREGROUND);
        txtArea.setBackground(ColorTheme.BACKGROUND);
        txtArea.setCaretColor(ColorTheme.FOREGROUND);
        txtArea.setHighlighter(null);
        this.api = api;
    }

    public Component getAWTComponent() {
        return this;
    }

    private void disableArrowKeys(InputMap inputMap) {
        String[] keystrokeNames = {"UP", "DOWN", "LEFT", "RIGHT", "HOME", "PAGE_DOWN", "PAGE_UP"};
        for (int i = 0; i < keystrokeNames.length; ++i) {
            inputMap.put(KeyStroke.getKeyStroke(keystrokeNames[i]), "none");
        }
    }

    public void clear() {
        txtArea.setText("");
        reset();
    }

    @Override
    public void write(byte[] data) {
        String s = new String(data);
        int end = s.indexOf(0); // la cadena puede acabar con \0 antes de final de buffer
        if (end < 0) {
            txtArea.append(s);
        } else {
            txtArea.append(s.substring(0, end));
        }
    }

    @Override
    public byte[] read(int count) throws IOException {
        if (sbuffer.length() == 0) {
            try {
                sbuffer = queue.take();
            } catch (InterruptedException ex) {
                //Logger.getLogger(TerminalWindow.class.getName()).log(Level.SEVERE, null, ex);
                api.getLog().println("Terminal: read call interrupted", ColorTheme.WARNING);
            }
        }
        String out;
        if (sbuffer.length() <= count) {
            out = sbuffer;
            sbuffer = "";
        } else {
            out = sbuffer.substring(0, count);
            sbuffer = sbuffer.substring(count);
        }
        return out.getBytes();
    }

    @Override
    public void reset() {
        queue.clear();
        buffer.clear();
        sbuffer = "";
    }

    private class KeyListener extends KeyAdapter {

        private final int ENTER_KEY = KeyEvent.VK_ENTER;
        private final int BACK_SPACE_KEY = KeyEvent.VK_BACK_SPACE;
        private final String BACK_SPACE_KEY_BINDING = getKeyBinding(
                txtArea.getInputMap(), "BACK_SPACE");

        private boolean isKeysDisabled;
        private int lineCount = 0;

        private String getKeyBinding(InputMap inputMap, String name) {
            return (String) inputMap.get(KeyStroke.getKeyStroke(name));
        }

        public void keyPressed(KeyEvent evt) {
            txtArea.setCaretPosition(txtArea.getDocument().getLength());
            int keyCode = evt.getKeyCode();
            if (keyCode == BACK_SPACE_KEY) {
                if (lineCount == 0 && !isKeysDisabled) {
                    disableBackspaceKey();
                } else if (lineCount > 0) {
                    if (isKeysDisabled) {
                        enableBackspaceKey();
                    }
                    buffer.remove(buffer.size() - 1);
                    lineCount--;
                }
            } else {
                if (font.canDisplay(evt.getKeyChar())) {
                    buffer.add(evt.getKeyChar());
                    lineCount++;
                }
            }
        }

        public void keyReleased(KeyEvent evt) {
            int keyCode = evt.getKeyCode();
            if (keyCode == ENTER_KEY) {
                try {
                    queue.put(buffer.stream().map(String::valueOf).collect(Collectors.joining()) + LINE_SEPARATOR);
                    buffer.clear();
                    lineCount = 0;
                } catch (InterruptedException ex) {
                    //Logger.getLogger(TerminalWindow.class.getName()).log(Level.SEVERE, null, ex);
                    api.getLog().println("Terminal: interrupted", ColorTheme.WARNING);
                }
            }
        }

        private void disableBackspaceKey() {
            isKeysDisabled = true;
            txtArea.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"),
                    "none");
        }

        private void enableBackspaceKey() {
            isKeysDisabled = false;
            txtArea.getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"),
                    BACK_SPACE_KEY_BINDING);
        }

    }

    public void enableTerminal() {
        txtArea.setEnabled(true);
    }

    public void disableTerminal() {
        txtArea.setEnabled(false);
    }

    public static class Barrier {

        public synchronized void block() throws InterruptedException {
            wait();
        }

        public synchronized void release() throws InterruptedException {
            notify();
        }

        public synchronized void releaseAll() throws InterruptedException {
            notifyAll();
        }

    }

}
