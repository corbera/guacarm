/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package guacarm.plugins.builtin.terminal;

import java.io.IOException;

/**
 *
 * @author corbera
 */
public interface TerminalAPI {

    /**
     * Imprime por terminal los caracteres almacenados en el array data
     *
     * @param data caracteres a imprimir
     */
    public void write(byte[] data);

    /**
     * Lee del terminal un conjunto de caracteres (mantiene buffer mas allá de
     * count)
     *
     * @return caracteres leidos
     */
    public byte[] read(int count) throws IOException;

    /**
     * Resetea el terminal (vacía buffers)
     */
    public void reset();
    
    /**
     * Limpia el contenido del terminal (GUI)
     */
    public void clear();
}
