/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package guacarm.plugins.builtin.terminal;

import guacarm.plugins.interfaces.PluginInterface;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author corbera
 */
public class TerminalText implements TerminalAPI {

    private String buffer = "";
    private final String LINE_SEPARATOR = System.lineSeparator();
    private final PluginInterface api;

    public TerminalText(PluginInterface api) {
        this.api = api;
    }

    /**
     * Imprime por terminal los caracteres almacenados en el array data
     *
     * @param data caracteres a imprimir
     */
    @Override
    public void write(byte[] data) {
        String s = new String(data);
        int end = s.indexOf(0); // la cadena puede acabar con \0 antes de final de buffer
        if (end < 0) {
            System.out.print(s);
        } else {
            System.out.print(s.substring(0, end));
        }
    }

    /**
     * Lee del terminal un conjunto de caracteres (mantiene buffer mas allá de
     * count)
     *
     * @return
     */
    @Override
    public byte[] read(int count) throws IOException {
        if (buffer.length() == 0) {
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(System.in));
            buffer = reader.readLine() + LINE_SEPARATOR;
        }
        String out;
        if (buffer.length() <= count) {
            out = buffer;
            buffer = "";
        } else {
            out = buffer.substring(0, count);
            buffer = buffer.substring(count);
        }
        return out.getBytes();
    }

    @Override
    public void reset() {
        buffer = "";
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
