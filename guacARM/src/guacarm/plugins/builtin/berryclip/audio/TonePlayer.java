/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.audio;

import guacarm.Pair;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class TonePlayer {

    private static final int SAMPLE_RATE = 16 * 1024; // ~44KHz
    private final AudioFormat af;
    private final SourceDataLine line;

    public TonePlayer() throws LineUnavailableException {
        af = new AudioFormat(SAMPLE_RATE, 8, 1, true, true);
        line = AudioSystem.getSourceDataLine(af);
        line.open(af, SAMPLE_RATE);
    }

    /**
     * Reproduce un tono de periodo dado durante un tiempo dado, esperando a que termine el sonido
     *
     * @param period periodo del tono en microsegundos
     * @param duration duracion del tono en milisegundos
     * @throws LineUnavailableException
     */
    public void play(int period, int duration) throws LineUnavailableException, InterruptedException {
        if (period < (2 * 1000000) / SAMPLE_RATE) {
            if (duration >= 0) {
                Thread.sleep(duration);
            }
            return;
        }
        Pair<byte[], Integer> pair = getSquareSample(period);
        byte[] wave = pair.a;
        int length = pair.b;
        int count = (int) Math.floor(duration * 1000 / period);
        int rem = duration * 1000 - count * period; //us
        rem = (int) ((rem * 1.0 / period * 1.0) * length);
        //long start = System.nanoTime();
        line.start();
        for (int i = 0; i < count; i++) {
            line.write(wave, 0, length);
        }
        line.write(wave, 0, rem);
        line.drain();

        //long end = System.nanoTime();
        //System.err.println("R1: " + duration + " real: " + (end - start) / 1000000);
    }
    
    /**
     * Reproduce un tono de periodo dado durante un tiempo dado, retornando sin espear a que termine el sonido
     *
     * @param period periodo del tono en microsegundos
     * @param duration duracion del tono en milisegundos
     * @throws LineUnavailableException
     */
    public void playNonBlock(int period, int duration) throws LineUnavailableException, InterruptedException {
        if (period < (2 * 1000000) / SAMPLE_RATE) {
            if (duration >= 0) {
                Thread.sleep(duration);
            }
            return;
        }
        Pair<byte[], Integer> pair = getSquareSample(period);
        byte[] wave = pair.a;
        int length = pair.b;
        int count = (int) Math.floor(duration * 1000 / period);
        int rem = duration * 1000 - count * period; //us
        rem = (int) ((rem * 1.0 / period * 1.0) * length);
        //long start = System.nanoTime();
        line.start();
        for (int i = 0; i < count; i++) {
            line.write(wave, 0, length);
        }
        //line.write(wave, 0, rem);
        //line.drain();

        //long end = System.nanoTime();
        //System.err.println("R1: " + duration + " real: " + (end - start) / 1000000);
    }
    
   /**
     * Devuelve un array con una onda cuadrada muestreada a SAMPLE_RATE del
     * periodo indicado
     *
     * @param period periodo de la onda
     * @return El array con los datos y el numero de datos
     */
    private static Pair<byte[], Integer> getSquareSample(int period) { // micro seconds
        int samples = (int) (SAMPLE_RATE * period / 1000000.0);
        byte[] square = new byte[samples];
        for (int i = 0; i < samples / 2; i++) {
            square[i] = -120;
            square[samples - i - 1] = 120;
        }
        return new Pair<>(square, samples);
    }

    /**
     * Devuelve un array con una onda muestreada a SAMPLE_RATE del periodo
     * indicado
     *
     * @param period periodo de la onda
     * @return El array con los datos y el numero de datos
     */
    private static Pair<byte[], Integer> getSample(int period) { // micro seconds
        int samples = (int) (SAMPLE_RATE * period / 1000000.0);
        byte[] sin = new byte[2 * samples];
        double twoPi = 2.0 * Math.PI;
        double incAngle = twoPi / samples;
        int i = 1;
        double angle;
        do {
            angle = incAngle * i;
            sin[i++] = (byte) (Math.sin(angle) * 127f);
        } while (angle < twoPi);
        return new Pair<>(sin, i);
    }
 }
