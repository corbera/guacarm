/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

/**
 *
 * @author corbera
 */
public interface TimerCallBack {
    /**
     * metodo que se llamará cuando el contador del timer tomer el valor del
     * retistro c1 o c3 del timer
     * 
     * @param counter registro que se ha alcanzado
     */
    void onChange(int counter);
}
