/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

import guacarm.log.Log;
import guacarm.plugins.interfaces.PluginInterface;
import java.util.TimerTask;

/**
 *
 * @author corbera
 */
public class Timer {

    /*
     * TEMPORIZADOR (reloj a 1MHz, se incrementa contador cada 1 microsegundo)
     * Contador de 64 bits (CLO y CHI) y 4 comparadores.
     * Direccion    Registro     
     * 20003000     CS 		M3M2M1M0.
     * 20003004     CLO 	parte baja del contador ascendente 64 bits.
     * 20003008     CHI 	32 bit superiores del contador de 64 bits.
     * 2000300C     C0 		comp0, si conincide con CLO activa M0.
     * 20003010     C1 		comp1, si conincide con CLO activa M1.
     * 20003014     C2 		comp2, si conincide con CLO activa M2.
     * 20003018     C3 		comp3, si conincide con CLO activa M3.
     *  
     * Los comparadores son puertos que se pueden modificar y se comparan con CLO. 
     * En el momento que uno de los 4 comparadores coincida y estén habilitadas 
     * las interrupciones para dicho comparador, se produce una interrupción y se 
     * activa el correspondiente bit Mx asociado al puerto CS (para que en la 
     * rutina de tratamiento de interrupción o RTI sepamos qué comparador ha 
     * provocado la interrupción). Los comparadores C0 y C2 los emplea la GPU 
     * internamente, por lo que nosotros nos ceñiremos a los comparadores C1 y C3.
     */
    private long initTime; // tiempo base para las comparaciones
    private final int STBASE;
    private final int CS;
    private final int CLO;
    private final int CHI;
    private final int C0;
    private final int C1;
    private final int C2;
    private final int C3;
    private final int[] registers = {0, 0, 0, 0, 0, 0, 0};
    public int speedupFactor = 1;  // factor de aceleración del contador del timer (1 -> 1 microsegundo)
    private static final int MIN_DELTAT = 1; // ms mínimo para programar una interrupción

    public boolean debug = false;
    private Log log;
    private final TimerCallBack interruptionCallback;

    private TimerTask taskC1;
    private TimerTask taskC3;

    public Timer(PluginInterface appAPI, TimerCallBack intCallback, int stbase) {
        initTime = System.nanoTime();
        this.log = appAPI.getLog();
        interruptionCallback = intCallback;

        STBASE = stbase;
        CS = STBASE + 0x00;
        CLO = STBASE + 0x04;
        CHI = STBASE + 0x08;
        C0 = STBASE + 0x0C;
        C1 = STBASE + 0x10;
        C2 = STBASE + 0x14;
        C3 = STBASE + 0x18;
    }

    public int getMemSize() {
        return 0x30;
    }

    /**
     * Devuelve el numero de microsegundos desde la inicialización/reset del
     * timer
     *
     * @return numero de microsegundos
     */
    public int getMicrosElapsed() {
        long actualTime = System.nanoTime();
        long elapsedMicros = (actualTime - initTime) / (1000 * speedupFactor);
        return (int) (elapsedMicros & 0x0FFFFFFFFL);
    }

    /**
     * Devuelve el numero de milisegundos desde la inicialización/reset del
     * timer
     *
     * @return numero de milisegundos
     */
    public int getMillisElapsed() {
        return getMicrosElapsed() / 1000;
    }

    /**
     * Actualiza el contador en función del tiempo transcurrido
     */
    private void updateCounter() {
        // actualización del contador
        long actualTime = System.nanoTime();
        long elapsedMicros = (actualTime - initTime) / (1000 * speedupFactor);
        synchronized (registers) {
            registers[(CLO - CS) >> 2] = (int) (elapsedMicros & 0x0FFFFFFFFL);
            registers[(CHI - CS) >> 2] = (int) (elapsedMicros >>> 32);
        }
    }

    /**
     * Lee el contenido de uno de los registros asociados al temporizador
     *
     * @param addr dirección a leer
     * @param size tamaño a leer (1, 2 o 4 bytes)
     * @return el valor leido
     * @throws Exception si se preoduce un error de lectura
     */
    public int read(int addr, int size) throws Exception {
        int val;

        updateCounter();

        if (!memAlign(addr, size)) {
            String error = "Timer read: Bad address align 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        } else if (addr < CS || addr > C3 + 3) {
            return 0;
        }

        // lectura del resgistro correspondiente
        int nreg = ((addr & 0xFFFFFFFC) - CS) >>> 2;
        synchronized (registers) {
            val = registers[nreg];
        }
        int offset = addr & 0x00000003;
        val = val >>> (offset << 3);
        int mask = (size == 1) ? 0x0FF : ((size == 2) ? 0x0FFFF : 0xFFFFFFFF);
        return val & mask;
    }

    /**
     * Escribe en el contenido de uno de los registros asociados al temporizador
     *
     * @param addr dirección a escribir
     * @param size tamaños a escribir (1, 2 o 4 bytes)
     * @param val valor a escribir
     * @throws Exception si no se puede escribir
     */
    public void write(int addr, int size, int val) throws Exception {
        if (!memAlign(addr, size)) {
            String error = "Timer write: Bad address align 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        } else if (addr < CS || addr > C3 + 3) {
            return;
        }

        updateCounter();

        if (addr >= C0 && addr <= C3 + 3) {
            int nreg = ((addr & 0xFFFFFFFC) - CS) >>> 2;
            int oldval = 0;
            synchronized (registers) {
                oldval = registers[nreg];
            }
            int offset = addr & 0x00000003;
            int mask = (size == 1) ? 0x0FF : ((size == 2) ? 0x0FFFF : 0xFFFFFFFF);
            val = val & mask;
            val = val << (offset << 3);
            mask = mask << (offset << 3);
            mask = ~mask;
            oldval = (oldval & mask) | val;
            synchronized (registers) {
                registers[nreg] = oldval;
            }
            if (nreg == 4 || nreg == 6) { // c1 o c3
                int diff = 0;
                synchronized (registers) {
                    diff = registers[nreg] - registers[(CLO - CS) >> 2];
                }
                diff = (diff <= 0) ? 1 : diff;
                long howlong = diff & 0x0FFFFFFFFL; // microsegundos
                long howlongReal = howlong / 1000; // milisegundos (es la resolución del timer de Java)
                howlongReal *= speedupFactor;
                howlongReal = (howlongReal < MIN_DELTAT) ? MIN_DELTAT : howlongReal;
                deb("Scheduled interrupt in " + howlong + " us (" + howlongReal + " ms real)");
                java.util.Timer timer = new java.util.Timer();
                int counter = nreg - 3;
                TimerTask tt;
                if (counter == 1) {
                    if (taskC1 != null) {
                        taskC1.cancel();
                    }
                    taskC1 = new MyTimerTask(1);
                    tt = taskC1;
                } else {
                    if (taskC3 != null) {
                        taskC3.cancel();
                    }
                    taskC3 = new MyTimerTask(3);
                    tt = taskC3;
                }
                timer.schedule(tt, howlongReal);
            }
        } else if (addr == CS) {
            // poner a 0 aquellos bits en los que haya un 1 en el valor a escribir
            // solo tengo en cuenta los 4 bits menos significativos 
            if (debug) {
                String msg = "";
                if ((val & 0x2) != 0) {
                    msg = "bit M1 ";
                }
                if ((val & 0x8) != 0) {
                    msg += ((msg != "") ? "and " : "") + "bit M3 ";
                }
                if (msg != "") {
                    deb("cleanning " + msg + "of CS");
                }
            }
            synchronized (registers) {
                //int r = registers[0];
                registers[0] &= ~(val & 0x0F);
                //deb("register antes: "+Integer.toHexString(r)+" despues: "+Integer.toHexString(registers[0]));
            }
        }
    }

    /**
     * Tarea asociada a los timers que simularan el final de cuenta de los
     * distintos contadores (c1 y c3)
     */
    private class MyTimerTask extends TimerTask {

        private int counter;

        MyTimerTask(int c) {
            super();
            counter = c;
        }

        /**
         * Comprobamos que contador es el que ha terminado, lo limpiamos y
         * llamamos a la funcion de finalización que provocará la interrupcion
         */
        @Override
        public void run() {
            if (taskC1 == this) {
                taskC1 = null;
            } else {
                taskC3 = null;
            }
            endCount(counter);
        }

    }

    /**
     * Realiza las acciones necesarias cuando el contador alcanza el valor de
     * uno de los registros C1 o C3 (actualizra bit de CS y lanzar interrupcion)
     *
     * @param counter
     */
    private synchronized void endCount(int counter) {
        deb("end of C" + counter + " count");
        int mask = 1 << counter;
        // si el bit correspondiente en CS está a uno, no se lanza interrupción
        synchronized (registers) {
            if ((registers[0] & mask) != 0) {
                deb("Bit " + counter + " of CS is 1, interrupt not raised");
                return;
            }
            // actualizar bit del registro CS
            registers[0] |= mask;
        }
        // lanzar la interrupcion
        if (interruptionCallback != null) {
            deb("raise interrupt for C" + counter);
            interruptionCallback.onChange(counter);
        }
    }

    /**
     * comprueba que la direccion esta correctamente alineada al tamaño memSize
     *
     * @param addr dirección
     * @param size tamaño
     * @return true si está bien alineada
     */
    public boolean memAlign(int addr, int size) {
        if (size < 1 || size > 4 || size == 3) {
            return false;
        }
        return (addr & (size - 1)) == 0;
    }

    /**
     * Realiza un reset del timer
     */
    public void reset() {
        for (int i = 0; i < registers.length; i++) {
            registers[i] = 0;
        }
        initTime = System.nanoTime();
    }

    public int getSpeedupFactor() {
        return speedupFactor;
    }

    public void setSpeedupFactor(int speedupFactor) {
        this.speedupFactor = speedupFactor;
    }

    private void deb(String str) {
        guacarm.plugins.builtin.berryclip.Plugin.printDebug(log, debug, "Timer", str);
    }
}
