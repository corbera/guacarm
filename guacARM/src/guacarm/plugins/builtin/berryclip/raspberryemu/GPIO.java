/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import guacarm.plugins.interfaces.PluginInterface;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author corbera Clase para emular los GPIOs de la raspberry pi
 */
public class GPIO {

    /**
     * Modos de funcionamiento del pin
     */
    public enum PinMode {
        UNK, IN, OUT
    };

    /**
     * estado del pin
     */
    public enum PinState {
        DOWN, UP
    }

    private static final int NGPIOS = 54;
    private final int GPBASE;

    /**
     * callbacks para avisar que se ha producido un cambio de estado, por
     * software, en algún pin
     */
    private final Set<GPIOCallBack>[] GPIOCallBacks = new HashSet[NGPIOS];

    /**
     * direcciones base de los registros GPFSEL (para configuracion de los pines
     * del GPIO) (5 registros de 32 bits y agrupaciones de 3 bits por pin)
     */
    private final GPIOMemMap GPFSEL;
    /**
     * direcciones base de los registros GPSET (para poner un 1 en los pines del
     * GPIO) (2 registros de 32 bits y agrupaciones de 1 bit por pin)
     */
    private final GPIOMemMap GPSET;
    /**
     * direcciones base de los registros GPCLR (para poner un 0 en los pines del
     * GPIO)
     */
    private final GPIOMemMap GPCLR;
    /**
     * direcciones base de los registros GPLEV (para leer el valor en los pines
     * del GPIO)
     */
    private final GPIOMemMap GPLEV;
    /**
     * direcciones base de los registros GPEDS (Sirven para detectar qué pin ha
     * provocado una interrupción en caso de usarlo como lectura. Al escribir en
     * ellos también podemos notificar que ya hemos procesado la interrupción y
     * que por tanto estamos listos para que nos vuelvan a interrumpir sobre los
     * pines que indiquemos.)
     */
    private final GPIOMemMap GPEDS;
    /**
     * direcciones base de los registros GPREN (Con estos puertos enmascaramos
     * los pines que queremos que provoquen una interrupción en flanco de
     * subida, esto es cuando hay una transición de 0 a 1 en el pin de entrada.)
     */
    private final GPIOMemMap GPREN;
    /**
     * direcciones base de los registros GPFEN (Con estos puertos enmascaramos
     * los pines que queremos que provoquen una interrupción en flanco de
     * bajada, esto es cuando hay una transición de 1 a 0 en el pin de entrada.)
     */
    private final GPIOMemMap GPFEN;
    /**
     * direcciones base de los registros GPHEN (Enmascaramos los pines que
     * provocarán una interrupción al detectar un nivel alto (3.3V) por dicho
     * pin.)
     */
    private final GPIOMemMap GPHEN;
    /**
     * direcciones base de los registros GPLEN (Enmascaramos los pines que
     * provocarán una interrupción al detectar un nivel bajo (0V) por dicho
     * pin.)
     */
    private final GPIOMemMap GPLEN;

    private final GPIOMemMap[] memMaps;

    public boolean debug = false;
    private Log log;

    public GPIO(PluginInterface appApi, int gpbase) {
        for (int i = 0; i < NGPIOS; i++) {
            GPIOCallBacks[i] = new HashSet<>();
        }
        this.log = appApi.getLog();
        GPBASE = gpbase;
        GPFSEL = new GPIOMemMap(GPBASE + 0x0, 6 * 4, 3);
        GPSET = new GPIOMemMap(GPBASE + 0x1C, 2 * 4, 1, GPIOMemMap.WRITE);
        GPCLR = new GPIOMemMap(GPBASE + 0x28, 2 * 4, 1, GPIOMemMap.WRITE);
        GPLEV = new GPIOMemMap(GPBASE + 0x34, 2 * 4, 1, GPIOMemMap.READ);
        GPEDS = new GPIOMemMap(GPBASE + 0x40, 2 * 4, 1);
        GPREN = new GPIOMemMap(GPBASE + 0x4C, 2 * 4, 1);
        GPFEN = new GPIOMemMap(GPBASE + 0x58, 2 * 4, 1);
        GPHEN = new GPIOMemMap(GPBASE + 0x64, 2 * 4, 1);
        GPLEN = new GPIOMemMap(GPBASE + 0x70, 2 * 4, 1);
        memMaps = new GPIOMemMap[]{GPFSEL, GPSET, GPCLR, GPLEV, GPEDS, GPREN, GPFEN, GPHEN, GPLEN};
    }

    public int getMemSize() {
        return 0x100;
    }

    /**
     * Resetea (pone a 0) el contenido de todas las posiciones de memoria
     * asociadas al GPIO
     */
    public void reset() {
        for (GPIOMemMap memMap : memMaps) {
            memMap.reset();
        }
    }

    /**
     * Establece un callback que se llamara cuando suceda un evento en un pin
     * del GPIO
     *
     * @param pin pin al que asociamos el callback
     * @param callBack callback a registrar
     * @return true si se ha registrado (no estaba previamente)
     */
    public boolean registerGPIOCallBack(int pin, GPIOCallBack callBack) {
        return GPIOCallBacks[pin].add(callBack);
    }

    /**
     * Elimina un callback
     *
     * @param pin pin al que eliminamos el callback
     * @param callBack callback a eliminar
     * @return true si se ha eliminado
     */
    public boolean unRegisterGPIOCallBack(int pin, GPIOCallBack callBack) {
        return GPIOCallBacks[pin].remove(callBack);
    }

    /**
     * Etablece el modo para el pin dado
     *
     * @param pin pin a cambiar el estado
     * @param newMode nuevo estado
     * @return true si ha podido establecer el modo
     */
    public boolean setModeGPIOPin(int pin, PinMode newMode) {
        if (pin >= 0 && pin <= NGPIOS) {
            int mode = pinMode2Val(newMode);
            try {
                deb("seting mode " + mode + " for pin " + pin);
                return GPFSEL.setBitGroup(pin, mode);
            } catch (Exception ex) {
            }
        }
        return false;
    }

    /**
     * Devuelve el modo en que está configurado un pin del GPIO
     *
     * @param pin pin por el que se pregunta
     * @return modo del pin
     */
    public PinMode getModeGPIOPin(int pin) {
        if (pin >= 0 && pin <= NGPIOS) {
            try {
                int mode = GPFSEL.getBitGroup(pin);
                return val2PinMode(mode);
            } catch (Exception ex) {
            }
        }
        return PinMode.UNK;
    }

    /**
     * Dada una direccion y un valor a escribir, imprimer los GPIO pins que van
     * a cambiar de modo de funcionamiento
     *
     * @param address dirección a escribir
     * @param size tamaño de la escritura
     * @param val dato a escribir
     */
    private void debugPrintSetModesGPIOs(int address, int size, int val) {
        if (debug) {
            if (address >= GPFSEL.getMemBaseAddress() && address < GPFSEL.getMemBaseAddress() + GPFSEL.getMemSize()) {
                int reg = (address - GPFSEL.getMemBaseAddress()) >> 2;
                int regoff = GPFSEL.getMemBaseAddress() + (reg << 2);
                int offset = address - regoff;
                int newval = val << (offset << 3);
                int oldval = newval;
                try {
                    oldval = GPFSEL.read(regoff, 4);
                } catch (Exception ex) {
                    Logger.getLogger(GPIO.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (int i = 0; i < 10; i++) {
                    int newmode = newval & 0x07;
                    int oldmode = oldval & 0x07;
                    if (newmode != oldmode) {
                        String mode = (val2PinMode(newmode) == PinMode.IN)
                                ? "input"
                                : ((val2PinMode(newmode) == PinMode.OUT)
                                ? "output"
                                : "unknown");
                        deb("setting GPIO " + (reg * 10 + i) + " to mode: " + mode);
                    }
                    newval = newval >> 3;
                    oldval = oldval >> 3;
                }
            }
        }
    }

    /**
     * Para los pines configurados como entrada, establece el valor del pin
     * desde el hardware
     *
     * @param pin pin a cambiar
     * @param newState nuevo estado del pin
     * @return true si el pin ha cambiado de valor (0->1, 1->0)
     */
    public boolean setStateGPIOPin(int pin, int newState) {
        return setStateGPIOPin(pin, newState, true);
    }

    /**
     * Para los pines configurados como salida, etablece el valor del pin desde
     * software
     *
     * @param pin pin a cambiar
     * @param newState nuevo estado del pin
     * @return true si el pin ha cambiado de valor (0->1, 1->0)
     */
    public boolean setStateGPIOPinSoftware(int pin, int newState) {
        return setStateGPIOPin(pin, newState, false);
    }

    public boolean setStateGPIOPin(int pin, int newState, boolean hardware) {
        deb("setting state " + newState + " for pin " + pin);
        try {
            if (pin >= 0 && pin <= NGPIOS && newState >= 0 && newState <= 1) {
                if (((getModeGPIOPin(pin).equals(PinMode.IN) && hardware)
                        || (getModeGPIOPin(pin).equals(PinMode.OUT) && !hardware))
                        && GPLEV.setBitGroup(pin, newState)) {
                    GPIOCallBacks[pin].forEach((callback) -> {
                        callback.onChange(pin);
                    });
                    return true;
                }
            }
        } catch (Exception ex) {
        }
        return false;
    }

    /**
     * Devuelve el estado (0/1) de un pin del GPIO
     *
     * @param pin pin por el que se pregunta
     * @return estado del pin
     */
    public int getStateGPIOPin(int pin) {
        try {
            if (pin >= 0 && pin <= NGPIOS) {
                return GPLEV.getBitGroup(pin);
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    /**
     * Establece a 1 el bit que indica que se ha producido una interrupcion en
     * el pin indicado
     *
     * @param pin
     * @return true si se ha podido establecer
     */
    public boolean setIntPendingGPIOPin(int pin) {
        deb("setting interrupt pending for pin " + pin);
        try {
            if (pin >= 0 && pin <= NGPIOS) {
                return GPEDS.setBitGroup(pin, 1);
            }
        } catch (Exception ex) {
        }
        return false;
    }

    /**
     * Establece a 0 el bit que indica que se ha producido una interrupcion en
     * el pin indicado
     *
     * @param pin
     * @return true si se ha podido establecer
     */
    public boolean clearIntPendingGPIOPin(int pin) {
        deb("clearing interrupt pending for pin " + pin);
        try {
            if (pin >= 0 && pin <= NGPIOS) {
                return GPEDS.setBitGroup(pin, 0);
            }
        } catch (Exception ex) {
        }
        return false;
    }

    /**
     * Establece a 0 los bits indicados de los pines que han provocado interrupciones
     * 
     * @param address direccion del registro
     * @param size tamaño a escribir
     * @param val valor a escribir
     */
    private void clearIntPendingGPIO(int address, int size, int val) {
        if (!GPIOMemMap.memAlign(address, size)) {
            log.println("GPIO write: Bad address align", ColorTheme.WARNING);
            return;
        }
        int byteOffset = address - GPEDS.getMemBaseAddress();
        int pin = byteOffset << 3;
        int bit = 0;
        while (val != 0 && bit < (size << 3)) {
            if ((val & 0x01) == 1) {
                clearIntPendingGPIOPin(pin);
            }
            bit ++;
            pin ++;
            val >>>= 1;
        }
    }
    
    /**
     * Lectura de la posición de moria address de size bytes
     *
     * @param address direccion a leer
     * @param size numero de bytes a leer
     * @return valor leido
     */
    public int read(int address, int size) {
        for (GPIOMemMap memmap : memMaps) {
            if (memmap.getMemBaseAddress() <= address
                    && memmap.getMemBaseAddress() + memmap.getMemSize() >= address + size) {
                try {
                    return memmap.read(address, size);
                } catch (Exception ex) {
                    break;
                }
            }
        }
        return 0;
    }

    /**
     * Escribe un valor de tamaño size bytes en la posicion indicada (si se
     * puede escribir en dicha posicion)
     *
     * @param address dirección a escribir
     * @param size tamño en bytes a escribir
     * @param val valor a escribir
     */
    public void write(int address, int size, int val) {
        for (GPIOMemMap memmap : memMaps) {
            if (memmap.getMemBaseAddress() <= address
                    && memmap.getMemBaseAddress() + memmap.getMemSize() >= address + size) {
                if (memmap == GPFSEL) {
                    debugPrintSetModesGPIOs(address, size, val);
                }
                if (memmap == GPEDS) {
                    clearIntPendingGPIO(address, size, val);
                    break;
                }
                try {
                    Set<Integer> touchedPins = memmap.write(address, size, val);
                    if (!touchedPins.isEmpty() && (memmap == GPSET || memmap == GPCLR)) {
                        memmap.write(address, size, 0);
                        int newPinState = (memmap == GPSET) ? 1 : 0;
                        touchedPins.forEach((pin) -> {
                            setStateGPIOPinSoftware(pin, newPinState);
                        });
                    }
                } catch (Exception ex) {
                    break;
                }
                break;
            }
        }
    }

    /**
     * Convierte un valor de 3 bits a su correspondiente modo de GPFSEL
     *
     * @param val valor de 3 bits
     * @return modo del GPFSEL (in, out, unk)
     */
    private PinMode val2PinMode(int val) {
        switch (val) {
            case 0:
                return PinMode.IN;
            case 1:
                return PinMode.OUT;
            default:
                return PinMode.UNK;
        }
    }

    /**
     * Convierte un modo de GPFSEL a su correspondiente valor numérico de 3 bits
     *
     * @param mode modo del GPFSEL
     * @return valor numerico de dicho modo
     */
    private int pinMode2Val(PinMode mode) {
        switch (mode) {
            case IN:
                return 0;
            case OUT:
                return 1;
            default:
                return 7;
        }
    }

    /**
     * Indica si el flanco producido en un pin tiene que provocar interrupcion
     * del GPIO, en cuyo caso activa el bit correspondiente de GPEDS
     *
     * @param pin pin en el que se ha producido el flanco
     * @param edge tipo del flanco (0 bajada, 1 subida)
     * @return true si produce interrupcion
     */
    public boolean throwGPIOInt(int pin, int edge) {
        String edgestr = (edge == 0) ? "(falling edge/high)" : "(rising edge/low)";
        if (pin >= 0 && pin <= NGPIOS) {
            try {
                if (GPEDS.getBitGroup(pin) == 1) { // no se ha borrado bit de deteccion de int, no enviar nueva int.
                    deb("GPIO interrupt cannot be raised for pin " + pin + " (GPEDS not cleaned)");
                    return false;
                }
                if (edge == 1) {
                    if (GPREN.getBitGroup(pin) == 1
                            || GPHEN.getBitGroup(pin) == 1) {
                        return setIntPendingGPIOPin(pin);
                    }
                } else if (edge == 0) {
                    if (GPFEN.getBitGroup(pin) == 1
                            || GPLEN.getBitGroup(pin) == 1) {
                        return setIntPendingGPIOPin(pin);
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(GPIO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        deb("GPIO interrupt cannot be raised for pin " + pin + " " + edgestr + " (not enabled)");
        return false;
    }

    private void deb(String str) {
        guacarm.plugins.builtin.berryclip.Plugin.printDebug(log, debug, "GPIO", str);
    }
}
