/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

import guacarm.Pair;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author corbera
 */
public class GPIOMemMap {

    public static final int READ = 1;
    public static final int WRITE = 2;

    private final int memBaseAddress;
    private final int memSize;
    private final byte[] memMap;
    private final int bitGroupSize;
    private final int mode;

    public GPIOMemMap(int baseAddr, int size, int bitGroupSize) {
        this(baseAddr, size, bitGroupSize, READ | WRITE);
    }

    public GPIOMemMap(int baseAddr, int size, int bitGroupSize, int mode) {
        this.memBaseAddress = baseAddr;
        this.memSize = size;
        this.bitGroupSize = bitGroupSize;
        this.mode = mode;
        memMap = new byte[size + 1];  // 1 byte mas de guarda para simplificar fucion que activa bits individuales
    }

    public int getMemBaseAddress() {
        return memBaseAddress;
    }

    public int getMemSize() {
        return memSize;
    }

    /**
     * Pone a 0 el contenido de la memoria
     */
    public void reset() {
        for (int i = 0; i <= memSize; i++) {
            memMap[i] = 0;
        }
    }

    /**
     * Escribe un valor en una posición de memoria
     *
     * @param addr posicion de memoria a escribir
     * @param size numero de bytes a escribir (1, 2 o 4)
     * @param newVal valor a escribir
     * @return conunto de grupos de bits que han cambiado con la escritura
     * @throws java.lang.Exception si la direccion no se puede escribir
     */
    public Set<Integer> write(int addr, int size, int newVal) throws Exception {
        Set<Integer> set = new HashSet<>();
        if ((mode & WRITE) == 0) {
            return set;
        }
        if (addr < memBaseAddress || addr + size >= memBaseAddress + memSize) {
            throw new Exception("GPIO write: Bad memory address");
        } else if (!memAlign(addr, size)) {
            throw new Exception("GPIO write: Bad address align");
        }
        int offset = addr - memBaseAddress;
        int oldVal = 0;
        int val = newVal;
        for (int i = 0; i < size; i++) {
            oldVal = oldVal | (memMap[offset] << (8 * i));
            memMap[offset++] = (byte) (val & 0x0ff);
            val = val >>> 8;
        }
        int ch = oldVal ^ newVal;
        int numBit = (addr - memBaseAddress) * 8;
        while (ch != 0) {
            if ((ch & 0x01) != 0) {
                set.add(groupBitNumberFromBit(numBit));
            }
            numBit++;
            ch = ch >>> 1;
        }
        return set;
    }

    /**
     * Lee el valor de tantos bytes como indica memSize de la posicion indicada
     *
     * @param addr posicion a leer
     * @param size numero de bytes a leer (1, 2 o 4)
     * @return valor leido
     * @throws java.lang.Exception si la direccion no se puede leer
     */
    public int read(int addr, int size) throws Exception {
        int val = 0;
        if ((mode & READ) == 0) {
            return val;
        }
        if (addr < memBaseAddress || addr + size >= memBaseAddress + memSize) {
            String error = "GPIO read: Bad memory address 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        } else if (!memAlign(addr, size)) {
            String error = "GPIO read: Bad address align 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        }
        int offset = addr - memBaseAddress;
        for (int i = 0; i < size; i++) {
            val = (val >>> 8) | (memMap[offset++] << 24);
        }
        val = val >>> ((4 - size) << 3);
        return val;
    }

    /**
     * comprueba que la direccion esta correctamente alineada al tamaño memSize
     *
     * @param addr dirección
     * @param size tamaño
     * @return true si está bien alineada
     */
    public static boolean memAlign(int addr, int size) {
        if (size < 1 || size > 4 || size == 3) {
            return false;
        }
        if ((addr & (size - 1)) != 0) {
            return false;
        }
        return true;
    }

    /**
     * Devuelve el byte de memoria al que pertenece el grupo de bits dado
     *
     * @param numGroup grupo de bits
     * @return byte en el que estaría representado
     */
    private Pair<Integer, Integer> getByteOffset(int numGroup) {
        int numGroupsPerWord = (int) Math.floor(32 / bitGroupSize);
        int word = ((int) Math.floor(numGroup / numGroupsPerWord));
        int relativeNumGroup = numGroup - word * numGroupsPerWord;
        int byteOffset =  (word << 2) + ((relativeNumGroup * bitGroupSize) >>> 3);
        int bitOffset = (relativeNumGroup * bitGroupSize) & 0x07;
        return new Pair<>(byteOffset, bitOffset);
    }

    /**
     * Establece el valor del grupo de bits numGroup al valor dado
     *
     * @param numGroup numero del grupo de bits
     * @param val nuevo valor para los bits
     * @return true si el nuevo valor es distinto al anterior
     * @throws Exception si el direccionamiento no es correcto
     */
    public boolean setBitGroup(int numGroup, int val) throws Exception {
        Pair<Integer, Integer> p = getByteOffset(numGroup);
        int byteOffset = p.a;
        int bitOffset = p.b;
        if (((byteOffset << 3) + bitOffset + bitGroupSize) >= (memSize << 3) + 8) {
            throw new Exception("GPIOMemMap.setBitGroup: numGroup out of bounds");
        }
        int oldVal = (memMap[byteOffset] & 0x0FF) | ((memMap[byteOffset + 1] << 8) & 0x0FF00);
        int newVal = setBits(oldVal, bitOffset, val);
        memMap[byteOffset] = (byte) (newVal & 0x0ff);
        memMap[byteOffset + 1] = (byte) ((newVal >>> 8) & 0x0ff);
        if (oldVal != newVal) {
            return true;
        }
        return false;
    }

    /**
     * Devuelve el valor de grupo de bits indicado
     *
     * @param numGroup numero del grupo de bits
     * @return valor del grupo de bits
     * @throws Exception si el direccionamiento no es correcto
     */
    public int getBitGroup(int numGroup) throws Exception {
        Pair<Integer, Integer> p = getByteOffset(numGroup);
        int byteOffset = p.a;
        int bitOffset = p.b;
        if (((byteOffset << 3) + bitOffset + bitGroupSize) >= (memSize << 3) + 8) {
            throw new Exception("GPIOMemMap.getBitGroup: numGroup out of bounds");
        }
        int val = (memMap[byteOffset] & 0x0FF) | ((memMap[byteOffset + 1] << 8) & 0x0FF00);
        return getBits(val, bitOffset);
    }

    /**
     * Establece el valor un grupo de bits de tamaño bitGroupSize a partir de
     * bitOffset de val a newVal
     *
     * @param val palabra con los bits a cambiar
     * @param bitOffset offset del primer bit a cambiar (de los bitGroupSize
     * bits a cambiar)
     * @param newVal nuevo valor de los bits a cambiar (solo se consideran los
     * bitGroupSize bits menos sig)
     * @return la palabra con los bits correspondientes cambiados
     */
    private int setBits(int val, int bitOffset, int newVal) {
        int mask = ((1 << bitGroupSize) - 1) << bitOffset;
        return ((val & ~mask) | ((newVal << bitOffset) & mask));
    }

    /**
     * Devuelve el valor del grupo de bits de tamaño bitGroupSize a partir del
     * bit bitoffset del valor val
     *
     * @param val palabra con los bits
     * @param bitOffset desplazamiento del primer bit del grupo
     * @return valor del grupo de bits que empieza en bitOffset
     */
    private int getBits(int val, int bitOffset) {
        int mask = ((1 << bitGroupSize) - 1);
        return ((val >>> bitOffset) & mask);
    }

    /**
     * Devuelve el numero del grupo de bits al que pertenece un bit dado
     * (contando desde la base de la memoria)
     *
     * @param numBit numero del bit desde memBaseAddress
     * @return numero de grupo al que pertenece
     */
    private int groupBitNumberFromBit(int numBit) {
        return (int) Math.floor(numBit / bitGroupSize);
    }
}
