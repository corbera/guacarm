/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

/**
 *
 * @author corbera
 */
public interface GPIOCallBack {
    /**
     * metodo que se llamará cuando alguno de los pines del GPIO cambie de estado
     * por software (write en GPSET o GPCLR)
     * @param pin pin que ha cambiado su estado
     */
    void onChange(int pin);
}
