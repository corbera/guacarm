/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.raspberryemu;

import guacarm.log.Log;
import guacarm.plugins.interfaces.PluginInterface;

/**
 * Controlador de interrupciones de la raspberry
 *
 * @author corbera
 */
public class InterruptController {

    /*
     * CONTROLADOR DE INTERRUPCIONES RASPBERRY PI
     * 10 puertos de 32 bits cada uno.
     *
     * Direccion    Puerto
     * 2000B200     IRQ basic pending
     * 2000B204     IRQ pending 1
     * 2000B208     IRQ pending 2
     * 2000B20C     FIQ control
     * 2000B210     Enable IRQs 1
     * 2000B214     Enable IRQs 2
     * 2000B218     Enable basic IRQs
     * 2000B21C     Disable IRQs 1
     * 2000B220     Disable IRQs 2
     */
    private final int INTBASE;
    private final int INTPENBASIC;
    private final int INTPENIRQ1;
    private final int INTPENIRQ2;
    private final int INTFIQ;
    private final int INTENIRQ1;
    private final int INTENIRQ2;
    private final int INTENBASIC;
    private final int INTDISIRQ1;
    private final int INTDISIRQ2;
    private final int INTDISBASIC;
    private final int ENDADDR;

    private byte[] registers;

    private boolean gpioIRQEnabled = false;
    private boolean c1IRQEnabled = false;
    private boolean c3IRQEnabled = false;
    private boolean gpioIRQPending = false;
    private boolean c1IRQPending = false;
    private boolean c3IRQPending = false;
    private boolean gpioFIQEnabled = false;
    private boolean c1FIQEnabled = false;
    private boolean c3FIQEnabled = false;

    private static final int IRQOffset = 0x18;
    private static final int FIQOffset = 0x1C;

    private final PluginInterface appApi;

    public boolean debug = false;
    private Log log;

    public InterruptController(PluginInterface appApi, int intbase) {
        this.appApi = appApi;
        this.log = appApi.getLog();

        INTBASE = intbase;
        INTPENBASIC = INTBASE;
        INTPENIRQ1 = INTBASE + 4;
        INTPENIRQ2 = INTBASE + 8;
        INTFIQ = INTBASE + 12;
        INTENIRQ1 = INTBASE + 16;
        INTENIRQ2 = INTBASE + 20;
        INTENBASIC = INTBASE + 24;
        INTDISIRQ1 = INTBASE + 28;
        INTDISIRQ2 = INTBASE + 32;
        INTDISBASIC = INTBASE + 36;
        ENDADDR = INTDISBASIC + 4;

        registers = new byte[ENDADDR - INTBASE];
    }

    public int getMemSize() {
        return ENDADDR - INTBASE;
    }

    /**
     * Lee el contenido de uno de los registros asociados al timer
     *
     * @param addr dirección a leer
     * @param size tamaño a leer (1, 2 o 4 bytes)
     * @return el valor leido
     * @throws Exception si se preoduce un error de lectura
     */
    public int read(int addr, int size) throws Exception {
        if (!memAlign(addr, size)) {
            String error = "InterruptController read: Bad address align 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        } else if (addr < INTPENBASIC || addr > INTPENIRQ2 + 3) {  // solo se pueden leer los puertos "pending"
            return 0;
        }
        int ret = 0;
        synchronized (registers) {
            ret = guacarm.Utils.readIntFromBytes(registers, size, addr - INTBASE);
        }
        return ret;
    }

    /**
     * Escribe en el contenido de uno de los registros asociados al temporizador
     *
     * @param addr dirección a escribir
     * @param size tamaños a escribir (1, 2 o 4 bytes)
     * @param val valor a escribir
     * @throws Exception si no se puede escribir
     */
    public void write(int addr, int size, int val) throws Exception {
        if (!memAlign(addr, size)) {
            String error = "Timer write: Bad address align 0x" + Integer.toHexString(addr) + " (" + size + " bytes)";
            throw new Exception(error);
        } else if (addr < INTFIQ || addr > INTDISBASIC + 3) {
            return;
        }
        synchronized (registers) {
            guacarm.Utils.writeIntToBytes(val, registers, size, addr - INTBASE);
            checkControl(addr);
            guacarm.Utils.writeIntToBytes(0, registers, size, addr - INTBASE);
        }
    }

    /**
     * Chequea los puertos de habilitación y deshabilitación de IRQs y el de
     * control FIQ, para habiiltar/deshabilitar las correspondientes
     * interrupciones
     */
    static final int bitC1 = 1;
    static final int bitC3 = 3;
    static final int bitGPIO = 20;

    private void checkControl(int addr) {
        synchronized (registers) {
            if (addr == INTFIQ) {  // FIQ
                int val = registers[INTFIQ - INTBASE];
                boolean enable = (val & 0x080) != 0;
                String msg = enable ? "enabled" : "disabled";
                int source = val & 0x07f;
                switch (source) {
                    case bitC1:
                        c1FIQEnabled = enable;
                        deb("FIQ C1 " + msg);
                        break;
                    case bitC3:
                        c3FIQEnabled = enable;
                        deb("FIQ C3 " + msg);
                        break;
                    case 32 + bitGPIO:
                        gpioFIQEnabled = enable;
                        deb("FIQ GPIO " + msg);
                        break;
                }
            } else {  // IRQs
                try {
                    int val = guacarm.Utils.readIntFromBytes(registers, 4, INTENIRQ1 - INTBASE);
                    if ((val & (1 << bitC1)) != 0) {
                        c1IRQEnabled = true;
                        deb("IRQ C1 enabled");
                    }
                    if ((val & (1 << bitC3)) != 0) {
                        c3IRQEnabled = true;
                        deb("IRQ C3 enabled");
                    }
                    val = guacarm.Utils.readIntFromBytes(registers, 4, INTENIRQ2 - INTBASE);
                    if ((val & (1 << bitGPIO)) != 0) {
                        gpioIRQEnabled = true;
                        deb("IRQ GPIO enabled");
                    }
                    val = guacarm.Utils.readIntFromBytes(registers, 4, INTDISIRQ1 - INTBASE);
                    if ((val & (1 << bitC1)) != 0) {
                        c1IRQEnabled = false;
                        deb("IRQ C1 disabled");
                    }
                    if ((val & (1 << bitC3)) != 0) {
                        c3IRQEnabled = false;
                        deb("IRQ C3 disabled");
                    }
                    val = guacarm.Utils.readIntFromBytes(registers, 4, INTDISIRQ2 - INTBASE);
                    if ((val & (1 << bitGPIO)) != 0) {
                        gpioIRQEnabled = false;
                        deb("IRQ GPIO disabled");
                    }
                } catch (Exception e) {
                    System.err.println("Error reading control registers of interrupt controller");
                }
            }
        }
    }

    /**
     * Activacion de la linea de interrupcion conectada a la entrada GPIO del
     * controlador de interrupciones
     */
    public void throwGPIOInt() {
        if (gpioFIQEnabled) {
            setGPIOIntPending();
            appApi.throwFIQ();
            deb("raise FIQ GPIO interrupt");
        } else {
            deb("cannot raise FIQ GPIO interrupt (disabled)");
        }
        if (gpioIRQEnabled) {
            setGPIOIntPending();
            appApi.throwIRQ();
            deb("raise IRQ GPIO interrupt");
        } else {
            deb("cannot raise IRQ GPIO interrupt (disabled)");
        }
    }

    /**
     * Activacion de una de las lineas de interrupcion conectadas a los
     * contadores del timer
     *
     * @param cont contador asociado a la linea activada
     */
    public void throwTimerInt(int cont) {
        if (cont == 1) { // contador c1
            if (c1FIQEnabled) {
                setC1IntPending();
                appApi.throwFIQ();
                deb("raise FIQ C1 interrupt");
            } else {
                deb("cannot raise FIQ C1 interrupt (disabled)");
            }
            if (c1IRQEnabled) {
                setC1IntPending();
                appApi.throwIRQ();
                deb("raise IRQ C1 interrupt");
            } else {
                deb("cannot raise IRQ C1 interrupt (disabled)");
            }
        } else if (cont == 3) { // contador c3
            if (c3FIQEnabled) {
                setC3IntPending();
                appApi.throwFIQ();
                deb("raise FIQ C3 interrupt");
            } else {
                deb("cannot raise FIQ C3 interrupt (disabled)");
            }
            if (c3IRQEnabled) {
                setC3IntPending();
                appApi.throwIRQ();
                deb("raise IRQ C3 interrupt");
            } else {
                deb("cannot raise IRQ C3 interrupt (disabled)");
            }
        }
    }

    private void setGPIOIntPending() {
        setIntPending(bitGPIO);
    }

    private void setC1IntPending() {
        setIntPending(bitC1);
    }

    private void setC3IntPending() {
        setIntPending(bitC3);
    }

    /**
     * Activa los bits correspondientes para indicar que se ha producido una
     * interrupcion y que está pendiente
     *
     * @param type tipo de interrupcion (bitGPIO gpio, bitC1 c1, bitC3 c3)
     */
    private static final int BITINPR1 = 8;
    private static final int BITINPR2 = 9;

    private void setIntPending(int type) {
        synchronized (registers) {
            try {
                switch (type) {
                    case bitGPIO:
                        int val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENBASIC - INTBASE);
                        val |= (1 << BITINPR2);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENBASIC - INTBASE);
                        val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENIRQ2 - INTBASE);
                        val |= (1 << bitGPIO);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENIRQ2 - INTBASE);
                        gpioIRQPending = true;
                        break;
                    case bitC1:
                        val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENBASIC - INTBASE);
                        val |= (1 << BITINPR1);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENBASIC - INTBASE);
                        val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENIRQ1 - INTBASE);
                        val |= (1 << bitC1);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENIRQ1 - INTBASE);
                        c1IRQPending = true;
                        break;
                    case bitC3:
                        val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENBASIC - INTBASE);
                        val |= (1 << BITINPR1);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENBASIC - INTBASE);
                        val = guacarm.Utils.readIntFromBytes(registers, 4, INTPENIRQ1 - INTBASE);
                        val |= (1 << bitC3);
                        guacarm.Utils.writeIntToBytes(val, registers, 4, INTPENIRQ1 - INTBASE);
                        c3IRQPending = true;
                        break;
                }
            } catch (Exception e) {
                System.err.println("Error setting bits of pending interrupt register of interrupt controller");
            }
        }
    }

    /**
     * comprueba que la direccion esta correctamente alineada al tamaño memSize
     *
     * @param addr dirección
     * @param size tamaño
     * @return true si está bien alineada
     */
    public boolean memAlign(int addr, int size) {
        if (size < 1 || size > 4 || size == 3) {
            return false;
        }
        return (addr & (size - 1)) == 0;
    }

    /**
     * Realiza un reset del timer
     */
    public void reset() {
        for (int i = 0; i < registers.length; i++) {
            registers[i] = 0;
        }
    }

    private void deb(String str) {
        guacarm.plugins.builtin.berryclip.Plugin.printDebug(log, debug, "Int.Controller", str);
    }
}
