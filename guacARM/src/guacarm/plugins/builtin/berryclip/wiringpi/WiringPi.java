/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.wiringpi;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import guacarm.plugins.builtin.berryclip.audio.TonePlayer;
import guacarm.plugins.builtin.berryclip.raspberryemu.GPIO;
import guacarm.plugins.builtin.berryclip.raspberryemu.Timer;
import guacarm.plugins.interfaces.PluginSVCCallBack;
import guacarm.plugins.interfaces.SimulatorInterface;
import javax.sound.sampled.LineUnavailableException;

/**
 * WiringPi library emulation for the berryclip plugin
 *
 * @author corbera
 */
public class WiringPi {

    public static final int SVCID = 14;

    private final GPIO gpio;
    private final Timer timer;
    private TonePlayer tonePlayer;
    private final PluginSVCCallBack serviceCallBack;
    private final Log log;
    private boolean init;

    private static final int[] LEDS = {13, 12, 14, 0, 3, 2};
    private static final int BUZZER = 7;
    private static final int[] BUTTONS = {8, 9};

    public WiringPi(GPIO gpio, Timer timer, Log log) {
        this.gpio = gpio;
        this.timer = timer;
        serviceCallBack = new WiringPiCallBack();
        this.log = log;
        init = false;
        try {
            tonePlayer = new TonePlayer();
        } catch (LineUnavailableException ex) {
            log.println("Error: cannot play sounds", ColorTheme.WARNING);
        }
    }

    public void reset() {
        init = false;
    }

    public PluginSVCCallBack getServiceCallBack() {
        return serviceCallBack;
    }

    private int wiringPi2GPIO(int wiringpin) {
        int[] trans = new int[]{
            17, // 0
            -1, // 1
            27, // 2
            22, // 3
            -1, // 4
            -1, // 5
            -1, // 6
            4, // 7
            2, // 8
            3, // 9
            -1, // 0
            -1, // 11
            10, // 12
            9, // 13
            11 // 14
        };
        if (wiringpin >= 0 && wiringpin < trans.length) {
            return trans[wiringpin];
        }
        return -1;
    }

    /**
     * Implements all the basic functions of library wiringPi called by service
     * call SVCID (r7). Function is selcected by r6
     *
     * @param api api with the simulator
    */
    private void doWiringPiFunctionCall(SimulatorInterface api) {
        try {
            int function = api.readRegister(6);
            if (init || function == 0 || function == 100) {
                init = true;
                switch (function) {
                    case 1: // void pinMode(int pin, int mode)
                        int r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        int gpiopin = wiringPi2GPIO(r0Content);
                        pinMode(gpiopin, api.readRegister(SimulatorInterface.REGISTER_R1));
                        break;
                    case 2: // void digitalWrite(int pin, int value)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        gpiopin = wiringPi2GPIO(r0Content);
                        digitalWrite(gpiopin, api.readRegister(SimulatorInterface.REGISTER_R1));
                        break;
                    case 3: // int  digitalRead(int pin)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        gpiopin = wiringPi2GPIO(r0Content);
                        api.writeRegister(SimulatorInterface.REGISTER_R0, digitalRead(gpiopin));
                        break;
                    case 4: // unsigned int millis()
                        api.writeRegister(SimulatorInterface.REGISTER_R0, millis());
                        break;
                    case 5: // unsigned int micros()
                        api.writeRegister(SimulatorInterface.REGISTER_R0, micros());
                        break;
                    case 6: // void delay(unsigned int r0Content)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        delay(r0Content);
                        break;
                    case 7: // void delayMicroseconds(unsigned int r0Content)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        delayMicroseconds(r0Content);
                        break;
                    /**
                     * funciones de la librería libBerry de las practicas de TC
                     */
                    case 100: // initBerry
                        initClipBerryPins();
                        break;
                    case 101: // void setLeds(int ledsVals)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        setLedsState(r0Content);
                        break;
                    case 102: // void playNote(int period_us, int duration_ms)
                        r0Content = api.readRegister(SimulatorInterface.REGISTER_R0);
                        int r1Content = api.readRegister(SimulatorInterface.REGISTER_R1);
                        tonePlayer.play(
                                r0Content,
                                r1Content
                        );
                        break;
                }
            }
        } catch (LineUnavailableException ex) {
            log.println("Libberry library: error in line", ColorTheme.WARNING);            
        } catch (Exception ex) {
            log.println("WiringPi library: error reading/writing register", ColorTheme.WARNING);
        }
    }

    /**
     * This sets the mode of a pin to either INPUT (0), OUTPUT (1)
     *
     * @param pin pin number
     * @param mode mode (0 - input, 1 - output)
     */
    private void pinMode(int pin, int mode) {
        gpio.setModeGPIOPin(pin, mode == 0 ? GPIO.PinMode.IN : GPIO.PinMode.OUT);
    }

    private void initClipBerryPins() {
        for (int pin : LEDS) {
            gpio.setModeGPIOPin(wiringPi2GPIO(pin), GPIO.PinMode.OUT);
        }
        gpio.setModeGPIOPin(wiringPi2GPIO(BUZZER), GPIO.PinMode.OUT);
        for (int pin : BUTTONS) {
            gpio.setModeGPIOPin(wiringPi2GPIO(pin), GPIO.PinMode.IN);
        }
    }

    /**
     * Writes the value HIGH or LOW (1 or 0) to the given pin which must have
     * been previously set as an output.
     *
     * @param pin pin to write
     * @param value value to write in pin
     */
    private void digitalWrite(int pin, int value) {
        gpio.setStateGPIOPinSoftware(pin, value);
    }

    /**
     * Establece el estado de los 6 leds a la vez (los 6 bits menos sig. del
     * valor dado, con el bit menos significativo para LED Rojo 1 y el más
     * significativo para LED Verde 2.
     *
     * @param state
     */
    private void setLedsState(int state) {
        for (int i = 0; i < 6; i++) {
            gpio.setStateGPIOPinSoftware(wiringPi2GPIO(LEDS[i]), state & 0x01);
            state >>>= 1;
        }
    }

    /**
     * This function returns the value read at the given pin. It will be HIGH or
     * LOW (1 or 0) depending on the logic level at the pin.
     *
     * @param pin pin to read
     * @return value readed
     */
    private int digitalRead(int pin) {
        return gpio.getStateGPIOPin(pin);
    }

    /**
     * This returns a number representing the number of milliseconds since timer
     * was initialized. It returns an unsigned 32-bit number.
     *
     * @return number of milliseconds since initialization
     */
    private int millis() {
        return timer.getMillisElapsed();
    }

    /**
     * This returns a number representing the number of microseconds since timer
     * was initialized. It returns an unsigned 32-bit number.
     *
     * @return number of microseconds since initialization
     */
    private int micros() {
        return timer.getMicrosElapsed();
    }

    /**
     * This causes program execution to pause for at least r0Content
     * milliseconds.
     *
     * @param howLong number of milliseconds
     */
    private void delay(int howLong) {
        try {
            Thread.sleep(howLong);
        } catch (InterruptedException ex) {
            log.println("WiringPi library: delay() interrupted", ColorTheme.WARNING);
        }
    }

    /**
     * This causes program execution to pause for at least r0Content
     * microseconds.
     *
     * @param howLong number of microseconds
     */
    private void delayMicroseconds(int howLong) {
        long start = System.nanoTime();
        long end;
        do {
            end = System.nanoTime();
        } while ((end - start) / 1000 < howLong);
    }

    private class WiringPiCallBack implements PluginSVCCallBack {

        @Override
        public void callBack(SimulatorInterface api) {
            doWiringPiFunctionCall(api);
        }
    }

}
