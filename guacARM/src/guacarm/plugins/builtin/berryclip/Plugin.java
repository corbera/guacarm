/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import guacarm.plugins.builtin.BuiltinPluginClass;
import guacarm.plugins.builtin.berryclip.audio.TonePlayer;
import guacarm.plugins.interfaces.PluginInterface;
import guacarm.plugins.interfaces.PluginMemCallBack;
import guacarm.plugins.interfaces.SimulatorInterface;
import guacarm.plugins.builtin.berryclip.ui.BerryclipPanel;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JMenu;
import guacarm.plugins.builtin.berryclip.raspberryemu.GPIO;
import guacarm.plugins.builtin.berryclip.raspberryemu.GPIOCallBack;
import guacarm.plugins.builtin.berryclip.raspberryemu.InterruptController;
import guacarm.plugins.builtin.berryclip.raspberryemu.Timer;
import guacarm.plugins.builtin.berryclip.raspberryemu.TimerCallBack;
import guacarm.plugins.builtin.berryclip.ui.ButtonCallBack;
import guacarm.plugins.builtin.berryclip.wiringpi.WiringPi;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * Berryclip plugin
 *
 * @author corbera
 * @param <T>
 */
public class Plugin<T> implements BuiltinPluginClass<T> {

    private class LED {

        public boolean status;
        public int pin;
        public String id;
        public int nled;

        LED(boolean status, int pin, String id, int nled) {
            this.status = status;
            this.pin = pin;
            this.id = id;
            this.nled = nled;
        }
    }

    private T type;

    private final int[] buttonGPIO = {2, 3};
    private final int[] ledGPIO = {9, 10, 11, 17, 22, 27};
    private final String ledID[] = {"RED1", "RED2", "YELLOW1", "YELLOW2", "GREEN1", "GREEN2"};
    private final int buzzerGPIO = 4;

    private boolean initialized = false;
    private int NLEDS;
    private Map<Integer, LED> leds;

    private GPIO gpio;
    private Timer timer;
    private InterruptController intController;
    private WiringPi wiringPi;
    private BerryclipPanel panel;
    private PluginInterface appAPI;
    private boolean wiringPiLibrary = false;
    private boolean libberryLibrary = false;
    private Properties config;

    private final int GPBASE = 0x3F200000;
    private final int STBASE = 0x3F003000;
    private final int INTBASE = 0x3F00B200;

    @Override
    public BuiltinPluginClass<T> setType(T type) {
        this.type = type;
        return this;
    }

    @Override
    public T getType() {
        return type;
    }

    @Override
    public boolean initializeGUIPlugIn(PluginInterface pi) {
        boolean ledStatus[] = {false, false, false, false, false, false};

        appAPI = pi;
        if (ledStatus.length != ledGPIO.length || ledStatus.length != ledID.length) {
            return false;
        }
        NLEDS = ledStatus.length;

        gpio = new GPIO(appAPI, GPBASE);
        timer = new Timer(appAPI, new MyTimerCallBack(), STBASE);
        intController = new InterruptController(appAPI, INTBASE);

        config = pi.getConfiguration();
        gpio.debug = "T".equals(config.getProperty("gpiodebug", "F"));
        timer.debug = "T".equals(config.getProperty("timerdebug", "F"));
        intController.debug = "T".equals(config.getProperty("intControllerdebug", "F"));
        wiringPiLibrary = "T".equals(config.getProperty("wiringPiLibrary", "F"));
        libberryLibrary = "T".equals(config.getProperty("libberryLibrary", "F"));

        // register callback for berryclip buttons events
        ButtonCallBack myButtonCallBack = new MyButtonCallBack();
        panel = new BerryclipPanel(myButtonCallBack);

        // register a callback for all the GPIO pins wired with leds in the berryclip borad
        MyGPIOCallBack myGPIOCallBack = new MyGPIOCallBack();
        leds = new HashMap<>();
        for (int i = 0; i < NLEDS; i++) {
            leds.put(ledGPIO[i], new LED(ledStatus[i], ledGPIO[i], ledID[i], i));
            gpio.registerGPIOCallBack(ledGPIO[i], myGPIOCallBack);
        }

        // register a callback for the GPIO pin wired with the buzzer in the berryclip borad
        MyBuzzerGPIOCallBack myBuzzerGPIOCallBack = new MyBuzzerGPIOCallBack(panel, timer);
        gpio.registerGPIOCallBack(buzzerGPIO, myBuzzerGPIOCallBack);

        // register callback for memory access to GPIO registers
        GPIOMemCallBack myGPIOMemCallBack = new GPIOMemCallBack();
        if (!pi.registerMemoryReadWriteHook(GPBASE, gpio.getMemSize(), myGPIOMemCallBack)) {
            pi.getLog().println("Memory hook for GPIO can't be create", ColorTheme.WARNING);
            return false;
        }

        // register callback for memory access to Timer registers
        TimerMemCallBack myTimerMemCallBack = new TimerMemCallBack();
        if (!pi.registerMemoryReadWriteHook(STBASE, timer.getMemSize(), myTimerMemCallBack)) {
            pi.getLog().println("Memory hook for Timer can't be create", ColorTheme.WARNING);
            return false;
        }

        // register callback for memory access to Interrupt Controller registers
        IntControllerMemCallBack myIntControllerMemCallBack = new IntControllerMemCallBack();
        if (!pi.registerMemoryReadWriteHook(INTBASE, intController.getMemSize(), myIntControllerMemCallBack)) {
            pi.getLog().println("Memory hook for Interrupt controller can't be create", ColorTheme.WARNING);
            return false;
        }

        // create the wiringPi library and register the service call
        // SVCID (r7=SVCID) for access to all the library functions
        wiringPi = new WiringPi(gpio, timer, pi.getLog());
        if (!pi.registerSVCCall(wiringPi.SVCID, wiringPi.getServiceCallBack())) {
            pi.getLog().println("Service call (swi/svc SVCID) for wiringPi library can't be created", ColorTheme.WARNING);
            return false;
        }

        // habilitamos las interrupciones
        appAPI.enableInterruptions();

        initialized = true;
        return true;
    }

    @Override
    public void finalizePlugIn() {
        appAPI.disableInterruptions();
        appAPI.saveConfiguration(config);
    }

    @Override
    public boolean initializeConsolePlugIn(PluginInterface pi) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        return "Berryclip Plugin";
    }

    @Override
    public String getShortName() {
        return "Berryclip";
    }

    @Override
    public String getAuthor() {
        return "Francisco Corbera";
    }

    @Override
    public String getVersion() {
        return "0.1";
    }

    @Override
    public Component getAWTComponent() {
        return panel;
    }

    private JCheckBoxMenuItem wiringPiBoxItem;
    private JCheckBoxMenuItem libBerryBoxItem;

    @Override
    public JMenu getJMenu() {
        JMenu m = new JMenu();

        wiringPiBoxItem = new JCheckBoxMenuItem("wiringPi library");
        wiringPiBoxItem.setSelected(wiringPiLibrary);
        wiringPiBoxItem.addActionListener(e -> {
            wiringPiLibrary = wiringPiBoxItem.getState();
            config.setProperty("wiringPiLibrary", wiringPiLibrary ? "T" : "F");
            if (!wiringPiLibrary) {
                libberryLibrary = false;
                libBerryBoxItem.setState(false);
                config.setProperty("libberryLibrary", "F");
            }
        });
        m.add(wiringPiBoxItem);

        libBerryBoxItem = new JCheckBoxMenuItem("libBerry library");
        libBerryBoxItem.setSelected(libberryLibrary);
        libBerryBoxItem.addActionListener(e -> {
            libberryLibrary = libBerryBoxItem.getState();
            config.setProperty("libberryLibrary", libberryLibrary ? "T" : "F");
            if (libberryLibrary) {
                wiringPiLibrary = true;
                wiringPiBoxItem.setState(true);
                config.setProperty("wiringPiLibrary", "T");
            }
        });
        m.add(libBerryBoxItem);

        JMenu debug = new JMenu("Debug");
        final JCheckBoxMenuItem boxItem2 = new JCheckBoxMenuItem("Debug GPIO");
        boxItem2.setSelected(gpio.debug);
        boxItem2.addActionListener(e -> {
            gpio.debug = boxItem2.isSelected();
            config.setProperty("gpiodebug", gpio.debug ? "T" : "F");
        });
        final JCheckBoxMenuItem boxItem3 = new JCheckBoxMenuItem("Debug Timer");
        boxItem3.setSelected(timer.debug);
        boxItem3.addActionListener(e -> {
            timer.debug = boxItem3.isSelected();
            config.setProperty("timerdebug", timer.debug ? "T" : "F");
        });
        final JCheckBoxMenuItem boxItem4 = new JCheckBoxMenuItem("Debug Int. Contr.");
        boxItem4.setSelected(intController.debug);
        boxItem4.addActionListener(e -> {
            intController.debug = boxItem4.isSelected();
            config.setProperty("intControllerdebug", intController.debug ? "T" : "F");
        });
        debug.add(boxItem2);
        debug.add(boxItem3);
        debug.add(boxItem4);
        m.add(debug);

        JMenuItem item = new JMenuItem("Speedup factor");
        item.addActionListener(e -> {
            String out = JOptionPane.showInputDialog(m, "Speedup factor", timer.getSpeedupFactor());
            if (out == null) {
                return;
            }
            int newval = -1;
            try {
                newval = Integer.parseInt(out);
            } catch (NumberFormatException ex) {
            }
            if (newval < 0 || newval > 1000) {
                JOptionPane.showMessageDialog(m, "Speedup factor must be between 1 and 1000", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                timer.setSpeedupFactor(newval);
            }
        });
        m.add(item);
        return m;
    }

    @Override
    public void reset(SimulatorInterface si) {
        leds.values().forEach((led) -> {
            led.status = false;
        });
        gpio.reset();
        timer.reset();
        wiringPi.reset();
        panel.reset();
        gpio.setStateGPIOPin(buttonGPIO[0], 1);
        gpio.setStateGPIOPin(buttonGPIO[1], 1);
        appAPI.enableInterruptions();
        refreshStatus();
    }

    @Override
    public List<String> getBuiltinLibraries() {
        List<String> builtin = new ArrayList<>();
        if (wiringPiLibrary) {
            builtin.add("/resources/wiringpi/libraryWiringPiPlugin.s");
            if (libberryLibrary) {
                builtin.add("/resources/wiringpi/libraryLibBerryPlugin.s");
            }
        }
        return builtin;
    }

    /**
     * Callbak associated with the memory block hook
     */
    public class GPIOMemCallBack implements PluginMemCallBack {

        @Override
        public void callBack(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
            doActionGPIO(si, type, offset, size, dataWrite);
        }
    }

    /**
     * Actions to do when memory block is accessed
     *
     * @param si Inteface to talk with the simulator
     * @param type type of memory access (PluginMemCallBack.AccessType.READ or
     * PluginMemCallBack.AccessType.WRITE)
     * @param offset address of memory access
     * @param size size in bytes of memory access
     * @param dataWrite data to write in case of write access
     */
    private void doActionGPIO(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
        if (!initialized) {
            return;
        }
        byte[] myData = {0, 0, 0, 0};

        if (type.equals(PluginMemCallBack.AccessType.WRITE)) {
            gpio.write(offset, size, dataWrite);
        } else {
            dataWrite = gpio.read(offset, size);
        }
        try {
            myData[0] = (byte) (dataWrite & 0x000000FF);
            myData[1] = (byte) ((dataWrite & 0x0000FF00) >>> 8);
            myData[2] = (byte) ((dataWrite & 0x00FF0000) >>> 16);
            myData[3] = (byte) ((dataWrite & 0xFF000000) >>> 24);
            // write at the memory offset
            si.writeMemory(offset, myData, size);
        } catch (Exception ex) {
            appAPI.getLog().println("Error writing in memory", ColorTheme.WARNING);
        }
    }

    /**
     * This callback will be called when one of the GPIO pinnes associated with
     * the leds has changed due to a write in the GPIO memory
     */
    private class MyGPIOCallBack implements GPIOCallBack {

        @Override
        public void onChange(int pin) {
            leds.get(pin).status = (gpio.getStateGPIOPin(pin) == 1);
            LED led = leds.get(pin);
            //System.out.println("Led " + led.id + " está " + (led.status ? "encendido" : "apagado"));
            panel.setLed(led.nled, led.status);
            appAPI.awtComponentModifed();
        }

    }

    /**
     * This callback will be called when the GPIO pin associated with the buzzer
     * has changed due to a write in the GPIO memory
     */
    private class MyBuzzerGPIOCallBack implements GPIOCallBack {

        private long[] count = {0, 0, 0, 0};
        private long[] times = {0, 0, 0, 0};
        private int ind = 0;
        private final long delta = 1000000000 / times.length; // 0.25 segundos
        private final BerryclipPanel panel;
        private TonePlayer player;
        private final Timer timer;

        public MyBuzzerGPIOCallBack(BerryclipPanel panel, Timer timer) {
            this.panel = panel;
            this.timer = timer;
            this.player = null;
            try {
                this.player = new TonePlayer();
            } catch (LineUnavailableException ex) {
                Logger.getLogger(Plugin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //private long c = 0;
        @Override
        public void onChange(int pin) {
            //System.out.println("c "+(c++));
            long actualTime = System.nanoTime();
            if (actualTime - times[ind] < delta * timer.speedupFactor) {
                count[ind]++;
            } else {
                //System.out.println((count[ind] << 1) + "Hz " + ind);
                if (panel != null) {
                    panel.setBuzzerHz((int) (count[ind] << 1));
                }
                int pl = (ind + times.length - 1) % times.length;
                if (panel.soundActive() && player != null && count[pl] > 0) {
                    try {
                        int pe = (int) (1000000L / count[pl]);
                        player.playNonBlock(pe, 250 * timer.speedupFactor);
                    } catch (Exception ex) {
                    }
                }
                ind = (ind + 1) % times.length;
                count[ind] = 1;
                times[ind] = actualTime;
            }
        }

    }

    private void refreshStatus() {
        leds.values().forEach((led) -> {
            //System.out.println("Led " + led.id + " está " + (led.status ? "encendido" : "apagado"));
            panel.setLed(led.nled, led.status);
        });
    }

    /**
     * Callbak associated with the memory block hook
     */
    public class TimerMemCallBack implements PluginMemCallBack {

        @Override
        public void callBack(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
            doActionTimer(si, type, offset, size, dataWrite);
        }
    }

    /**
     * Actions to do when memory block is accessed
     *
     * @param si Inteface to talk with the simulator
     * @param type type of memory access (PluginMemCallBack.AccessType.READ or
     * PluginMemCallBack.AccessType.WRITE)
     * @param offset address of memory access
     * @param size size in bytes of memory access
     * @param dataWrite data to write in case of write access
     */
    private void doActionTimer(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
        if (!initialized) {
            return;
        }
        byte[] myData = {0, 0, 0, 0};

        try {
            if (type.equals(PluginMemCallBack.AccessType.WRITE)) {
                timer.write(offset, size, dataWrite);
            } else {
                dataWrite = timer.read(offset, size);
            }
        } catch (Exception ex) {
            appAPI.getLog().println(ex.toString(), ColorTheme.WARNING);
        }
        try {
            myData[0] = (byte) (dataWrite & 0x000000FF);
            myData[1] = (byte) ((dataWrite & 0x0000FF00) >>> 8);
            myData[2] = (byte) ((dataWrite & 0x00FF0000) >>> 16);
            myData[3] = (byte) ((dataWrite & 0xFF000000) >>> 24);
            // write at the memory offset
            si.writeMemory(offset, myData, size);
        } catch (Exception ex) {
            appAPI.getLog().println("Error writing in memory", ColorTheme.WARNING);
        }
    }

    /**
     * This callback will be called when one of the buttons on the berryclip UI
     * get pressed
     */
    public class MyButtonCallBack implements ButtonCallBack {

        @Override
        public void onChange(int button, int state) {  // state = 0 pulsar, stat = 1 soltar
            gpio.setStateGPIOPin(buttonGPIO[button], state);
            if (gpio.throwGPIOInt(buttonGPIO[button], state)) {
                intController.throwGPIOInt();
            }
        }

    }

    /**
     * This callback will be called when the counter CLO reaches C1 or C3
     * register values
     */
    public class MyTimerCallBack implements TimerCallBack {

        @Override
        public void onChange(int counter) {
            intController.throwTimerInt(counter);
        }

    }

    /**
     * Callbak associated with the interrupt controller memory block hook
     */
    public class IntControllerMemCallBack implements PluginMemCallBack {

        @Override
        public void callBack(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
            doActionIntController(si, type, offset, size, dataWrite);
        }
    }

    /**
     * Actions to do when memory block of Interrupt controller is accessed
     *
     * @param si Inteface to talk with the simulator
     * @param type type of memory access (PluginMemCallBack.AccessType.READ or
     * PluginMemCallBack.AccessType.WRITE)
     * @param offset address of memory access
     * @param size size in bytes of memory access
     * @param dataWrite data to write in case of write access
     */
    private void doActionIntController(SimulatorInterface si, PluginMemCallBack.AccessType type, int offset, int size, int dataWrite) {
        if (!initialized) {
            return;
        }
        try {
            if (type.equals(PluginMemCallBack.AccessType.WRITE)) {
                intController.write(offset, size, dataWrite);
            } else {
                dataWrite = intController.read(offset, size);
            }
        } catch (Exception ex) {
            appAPI.getLog().println(ex.toString(), ColorTheme.WARNING);
        }
//        try {
//            myData[0] = (byte) (dataWrite & 0x000000FF);
//            myData[1] = (byte) ((dataWrite & 0x0000FF00) >>> 8);
//            myData[2] = (byte) ((dataWrite & 0x00FF0000) >>> 16);
//            myData[3] = (byte) ((dataWrite & 0xFF000000) >>> 24);
//            // write at the memory offset
//            si.writeMemory(offset, myData, size);
//        } catch (Exception ex) {
//            appAPI.getLog().println("Error writing in memory", ColorTheme.WARNING);
//        }
    }

    public static void printDebug(Log log, boolean debug, String id, String msg) {
        if (debug) {
            log.println("[" + System.nanoTime() / 1000 + "us][" + id + "] " + msg, ColorTheme.DEBUG);
        }
    }
}
