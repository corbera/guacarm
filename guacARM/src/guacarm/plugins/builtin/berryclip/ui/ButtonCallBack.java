/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.builtin.berryclip.ui;

/**
 *
 * @author casa
 */
public interface ButtonCallBack {
    /**
     * Método que se llamará cuando en el panel cambie el estado de un boton
     * @param button botón que ha cambiado de estado
     * @param state nuevo estado del botón (0 pulsado, 1 libre)
     */
    public void onChange(int button, int state);
}
