package guacarm.plugins;

import guacarm.simulator.SimulatorHooks;
import guacarm.simulator.Simulator;
import guacarm.plugins.interfaces.PluginClass;
import guacarm.plugins.interfaces.PluginSVCCallBack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author corbera
 */
public class PluginSVC {

    private final Simulator simulator;
    private final PluginClass[] services = new PluginClass[SimulatorHooks.NSVC];

    public PluginSVC(Simulator s) {
        simulator = s;
    }

    /**
     * Registra el callback para un numero de servicio dado (si no esta
     * previamente registrado)
     *
     * @param svc numero del servicio a registrar
     * @param callBack
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerSVC(int svc, PluginSVCCallBack callBack, PluginClass plID) {
        if (svc < 0 || svc >= SimulatorHooks.NSVC
                || services[svc] != null) {
            return false;
        }
        if (!simulator.services.registerSVC(svc, callBack))
            return false;
        services[svc] = plID;
        return true;
    }

    /**
     * Elimina el registro de un servicio
     *
     * @param svc numero del servicio
     * @param plID plugin que lo intenta eliminar
     * @return true si se ha eliminado
     */
    public boolean unRegisterSVC(int svc, PluginClass plID) {
        if (svc < 0 || svc >= SimulatorHooks.NSVC
                || services[svc] != plID) {
            return false;
        }
        simulator.services.unRegisterSVC(svc);        
        services[svc] = null;
        return true;
    }

    /**
     * Devuelve el plugin asociado a un servicio dado
     *
     * @param svc numero del servicio
     * @return plugin asociado o null
     */
    public PluginClass getPlugInSVC(int svc) {
        return services[svc];
    }

    /**
     * Elimina todos los servicios registrados a un plugin
     *
     * @param pl plugin
     */
    public void unRegisterPlugIn(PluginClass pl) {
        for (int i = 0; i < SimulatorHooks.NSVC; i++) {
            if (services[i] == pl) {
                services[i] = null;
                simulator.services.unRegisterSVC(i);        
            }
        }
    }
}
