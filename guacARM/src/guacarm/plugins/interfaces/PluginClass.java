/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

import java.awt.Component;
import javax.swing.JMenu;

/**
 *
 * @author corbera
 */
public interface PluginClass {

    /**
     * Inicializa el plugin para simulacion en modo ventana y le pasa el objeto
     * API para acceder a la funcionalidad del simulador
     *
     * @param api API de los plugins
     * @return true si se ha podido inicializar
     */
    public boolean initializeGUIPlugIn(PluginInterface api);

    /**
     * Inicializa el plugin para simulacion en modo linea de comandos y le pasa
     * el objeto API para acceder a la funcionalidad del simulador
     *
     * @param api API de los plugins
     * @return true si se ha podido inicializar
     */
    public boolean initializeConsolePlugIn(PluginInterface api);

    /**
     * Para llevar a cabo las acciones asociadas a la desactivacion del plugin
     */
    public void finalizePlugIn();

    /**
     * Devuelve el nombre del plugin
     *
     * @return nombre del plugin
     */
    public String getName();

    /**
     * Devuelve el nombre corto del plugin, usado para su identificacion dentro
     * de la aplicacion
     *
     * @return nombre corto del plugin
     */
    public String getShortName();

    /**
     * Devuelve el nombre del autor del plugin
     *
     * @return nombre del autor
     */
    public String getAuthor();

    /**
     * Devuelve la getVersion del plugin
     *
     * @return getVersion
     */
    public String getVersion();

    /**
     * Devuelve le componente (AWT/SWING) que usara el plugin (null si no
     * utiliza ninguno)
     *
     * @return awt/swing componente del plugin
     */
    public Component getAWTComponent();

    /**
     * Devuelve un menu asociado al plugin
     *
     * @return el jmenu correspondiente
     */
    public JMenu getJMenu();

    /**
     * Metodo que se invocara cada vez que se realizce un reset del simulador
     *
     * @param api API del simulador
     */
    public void reset(SimulatorInterface api);
}
