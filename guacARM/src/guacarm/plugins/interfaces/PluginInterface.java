/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

import guacarm.log.Log;
import java.util.Properties;

/**
 *
 * @author corbera
 */
public interface PluginInterface {

    /**
     * Registra el callback para un numero de servicio dado (si no esta
     * previamente registrado)
     *
     * @param svc numero del servicio a registrar
     * @param callBack callback asociado al servicio
     * @return true si se ha podido registrar
     */
    public boolean registerSVCCall(int svc, PluginSVCCallBack callBack);

    /**
     * Elimina el callback asociado al numero de servicio dado si pertence a
     * plugin que lo llama
     *
     * @param svc numero del servico
     * @return true si lo ha podido eliminar
     */
    public boolean unRegisterSVCCall(int svc);

    /**
     * Indica si el servicio con identificador svc esta ya registrado
     *
     * @param svc numero de servicio
     * @return true si esta registrado
     */
    public boolean isRegisteredSVC(int svc);

    /**
     * Registra un callback para los accesos de lectura a una region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura en la
     * region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadHook(int offset, int size, PluginMemCallBack callback);

    /**
     * Registra un callback para los accesos de escritura a una region de
     * memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una escritura en
     * la region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryWriteHook(int offset, int size, PluginMemCallBack callback);

    /**
     * Registra un callback para los accesos de lectura/escritura a una region
     * de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una
     * lectura/escritura en la region
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback);

    /**
     * Elimina el registro de un hook de memoria
     *
     * @param offset direccion base de la memoria asociada al hook
     * @param size tamaño de la memoria asociada al hook
     * @return true si lo ha eliminado con exito
     */
    public boolean unRegisterMemoryHook(int offset, int size);

    /**
     * Devuelve puntero al objeto log de la aplicacion
     *
     * @return objeto log
     */
    public Log getLog();

    /**
     * Descarga el plugin a peticion del propio plugin
     */
    public void unLoad();

    /**
     * Indiaca a la aplicacion que el componente awt/swing asociado al plugin ha
     * cambiado
     */
    public void awtComponentModifed();
    
    /**
     * Habilita las interrupciones en el simulador
     */
    public void enableInterruptions();

    /**
     * Deshabilita las interrupciones en el simulador
     */
    public void disableInterruptions();
    
    /**
     * Simula la activacion de una linea de interrupcion del tipo IRQ
     */
    public void throwIRQ();
    
    /**
     * Simula la activacion de una linea de interrupcion del tipo FIQ
     */
    public void throwFIQ();
    
    /**
     * Guarda la configuracion del plugin que se le pase como parametro
     * @param config 
     */
    public void saveConfiguration(Properties config);
    
    /**
     * Lee la configuracion del plugin previamente guardada
     * @return la configuracion
     */
    public Properties getConfiguration();
}
