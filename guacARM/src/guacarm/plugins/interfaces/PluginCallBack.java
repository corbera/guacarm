/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

/**
 * Interfaz que tienen que implementar las clases que quieran registrar una
 * ServiceCall. Cuando se produzca la svc adecuada se invocara el método
 * callBack de la clase SVCCallBack
 * 
 *  * @author corbera
 */
public interface PluginCallBack {
    public void callBack(SimulatorInterface api);
}


