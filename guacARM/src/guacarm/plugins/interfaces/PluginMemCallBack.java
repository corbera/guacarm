/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

/**
 * Interfaz que tienen que implementar las clases que quieran registrar una
 * ServiceCall. Cuando se produzca la svc adecuada se invocara el método
 * callBack de la clase SVCCallBack
 * 
 *  * @author corbera
 */
public interface PluginMemCallBack {
    public enum AccessType {READ, WRITE};
    /**
     * metodo que se llamara cuando se ejecute una instruccion que lea o escriba
     * en alguna posicion de memoria registrada por este plugin
     * @param api API para acceder a las funciones del simulador por parte del plugin
     * @param type typo de acceso (READ, WRITE)
     * @param address direccion accedida
     * @param size numero de bytes accedidos
     * @param data dato a escribir en el caso de un acceso tipo WRITE
     */
    public void callBack(SimulatorInterface api, AccessType type, int address, int size, int data);
}


