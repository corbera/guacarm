/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

/**
 *
 * @author corbera
 */
public interface SimulatorInterface {
    /**
     * Register names
     */
    public static final int REGISTER_R0 = 0;
    public static final int REGISTER_R1 = 1;
    public static final int REGISTER_R2 = 2;
    public static final int REGISTER_R3 = 3;
    public static final int REGISTER_R4 = 4;
    public static final int REGISTER_R5 = 5;
    public static final int REGISTER_R6 = 6;
    public static final int REGISTER_R7 = 7;
    public static final int REGISTER_R8 = 8;
    public static final int REGISTER_R9 = 9;
    public static final int REGISTER_R10 = 10;
    public static final int REGISTER_R11 = 11;
    public static final int REGISTER_R12 = 12;
    public static final int REGISTER_R13 = 13;
    public static final int REGISTER_R14 = 14;
    public static final int REGISTER_R15 = 15;
    public static final int REGISTER_SP = 13;
    public static final int REGISTER_LR = 14;
    public static final int REGISTER_PC = 15;
    public static final int REGISTER_CPSR = 16;    
    /**
     * Escribe un valor en un registro
     * 
     * @param reg registro a escribir
     * @param val valor a escribir
     * @throws Exception si no se puede escribir en el registro
     */
    public void writeRegister(int reg, int val) throws Exception;
    
    /**
     * Lee el contenido del registro indicado
     * 
     * @param reg registro a leer
     * @return valor almacenado en el registro
     * @throws Exception si no se puede leer el registro
     */
    public int readRegister(int reg) throws Exception;
    
    /**
     * Lee el contenido de memoria especidicado por offset y size
     *
     * @param offset
     * @param size
     * @return
     */
    public byte[] readMemory(int offset, int size) throws Exception;

    /**
     * Escribe en la posicion de memoria offset los bytes indicados por size
     * leidos de buff
     *
     * @param offset posicion de memoria donde escribir
     * @param buff datos a escribir
     * @param size numero de bytes a escribir
     */
    public void writeMemory(int offset, byte[] buff, int size) throws Exception;
    
    /**
     * Return the data segment offset
     * @return 
     */
    public int getDataOffset();

    /**
     * Return the code segment offset
     * @return 
     */
    public int getCodeOffset();

    /**
     * Return the stack segment offset
     * @return 
     */
    public int getStackOffset();
}
