/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guacarm.plugins.interfaces;

/**
 * Interfaz que tienen que implementar las clases que quieran registrar una
 * ServiceCall. Cuando se produzca la svc adecuada se invocara el método
 * callBack de la clase SVCCallBack
 * 
 *  * @author corbera
 */
public interface PluginSVCCallBack {
    /**
     * metodo que se llamara cuando se ejecute la correspondiente instruccion
     * SVC/SWI registrada por el plugin
     * @param api API para acceder a las funciones del simulador por parte del plugin
     */
    public void callBack(SimulatorInterface api);
}


