package guacarm.plugins;

import guacarm.GUI.ColorTheme;
import guacarm.log.Log;
import guacarm.simulator.Simulator;
import guacarm.plugins.interfaces.PluginClass;
import guacarm.plugins.interfaces.PluginMemCallBack;
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author corbera
 */
public class PluginMemHook {

    private class MemBlocksRegistered extends HashMap<PluginClass, HashMap<Integer, Integer>> {

        public void put(PluginClass plugin, Integer offset, Integer size) {
            if (!this.containsKey(plugin)) {
                HashMap<Integer, Integer> map = new HashMap<>();
                map.put(offset, size);
                this.put(plugin, map);
            } else {
                this.get(plugin).put(offset, size);
            }
        }
    }

    private final Simulator simulator;
    private final MemBlocksRegistered memBlocksRegistered;
    private final Log log;

    public PluginMemHook(Simulator s, Log log) {
        simulator = s;
        memBlocksRegistered = new MemBlocksRegistered();
        this.log = log;
    }

    /**
     * Registra un callback para los accesos de lectura a una region de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una lectura en la
     * region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        size = alingSize(size);
        boolean reg = simulator.services.registerMemoryReadHook(offset, size, callback);
        if (reg) {
            memBlocksRegistered.put(plID, offset, size);
        }
        return reg;
    }

    /**
     * Registra un callback para los accesos de escritura a una region de
     * memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una escritura en
     * la region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryWriteHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        size = alingSize(size);
        boolean reg = simulator.services.registerMemoryWriteHook(offset, size, callback);
        if (reg) {
            memBlocksRegistered.put(plID, offset, size);
        }
        return reg;
    }

    /**
     * Registra un callback para los accesos de lectura/escritura a una region
     * de memoria
     *
     * @param offset direccion de comienzo de la region de memoria
     * @param size tamaño de la region de memoria
     * @param callback callback que se invocara cuando se haga una
     * lectura/escritura en la region
     * @param plID identificador del plugin que quiere registrar el servico
     * @return true si se ha podido registrar
     */
    public boolean registerMemoryReadWriteHook(int offset, int size, PluginMemCallBack callback, PluginClass plID) {
        size = alingSize(size);
        boolean reg = simulator.services.registerMemoryReadWriteHook(offset, size, callback);
        if (reg) {
            memBlocksRegistered.put(plID, offset, size);
        }
        return reg;
    }

    /**
     * Elimina el registro de un hook de memoria
     *
     * @param offset direccion de comienzo del bloque de memoria
     * @param size tamaño del bloque de memoria
     * @param plID identificador del plugin que se quiere quitar
     * @return true si lo ha eliminado con exito
     */
    public boolean unRegisterMemoryHook(int offset, int size, PluginClass plID) {
        if (memBlocksRegistered.containsKey(plID)
                && memBlocksRegistered.get(plID).containsKey(offset)) {
            int plsize = memBlocksRegistered.get(plID).get(offset);
            if (plsize != size) {
                log.println("Size mismatch unregistering memory hook", ColorTheme.WARNING);
            }
            if (simulator.services.unRegisterMemoryHook(offset, plsize)) {
                memBlocksRegistered.get(plID).remove(offset);
                return true;
            }
        }
        log.println("Memory hook can't be unregistered (plugin " + plID.getShortName() + ")", ColorTheme.WARNING);
        return false;
    }

    /**
     * Elimina todos los servicios registrados a un plugin
     *
     * @param pl plugin
     */
    public void unRegisterPlugIn(PluginClass pl) {
        boolean ok = true;
        if (memBlocksRegistered.containsKey(pl)) {
            HashMap<Integer, Integer> map = memBlocksRegistered.get(pl);
            for (int off : map.keySet()) {
                int size = map.get(off);
                if (!simulator.services.unRegisterMemoryHook(off, size)) {
                    log.println("Error unregistering memory hook at 0x" + off, ColorTheme.WARNING);
                    ok = false;
                }
            }
            map.clear();
        }
        if (!ok) {
            log.println("Plugin " + pl.getShortName() + " can't be completely removed", ColorTheme.WARNING);
        }
    }

    /**
     * Devuelve un multiplo de 1024 mayor o igual que size+1024 Se le suma 1024
     * a size ya que en la escritura en memoria si se escribe menos de 1024 da
     * error el simulador. Por eso, si se va a escribir al final del bloque de
     * tamaño size, se escribirá un bloque de 1024 a partir de él.
     *
     * @param size
     * @return
     */
    private int alingSize(int size) {
        return ((((size + 1024) - 1) >> 10) + 1) << 10;
    }
}
