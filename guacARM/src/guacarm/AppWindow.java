package guacarm;

import guacarm.log.Log;
import guacarm.log.StringLog;
import guacarm.log.WindowsLog;
import guacarm.GUI.FileActions;
import guacarm.GUI.RSyntaxtCodePane;
import guacarm.GUI.RegisterView;
import guacarm.GUI.ARMBinaryCodePane;
import guacarm.GUI.ColorTheme;
import guacarm.GUI.HelpWindow;
import guacarm.GUI.MemoryView;
import guacarm.plugins.PluginManager;
import guacarm.simulator.Simulator;
import guacarm.simulator.Assembler;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author casa
 */
public class AppWindow extends javax.swing.JFrame {

    public int newTabCount = 0;
    private final WindowsLog log;
    private FileActions fileActions;
    private Configuration configuration;
    private Simulator simulator;
    private Assembler assembler;
    private ARMBinaryCodePane simulationPane;
    private Callable modifiedCallBack;
    private boolean highlighted;
    private RegisterView registerView;
    private MemoryView memoryView;
    private SimulationWorker simulationWorker;
    private PluginManager plmanager;
    private AppLifeCycle appLifeCycle;
    private HelpWindow helpWindow;

    /**
     * Creates new form NewJFrame
     */
    public AppWindow() {
        this.setTitle(Configuration.APPNAME + " " + Configuration.VERSION);
        // log para el modo grafico
        log = new WindowsLog();
        // Acciones relacionadas con el menu File. Se inicializa con el 
        // directorio leido de la configuracion o el del usuario si no se guardo
        fileActions = new FileActions(log);
        // simulador de ensamblador ARM
        simulator = new Simulator(log);
        // ensamblador de codigo ARM
        assembler = new Assembler(log);

        // callback asociado a la modificacion de un codigo ARM. Debe poner
        // la aplicacion en modo edicion (no se puede simular)
        // tambien quita el resalte de las lineas del codigo
        // Se cambia el titulo del tap para indicar que el fichero ha sido
        // modificado
        modifiedCallBack = new Callable() {
            @Override
            public Object call() {
                appLifeCycle.doAction(Actions.FILEACTION);
                //setEditionMode();
                if (highlighted) {
                    highlighted = false;
                    for (Component rsc : codejTabbedPane.getComponents()) {
                        ((RSyntaxtCodePane) rsc).unsetHighlight();
                    }
                }
                renameTabs(codejTabbedPane);
                return null;
            }
        };
        // no hay texto resaltado
        highlighted = false;

        initComponents();

        // controlador de los estados de la simulacion
        appLifeCycle = new AppLifeCycle();

        // anadimos un listener para cuando se cierra la aplicacion que pregunte
        // se quiere guardar los ficheros abiertos
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                fileActions.fileMenuSaveAllAction(codejTabbedPane);
                saveConfiguration();
                plmanager.finalizeAllPlugins();
                //fileActions.fileMenuCloseAllAction(codejTabbedPane);
            }
        });

        // asociamos el textarea del log al logger
        log.setjTextArea(logjTextPane);

        //inicializamos la vista de los registros
        registerView = new RegisterView(simulator, registersjPanel);
        registerView.init();

        // inicializamos la vista de la memoria
        memoryView = new MemoryView(simulator, memoryFormatjPanel, memoryjPanel,
                log, 0, 4);
        /**
         * Plugins *
         */
//        rwSVC = plmanager.testLoad();
//        Component plug = plmanager.getAWTComponent(rwSVC);
//        if (plug != null) {
//            jTabbedPane3.add(rwSVC.shortName(), plug);
//        }

        // configuracion del modo grafico de la aplicacion 
        configuration = new Configuration();
        plmanager = new PluginManager(simulator, log, configuration, fileActions, consolejTabbedPane, pluginsjMenu, true);
        // leemos la ultima configuracion de la aplicacion y la establecemos
        loadConfiguration();

        loadjButton.setToolTipText("Open file");
        newjButton.setToolTipText("New file");
        closejButton.setToolTipText("Close seltected tab");
        assemblejButton.setToolTipText("Assemble all files");
        stepinjButton.setToolTipText("Step into");
        stepjButton.setToolTipText("Step over");
        runjButton.setToolTipText("Run simulation");
        stopjButton.setToolTipText("Stop simulation");
        resetjButton.setToolTipText("Reset simulation");
        savejButton.setToolTipText("Save file");
        canceljButton.setToolTipText("Abort simulation");

        URL url32 = getClass().getResource("/resources/guacARMv2_32.png");
        URL url16 = getClass().getResource("/resources/guacARMv2_16.png");
        final List<Image> icons = new ArrayList<>();
        try {
            icons.add(ImageIO.read(url16));
            icons.add(ImageIO.read(url32));
        } catch (IOException ex) {
            Logger.getLogger(AppWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.setIconImages(icons);

        pack();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Devuevelve un nuevo nombre de fichero para una nueva tab
     *
     * @return Nombre del fichero
     */
    private String getNewFileName() {
        return "new" + (++newTabCount) + ".s";
    }

    /**
     * Crea una nueva tab con un nombre de fichero generico y la establece como
     * seleccionada
     */
    private void newTab() {
        RSyntaxtCodePane rscp = new RSyntaxtCodePane(modifiedCallBack, log);
        codejTabbedPane.add(getNewFileName() + "*", rscp);
        codejTabbedPane.setSelectedComponent(rscp);
        rscp.modified = true;
        pack();
    }

    /**
     * Guarda la configuracion de la aplicacion para reestablecerla la proxima
     * vez que se ejecute
     */
    private void saveConfiguration() {
        // salvamos la lista de ficheros abiertos
        List<String> files = new ArrayList<>();
        for (Component code : codejTabbedPane.getComponents()) {
            if (((RSyntaxtCodePane) code).binary
                    || ((RSyntaxtCodePane) code).associateFile == null) {
                continue;
            }
            files.add(((RSyntaxtCodePane) code).associateFile.getAbsolutePath());
        }
        configuration.setFileList(files, "SourceFiles");
        // salvamos el directorio de trabajo actual
        configuration.setConfig("currentDirectory", fileActions.getCurrentDirectory());
        // salvamos el formato de visualizacion de cada registro y el general
        configuration.setConfig("formatRegisterFile", registerView.getRegisterFileFormat());
        int[] regs = registerView.getRegisterFormat();
        for (int i = 0; i < regs.length; i++) {
            configuration.setConfig("R" + i, regs[i]);
        }
        // salvamos el formato de visualizacion de la memoria
        configuration.setConfig("formatMemory", memoryView.getMemoryFormat());
        configuration.setConfig("sizeMemory", memoryView.getMemorySize());
        // salvamos el tamaño actual de la ventana
        Dimension dim = this.getSize();
        configuration.setConfig("frameHeight", (int) dim.getHeight());
        configuration.setConfig("frameWidth", (int) dim.getWidth());

        // salvamos los plugins cargados
        List<String> pluginsFilenames = plmanager.getPluginsFileNames();
        configuration.setFileList(pluginsFilenames, "Plugins");
        String[] builtinPlugins = plmanager.getBuiltInPluginsState();
        for (int i = 0; i < builtinPlugins.length; i++) {
            configuration.setConfig("builtinPlugin" + i, builtinPlugins[i]);
        }
        // escribimos en disco la configuracion
        configuration.saveConfiguration();
    }

    /**
     * Carga la configuracion de la aplicacion salvada la ultima vez
     */
    private void loadConfiguration() {
        // leemos la lista de ficheros que habia abierta la ultima vez
        List<String> files = configuration.getFileList("SourceFiles");
        if (files != null) {
            fileActions.fileMenuLoadAction(codejTabbedPane, files, modifiedCallBack);
        }
        // leemos la lista de plugins que habia cargados la ultima vez
        files = configuration.getFileList("Plugins");
        if (files != null) {
            //plmanager.loadGUIPlugins(files);
            files.forEach(filename -> {
                loadPlugin(filename, false);
            });
        }
        String[] builtinPlugins = plmanager.getBuiltInPluginsState();
        for (int i = 0; i < builtinPlugins.length; i++) {
            builtinPlugins[i] = configuration.getConfig("builtinPlugin" + i, "NONE");
        }
        plmanager.loadBuiltInPlugins(builtinPlugins);

        // leemos el directorio de trabajo
        fileActions.setCurrentDirectory(configuration.getConfig(
                "currentDirectory", System.getProperty("user.dir")));
        // leemos los formatos de representacion de los registros
        registerView.setSelectedFormat(
                configuration.getConfig("formatRegisterFile", Configuration.HEX));
        int[] regs = registerView.getRegisterFormat();
        for (int i = 0; i < regs.length; i++) {
            regs[i] = configuration.getConfig("R" + i, Configuration.HEX);
        }
        registerView.setRegisterFileFormat(regs);
        // leemos el formato de representacion de la memoria
        memoryView.setSelectedFormat(
                configuration.getConfig("formatMemory", Configuration.HEX));
        memoryView.setSelectedSize(
                configuration.getConfig("sizeMemory", Configuration.B32));
        // leemos el tamaño del frame
        int height = configuration.getConfig("frameHeight", Configuration.FRAMEHEIGHT);
        int width = configuration.getConfig("frameWidth", Configuration.FRAMEWIDTH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        height = (height > screenSize.getHeight()) ? (int) screenSize.getHeight() : height;
        width = (width > screenSize.getWidth()) ? (int) screenSize.getWidth() : width;
        setPreferredSize(new Dimension(width, height));
    }

    /**
     * Resalta las lineas con errores detectados al esnamblar
     *
     * @param errors Lista de lineas erroneas. El primer valor es el indice del
     * fichero erroneo
     */
    private void highlightErrors(List<Integer> errors) {
        int numPane = errors.get(0);
        RSyntaxtCodePane pane = getCodePaneAt(numPane);
        errors.remove(0);
        if (pane != null) {
            pane.setHighlightLines(errors);
            codejTabbedPane.setSelectedComponent(pane);
            highlighted = true;
        }
    }

    /**
     * Devuelve de todos los codigos abiertos, el que ocupa la posicion indicada
     *
     * @param pos posicion del codigo
     * @return panel con el codigo
     */
    private RSyntaxtCodePane getCodePaneAt(int pos) {
        int c = 0;
        for (int i = 0; i < codejTabbedPane.getComponentCount(); i++) {
            RSyntaxtCodePane pane = (RSyntaxtCodePane) codejTabbedPane.getComponent(i);
            if (pane.binary) {
                continue;
            }
            if (c == pos) {
                return pane;
            }
            c++;
        }
        return null;
    }

    /**
     * Ensambla el codigo para poder simularlo
     *
     * @return si lo ha podido ensamblar sin errores
     */
    private boolean assemble() {
        boolean ask = false;
        for (Component rsc : codejTabbedPane.getComponents()) {
            if (((RSyntaxtCodePane) rsc).binary) {
                continue;
            }
            if (((RSyntaxtCodePane) rsc).modified
                    || ((RSyntaxtCodePane) rsc).associateFile == null) {
                ask = true;
                break;
            }
        }
        if (ask) {
            int op = JOptionPane.showConfirmDialog(codejTabbedPane,
                    "Save modified files?",
                    "Save confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (op == 0) {
                fileActions.fileMenuSaveAllAction(codejTabbedPane);
            } else {
                return false;
            }
        }
        List<String> files = new ArrayList<>();
        for (int i = 0; i < codejTabbedPane.getComponents().length; i++) {
            if (((RSyntaxtCodePane) codejTabbedPane.getComponentAt(i)).binary) {
                continue;
            }
            files.add(((RSyntaxtCodePane) codejTabbedPane.getComponentAt(i)).associateFile.getAbsolutePath());
        }

        if (files.size() > 0) {
            log.println("Assembling ...");
            List<Integer> errors;
            List<String> builtinLibraries = plmanager.getBuiltinLibraries();
            if ((errors = assembler.assembleFiles(files, builtinLibraries)) != null) {
                log.println("Error assembling.");
                if (errors.get(0) > -1) {
                    highlightErrors(errors);
                }
                return false;
            }
            log.println("Assembling done.");

            // informamos si ha habido error
            for (int i = 0; i < assembler.errorno.length; i++) {
                if (assembler.errorno[i] != 0) {
                    log.println(assembler.stdout[i]);
                    log.println(assembler.stderr[i]);
                    return false;
                }
                String warning = assembler.stderr[i];
                if (warning.compareTo("") != 0) {
                    log.println(warning, ColorTheme.WARNING);
                }
            }

            if (simulationPane == null) {
                simulationPane = new ARMBinaryCodePane(modifiedCallBack, log, simulator);
                codejTabbedPane.add("Binary", simulationPane);
            }

            simulationPane.unSetAllBreakPoints();
            simulationPane.setText(assembler.elfDumpOut);
            codejTabbedPane.setSelectedComponent(simulationPane);

            simulator.initialize(
                    assembler.textSegment,
                    assembler.dataSegment,
                    assembler.entryPoint,
                    assembler.textOff,
                    assembler.dataOff,
                    0x400
            );

            memoryView.cleanMemoryAddress();
            if (assembler.dataSegment != null) {
                memoryView.setMemoryAddress("0x"
                        + Integer.toHexString(simulator.getDataBaseAddr())
                        + " (.data)");
            }
            memoryView.setMemoryAddress("0x"
                    + Integer.toHexString(simulator.getCodeBaseAddr())
                    + " (.text)");
            memoryView.setMemoryAddress("0x"
                    + Integer.toHexString(simulator.getStackBaseAddr())
                    + " (stack)");

            //simulationPane.setPC((int) assembler.entryPoint);
            //registerView.update();
            //plmanager.resetPlugins();
            reset();

            //memoryView.update();
            //setRunMode();
            return true;
        }
        return false;
    }

    /**
     * Resetea la simulacion
     *
     * @return
     */
    private boolean reset() {
        log.println("Resetting");
        simulator.reset();
        plmanager.resetPlugins();
        registerView.update();
        memoryView.update();
        simulationPane.setPC(simulator.getPC());
        return true;
    }

    private void updateSimulationInfo(int PC, Log log, boolean step) {
        registerView.update();
        memoryView.update();
        if (PC == simulator.getEndPC()) {
            log.println("End of simulation", ColorTheme.UPDATED);
            simulationPane.unSetPC();
        } else {
            simulationPane.setPC(PC);
        }
    }

    /**
     * Realiza un paso de simulacion
     *
     */
    private boolean stepInSimulation() {
        int prevPC = simulator.getPC();
        if (prevPC == simulator.getEndPC()) {
            return false;
        }
        return runSimulationStep();
    }

    private int stepInSimulationRunMode(Log auxlog) {
        return simulator.step(1, auxlog);
    }

    /**
     * Clase para simular un programa hasta el final o hasta que se encuentra un
     * BREAKPOINT o hasta que se llega a la instruccion indicada en el
     * constructor (ui).
     */
    private class SimulationWorker extends SwingWorker<Integer, Integer> {

        private int untilInst;
        private StringLog localLog;
        public boolean step;
        private RunningAnimation runningAnimation;
        private boolean stopped;

        public SimulationWorker() {
            this(simulator.getEndPC());
        }

        public SimulationWorker(int ui) {
            stopped = false;
            untilInst = ui;
            localLog = new StringLog();
            step = false;
            runningAnimation = new RunningAnimation();
        }

        @Override
        protected Integer doInBackground() throws Exception {
            int PC = -1;
            if (!step) {
                runningAnimation.init();
            }
            while (!stopped) {
                if (step) {
                    PC = stepInSimulationRunMode(localLog);
                    //System.err.println(Integer.toHexString(PC));
                } else {
                    PC = stepInSimulationRunMode(localLog);
                    /*
                    PC = simulator.run(100000, untilInst); // simulamos 100000 microsegundos o hasta la instruccion indicada
                    System.out.println(Integer.toHexString(PC));
                    System.out.println("r8="+Integer.toHexString(simulator.readRegister(8)));
                    System.out.println("r9="+Integer.toHexString(simulator.readRegister(9)));
                    System.out.println("r10="+Integer.toHexString(simulator.readRegister(10)));
                    System.out.println("r11="+Integer.toHexString(simulator.readRegister(11)));
                    System.out.println("r12="+Integer.toHexString(simulator.readRegister(12)));
                    System.out.println("r13="+Integer.toHexString(simulator.readRegister(13)));
                    System.out.println("r14="+Integer.toHexString(simulator.readRegister(14)));*/
                }
                if (step || PC < 0 || PC == simulator.getEndPC()
                        || simulator.isBreakpoint()
                        || PC == untilInst
                        || isCancelled()) {
                    break;
                }
                if (!step) {
                    publish(0);
                }
            }
            if (!step) {
                publish(1);
            }
            return PC;
        }

        @Override
        protected void process(List<Integer> chunks) {
            if (chunks.get(0) == 0) {
                runningAnimation.step();
            } else {
                //runningAnimation.end();
            }
        }

        @Override
        protected void done() {
            try {
                String l = localLog.getLog();
                Integer PC = get();
                if (l.compareTo("") != 0) {
                    log.println(l, (PC == -1) ? ColorTheme.WARNING : ColorTheme.UPDATED);
                }
                if (!step) {
                    runningAnimation.end();
                }
                if (isCancelled()) {
                    log.print("Aborted.\n", ColorTheme.UPDATED);
                    appLifeCycle.event(Events.SIMULATIONCANCEL);
                } else {
                    if (PC != -1) {
                        updateSimulationInfo(PC, log, false);
                        if (simulator.isBreakpoint()) {
                            log.print("Breakpoint at 0x" + Integer.toHexString(PC) + "\n", ColorTheme.UPDATED);
                        } else if (stopped) {
                            log.print("Stopped at 0x" + Integer.toHexString(PC) + "\n", ColorTheme.UPDATED);
                        }

                        simulator.cleanBreakpoint();

                        if (PC != simulator.getEndPC()) {
                            appLifeCycle.event(Events.SIMULATIONSTOP);
                        } else {
                            appLifeCycle.event(Events.SIMULATIONEND);
                        }
                    } else { //error en la simulacion 
                        appLifeCycle.event(Events.SIMULATIONEND);
                    }
                }
            } catch (Exception e) {
            }
            simulationWorker = null;
        }

        public void stop() {
            stopped = true;
        }

        public void cancel() {
            this.cancel(true);
        }
    }

    /**
     * Lanza thread y ejecuta en el una simulacion
     */
    private boolean runSimulation() {
        return runSimulation(simulator.getEndPC());
    }

    /**
     * Lanza thread y ejecuta en el una simulacion hasta alcanzar la instruccion
     * indicada
     */
    private boolean runSimulation(int untilInst) {
        int prevPC = simulator.getPC();
        if (prevPC == simulator.getEndPC()) {
            return false;
        }
        simulationWorker = new SimulationWorker(untilInst);
        simulationWorker.execute();
        return true;
    }

    /**
     * Ejecuta un paso de simulacion en un thread distinto al del UI
     */
    private boolean runSimulationStep() {
        int prevPC = simulator.getPC();
        if (prevPC == simulator.getEndPC()) {
            return false;
        }
        simulationWorker = new SimulationWorker();
        simulationWorker.step = true;
        simulationWorker.execute();
        return true;
    }

    /**
     * Para la simulacion (termina de ejecutar instruccion actual y deja un
     * estado que se puede continuar simulando)
     */
    private boolean stopSimulation() {
        if (simulationWorker != null) {
            simulationWorker.stop();
        }
        return false;
    }

    /**
     * Cancela la simulacion (cancela la ejecucion de la instruccion actual, no
     * se puede segir simulando)
     */
    private boolean cancelSimulation() {
        if (simulationWorker != null) {
            simulationWorker.cancel();
            return true;
        }
        return false;
    }

    /**
     * Ejecuta una instruccion o toda una funcion sin simularla paso a paso
     */
    private boolean stepOverSimulation() throws Exception {
        int prevPC = simulator.getPC();
        if (prevPC == simulator.getEndPC()) {
            return false;
        }
        if (simulator.isNextInstructionBL()) {
            return runSimulation(prevPC + 4);
        } else {
            return stepInSimulation();
        }
    }

    private void renameTabs(JTabbedPane jtp) {
        for (Component c : jtp.getComponents()) {
            RSyntaxtCodePane rsc = (RSyntaxtCodePane) c;
            if (!rsc.binary && rsc.associateFile != null) {
                String name = rsc.associateFile.getName();
                if (rsc.modified) {
                    name += "*";
                }
                jtp.setTitleAt(jtp.indexOfComponent(rsc), name);
            }
        }
    }

    private class RunningAnimation {

        private int tag;
        private String dots = "";

        private void init() {
            tag = log.printTag("Running \n", ColorTheme.UPDATED);
            dots = "";
        }

        private void step() {
            dots += ".";
            String s = "Running " + dots + "\n";
            log.replaceTag(tag, s, ColorTheme.UPDATED);
            if (dots.length() == 3) {
                dots = "";
            }
        }

        private void stop() {
            //log.println(" stopped.");
        }

        private void end() {
            log.replaceTag(tag, "Running ... \n", ColorTheme.UPDATED);
        }
    }

    /**
     * Carga un plugin, abriendo ventana de dialogo para buscar fichero
     */
    private void loadPlugin() {
        // Para probar y depurar plugins dentro de la applicacion
        loadPlugin(null, true);
    }

    /**
     * Carga un plugin dado el nombre del fichero. Si el nombre del fichero es
     * null, abre ventana de dialogo para buscarlo.
     *
     * @param filename nombre del fichero con el plugin
     * @param verbose true si queremos mostrar mensajes de error en la carga
     */
    private void loadPlugin(String filename, boolean verbose) {
        if (filename == null) {
            plmanager.loadGUIPlugin();
        } else {
            plmanager.loadGUIPlugin(filename, verbose);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menujPanel = new javax.swing.JPanel();
        stepjButton = new javax.swing.JButton();
        stepinjButton = new javax.swing.JButton();
        runjButton = new javax.swing.JButton();
        stopjButton = new javax.swing.JButton();
        canceljButton = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        codejTabbedPane = new javax.swing.JTabbedPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        consolejTabbedPane = new javax.swing.JTabbedPane();
        logjScrollPane = new javax.swing.JScrollPane();
        logjTextPane = new javax.swing.JTextPane();
        jSplitPane3 = new javax.swing.JSplitPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        registersjPanel = new javax.swing.JPanel();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        memoryFormatjPanel = new javax.swing.JPanel();
        memoryjScrollPane = new javax.swing.JScrollPane();
        memoryjPanel = new javax.swing.JPanel();
        loadjButton = new javax.swing.JButton();
        newjButton = new javax.swing.JButton();
        assemblejButton = new javax.swing.JButton();
        resetjButton = new javax.swing.JButton();
        closejButton = new javax.swing.JButton();
        savejButton = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jMenuBar1 = new javax.swing.JMenuBar();
        filejMenu = new javax.swing.JMenu();
        newjMenuItem = new javax.swing.JMenuItem();
        openjMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        savejMenuItem = new javax.swing.JMenuItem();
        saveAsjMenuItem = new javax.swing.JMenuItem();
        saveAlljMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        closejMenuItem = new javax.swing.JMenuItem();
        closeAlljMenuItem = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        exitjMenuItem = new javax.swing.JMenuItem();
        pluginsjMenu = new javax.swing.JMenu();
        loadPluginjMenuItem = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        helpjMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1024, 720));

        stepjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/stepoverBN32.png"))); // NOI18N
        stepjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        stepjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        stepjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        stepjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepjButtonActionPerformed(evt);
            }
        });

        stepinjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/stepintoBN32.png"))); // NOI18N
        stepinjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        stepinjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        stepinjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        stepinjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stepinjButtonActionPerformed(evt);
            }
        });

        runjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/runBN32.png"))); // NOI18N
        runjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        runjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        runjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        runjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runjButtonActionPerformed(evt);
            }
        });

        stopjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/stopBN32.png"))); // NOI18N
        stopjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        stopjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        stopjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        stopjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopjButtonActionPerformed(evt);
            }
        });

        canceljButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/cancelBN32.png"))); // NOI18N
        canceljButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                canceljButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout menujPanelLayout = new javax.swing.GroupLayout(menujPanel);
        menujPanel.setLayout(menujPanelLayout);
        menujPanelLayout.setHorizontalGroup(
            menujPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menujPanelLayout.createSequentialGroup()
                .addComponent(stepinjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stepjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(runjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stopjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(canceljButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        menujPanelLayout.setVerticalGroup(
            menujPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menujPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(menujPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menujPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(stepjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(stepinjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(runjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(stopjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(canceljButton, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        jSplitPane1.setBorder(null);
        jSplitPane1.setAutoscrolls(true);

        codejTabbedPane.setMinimumSize(new java.awt.Dimension(600, 21));
        codejTabbedPane.setPreferredSize(new java.awt.Dimension(512, 146));
        jSplitPane1.setLeftComponent(codejTabbedPane);

        jSplitPane2.setBorder(null);
        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setMinimumSize(new java.awt.Dimension(151, 100));

        logjScrollPane.setMinimumSize(new java.awt.Dimension(400, 100));
        logjScrollPane.setViewportView(logjTextPane);

        consolejTabbedPane.addTab("Log", logjScrollPane);

        jSplitPane2.setRightComponent(consolejTabbedPane);

        jSplitPane3.setBorder(null);
        jSplitPane3.setMinimumSize(new java.awt.Dimension(500, 460));

        jTabbedPane2.setMaximumSize(new java.awt.Dimension(160, 550));
        jTabbedPane2.setMinimumSize(new java.awt.Dimension(160, 550));
        jTabbedPane2.setPreferredSize(new java.awt.Dimension(160, 550));
        jTabbedPane2.setSize(new java.awt.Dimension(160, 550));

        javax.swing.GroupLayout registersjPanelLayout = new javax.swing.GroupLayout(registersjPanel);
        registersjPanel.setLayout(registersjPanelLayout);
        registersjPanelLayout.setHorizontalGroup(
            registersjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 129, Short.MAX_VALUE)
        );
        registersjPanelLayout.setVerticalGroup(
            registersjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 414, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Registers", registersjPanel);

        jSplitPane3.setLeftComponent(jTabbedPane2);

        jTabbedPane4.setMinimumSize(new java.awt.Dimension(200, 146));

        javax.swing.GroupLayout memoryFormatjPanelLayout = new javax.swing.GroupLayout(memoryFormatjPanel);
        memoryFormatjPanel.setLayout(memoryFormatjPanelLayout);
        memoryFormatjPanelLayout.setHorizontalGroup(
            memoryFormatjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 136, Short.MAX_VALUE)
        );
        memoryFormatjPanelLayout.setVerticalGroup(
            memoryFormatjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 37, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout memoryjPanelLayout = new javax.swing.GroupLayout(memoryjPanel);
        memoryjPanel.setLayout(memoryjPanelLayout);
        memoryjPanelLayout.setHorizontalGroup(
            memoryjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 381, Short.MAX_VALUE)
        );
        memoryjPanelLayout.setVerticalGroup(
            memoryjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 420, Short.MAX_VALUE)
        );

        memoryjScrollPane.setViewportView(memoryjPanel);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(memoryFormatjPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(memoryjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(memoryFormatjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(memoryjScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Memory", jPanel2);

        jSplitPane3.setRightComponent(jTabbedPane4);

        jSplitPane2.setLeftComponent(jSplitPane3);

        jSplitPane1.setRightComponent(jSplitPane2);

        loadjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/openBN32.png"))); // NOI18N
        loadjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        loadjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        loadjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        loadjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadjButtonActionPerformed(evt);
            }
        });

        newjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/newBN32.png"))); // NOI18N
        newjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        newjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        newjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        newjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newjButtonActionPerformed(evt);
            }
        });

        assemblejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/buildBN32.png"))); // NOI18N
        assemblejButton.setMaximumSize(new java.awt.Dimension(44, 44));
        assemblejButton.setMinimumSize(new java.awt.Dimension(44, 44));
        assemblejButton.setPreferredSize(new java.awt.Dimension(44, 44));
        assemblejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assemblejButtonActionPerformed(evt);
            }
        });

        resetjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/resetBN32.png"))); // NOI18N
        resetjButton.setMaximumSize(new java.awt.Dimension(44, 44));
        resetjButton.setMinimumSize(new java.awt.Dimension(44, 44));
        resetjButton.setPreferredSize(new java.awt.Dimension(44, 44));
        resetjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetjButtonActionPerformed(evt);
            }
        });

        closejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/closeBN32.png"))); // NOI18N
        closejButton.setMaximumSize(new java.awt.Dimension(44, 44));
        closejButton.setMinimumSize(new java.awt.Dimension(44, 44));
        closejButton.setPreferredSize(new java.awt.Dimension(44, 44));
        closejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closejButtonActionPerformed(evt);
            }
        });

        savejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/saveBN32.png"))); // NOI18N
        savejButton.setMaximumSize(new java.awt.Dimension(44, 44));
        savejButton.setMinimumSize(new java.awt.Dimension(44, 44));
        savejButton.setPreferredSize(new java.awt.Dimension(44, 44));
        savejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savejButtonActionPerformed(evt);
            }
        });

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        filejMenu.setText("File");

        newjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newjMenuItem.setText("New");
        newjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newjMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(newjMenuItem);

        openjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openjMenuItem.setText("Open");
        openjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openjMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(openjMenuItem);
        filejMenu.add(jSeparator1);

        savejMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        savejMenuItem.setText("Save");
        savejMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savejMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(savejMenuItem);

        saveAsjMenuItem.setText("Save as");
        saveAsjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsjMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(saveAsjMenuItem);

        saveAlljMenuItem.setText("Save all");
        saveAlljMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAlljMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(saveAlljMenuItem);
        filejMenu.add(jSeparator2);

        closejMenuItem.setText("Close");
        closejMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closejMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(closejMenuItem);

        closeAlljMenuItem.setText("Close all");
        closeAlljMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeAlljMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(closeAlljMenuItem);
        filejMenu.add(jSeparator3);

        exitjMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitjMenuItem.setText("Exit");
        exitjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitjMenuItemActionPerformed(evt);
            }
        });
        filejMenu.add(exitjMenuItem);

        jMenuBar1.add(filejMenu);

        pluginsjMenu.setText("Plugins");

        loadPluginjMenuItem.setText("Load");
        loadPluginjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadPluginjMenuItemActionPerformed(evt);
            }
        });
        pluginsjMenu.add(loadPluginjMenuItem);
        pluginsjMenu.add(jSeparator5);

        jMenuBar1.add(pluginsjMenu);

        helpjMenu.setText("Help");
        helpjMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                helpjMenuMouseClicked(evt);
            }
        });

        jMenuItem1.setText("Help");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        helpjMenu.add(jMenuItem1);

        jMenuItem2.setText("About");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        helpjMenu.add(jMenuItem2);

        jMenuBar1.add(helpjMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(loadjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(newjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(savejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(closejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(assemblejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resetjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(menujPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(menujPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(loadjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(newjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(assemblejButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(resetjButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(closejButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(savejButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void newjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newjButtonActionPerformed
        newTab();
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_newjButtonActionPerformed

    private void openjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openjMenuItemActionPerformed
        if (fileActions.fileMenuLoadAction(codejTabbedPane, modifiedCallBack)) {
            appLifeCycle.doAction(Actions.FILEACTION);
        }
    }//GEN-LAST:event_openjMenuItemActionPerformed

    private void newjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newjMenuItemActionPerformed
        newTab();
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_newjMenuItemActionPerformed

    private void savejMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savejMenuItemActionPerformed
        RSyntaxtCodePane codepane = (RSyntaxtCodePane) codejTabbedPane.getSelectedComponent();
        if (codepane != null) {
            fileActions.fileMenuSaveAction(codejTabbedPane, codepane);
            renameTabs(codejTabbedPane);
            appLifeCycle.doAction(Actions.FILEACTION);
        }
    }//GEN-LAST:event_savejMenuItemActionPerformed

    private void saveAsjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsjMenuItemActionPerformed
        RSyntaxtCodePane codepane = (RSyntaxtCodePane) codejTabbedPane.getSelectedComponent();
        if (codepane != null) {
            fileActions.fileMenuSaveAsAction(codejTabbedPane, codepane);
            renameTabs(codejTabbedPane);
            appLifeCycle.doAction(Actions.FILEACTION);
        }
    }//GEN-LAST:event_saveAsjMenuItemActionPerformed

    private void saveAlljMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAlljMenuItemActionPerformed
        fileActions.fileMenuSaveAllAction(codejTabbedPane);
        renameTabs(codejTabbedPane);
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_saveAlljMenuItemActionPerformed

    private void closejMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closejMenuItemActionPerformed
        RSyntaxtCodePane codepane = (RSyntaxtCodePane) codejTabbedPane.getSelectedComponent();
        if (codepane != null) {
            if (codepane == simulationPane) {
                simulationPane = null;
            }
            fileActions.fileMenuCloseAction(codejTabbedPane, (RSyntaxtCodePane) codejTabbedPane.getSelectedComponent());
            appLifeCycle.doAction(Actions.FILEACTION);
        }
    }//GEN-LAST:event_closejMenuItemActionPerformed

    private void closeAlljMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeAlljMenuItemActionPerformed
        fileActions.fileMenuCloseAllAction(codejTabbedPane);
        simulationPane = null;
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_closeAlljMenuItemActionPerformed

    private void exitjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitjMenuItemActionPerformed
        saveConfiguration();
        plmanager.finalizeAllPlugins();
        fileActions.fileMenuCloseAllAction(codejTabbedPane);
        System.exit(0);
    }//GEN-LAST:event_exitjMenuItemActionPerformed

    private void assemblejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assemblejButtonActionPerformed
        appLifeCycle.doAction(Actions.ASSEMBLE);
    }//GEN-LAST:event_assemblejButtonActionPerformed

    private void stepinjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepinjButtonActionPerformed
        appLifeCycle.doAction(Actions.STEPINTO);
    }//GEN-LAST:event_stepinjButtonActionPerformed

    private void resetjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetjButtonActionPerformed
        appLifeCycle.doAction(Actions.RESET);
    }//GEN-LAST:event_resetjButtonActionPerformed

    private void runjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runjButtonActionPerformed
        appLifeCycle.doAction(Actions.RUN);
    }//GEN-LAST:event_runjButtonActionPerformed

    private void stopjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopjButtonActionPerformed
        appLifeCycle.doAction(Actions.STOP);
    }//GEN-LAST:event_stopjButtonActionPerformed

    private void loadjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadjButtonActionPerformed
        openjMenuItemActionPerformed(evt);
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_loadjButtonActionPerformed

    private void closejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closejButtonActionPerformed
        closejMenuItemActionPerformed(evt);
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_closejButtonActionPerformed

    private void savejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savejButtonActionPerformed
        savejMenuItemActionPerformed(evt);
        appLifeCycle.doAction(Actions.FILEACTION);
    }//GEN-LAST:event_savejButtonActionPerformed

    private void stepjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stepjButtonActionPerformed
        appLifeCycle.doAction(Actions.STEPOVER);
    }//GEN-LAST:event_stepjButtonActionPerformed

    private void canceljButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_canceljButtonActionPerformed
        appLifeCycle.doAction(Actions.CANCEL);
    }//GEN-LAST:event_canceljButtonActionPerformed

    private void loadPluginjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadPluginjMenuItemActionPerformed
        appLifeCycle.doAction(Actions.LOADPLUGIN);
    }//GEN-LAST:event_loadPluginjMenuItemActionPerformed

    private void helpjMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_helpjMenuMouseClicked
    }//GEN-LAST:event_helpjMenuMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        String msg = Configuration.APPNAME + " - " + Configuration.VERSION;
        msg += "\n\n" + Configuration.AUTHOR;
        msg += " (" + Configuration.AUTHOREMAIL + ")";
        for (String s : Configuration.AFF) {
            msg += "\n" + s;
        }
        ImageIcon icon = new ImageIcon("/resources/guacARMv2_32.png");
        //JOptionPane.showMessageDialog(this, msg);
        JOptionPane.showMessageDialog(this,
                msg,
                "About",
                JOptionPane.INFORMATION_MESSAGE,
                configuration.getProgramIcon());
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        if (helpWindow == null) {
            helpWindow = new HelpWindow();
        }
        helpWindow.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assemblejButton;
    private javax.swing.JButton canceljButton;
    private javax.swing.JMenuItem closeAlljMenuItem;
    private javax.swing.JButton closejButton;
    private javax.swing.JMenuItem closejMenuItem;
    private javax.swing.JTabbedPane codejTabbedPane;
    private javax.swing.JTabbedPane consolejTabbedPane;
    private javax.swing.JMenuItem exitjMenuItem;
    private javax.swing.JMenu filejMenu;
    private javax.swing.JMenu helpjMenu;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JMenuItem loadPluginjMenuItem;
    private javax.swing.JButton loadjButton;
    private javax.swing.JScrollPane logjScrollPane;
    private javax.swing.JTextPane logjTextPane;
    private javax.swing.JPanel memoryFormatjPanel;
    private javax.swing.JPanel memoryjPanel;
    private javax.swing.JScrollPane memoryjScrollPane;
    private javax.swing.JPanel menujPanel;
    private javax.swing.JButton newjButton;
    private javax.swing.JMenuItem newjMenuItem;
    private javax.swing.JMenuItem openjMenuItem;
    private javax.swing.JMenu pluginsjMenu;
    private javax.swing.JPanel registersjPanel;
    private javax.swing.JButton resetjButton;
    private javax.swing.JButton runjButton;
    private javax.swing.JMenuItem saveAlljMenuItem;
    private javax.swing.JMenuItem saveAsjMenuItem;
    private javax.swing.JButton savejButton;
    private javax.swing.JMenuItem savejMenuItem;
    private javax.swing.JButton stepinjButton;
    private javax.swing.JButton stepjButton;
    private javax.swing.JButton stopjButton;
    // End of variables declaration//GEN-END:variables

    public enum States {
        EDITION, READY, RUNNING, ENDSIMULATION
    };

    public enum Actions {
        ASSEMBLE, FILEACTION, RESET, RUN, STEPINTO, STEPOVER, STOP, CANCEL, LOADPLUGIN
    };

    public enum Events {
        SIMULATIONEND, SIMULATIONSTOP, SIMULATIONCANCEL
    };

    public class AppLifeCycle {

        private States state;

        public AppLifeCycle() {
            state = States.EDITION;
            setSimState(state);
        }

        /**
         * Realiza las acciones asociadas a la accion indicada por ac
         *
         * @param ac accion realizada por el usuario
         */
        public void doAction(Actions ac) {
            States oldState = state;
            switch (state) {
                case EDITION:
                    doActionOnEditionState(ac);
                    break;
                case READY:
                    doActionOnReadyState(ac);
                    break;
                case ENDSIMULATION:
                    doActionOnEndSimulationState(ac);
                    break;
                case RUNNING:
                    doActionOnRunningState(ac);
                    break;
            }
            if (oldState != state) {
                setSimState(state);
            }
        }

        /**
         * Realiza las acciones asociadas a la accion dada en el estado EDITION
         *
         * @param ac accion a realizar
         */
        private void doActionOnEditionState(Actions ac) {
            switch (ac) {
                case ASSEMBLE:
                    if (assemble()) {
                        state = States.READY;
                    }
                    break;
                case LOADPLUGIN:
                    loadPlugin();
                    break;
            }
        }

        /**
         * Realiza las acciones asociadas a la accion dada en el estado READY
         *
         * @param ac accion a realizar
         */
        private void doActionOnReadyState(Actions ac) {
            switch (ac) {
                case FILEACTION:
                    state = States.EDITION;
                    break;
                case RUN:
                    if (runSimulation()) {
                        state = States.RUNNING;
                    }
                    break;
                case STEPINTO:
                    if (stepInSimulation()) {
                        state = States.RUNNING;
                    }
                    break;
                case STEPOVER: {
                    try {
                        if (stepOverSimulation()) {
                            state = States.RUNNING;
                        }
                    } catch (Exception ex) {
                        state = States.ENDSIMULATION;
                        log.println("Unknown error in simulation", ColorTheme.WARNING);
                    }
                }
                break;

                case RESET:
                    reset();
                    break;
                case LOADPLUGIN:
                    loadPlugin();
                    break;
            }
        }

        /**
         * Realiza las acciones asociadas a la accion dada en el estado
         * ENDSIMULATION
         *
         * @param ac accion a realizar
         */
        private void doActionOnEndSimulationState(Actions ac) {
            switch (ac) {
                case FILEACTION:
                    state = States.EDITION;
                    break;
                case RESET:
                    if (reset()) {
                        state = States.READY;
                    }
                    break;
                case LOADPLUGIN:
                    loadPlugin();
                    break;
            }
        }

        /**
         * Realiza las acciones asociadas a la accion dada en el estado RUNNIG
         *
         * @param ac accion a realizar
         */
        private void doActionOnRunningState(Actions ac) {
            switch (ac) {
                case STOP:
                    if (stopSimulation()) {
                        state = States.READY;
                    }
                    break;
                case CANCEL:
                    if (cancelSimulation()) {
                        state = States.ENDSIMULATION;
                    }
                    break;
            }
        }

        /**
         * Realiza los acciones y cambios producidos por un evento
         *
         * @param ev evento que se ha producido
         */
        private void event(Events ev) {
            if (state != States.RUNNING) {
                System.err.println("Event " + ev + " on state " + state);
            } else {
                switch (ev) {
                    case SIMULATIONEND:
                        state = States.ENDSIMULATION;
                        break;
                    case SIMULATIONSTOP:
                        if (simulator.getPC() != simulator.getEndPC()) {
                            state = States.READY;
                        } else {
                            state = States.ENDSIMULATION;
                        }
                        break;
                    case SIMULATIONCANCEL:
                        state = States.ENDSIMULATION;
                        break;
                }
                setSimState(state);
            }
        }
    }

    /**
     * Establece el interfaz de la app (botones y opciones de menu)
     * correspondiente al estado dado
     *
     * @param state estado de la aplicacion
     */
    public void setSimState(States state) {
        switch (state) {
            case EDITION:
                setEditionMode();
                break;
            case READY:
                setRunMode();
                break;
            case ENDSIMULATION:
                setEndSimulationMode();
                break;
            case RUNNING:
                setRunningMode();
                break;
        }
    }

    /**
     * Pone la aplicacion en modo edicion de codigo fuente
     */
    private void setEditionMode() {
        stepjButton.setEnabled(false);
        stepinjButton.setEnabled(false);
        runjButton.setEnabled(false);
        stopjButton.setEnabled(false);
        newjButton.setEnabled(true);
        assemblejButton.setEnabled(true);
        resetjButton.setEnabled(false);
        closejButton.setEnabled(true);
        loadjButton.setEnabled(true);
        savejButton.setEnabled(true);
        canceljButton.setEnabled(false);

        filejMenu.setEnabled(true);
        pluginsjMenu.setEnabled(true);
    }

    /**
     * Pone la aplicacion en modo simulacion
     */
    private void setRunMode() {
        codejTabbedPane.setSelectedComponent(simulationPane);
        stepjButton.setEnabled(true);
        stepinjButton.setEnabled(true);
        runjButton.setEnabled(true);
        stopjButton.setEnabled(false);
        newjButton.setEnabled(true);
        assemblejButton.setEnabled(true);
        resetjButton.setEnabled(true);
        closejButton.setEnabled(true);
        loadjButton.setEnabled(true);
        savejButton.setEnabled(true);
        canceljButton.setEnabled(false);

        filejMenu.setEnabled(true);
        pluginsjMenu.setEnabled(true);
    }

    /**
     * Pone la aplicacion en modo running
     */
    private void setRunningMode() {
        codejTabbedPane.setSelectedComponent(simulationPane);
        stepjButton.setEnabled(false);
        stepinjButton.setEnabled(false);
        runjButton.setEnabled(false);
        stopjButton.setEnabled(true);
        newjButton.setEnabled(false);
        assemblejButton.setEnabled(false);
        resetjButton.setEnabled(false);
        closejButton.setEnabled(false);
        loadjButton.setEnabled(false);
        savejButton.setEnabled(false);
        canceljButton.setEnabled(true);

        filejMenu.setEnabled(false);
        pluginsjMenu.setEnabled(false);
    }

    /**
     * Pone la aplicacion en modo running
     */
    private void setEndSimulationMode() {
        stepjButton.setEnabled(false);
        stepinjButton.setEnabled(false);
        runjButton.setEnabled(false);
        stopjButton.setEnabled(false);
        newjButton.setEnabled(true);
        assemblejButton.setEnabled(true);
        resetjButton.setEnabled(true);
        closejButton.setEnabled(true);
        loadjButton.setEnabled(true);
        savejButton.setEnabled(true);
        canceljButton.setEnabled(false);

        filejMenu.setEnabled(true);
        pluginsjMenu.setEnabled(true);
        simulationPane.unSetPC();
    }
}
