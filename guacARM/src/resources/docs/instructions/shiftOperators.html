<STYLE>A {text-decoration: none;} </STYLE>

<div class="sect2" lang="en" xml:lang="en">
    <div class="titlepage">
        <div>
            <div>
                <h3 class="title">
                    <a id="CIHDDCIF"></a>Shift Operations</h3>
            </div>
        </div>
    </div>
    <p>Register shift operations move the bits in a register left
        or right by a specified number of bits, the <span class="emphasis">
            <em>shift length</em>
        </span>.
        Register shift can be performed directly by the instructions <code class="code">ASR</code>, <code class="code">LSR</code>, <code class="code">LSL</code>,and <code class="code">ROR</code> and
        the result is written to a destination register.</p>
    <p>The permitted shift lengths depend on the shift type and the
        instruction, see the individual instruction description. If the
        shift length is 0, no shift occurs. Register shift operations update
        the carry flag except when the specified shift length is 0. The following
        sub-sections describe the various shift operations and how they
        affect the carry flag. In these descriptions, <em class="parameter">
            <code>
                <em class="replaceable">
                    <code>Rm</code>
                </em>
            </code>
        </em> is
        the register containing the value to be shifted, and <em class="parameter">
            <code>
                <em class="replaceable">
                    <code>n</code>
                </em>
            </code>
        </em> is
        the shift length.</p>
    <div class="sect3" lang="en" xml:lang="en">
        <div class="titlepage">
            <div>
                <div>
                    <h4 class="title">
                        <a id="id3825942"></a>ASR</h4>
                </div>
            </div>
        </div>
        <p>Arithmetic shift right by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            moves the left-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>,
            to the right by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> places,
            into the right-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result, and it copies the original bit[31] of the register
            into the left-hand <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result. See Figure&nbsp;1.</p>
        <p>You can use the <em class="parameter">
                <code>ASR</code>
            </em> operation to divide
            the signed value in the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em> by
            2<sup>
                <em class="replaceable">
                    <code>n</code>
                </em>
            </sup>, with the
            result being rounded towards negative-infinity.</p>
        <p>When the instruction is <code class="code">ASRS</code> the carry flag is
            updated to the last bit shifted out, bit[<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em>-1], of
            the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>.</p>
        <div class="note">
            <h3 class="note">Note</h3>
            <div class="itemizedlist">
                <ul type="disc">
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            32 or more, then all the bits in the result are set to the value
                            of bit[31] of <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>Rm</code>
                                    </em>
                                </code>
                            </em>.</p>
                    </li>
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            32 or more and the carry flag is updated, it is updated to the value
                            of bit[31] of <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>Rm</code>
                                    </em>
                                </code>
                            </em>.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="figure">
            <a id="CIHFBGJH"></a>
            <p class="title">
                <b>Figure&nbsp;1.&nbsp;ASR #3</b>
            </p>
            <div class="figure-contents">
                <div class="mediaobject">
                    <img src="/resources/ASR3.png">
                </div>
            </div>
        </div>
        <br class="figure-break">
    </div>
    <div class="sect3" lang="en" xml:lang="en">
        <div class="titlepage">
            <div>
                <div>
                    <h4 class="title">
                        <a id="id3826143"></a>LSR</h4>
                </div>
            </div>
        </div>
        <p>Logical shift right by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            moves the left-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>,
            to the right by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> places,
            into the right-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result, and it sets the left-hand <code class="literal">
                <em class="replaceable">
                    <code>n</code>
                </em>
            </code> bits
            of the result to 0. See Figure&nbsp;2.</p>
        <p>You can use the <em class="parameter">
                <code>LSR</code>
            </em> operation to divide
            the value in the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em> by
            2<sup>
                <em class="replaceable">
                    <code>n</code>
                </em>
            </sup>, if the
            value is regarded as an unsigned integer.</p>
        <p>When the instruction is <code class="code">LSRS</code>, the carry flag
            is updated to the last bit shifted out, bit[<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em>-1], of
            the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>.</p>
        <div class="note">
            <h3 class="note">Note</h3>
            <div class="itemizedlist">
                <ul type="disc">
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            32 or more, then all the bits in the result are cleared to 0. </p>
                    </li>
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            33 or more and the carry flag is updated, it is updated to 0.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="figure">
            <a id="CIHHGFBA"></a>
            <p class="title">
                <b>Figure&nbsp;2.&nbsp;LSR #3</b>
            </p>
            <div class="figure-contents">
                <div class="mediaobject">
                    <img src="/resources/LSR3.png">
                </div>
            </div>
        </div>
        <br class="figure-break">
    </div>
    <div class="sect3" lang="en" xml:lang="en">
        <div class="titlepage">
            <div>
                <div>
                    <h4 class="title">
                        <a id="id3826320"></a>LSL</h4>
                </div>
            </div>
        </div>
        <p>Logical shift left by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            moves the right-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>,
            to the left by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> places,
            into the left-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result, and it sets the right-hand <code class="literal">
                <em class="replaceable">
                    <code>n</code>
                </em>
            </code> bits
            of the result to 0. See Figure&nbsp;3.</p>
        <p>You can use the <em class="parameter">
                <code>LSL</code>
            </em> operation to multiply
            the value in the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em> by
            2<sup>
                <em class="replaceable">
                    <code>n</code>
                </em>
            </sup>, if the
            value is regarded as an unsigned integer or a two’s complement signed
            integer. Overflow can occur without warning.</p>
        <p>When the instruction is <code class="code">LSLS</code> the carry flag is
            updated to the last bit shifted out, bit[<code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em>], of
            the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>.
            These instructions do not affect the carry flag when used with <em class="parameter">
                <code>LSL
                    #0</code>
            </em>.</p>
        <div class="note">
            <h3 class="note">Note</h3>
            <div class="itemizedlist">
                <ul type="disc">
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            32 or more, then all the bits in the result are cleared to 0.</p>
                    </li>
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            33 or more and the carry flag is updated, it is updated to 0.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="figure">
            <a id="CIHCBCBD"></a>
            <p class="title">
                <b>Figure&nbsp;3.&nbsp;LSL #3</b>
            </p>
            <div class="figure-contents">
                <div class="mediaobject">
                    <img src="/resources/LSL3.png">
                </div>
            </div>
        </div>
        <br class="figure-break">
    </div>
    <div class="sect3" lang="en" xml:lang="en">
        <div class="titlepage">
            <div>
                <div>
                    <h4 class="title">
                        <a id="id3826517"></a>ROR</h4>
                </div>
            </div>
        </div>
        <p>Rotate right by <code class="literal">
                <em class="replaceable">
                    <code>n</code>
                </em>
            </code> bits
            moves the left-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>,
            to the right by <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> places,
            into the right-hand <code class="literal">32</code>-<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result, and it moves the right-hand <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the register into the left-hand <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em> bits
            of the result. See Figure&nbsp;4.</p>
        <p>When the instruction is <code class="code">RORS</code> the carry flag is
            updated to the last bit rotation, bit[<em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>n</code>
                    </em>
                </code>
            </em>-1],
            of the register <em class="parameter">
                <code>
                    <em class="replaceable">
                        <code>Rm</code>
                    </em>
                </code>
            </em>.</p>
        <div class="note">
            <h3 class="note">Note</h3>
            <div class="itemizedlist">
                <ul type="disc">
                    <li>
                        <p>If <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em> is
                            32, then the value of the result is same as the value in <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>Rm</code>
                                    </em>
                                </code>
                            </em>,
                            and if the carry flag is updated, it is updated to bit[31] of <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>Rm</code>
                                    </em>
                                </code>
                            </em>. </p>
                    </li>
                    <li>
                        <p>
                            <code class="code">ROR</code> with shift length, <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em>,
                            greater than 32 is the same as <code class="code">ROR</code> with shift length <em class="parameter">
                                <code>
                                    <em class="replaceable">
                                        <code>n</code>
                                    </em>
                                </code>
                            </em>-32.</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="figure">
            <a id="CIHGDFHI"></a>
            <p class="title">
                <b>Figure&nbsp;4.&nbsp;ROR #3</b>
            </p>
            <div class="figure-contents">
                <div class="mediaobject">
                    <img src="/resources/ROR3.png">
                </div>
            </div>
        </div>
        <br class="figure-break">
    </div>
    <div class="sect3" lang="en" xml:lang="en">
        <div class="titlepage">
            <div>
                <div>
                    <h4 class="title">
                        <a id="id3826517"></a>RRX</h4>
                </div>
            </div>
        </div>
        <p> Rotate Right with Extend provides the value of the contents of a register shifted right one bit. 
The old carry flag is shifted into bit[31]. If the S suffix is present, the old bit[0] is placed in the carry flag.
See Figure&nbsp;5.</p>
        <div class="figure">
            <a id="CIHGDFHI"></a>
            <p class="title">
                <b>Figure&nbsp;5.&nbsp;RRX</b>
            </p>
            <div class="figure-contents">
                <div class="mediaobject">
                    <img src="/resources/RRX.png">
                </div>
            </div>
        </div>
        <br class="figure-break">
    </div>
</div>


