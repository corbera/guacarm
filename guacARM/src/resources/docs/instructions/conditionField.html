<STYLE>A {text-decoration: none;} </STYLE>

<h3>Condition codes for conditional execution</h3>
<p> The following table lists the available condition codes, their meanings (where the flags were set by a cmp or subs instruction), and the flags that are tested:</p>
<table>
<tr><td><strong>Code</strong></td> <td><strong>Meaning (for cmp or subs)</strong></td> <td><strong>Flags Tested</strong></td></tr>
<tr><td>EQ</td> <td>Equal.</td> <td>Z==1</td></tr>
<tr><td>NE</td> <td>Not equal.</td> <td>Z==0</td></tr>
<tr><td>CS or HS</td> <td>Unsigned higher or same (or carry set).</td> <td>C==1</td></tr>
<tr><td>CC or LO</td> <td>Unsigned lower (or carry clear).</td> <td>C==0</td></tr>
<tr><td>MI</td> <td>Negative. The mnemonic stands for "minus".</td> <td>N==1</td></tr>
<tr><td>PL</td> <td>Positive or zero. The mnemonic stands for "plus".</td> <td>N==0</td></tr>
<tr><td>VS</td> <td>Signed overflow. The mnemonic stands for "V set".</td> <td>V==1</td></tr>
<tr><td>VC</td> <td>No signed overflow. The mnemonic stands for "V clear".</td> <td>V==0</td></tr>
<tr><td>HI</td> <td>Unsigned higher.</td> <td>(C==1) && (Z==0)</td></tr>
<tr><td>LS</td> <td>Unsigned lower or same.</td> <td>(C==0) || (Z==1)</td></tr>
<tr><td>GE</td> <td>Signed greater than or equal.</td> <td>N==V</td></tr>
<tr><td>LT</td> <td>Signed less than.</td> <td>N!=V</td></tr>
<tr><td>GT</td> <td>Signed greater than.</td> <td>(Z==0) && (N==V)</td></tr>
<tr><td>LE</td> <td>Signed less than or equal.</td> <td>(Z==1) || (N!=V)</td></tr>
<tr><td>AL (or omitted)</td> <td>Always executed.</td> <td>None tested.</td></tr>
</table>
<h3>The Flags</h3>
<p>The simplest way to set the condition flags is to use a comparison operation, such as cmp. This mechanism is common to many processor architectures, and the semantics (if not the details) of cmp will likely be familiar. In addition, we have already seen that many instructions (such as sub in the example) can be modified to update the condition flags based on the result by adding an s suffix. That's all well and good, but what information is stored, and how can we access it?
<p>The additional information is stored in four condition flag bits in the APSR (Application Processor Status Register), or the CPSR (Current Processor Status Register) if you are used to pre-Armv7 terminology 3, 4. The flags indicate simple properties such as whether or not the result was negative, and are used in various combinations to detect higher-level relationships such as "greater than" and suchlike. Once I have described the flags, I will explain how they map onto condition codes (such as ne in the previous example).
<p><strong>N: Negative</strong>
<p>The N flag is set by an instruction if the result is negative. In practice, N is set to the two's complement sign bit of the result (bit 31).
<p><strong>Z: Zero</strong>
<p>The Z flag is set if the result of the flag-setting instruction is zero.
<p><strong>C: Carry (or Unsigned Overflow)</strong>
<p>The C flag is set if the result of an unsigned operation overflows the 32-bit result register. This bit can be used to implement 64-bit unsigned arithmetic, for example.
<p><strong>V: (Signed) Overflow</strong>
<p>The V flag works the same as the C flag, but for signed operations. For example, 0x7fffffff is the largest positive two's complement integer that can be represented in 32 bits, so 0x7fffffff + 0x7fffffff triggers a signed overflow, but not an unsigned overflow (or carry): the result, 0xfffffffe, is correct if interpreted as an unsigned quantity, but represents a negative value (-2) if interpreted as a signed quantity.
<p><strong>Flag-Setting Example</strong>
<p>Consider the following example:
<p style="font-family:courier, courier new, serif;">
ldr     r1, =0xffffffff<br>
ldr     r2, =0x00000001<br>
adds    r0, r1, r2<br>
</p>
<p>The result of the operation would be 0x100000000, but the top bit is lost because it does not fit into the 32-bit destination register and so the real result is 0x00000000. In this case, the flags will be set as follows:
<table>
<tr><td><strong>Flag</strong></td><td><strong>Explanation</strong></td></tr>
<tr><td>N = 0</td><td>The result is 0, which is considered positive, and so the N (negative) bit is set to 0.</td></tr>
<tr><td>Z = 1</td><td>The result is 0, so the Z (zero) bit is set to 1.</td></tr>
<tr><td>C = 1</td><td>We lost some data because the result did not fit into 32 bits, so the processor indicates this by setting C (carry) to 1.</td></tr>
<tr><td>V = 0</td><td>From a two's complement signed-arithmetic viewpoint, 0xffffffff really means -1, so the operation we did was really (-1) + 1 = 0. That operation clearly does not overflow, so V (overflow) is set to 0.</td></tr>
</table>
<p>If you fancy it, you can check this with the ccdemo application. The output looks like this:
<p style="font-family:courier, courier new, serif;">
$ ./ccdemo adds 0xffffffff 0x1
</p>
<p>The results (in various formats):
<p style="font-family:courier, courier new, serif;">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signed:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-1&nbsp;adds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Unsigned:&nbsp;4294967295&nbsp;adds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;=&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0<br>
&nbsp;&nbsp;Hexadecimal:&nbsp;0xffffffff&nbsp;adds&nbsp;0x00000001&nbsp;=&nbsp;0x00000000<br>
Flags:<br>
&nbsp;&nbsp;N&nbsp;(negative):&nbsp;0<br>
&nbsp;&nbsp;Z&nbsp;(zero)&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;1<br>
&nbsp;&nbsp;C&nbsp;(carry)&nbsp;&nbsp;&nbsp;:&nbsp;1<br>
&nbsp;&nbsp;V&nbsp;(overflow):&nbsp;0<br>
Condition&nbsp;Codes:<br>
&nbsp;&nbsp;EQ:&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;NE:&nbsp;0<br>
&nbsp;&nbsp;CS:&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;CC:&nbsp;0<br>
&nbsp;&nbsp;MI:&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;PL:&nbsp;1<br>
&nbsp;&nbsp;VS:&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;VC:&nbsp;1<br>
&nbsp;&nbsp;HI:&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;LS:&nbsp;1<br>
&nbsp;&nbsp;GE:&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;LT:&nbsp;0<br>
&nbsp;&nbsp;GT:&nbsp;0&nbsp;&nbsp;&nbsp;&nbsp;LE:&nbsp;1<br>
</p>
<h3>Reading the Flags</h3>
<p>We have worked out how to set the flags, but how does that result in the ability to conditionally execute some code? Being able to set the flags is pointless if you cannot then react to them.
<p>The most common method of testing the flags is to use conditional execution codes. This mechanism is similar to mechanisms used in other architectures, so if you are familiar with other machines you might recognize the following pattern, which maps cleanly onto C's if/else construct:
<p style="font-family:courier, courier new, serif;">
&nbsp;&nbsp;cmp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r0,&nbsp;#20<br>
&nbsp;&nbsp;bhi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;do_something_else<br>
do_something:<br>
&nbsp;&nbsp;@&nbsp;This&nbsp;code&nbsp;runs&nbsp;if&nbsp;(r0&nbsp;<=&nbsp;20).<br>
&nbsp;&nbsp;b&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;continue&nbsp;&nbsp;&nbsp;&nbsp;@&nbsp;Prevent&nbsp;do_something_else&nbsp;from&nbsp;executing.<br>
do_something_else:<br>
&nbsp;&nbsp;@&nbsp;This&nbsp;code&nbsp;runs&nbsp;if&nbsp;(r0&nbsp;>&nbsp;20).<br>
continue:<br>
&nbsp;&nbsp;@&nbsp;Other&nbsp;code.
</p>  
<p>In effect, attaching one of the condition codes to an instruction causes it to execute if the condition is true. Otherwise, it does nothing, and is essentially a nop.
<p>The first table lists the available condition codes, their meanings (where the flags were set by a cmp or subs instruction), and the flags that are tested.
<p>It is fairly obvious how the first few work because they test individual flags, but the others rely on specific combinations of flags. In practice, you very rarely need to know exactly what is happening; the mnemonics hide the complexity of the comparisons.
<p>Consider the following example:
<p style="font-family:courier, courier new, serif;">
&nbsp;&nbsp;mov&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;r4,&nbsp;#10<br>
loop_label:<br>
&nbsp;&nbsp;bl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;do_something<br>
&nbsp;&nbsp;subs&nbsp;&nbsp;&nbsp;&nbsp;r4,&nbsp;r4,&nbsp;#1<br>
&nbsp;&nbsp;bne&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;loop_label
</p> 
<p>It should now be easy enough to work out exactly what is happening here:
<p>The subs instruction sets the flags based on the result of r4-1. In particular, the Z flag will be set if the result is 0, and it will be clear if the result is anything else.
<p>The bne instruction only executes if condition ne is true. That condition is true if Z is clear, so the bne iterates the loop until Z is set (and therefore r4 is 0).
<h3>Dedicated Comparison Instructions</h3>
<p>The cmp instruction (that we saw in the first example) can be thought of as a sub instruction that doesn't store its result: if the two operands are equal, the result of the subtraction will be zero, hence the mapping between eq and the Z flag. Of course, we could just use a sub instruction with a dummy register, but you can only do that if you have a register to spare. Dedicated comparison instructions are therefore quite commonly used.
<p>There are actually four dedicated comparison instructions available, and they perform operations as described in the following table:
<table>
<tr><td><strong>Instruction</strong></td><td><strong>Description</strong><tr>
<tr><td>cmp</td><td>Works like subs, but does not store the result.</td></tr>
<tr><td>cmn</td><td>Works like adds, but does not store the result.</td></tr>
<tr><td>tst</td><td>Works like ands, but does not store the result.</td></tr>
<tr><td>teq</td><td>Works like eors, but does not store the result.</td></tr>
</table>
<p>Note that the dedicated comparison operations do not require the s suffix; they only update the flags, so the suffix would be redundant.

