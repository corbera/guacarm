var fs = require('fs');
fs.readFile('index1.html', function (err, data) {
    if (err) {
        console.log("No se puede leer el fichero index1.html");
        process.exit(-1);
    }
    var index = 0;
    data = data.toString();
    while ((newindex = data.indexOf("<!DOCTYPE", index+1)) > 0) {
        var page = data.substring(index, newindex);

        var name = '<div.+id="(page([^"]+))"';
        var regex = new RegExp(name, "gm");
        match = regex.exec(page);
        if (match != null && match.length > 2) {
            var filename = match[1]+".html";
            var pagenumber = match[2] * 1.0;
            var pos1 = page.search(name);
            var pos2 = page.search('</body>');
            console.log(filename + " " + pagenumber);
            var nav = '<div><table><tr><td>';
            nav += (pagenumber > 1) ? '<a href="page' + (pagenumber - 1) + '.html">prev</a></td>' : '</td>';
            nav += '<td><a href="page5.html">index</a></td><td>';
            nav += (pagenumber < 193) ? '<a href="page' + (pagenumber + 1) + '.html">next</a></td>' : '</td>';
            nav += '</tr></table>\n';
            page = page.substring(0, pos1) + nav + page.substring(pos1, pos2) + nav + page.substring(pos2);
            fs.writeFile(filename, page, function (err) {
                if (err) {
                    console.log("No se puede escribir el fichero " + filename);
                }
            });
        }
        index = newindex;
    }
});
