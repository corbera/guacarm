@ Library with funcitons to emulate raspberry pi library wiringPi for the berryclip pluging of guacARM
@
@ libBerry Functions: (Computer Tecnology Course @ University of Malaga)
@	initBerry		-- Initialises libBerry library
@	setLeds			-- Sets the state of all setLeds
@	playNote		-- Play a pure tone
@
@ Autor: F.Corbera, November 2019

	.global initBerry, setLeds, playNote

@ TEXT SEGMENT

.text
nop
nop
_____________________________________________________:	
nop
______________INSERTED_LIBBERRY_LIBARRY_______________:
nop
_______________________________________________________:
nop
nop

@
@	initBerry		-- Initialises libBerry library
@
@ Usage:
@    initBerry
@ Input parameters:
@    None
@ Result:
@    None
initBerry:
	mov r3, #100
	b genericCall

@
@	setLeds			-- Sets the state of all setLeds
@
@ Usage:
@    setLeds(r0)
@ Input parameters:
@    r0: LEDs state (bit0 -> RLED1, bit1 -> RLED2, bit2 -> YLED1, bit3 ->YLED2, bit4 -> GLED1, bit5 -> GLED2)
@ Result:
@    None
setLeds:
	mov r3, #101
	b genericCall

@
@	playNote		-- Play a pure tone
@
@ Usage:
@    playNote(r0, r1)
@ Input parameters:
@    r0: tone period in microseconds
@	 r1: tone duration in milliseconds
@ Result:
@    None
playNote:
	mov r3, #102
	b genericCall
