@ Library with funcitons to emulate raspberry pi library wiringPi for the berryclip pluging of guacARM
@
@ wiringPi Functions:
@	wiringPiSetup	-- Initialises the library
@	pinMode			-- Sets the mode of a pin (only INPUT or OUTPUT)
@	digitalWrite	-- Writes the value HIGH or LOW (1 or 0) to the given pin which must have been previously set as an output
@	digitalRead		-- Returns the value read at the given pin (HIGH or LOW, 1 or 0)
@	delay			-- Pause the program execution for the given milliseconds (centiseconds resolution in simulator)
@	delayMicroseconds -- Pause the program execution for the given microseconds (centiseconds resolution in simulator)
@	millis 			-- This returns a number representing the number of milliseconds since timer was initialized
@	micros 			-- This returns a number representing the number of microseconds since timer was initialized
@
@ Autor: F.Corbera, November 2019

	.global	wiringPiSetup, pinMode
	.global digitalWrite, digitalRead
	.global delay, delayMicroseconds
	.global millis, micros
	.global genericCall

@ TEXT SEGMENT

.text
nop
nop
_____________________________________________________:	
nop
______________INSERTED_WIRINGPI_LIBARRY_______________:
nop
_______________________________________________________:
nop
nop

@
@	wiringPiSetup	-- Library initialization
@
@ Usage:
@    wiringPiSetup()
@ Input parameters:
@    None
@ Result:
@    None
wiringPiSetup:
	mov r3, #0
	b genericCall

@
@	pinMode	-- This sets the mode of a pin to either INPUT (0), OUTPUT (1)
@
@ Usage:
@    pinMode(r0, r1)
@ Input parameters:
@    r0: pin number
@	 r1: mode
@ Result:
@    None
pinMode:
	mov r3, #1
	b genericCall

@
@	digitalWrite	-- Writes the value HIGH or LOW (<>0 or 0) to the given pin which must have been previously set as an output
@
@ Usage:
@    digitalWrite(r0, r1)
@ Input parameters:
@    r0: pin number
@	 r1: value
@ Result:
@    None
digitalWrite:
	mov r3, #2
	b genericCall

@
@	digitalRead	-- Returns the value read at the given pin (HIGH or LOW, 1 or 0)
@
@ Usage:
@    digitalRead(r0)
@ Input parameters:
@    r0: pin number
@ Result:
@    r0: value at the pin
digitalRead:
	mov r3, #3
	b genericCall

@
@	millis 			-- This returns a number representing the number of milliseconds since timer was initialized
@
@ Usage:
@    millis()
@ Input parameters:
@    None
@ Result:
@    r0: number of milliseconds
millis:
	mov r3, #4
	b genericCall

@
@	micros 			-- This returns a number representing the number of microseconds since timer was initialized
@
@ Usage:
@    micros()
@ Input parameters:
@    None
@ Result:
@    r0: number of microseconds
micros:
	mov r3, #5
	b genericCall

@
@	delay			-- Pause the program execution for the given milliseconds (centiseconds resolution in simulator)
@
@ Usage:
@    delay(r0)
@ Input parameters:
@    r0: number of milliseconds
@ Result:
@    None
delay:
	mov r3, #6
	b genericCall

@
@	delayMicroseconds -- Pause the program execution for the given microseconds (centiseconds resolution in simulator)
@
@ Usage:
@    delay(r0)
@ Input parameters:
@    r0: number of microseconds
@ Result:
@    None
delayMicroseconds:
	mov r3, #7
	b genericCall

genericCall:
	push {r6, r7, lr}
	mov r7, #14
	mov r6, r3
	swi #0
	pop {r6, r7, pc}
