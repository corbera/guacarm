.data
vals: .byte 1,2,3,4,5,6,7,8

.global _start
.text 
_start: 	push {lr}
			mov r0, #0					// direccion 0 
			ldr r1, [r0]				// lectura de la direccion 0 de memoria
			ldr r2, =#0x0a0b0c0d    // dato a escribir
			str r2, [r0]            // escritura en la direccion 0
			mov r2, #-1					// nuevo dato a escribir
			strb r2, [r0]           // escritura de byte en direccion 0
        	pop {lr}               
        	bx lr							// fin del programa


	
