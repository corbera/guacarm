.data 
tam:   .word 8 
datos: .word 2, 4, 6, 8, -2, -4, -6 -7
res:   .word 0

.text 
.global _start

_start:
	push {lr} 
	ldr r4, =datos
	ldr r5, =tam 
	ldr r5, [r5]
	ldr r6, [r4]
loop:
	cmp r5, #0
	beq exit
	sub r5, r5, #1
	ldr r7, [r4], #4
	cmp r7, r6
	movgt r6, r7
	b loop
exit:
	ldr r5, =res
	str r6, [r5]
	pop {pc}
   