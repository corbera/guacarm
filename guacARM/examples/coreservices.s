.data 
buff: .space 256, 0xff

.text 
.global _start
_start:
main:
	// pasamos el valor entero -24 (r0) a cadena de caracteres en buff
	mov r0, #-24
	ldr r1, =buff
	mov r7, #0x90	//itoa
	swi #0
	// pasamos la cadea de caracteres -24 en buff a valor entero en r0
	ldr r0, =buff
	mov r7, #0x80	//atoi
	swi #0
	// pasamos el valor entero 0x2A (r0) a cadena de caracteres en buff
	mov r0, #0x2A
	ldr r1, =buff
	mov r7, #0x91	//itoa
	swi #0
	// pasamos la cadena hexadecimal 2A en buff a entero en r0
	ldr r0, =buff
	mov r7, #0x81	//atoh
	swi #0
   // pasamos el valor entero -1 a cadena binaria en buff
	mov r0, #-1
	ldr r1, =buff
	mov r7, #0x92	//itoa
	swi #0
   // pasamos la cadena binaria que representa -1 en buff a valor entero en r0
	ldr r0, =buff
	mov r7, #0x82	//atob
	swi #0
   // terminamos el programa con llamada exit
	mov r7, #1	// exit
	swi #0	
	