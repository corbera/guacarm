// Clipberry basic example
//
// Blink red led number 1
// Push button 1 for exit

.include  "inter.inc"

.global _start
.text
_start:
		push {lr}
		ldr	r0,	=GPBASE
		/*           xx999888777666555444333222111000*/
		mov	r1, #0b00001000000000000000000000000000
		str	r1, [r0, #GPFSEL0]
		/*          10987654321098765432109876543210*/
		mov r1,	#0b00000000000000000000001000000000
		mov r6,  #0b00000000000000000000000000000100
		ldr	r2,	=STBASE
bucle:
		bl 	espera
		str	r1, [r0, #GPSET0]
		bl 	espera
		str	r1,	[r0, #GPCLR0]
		ldr	r7, 	[r0, #GPLEV0]
		ands r7, r7, r6
		popeq {pc}
		b bucle
espera:
		ldr	r3,	[r2, #STCLO]
		ldr	r4,	=500000
		//add	r4,	r3
ret1:	ldr	r5,	[r2, #STCLO]
		subs r5, r5, r3
		bxlt lr
		cmp r5, r4
		bxge lr
		b ret1
		