.data
notas: .word 3822, 3405, 3034, 2863, 2551, 2273, 2025, 1911
finnotas: .word 0
.include "wiringPiPins.s"  /* fichero con definiciones para el control de la placa */

.text

.global _start
_start:
main:
	push {lr}

	bl initBerry			// inicializamos la libreria

	ldr r4, =notas			// r4 va a apuntar a la nota actual
	ldr r5, =finnotas		// r5 para detectar final de las notas
	mov r6, r4
loop:
	cmp r4, r5				// si he llegado a la ultima nota voy al segundo bucle
	beq loop2
	ldr r0, [r4], #4		// cargo la nota actual y avanzo r4 (siguiente nota)
	mov r1, #500			// duración de la nota 500 milisegundos
	bl playNote				// llamo a la función playNote
	b loop					// vuelvo al bucle 
loop2:
	cmp r4, r6				// este bucle desciende hasta llegar de nuevo a la primera nota
	beq finloop			
	ldr r0, [r4, #-4]!	// cargo la nota actual y retrocedo r4 (nota anterior)
	mov r1, #500			// duración de la nota
	bl playNote				// llamo a la función playNote
	b loop2					// vuelvo al bucle 
finloop:
	pop {pc}
