.data
.include "wiringPiPins.s"  /* fichero con definiciones para el control de la placa */

.text

.global main
main:
	push {lr}

	bl initBerry		// inicializamos libreria (libberry e wiringpi)

	mov r4, #0			// va a contener estado de los 6 leds
	mov r5, #65			// vamos a contar de 0 a 65 (todos los posibles estados de los 6 leds)
loop:
	mov r0, r4			// movemos estado de leds actual a r0
	bl setLeds			// llamamos a funcion setLeds
	mov r0, #200
	bl delay				// esperamos 200 milisegundos
	add r4, r4, #1		// siguiente estado de los leds
	cmp r4, r5			// es el último (65)?
	bne loop				// si no, volvemos a cabecera de bucle

	pop {pc}
