// Clipberry & wiringPi library basic example
// 
// Blink red led number 1 
// Push button 1 for exit
.include "wiringPiPins.s"
.global _start
.text 
_start:
		push {lr}
		
		bl wiringPiSetup		// inicializamos librería wiringPi
		
		mov r0, #RLED1			// pin del led rojo 1
		mov r1, #1				// modo del pin salida
		bl pinMode				// llamada para establecer modo del pin
		
bucle:
		bl 	espera			// llamamos a funcion de espera 0.5 s
		mov r0, #RLED1			// pin del led rojo 1
		mov r1, #1				// valor 1
		bl digitalWrite		// ponemos 1 en el led rojo 1 (encendemos)
		
		bl 	espera			// esperamos 0.5 s
		
		mov r0, #RLED1			// pin del led rojo 1
		mov r1, #0				// valor 0
		bl digitalWrite		// ponemos 0 en el led rojo (apagamos)
		
		mov r0, #BUTTON1		// pin del pulsador 1
		bl digitalRead			// leemos su estado (r0)
		cmp r0, #0				// es 0? (ha sido pulsado)
		popeq {pc}				// si es igual, llevamos lr a pc (return)
		b bucle					// si no, volvemos al principio del bucle
		
espera:							// función de espera 0.5 s
		push {lr}
		mov r0, #500			// 500 milisegundos
		bl delay					// llamada a función delay
		pop {pc}
		