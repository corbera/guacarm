.data
cadena: .ascii "Escribe algo: "
endcadena:
.set cadenasize, endcadena - cadena
cadena2: .space 256

.text 
.global main
main:   push {lr}
        mov r0, #1				// salida estandar
        ldr r1, =cadena 		// direccion de donde leer
        mov r2, #cadenasize 	// numero de bytes a leer
        mov r7, #4				// id syscall write
        swi #0					   // syscall

        mov r0, #0            // entrada estandar
        ldr r1, =cadena2      // direccion buffer escritura
        mov r2, #256          // tamaño del buffer de escritura
        mov r7, #3            // id syscal read
        swi #0                // syscall

        mov r2, r0            // numero de bytes a escribir el devuelto por la llamada anterior
        mov r0, #1				// salida estandar
        ldr r1, =cadena2      // dirección de donde leer 
        mov r7, #4            // id syscall write
        swi #0                // syscall

        pop {lr}
        bx lr


	
